﻿using Nsf.FriendsLibrary.Business.Item_do_Empréstimo;
using Nsf.FriendsLibrary.DB;
using Nsf.FriendsLibrary.DB.Emprestimo;
using Nsf.FriendsLibrary.Tools.Validações;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.FriendsLibrary.Business.Emprestimo
{
    public class EmprestimoBusiness
    {
        public void Save(tb_emprestimo emprestimo, tb_livro livro)
        {
            EmprestimoDatabase db = new EmprestimoDatabase();
            EmprestimoItemBusiness itemBusiness = new EmprestimoItemBusiness();

            if (emprestimo.dt_emprestimo.Date < DateTime.Now.Date || emprestimo.dt_emprestimo.Date > DateTime.Now.Date)
                throw new ArgumentException("O EMPRÉSTIMO não pode ser feito com uma data menor ou maior do que a presente data." + "\n" + "Por favor preencha essa informação com os dados corretos!");

            if (emprestimo.dt_devolucao.Date < DateTime.Now.Date)
                throw new ArgumentException("A DEVOLUÇÃO não pode ser feito com uma data menor do que a presente data." + "\n" + "Por favor preencha essa informação com os dados corretos!");

            if (emprestimo.dt_devolucao.Day > 15)
                throw new ArgumentException("A DEVOLUÇÃO não pode ultrapassar mais do que 15 DIAS.");

            bool verificarStatus = itemBusiness.BookStatus(livro.id_livro);
            if (verificarStatus == false)
                throw new ArgumentException("LIVRO encontra-se INDISPONÍVEL." + "\n" + "Por favor espere pela DEVOLUÇÃO do mesmo");

            bool buscarID = db.BuscarNumeroID(emprestimo.id_leitor);
            if (buscarID == true)
                throw new ArgumentException("LEITOR excedeu o número máximo de empréstimos que pode ser feito." + "\n" + "Por favor preencha essa informação com os dados corretos!");

            string cpf = db.BringCPF(emprestimo.id_leitor);
            bool verificarCPF = ValidarIcones.IsBunifuCPFValid(cpf);
            if (verificarCPF == false)
                throw new ArgumentException("CPF inválido." + "\n" + "Por favor preencha essa informação com os dados corretos!");

            int idEmprestimo = db.Save(emprestimo);

            tb_emprestimo_item itemDTO = new tb_emprestimo_item();
            itemDTO.id_emprestimo = emprestimo.id_emprestimo;
            itemDTO.id_livro = livro.id_livro;

            itemBusiness.Save(itemDTO);
        }

        public void Update(tb_emprestimo emprestimo, tb_livro livro, tb_emprestimo_item itemDTO)
        {
            EmprestimoDatabase db = new EmprestimoDatabase();
            EmprestimoItemBusiness itemBusiness = new EmprestimoItemBusiness();

            if (emprestimo.dt_emprestimo.Date < DateTime.Now.Date || emprestimo.dt_emprestimo.Date > DateTime.Now.Date)
                throw new ArgumentException("O EMPRÉSTIMO não pode ser feito com uma data menor ou maior do que a presente data." + "\n" + "Por favor preencha essa informação com os dados corretos!");

            if (emprestimo.dt_devolucao.Date < DateTime.Now.Date)
                throw new ArgumentException("A DEVOLUÇÃO não pode ser feito com uma data menor do que a presente data." + "\n" + "Por favor preencha essa informação com os dados corretos!");

            if (emprestimo.dt_devolucao.Day > 15)
                throw new ArgumentException("A DEVOLUÇÃO não pode ultrapassar mais do que 15 DIAS.");

            bool verificarStatus = itemBusiness.BookStatus(livro.id_livro);
            if (verificarStatus == false)
                throw new ArgumentException("LIVRO encontra-se INDISPONÍVEL." + "\n" + "Por favor espere pela DEVOLUÇÃO do mesmo");

            string cpf = db.BringCPF(emprestimo.id_leitor);
            bool verificarCPF = ValidarIcones.IsBunifuCPFValid(cpf);
            if (verificarCPF == false)
                throw new ArgumentException("CPF inválido." + "\n" + "Por favor preencha essa informação com os dados corretos!");

            itemDTO.id_emprestimo = emprestimo.id_emprestimo;
            itemDTO.id_livro = livro.id_livro;

            int idEmprestimo = db.Update(emprestimo);
            itemBusiness.Update(itemDTO);
        }

        public void Remove(int emprestimo)
        {
            //Devolução
            EmprestimoDatabase db = new EmprestimoDatabase();
            db.Remove(emprestimo);
        }

        public List<tb_emprestimo> List()
        {
            EmprestimoDatabase db = new EmprestimoDatabase();
            var list = db.List();
            return list;
        }
        public List<tb_emprestimo> Filter(string dto)
        {
            EmprestimoDatabase db = new EmprestimoDatabase();
            var filter = db.Filter(dto);
            return filter;
        }

        public List<vw_consultar_emprestimo> ListLoan()
        {
            EmprestimoDatabase db = new EmprestimoDatabase();
            var listLoan = db.ListLoan();
            return listLoan;
        }

        public List<vw_consultar_emprestimo> FilterBook(string book)
        {
            EmprestimoDatabase db = new EmprestimoDatabase();
            var filterBook = db.FilterBook(book);
            return filterBook;
        }

        public List<vw_consultar_emprestimo> FilterReader(string reader)
        {
            EmprestimoDatabase db = new EmprestimoDatabase();
            var filterReader = db.FilterReader(reader);
            return filterReader;
        }

        public List<vw_consultar_emprestimo> FilterLoan(DateTime start, DateTime end)
        {
            EmprestimoDatabase db = new EmprestimoDatabase();
            var filterLoan = db.FilterLoan(start, end);
            return filterLoan;
        }

        public int GetCombobox (string cpf)
        {
            EmprestimoDatabase db = new EmprestimoDatabase();
            var retorno = db.GetCombobox(cpf);
            return retorno;
        }
    }
}
