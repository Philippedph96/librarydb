﻿using Nsf.FriendsLibrary.Business.Item_do_Empréstimo;
using Nsf.FriendsLibrary.DB;
using Nsf.FriendsLibrary.DB.Devolucao;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.FriendsLibrary.Business.Devolucao
{
    public class DevolucaoBusiness
    {
        public List<vw_devolucao_emprestimo> ListReturns()
        {
            DevolucaoDatabase db = new DevolucaoDatabase();
            var list = db.ListReturns();

            return list;
        }

        public List<vw_devolucao_emprestimo> FilterReaderReturns(string reader)
        {
            DevolucaoDatabase db = new DevolucaoDatabase();
            var filterReader = db.FilterReaderReturns(reader);

            return filterReader;
        }

        public List<vw_devolucao_emprestimo> FilterBookReturns(string book)
        {
            DevolucaoDatabase db = new DevolucaoDatabase();
            var filterBook = db.FilterBookReturns(book);

            return filterBook;
        }

        public List<vw_devolucao_emprestimo> FilterDateReturns(DateTime date)
        {
            DevolucaoDatabase db = new DevolucaoDatabase();
            var filterDate = db.FilterDateReturns(date);

            return filterDate;
        }
    }
}
