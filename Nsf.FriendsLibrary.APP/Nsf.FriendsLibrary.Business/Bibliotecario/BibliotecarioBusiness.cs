﻿using Nsf.FriendsLibrary.DB;
using Nsf.FriendsLibrary.DB.Bibliotecario;
using Nsf.FriendsLibrary.Tools.Validações;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.FriendsLibrary.Business.Bibliotecario
{
    public class BibliotecarioBusiness
    {
        public void Save(tb_bibliotecario dto)
        {
            BibliotecarioDatabase db = new BibliotecarioDatabase();

            if (dto.ds_senha.Length > 0 && dto.ds_senha.Length <= 7)
                throw new ArgumentException("O campo SENHA tem que ser maior/igual a 8 e menor/igual a 16");

            if (dto.nm_bibliotecario.Trim() == string.Empty)
                throw new ArgumentException("O campo NOME FUNCIONÁRIO está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_cpf.Trim() == string.Empty)
                throw new ArgumentException("O campo CPF está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_celular.Trim() == string.Empty)
                throw new ArgumentException("O campo NÚMERO CELULAR está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_email.Trim() == string.Empty)
                throw new ArgumentException("O campo EMAIL está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_cep.Trim() == string.Empty)
                throw new ArgumentException("O campo CEP está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_cidade   .Trim() == string.Empty)
                throw new ArgumentException("O campo CIDADE está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_endereco.Trim() == string.Empty)
                throw new ArgumentException("O campo ENDEREÇO está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_login.Trim() == string.Empty)
                throw new ArgumentException("O campo LOGIN está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_senha.Trim() == string.Empty)
                throw new ArgumentException("O campo SENHA está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.nr_numero <= 0)
                throw new ArgumentException("O campo NÚMERO CASA não pode ser nulo ou negativo.");

            bool verificarLogin = db.VerificarLogin(dto.ds_login);
            if (verificarLogin == false)
                throw new ArgumentException("Já existe um funcionário com esse LOGIN. " + "\n" + " Por favor, escolha outro!");

            bool verificarCPF = db.VerificarCPF(dto.ds_cpf);
            if (verificarCPF == true)
                throw new ArgumentException("Já existe um funcionário com esse CPF. " + "\n" + "Por favor, coloque a informação correta!");

            bool verificarEmail = db.VerificarEmail(dto.ds_email);
            if (verificarEmail == true)
                throw new ArgumentException("Já existe uma funcionário com esse EMAIL." + "\n" + "Por favor, coloque um outro EMAIL!");

            bool verificarEscritaEmail = ValidarIcones.IsBunifuEmailValid(dto.ds_email);
            if (verificarEscritaEmail == false)
                throw new ArgumentException("EMAIL inválido." + "\n" + "Verifique se você digitou o endereço do email corretamente!");

            bool verificarEscritaCPF = ValidarIcones.IsBunifuCPFValid(dto.ds_cpf);
            if (verificarEscritaCPF == false)
                throw new ArgumentException("CPF inválido." + "\n" + "Verifique se você digitou o número do CPF corretamente!");

            bool verificarCEP = ValidarIcones.IsBunifuCEPValid(dto.ds_cep);
            if (verificarCEP == false)
                throw new ArgumentException("CEP inválido." + "\n" + "Verifique se você digitou o número do CELULAR corretamente!");

            bool verificarCelular = ValidarIcones.IsBunifuCellPhoneValid(dto.ds_celular);
            if (verificarCelular == false)
                throw new ArgumentException("CELULAR inválido." + "\n" + "Verifique se você digitou o número do CEP corretamente!");

            db.Save(dto);
        }

        public void Update(tb_bibliotecario dto)
        {
            BibliotecarioDatabase db = new BibliotecarioDatabase();

            if (dto.ds_senha.Length > 0 && dto.ds_senha.Length <= 7)
                throw new ArgumentException("O campo SENHA tem que ser maior/igual a 8 e menor/igual a 16");

            if (dto.nm_bibliotecario.Trim() == string.Empty)
                throw new ArgumentException("O campo NOME FUNCIONÁRIO está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_cpf.Trim() == string.Empty)
                throw new ArgumentException("O campo CPF está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_celular.Trim() == string.Empty)
                throw new ArgumentException("O campo NÚMERO CELULAR está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_email.Trim() == string.Empty)
                throw new ArgumentException("O campo EMAIL está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_cep.Trim() == string.Empty)
                throw new ArgumentException("O campo CEP está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_endereco.Trim() == string.Empty)
                throw new ArgumentException("O campo ENDEREÇO está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_login.Trim() == string.Empty)
                throw new ArgumentException("O campo LOGIN está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_senha.Trim() == string.Empty)
                throw new ArgumentException("O campo SENHA está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.nr_numero == 0)
                throw new ArgumentException("O campo NÚMERO CASA não pode ser nulo ou negativo.");

            bool verificarEscritaEmail = ValidarIcones.IsBunifuEmailValid(dto.ds_email);
            if (verificarEscritaEmail == false)
                throw new ArgumentException("EMAIL inválido." + "\n" + "Verifique se você digitou o endereço do email corretamente!");

            bool verificarEscritaCPF = ValidarIcones.IsBunifuCPFValid(dto.ds_cpf);
            if (verificarEscritaCPF == false)
                throw new ArgumentException("CPF inválido." + "\n" + "Verifique se você digitou o número do CPF corretamente!");

            bool verificarCEP = ValidarIcones.IsBunifuCEPValid(dto.ds_cep);
            if (verificarCEP == false)
                throw new ArgumentException("CEP inválido." + "\n" + "Verifique se você digitou o número do CELULAR corretamente!");

            bool verificarCelular = ValidarIcones.IsBunifuCellPhoneValid(dto.ds_celular);
            if (verificarCelular == false)
                throw new ArgumentException("CELULAR inválido." + "\n" + "Verifique se você digitou o número do CEP corretamente!");

            db.Update(dto);
        }

        public void Remove(int id)
        {
            BibliotecarioDatabase db = new BibliotecarioDatabase();

            bool verificarID = db.VerificarID(id);
            if (verificarID == false)
                throw new ArgumentException("Não foi possível obter o código desse funcionário." + "\n" + "Tente novamente!!");

            db.Remove(id);
        }

        public List<tb_bibliotecario> List()
        {
            BibliotecarioDatabase db = new BibliotecarioDatabase();
            List<tb_bibliotecario> bibliotecario = db.List();

            return bibliotecario;
        }

        public List<tb_bibliotecario> FilterLibrarian(string _nomeBibliotecario)
        {
            BibliotecarioDatabase db = new BibliotecarioDatabase();
            List<tb_bibliotecario> bibliotecarios = db.FilterLibrarian(_nomeBibliotecario);

            return bibliotecarios;
        }

        public List<tb_bibliotecario> FilterCPF(string cpf)
        {
            BibliotecarioDatabase db = new BibliotecarioDatabase();
            List<tb_bibliotecario> bibliotecarios = db.FilterCPF(cpf);

            return bibliotecarios;
        }

        public tb_bibliotecario Login(string password)
        {
            BibliotecarioDatabase db = new BibliotecarioDatabase();
            return db.Login(password);
        }
    }
}
