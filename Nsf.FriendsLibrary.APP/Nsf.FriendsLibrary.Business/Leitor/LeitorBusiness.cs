﻿using Nsf.FriendsLibrary.DB;
using Nsf.FriendsLibrary.Tools.Validações;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.Banco.Database.Usuario
{
    public class LeitorBusiness
    {
        public void Save(tb_leitor dto)
        {
            LeitorDatabase db = new LeitorDatabase();

            if (dto.nm_leitor.Trim() == string.Empty)
                throw new ArgumentException("O campo NOME LEITOR está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_cpf.Trim() == string.Empty)
                throw new ArgumentException("O campo CPF está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_email.Trim() == string.Empty)
                throw new ArgumentException("O campo EMAIL está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_cep.Trim() == string.Empty)
                throw new ArgumentException("O campo CEP está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_endereco.Trim() == string.Empty)
                throw new ArgumentException("O campo ENDEREÇO está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_cidade.Trim() == string.Empty)
                throw new ArgumentException("O campo CIDADE está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.nr_numero <= 0)
                throw new ArgumentException("O campo NÚMERO CASA não pode ser nulo ou negativo.");

            bool verificarCPF = db.VerificarCPF(dto.ds_cpf);
            if (verificarCPF == true)
                throw new ArgumentException("CPF já cadastrado na base de dados!");

            bool verificarEmail = db.VerificarCPF(dto.ds_email);
            if (verificarEmail == true)
                throw new ArgumentException("EMAIL já cadastrado na base de dados!");

            bool verificarIdade = this.VerificarIdade(dto.dt_nascimento);
            if (verificarIdade == true)
                throw new ArgumentException("LEITOR precisa ter no mínimo 14 anos de idade");

            bool verificarEscritaEmail = ValidarIcones.IsBunifuEmailValid(dto.ds_email);
            if (verificarEscritaEmail == false)
                throw new ArgumentException("EMAIL inválido." + "\n" + "Verifique se você digitou o endereço do email corretamente!");

            bool verificarEscritaCPF = ValidarIcones.IsBunifuCPFValid(dto.ds_cpf);
            if (verificarEscritaCPF == false)
                throw new ArgumentException("CPF inválido." + "\n" + "Verifique se você digitou o número do CPF corretamente!");

            bool verificarCEP = ValidarIcones.IsBunifuCEPValid(dto.ds_cep);
            if (verificarCEP == false)
                throw new ArgumentException("CEP inválido." + "\n" + "Verifique se você digitou o número do CEP corretamente");

            db.Save(dto);
        }

        public void Update(tb_leitor dto)
        {
            LeitorDatabase db = new LeitorDatabase();

            if (dto.nm_leitor.Trim() == string.Empty)
                throw new ArgumentException("O campo NOME LEITOR está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_cpf.Trim() == string.Empty)
                throw new ArgumentException("O campo CPF está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_email.Trim() == string.Empty)
                throw new ArgumentException("O campo EMAIL está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_cep.Trim() == string.Empty)
                throw new ArgumentException("O campo CEP está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_endereco.Trim() == string.Empty)
                throw new ArgumentException("O campo ENDEREÇO está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.ds_cidade.Trim() == string.Empty)
                throw new ArgumentException("O campo CIDADE está vázio. " + "\n " + "Por favor preencha essa informação!");

            if (dto.nr_numero <= 0)
                throw new ArgumentException("O campo NÚMERO CASA não pode ser nulo ou negativo.");

            bool verificarIdade = this.VerificarIdade(dto.dt_nascimento);
            if (verificarIdade == true)
                throw new ArgumentException("LEITOR precisa ter no mínimo 14 anos de idade");

            bool verificarEscritaEmail = ValidarIcones.IsBunifuEmailValid(dto.ds_email);
            if (verificarEscritaEmail == false)
                throw new ArgumentException("EMAIL inválido." + "\n" + "Verifique se você digitou o endereço do email corretamente!");

            bool verificarEscritaCPF = ValidarIcones.IsBunifuCPFValid(dto.ds_cpf);
            if (verificarEscritaCPF == false)
                throw new ArgumentException("CPF inválido." + "\n" + "Verifique se você digitou o número do CPF corretamente!");

            bool verificarCEP = ValidarIcones.IsBunifuCEPValid(dto.ds_cep);
            if (verificarCEP == false)
                throw new ArgumentException("CEP inválido." + "\n" + "Verifique se você digitou o número do CEP corretamente");

            db.Update(dto);
        }

        public void Remove(int id)
        {
            LeitorDatabase db = new LeitorDatabase();

            bool verificarID = db.VerificarID(id);
            if (verificarID == true)
                throw new ArgumentException("Não foi possível obter o código desse funcionário." + "\n" + "Tente novamente!!");

            db.Remove(id);
        }

        public List<tb_leitor> List()
        {
            LeitorDatabase db = new LeitorDatabase();
            List<tb_leitor> list = db.List();

            return list;
        }

        public List<tb_leitor> FilterReader(string dto)
        {
            LeitorDatabase db = new LeitorDatabase();
            List<tb_leitor> filter = db.FilterReader(dto);

            return filter;
        }

        public List<tb_leitor> FilterCPF(string dto)
        {
            LeitorDatabase db = new LeitorDatabase();
            List<tb_leitor> filter = db.FilterCPF(dto);

            return filter;
        }

        public tb_leitor GetNamebyCPF(string cpf)
        {
            LeitorDatabase db = new LeitorDatabase();
            return db.GetNamebyCPF(cpf);
        }

        public string GetCPFbyName(string name)
        {
            LeitorDatabase db = new LeitorDatabase();
            return db.GetCPFbyName(name);
        }

        private bool VerificarIdade(DateTime dataAniversario)
        {
            var dataHoje = DateTime.Now;
            var idade = dataHoje.Year - dataAniversario.Year;

            if (idade < 14)
                return true;
            else
                return false;
        }

        public List<string> ListReaderCPF()
        {
            LeitorDatabase db = new LeitorDatabase();
            var cpf = db.ListReaderCPF();

            return cpf;
        }

        public List<tb_leitor> Filter(int id)
        {
            LeitorBusiness db = new LeitorBusiness();
            var filterID = db.Filter(id);

            return filterID;
        }
    }
}
