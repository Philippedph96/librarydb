﻿using Nsf.FriendsLibrary.DB;
using Nsf.FriendsLibrary.DB.Categoria;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.FriendsLibrary.Business.Complementos_dos_Livros.Categoria
{
    public class CategoriaBusiness
    {
        public void Save(tb_categoria dto)
        {
            if (dto.nm_categoria == string.Empty)
            {
                throw new ArgumentException("Categoria encontra-se {vázia}");
            }
            if (dto.ds_exemplo == string.Empty)
            {
                throw new ArgumentException("Exemplo encontra-se {vázio}");
            }

            CategoriaDatabase db = new CategoriaDatabase();
            db.Save(dto);
        }

        public void Update(tb_categoria dto)
        {
            if (dto.nm_categoria == string.Empty)
            {
                throw new ArgumentException("Categoria encontra-se {vázia}");
            }
            if (dto.ds_exemplo == string.Empty)
            {
                throw new ArgumentException("Exemplo encontra-se {vázio}");
            }

            CategoriaDatabase db = new CategoriaDatabase();
            db.Update(dto);
        }

        public void Remove(int id)
        {
            CategoriaDatabase db = new CategoriaDatabase();

            bool idVerify = db.VerificarID(id);
            if (idVerify == false)
            {
                throw new ArgumentException("Não foi possível obter o código desse funcionário." + "\n" + "Tente novamente!!");
            }

            db.Remove(id);
        }

        public List<tb_categoria> List()
        {
            CategoriaDatabase db = new CategoriaDatabase();
            List<tb_categoria> categoria = db.List();

            return categoria;
        }

        public List<tb_categoria> Filter(string _nomeCategoria)
        {
            CategoriaDatabase db = new CategoriaDatabase();
            List<tb_categoria> categoria = db.Filter(_nomeCategoria);

            return categoria;
        }
    }
}
