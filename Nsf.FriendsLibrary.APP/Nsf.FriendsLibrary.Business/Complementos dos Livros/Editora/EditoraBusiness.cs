﻿using Nsf.FriendsLibrary.DB;
using Nsf.FriendsLibrary.DB.Complemento_dos_Livros.Editora;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.FriendsLibrary.Business.Complementos_dos_Livros.Editora
{
    public class EditoraBusiness
    {
        public void Save (tb_editora dto)
        {
            if (dto.nm_editora.Trim() == string.Empty)
            {
                throw new ArgumentException("Nome da Editora encontra-se {Vázio}");
            }

            if (dto.ds_sede == string.Empty.Trim())
            {
                throw new ArgumentException("A sede encontra-se {Vázia}");
            }

            EditoraDatabase db = new EditoraDatabase();
            db.Save(dto);
        }

        public void Update(tb_editora dto)
        {
            if (dto.nm_editora.Trim() == string.Empty)
            {
                throw new ArgumentException("Nome da Editora encontra-se {Vázio}");
            }

            if (dto.ds_sede == string.Empty.Trim())
            {
                throw new ArgumentException("A sede encontra-se {Vázia}");
            }

            EditoraDatabase db = new EditoraDatabase();
            db.Update(dto);
        }

        public void Remove(int id)
        {
            EditoraDatabase db = new EditoraDatabase();

            bool verifyID = db.VerificarID(id);
            if (verifyID == false)
            {
                throw new ArgumentException("Não foi possível obter o código desse autor." + "\n" + "Tente novamente!!");
            }

            db.Remove(id);
        }

        public List<tb_editora> List()
        {
            EditoraDatabase db = new EditoraDatabase();
            List<tb_editora> editora = db.List();

            return editora;
        }

        public List<tb_editora> Filter(string _nomeEditora)
        {
            EditoraDatabase db = new EditoraDatabase();
            List<tb_editora> editora = db.Filter(_nomeEditora);

            return editora;
        }
    }
}
