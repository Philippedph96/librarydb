﻿using Nsf.FriendsLibrary.DB;
using Nsf.FriendsLibrary.DB.Autor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.FriendsLibrary.Business.Complementos_dos_Livros.Autor
{
    public class AutorBusiness
    {
        public void Save (tb_autor dto)
        {
            if (dto.nm_autor.Trim() == string.Empty)
            {   
                throw new ArgumentException("Nome do Autor encontra-se {Vázio}");
            }

            if (dto.ds_escrita == string.Empty.Trim())
            {
                throw new ArgumentException("O tipo da escrita encontra-se {Vázio}");
            }
            
            AutorDatabase db = new AutorDatabase();
            db.Save(dto);
        }

        public void Update(tb_autor dto)
        {
            if (dto.nm_autor == string.Empty)
            {
                throw new ArgumentException("Nome do Autor encontra-se {Vázio}");
            }

            if (dto.ds_escrita == string.Empty.Trim())
            {
                throw new ArgumentException("O tipo da escrita encontra-se {Vázio}");
            }

            AutorDatabase db = new AutorDatabase();
            db.Update(dto);
        }

        public void Remove(int id)
        {
            AutorDatabase db = new AutorDatabase();

            bool verificarID = db.VerificarID(id);
            if (verificarID == false)
            {
                throw new ArgumentException("Não foi possível obter o código desse autor." + "\n" + "Tente novamente!!");
            }

            db.Remove(id);
        }

        public List<tb_autor> List()
        {
            AutorDatabase db = new AutorDatabase();
            List<tb_autor> autor = db.List();

            return autor;
        }

        public List<tb_autor> Filter(string _nomeAutor)
        {
            AutorDatabase db = new AutorDatabase();
            List<tb_autor> autor = db.Filter(_nomeAutor);

            return autor;
        }
        public List<string> ListStyle ()
        {
            AutorDatabase db = new AutorDatabase();
            var autor = db.ListStyle();

            return autor;
        }
    }
}
