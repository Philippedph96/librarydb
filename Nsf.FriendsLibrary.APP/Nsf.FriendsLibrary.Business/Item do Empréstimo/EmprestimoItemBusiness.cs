﻿using Nsf.FriendsLibrary.DB;
using Nsf.FriendsLibrary.DB.Item_do_Emprestimo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.FriendsLibrary.Business.Item_do_Empréstimo
{
    public class EmprestimoItemBusiness
    {
        public void Save (tb_emprestimo_item dto)
        {
            EmprestimoItemDatabase db = new EmprestimoItemDatabase();
            db.Save(dto);
        }

        public void Update(tb_emprestimo_item dto)
        {
            EmprestimoItemDatabase db = new EmprestimoItemDatabase();
            db.Update(dto);
        }

        public bool BookStatus (int id)
        {
            EmprestimoItemDatabase db = new EmprestimoItemDatabase();
            return db.BookStatus(id);
        }
    }
}
