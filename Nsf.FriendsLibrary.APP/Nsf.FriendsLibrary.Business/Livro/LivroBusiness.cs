﻿using Nsf.FriendsLibrary.DB;
using Nsf.FriendsLibrary.DB.Livro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.FriendsLibrary.Business.Livro
{
    public class LivroBusiness
    {
        public void Save(tb_livro dto)
        {
            LivroDatabase db = new LivroDatabase();

            DateTime dataHoje = DateTime.Now;
            if(dto.dt_ano > dataHoje)
                throw new ArgumentException("A DATA DA PUBLICAÇÃO não pode ser maior do que a data de hoje." + "\n " + "Por favor preencha essa informação com os dados corretos!");

            if (dto.nm_livro.Trim() == string.Empty)
                throw new ArgumentException("O campo TÍTULO LIVRO está vázio. " + "\n " + "Por favor preencha essa informação com os dados corretos!");

            if(dto.nr_paginas <= 0)
                throw new ArgumentException("O campo NÚMERO PÁGINA não pode ser nulo ou negativo." + "\n " + "Por favor preencha essa informação com os dados corretos!");

            if (dto.img_livro == null)
                throw new ArgumentException("O campo IMAGEM não pode ser nulo." + "\n " + "Por favor preencha essa informação com os dados corretos!");

            db.Save(dto);
        }

        public void Update(tb_livro dto)
        {
            LivroDatabase db = new LivroDatabase();

            DateTime dataHoje = DateTime.Now;
            if (dto.dt_ano > dataHoje)
                throw new ArgumentException("A DATA da publicação não pode ser maior do que a data de hoje." + "\n " + "Por favor preencha essa informação com os dados corretos!");

            if (dto.nm_livro.Trim() == string.Empty)
                throw new ArgumentException("O campo TÍTULO LIVRO está vázio. " + "\n " + "Por favor preencha essa informação com os dados corretos!");

            if (dto.nr_paginas <= 0)
                throw new ArgumentException("O campo NÚMERO PÁGINA não pode ser nulo ou negativo." + "\n " + "Por favor preencha essa informação com os dados corretos!");

            if (dto.img_livro == null)
                throw new ArgumentException("O campo IMAGEM não pode ser nulo." + "\n " + "Por favor preencha essa informação com os dados corretos!");

            db.Update(dto);
        }

        public void Remove(int id)
        {
            LivroDatabase db = new LivroDatabase();

            bool verificarID = db.VerifyID(id);
            if (verificarID == false)
                throw new ArgumentException("Não foi possível obter o código desse funcionário." + "\n" + "Tente novamente!!");

            db.Remove(id);
        }

        public List<tb_livro> List()
        {
            LivroDatabase db = new LivroDatabase();
            List<tb_livro> list = db.List();

            return list;
        }

        public List<tb_livro> Filter(string dto)
        {
            LivroDatabase db = new LivroDatabase();
            List<tb_livro> filter = db.Filter(dto);

            return filter;
        }

        public List<vw_livro> ListView()
        {
            LivroDatabase db = new LivroDatabase();
            List<vw_livro> view = db.ListView();

            return view;
        }

        public List<vw_livro> FilterTitleView(string title)
        {
            LivroDatabase db = new LivroDatabase();
            List<vw_livro> view = db.FilterTitleView(title);

            return view;
        }

        public List<vw_livro> FilterAuthorView(string author)
        {
            LivroDatabase db = new LivroDatabase();
            List<vw_livro> view = db.FilterAuthorView(author);

            return view;
        }

        public List<vw_livro> FilterCategoryView(string category)
        {
            LivroDatabase db = new LivroDatabase();
            List<vw_livro> view = db.FilterCategoryView(category);

            return view;
        }

        public string FilterAuthorID(int id)
        {
            LivroDatabase db = new LivroDatabase();
            return db.FilterAuthorID(id);
        }

        public string FilterCategoryID(int id)
        {
            LivroDatabase db = new LivroDatabase();
            return db.FilterCategoryID(id);
        }

        
    }
}
