﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.FriendsLibrary.Tools.Utilitários.ErrorProviderService
{
    public static class ErrorProviderService
    {
        public static void SelectErrorProvider (Control control, string valor)
        {
            if (string.IsNullOrEmpty(control.Text))
            {
                var icon = "Nsf.FriendsLibrary.APP/Nsf.FriendsLibrary.Tools/Resources/Error.png";
                ExecuterErrorProvider(control, valor, icon);
            }
            if (control.Text.Trim().Length == 0)
            {
                var icon = Properties.Resources.Error.ToString();
                ExecuterErrorProvider(control, valor, icon);
            }
        }

        private static void ExecuterErrorProvider(Control control, string message, string icon)
        {
            ErrorProvider error = new ErrorProvider();
            error.Icon = new Icon(icon);

            error.SetError(control, message);
        }
    }
}
