﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.FriendsLibrary.Tools.Utilitários.Corrreios
{
    public class CorreiosResponse
    {
        public string Cep { get; set; }
        public string Localidade { get; set; }
        public string Logradouro { get; set; }
    }
}
