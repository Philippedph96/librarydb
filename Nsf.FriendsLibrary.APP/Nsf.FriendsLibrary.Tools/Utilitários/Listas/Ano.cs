﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.Utilitarios.Listas
{
    class Ano
    {
        public List<string> Anos()
        {
            List<string> anos = new List<string>();
            anos.Add("Selecione");
            anos.Add("1970");
            anos.Add("1971");
            anos.Add("1972");
            anos.Add("1973");
            anos.Add("1974");
            anos.Add("1975");
            anos.Add("1976");
            anos.Add("1977");
            anos.Add("1978");
            anos.Add("1979");
            anos.Add("1980");
            anos.Add("1981");
            anos.Add("1982");
            anos.Add("1983");
            anos.Add("1984");
            anos.Add("1985");
            anos.Add("1986");
            anos.Add("1987");
            anos.Add("1988");
            anos.Add("1989");
            anos.Add("1990");
            anos.Add("1991");
            anos.Add("1992");
            anos.Add("1993");
            anos.Add("1994");
            anos.Add("1995");
            anos.Add("1996");
            anos.Add("1997");
            anos.Add("1998");
            anos.Add("1999");
            anos.Add("2000");
            anos.Add("2001");
            anos.Add("2002");
            anos.Add("2003");
            anos.Add("2004");
            anos.Add("2005");
            anos.Add("2006");
            anos.Add("2007");
            anos.Add("2008");
            anos.Add("2009");
            anos.Add("2010");
            anos.Add("2011");
            anos.Add("2012");
            anos.Add("2013");
            anos.Add("2014");
            anos.Add("2015");
            anos.Add("2016");
            anos.Add("2017");
            anos.Add("2018");
            anos.Add("2019");

            return anos;
        }
    }
}
