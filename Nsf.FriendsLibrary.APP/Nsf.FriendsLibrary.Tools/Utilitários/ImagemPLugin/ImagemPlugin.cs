﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteca.Utilitarios.ImagemPLugin
{
    public class ImagemPlugin
    {
        public static byte[] ConverterParaString(Image arquivo)
        {
            MemoryStream memoria = new MemoryStream();
            Bitmap bmp = new Bitmap(arquivo);
            bmp.Save(memoria, ImageFormat.Bmp);
            byte[] imagebytes = memoria.ToArray();

            return imagebytes;
        }

        public static Image ConverterParaImagem(byte[] ImagemEmTexto)
        {
            var imagem = Image.FromStream(new MemoryStream(ImagemEmTexto));
            return imagem;
        }
    }
}
