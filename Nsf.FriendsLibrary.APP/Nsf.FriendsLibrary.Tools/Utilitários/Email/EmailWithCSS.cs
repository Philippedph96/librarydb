﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.FriendsLibrary.Tools.Utilitários.Email
{
    public class EmailWithCSS
    {
        public string EmailFuncionario (string nome, string mensagem)
        {
            string texto = @"<html xmlns='http://www.w3.org/1999/xhtml'>  
               <head>  
                  <title></title>  
                        </head>  
                        <body>  
                            <table>  
                                <tr>  
                                    <td>    
                                        <img src='https://i.imgur.com/zHzpJ1w.png' width='200px' height='120px' />  
                                        <br />
                                        <div style='border-top:3px solid #404040'> </div></br> 
                                        <span style='font-family:Arial;font-size:11pt'>  
                                              Prezado(a) <b>" + $"{nome}" + @"</b>, | <b> FriendsLibrary </b></br>  
                                             " + $"<br /><br />{mensagem}" +
                                            @"<br /><br />  Grato, 
                                            <br /> Equipe <b> FriendsLibrary </b>
                                        </span>  
                                    </td>  
                                </tr>  
                            </table>  
                        </body>  
                    </html>";
            return texto;
        }   
    }
}
