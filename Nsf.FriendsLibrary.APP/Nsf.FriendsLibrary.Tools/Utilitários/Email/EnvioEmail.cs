﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.Collections.Specialized;
using Nsf.FriendsLibrary.Tools.Utilitários.Email;


namespace Biblioteca.Utilitarios.Email
{
    public class EnvioEmail
    {

        public DateTime DataEmprestimo { get; set; }
        public DateTime DataDevolucao { get; set; }

        public void EnviarEmail(string para, string nome, int valor)
        {
            //Nome Funcionário
            string primeiroNome = this.PrimeiroNome(nome);
            int dias = this.DiasEmprestimo(this.DataDevolucao);
            
            //Escopo do corpo HTML
            string titulo = @"Frieds Library";
            string mensagem = this.DefinirHTMLMensagem(valor, dias, this.DataDevolucao);

            //Gmail
            bool emailGmail = para.Contains("gmail");
            string origemGmail = "philippedph96@gmail.com";
            string senhaGmail = "alessandra10.05";


            //OutLook
            bool emailOutlook = para.Contains("outlook");
            bool emailHotmail = para.Contains("hotmail");
            string origemHotmail = "philippedph96@hotmail.com";
            string senhaHotmail = "Fe80953517";

            //Condição para optar pelo envio com as regas smtpHost do Gmail ou do Outlook
            if (emailGmail == true)
                this.EnviarDados(para, "smtp.gmail.com", titulo, mensagem, primeiroNome, origemGmail, senhaGmail);

            else if (emailOutlook == true || emailHotmail == true)
                this.EnviarDados(para, "smtp.live.com", titulo, mensagem, primeiroNome, origemHotmail, senhaHotmail);
        }

        private string PrimeiroNome(string nome)
        {
            bool contemEspaco = nome.Contains(" ");
            string primeiroNome = string.Empty;
            int posicao = nome.IndexOf(" ");

            if (contemEspaco == true) //Se o nome contem sobrenome, se no caso tem espaço.
                primeiroNome = nome.Substring(0, posicao);
            else
                primeiroNome = nome; //Senão, ele apenas pega o nome digitalizado

            return primeiroNome;
        }

        private int DiasEmprestimo (DateTime data)
        {
            DateTime agora = DateTime.Now.Date;
            TimeSpan dtp = data - agora.Date;

            int resultado = Math.Abs(Convert.ToInt32(dtp.Days));
            return resultado;
        }

        private string DefinirHTMLMensagem( int valor, int dias, DateTime dataDevoulucao)
        {
            string mensagem = string.Empty;
            if (valor == 1) // Funcionário
            {
                mensagem = @"Parabéns!!" + " Você está no caminho para se tornar uma parte importante na vida de centenas de pessoas. <br />" +
                           "Estamos muito felizes que você fará parte da nossa equipe!";
            }
            else if (valor == 2) // Cliente
            {
                mensagem = @"Parabéns!!" + " Agradecemos imensamente pelo seu cadastro feito no Friends Library. <br /> É uma tremenda honra " +
                            "conseguir atender as suas necessidades. " +
                            "<br /> E, por conta disso, procuramos exercer as nossas funções com o máximo de esmero possível.";
            }
            else if (valor == 3) //Empréstimo
            {
               mensagem = @"Obrigado!!" + " Agradecemos imensamente pelo utilização dos nossos serviços. <br /> É uma tremenda honra " +
                            "conseguir atender as suas necessidades. " +
                            "<br /> Você poderá ficar com o livro por mais " + $"{dias} dias";
            }
            else //Devoulução
            {
                mensagem = @"AVISO!!" + " Devolução do livro na data " + $"{dataDevoulucao}" + " foi efetuada com esmero. <br / > " +
                            "Agradecemos pela pela compreenção";

            }

            return mensagem;
        }

        private void EnviarDados(string para, string smtpHost, string titulo, string mensagem, string nome, string origem, string senha)
        {
            EmailWithCSS css = new EmailWithCSS();
            MailMessage email = new MailMessage();
            MailAddress emailPara = new MailAddress(para);

            email.From = new MailAddress(origem);
            email.To.Add(emailPara);
            email.IsBodyHtml = true;
            email.Subject = titulo;
            email.Body = css.EmailFuncionario(nome, mensagem);

            SmtpClient smtp = new SmtpClient();
            smtp.Host = smtpHost;
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(origem, senha);

            smtp.Send(email);
        }
    }
}
