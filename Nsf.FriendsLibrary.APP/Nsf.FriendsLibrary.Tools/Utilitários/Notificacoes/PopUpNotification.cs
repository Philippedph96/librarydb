﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tulpep.NotificationWindow;

namespace Biblioteca.Utilitarios.Notificacoes
{
    public static class PopUpNotification
    {
        //Para Forms
        public static void SelectorClass(this Form form, int verificadora, string mensagem, Image imagem)
        {
            if (form.InvokeRequired)
            {
                form.Invoke((MethodInvoker)delegate
                {
                    var color = new Color();
                    if (verificadora == 1)
                    {
                        color = Color.FromArgb(56, 142, 60); //Verde
                        ExecuterClass(mensagem, imagem, color); // Se for Sucesso
                    }
                    else if (verificadora == 2)
                    {
                        color = Color.FromArgb(64, 64, 64); //Marrom
                        ExecuterClass(mensagem, imagem, color); // Se for informação
                    }
                    else
                    {
                        color = Color.FromArgb(244, 67, 54); //Vermelho
                        ExecuterClass(mensagem, imagem, color); // Se for Erro
                    }
                });
            }
        }

        //Para UserControls (Sobrecarga)
        public static void SelectorClass(this UserControl form, int verificadora, string mensagem, Image imagem)
        {
            if (form.InvokeRequired)
            {
                form.Invoke((MethodInvoker)delegate
                {
                    var color = new Color();
                    if (verificadora == 1)
                    {
                        color = Color.FromArgb(56, 142, 60); //Verde
                        ExecuterClass(mensagem, imagem, color); // Se for Sucesso
                    }
                    else if (verificadora == 2)
                    {
                        color = Color.FromArgb(64, 64, 64); //Marrom
                        ExecuterClass(mensagem, imagem, color); // Se for informação
                    }
                    else
                    {
                        color = Color.FromArgb(244, 67, 54); //Vermelho
                        ExecuterClass(mensagem, imagem, color); // Se for Erro
                    }
                });
            }
        }

        private static void ExecuterClass(string mensagem, Image imagem, Color color)
        {
            //Valores passados como Textbox
            PopupNotifier popup = new PopupNotifier();
            popup.TitleText = "Friends Library";
            popup.ContentText = $"\n{mensagem}";

            //Trabalhando com as propriedades do título
            popup.TitleFont.Bold.Equals(true);
            popup.TitleFont = new Font(popup.TitleFont.Name, 16);
            popup.TitleFont = new Font("Century Gothic", popup.TitleFont.Size);

            //Trabalhando com as propriedades do conteiner
            popup.ContentFont.Bold.Equals(true);
            popup.ContentFont = new Font(popup.ContentFont.Name, 10);
            popup.ContentFont = new Font("Segoe UI Semibold", popup.ContentFont.Size);

            //Tratando as cores
            popup.BodyColor = color;
            popup.TitleColor = Color.White;
            popup.ContentColor = Color.White;

            //Tratando o Hover
            popup.ButtonHoverColor = Color.White;
            popup.ContentHoverColor = Color.White;

            //Pegando a imagem e mostrando para o usuário
            popup.Image = imagem;
            popup.Popup();
        }
    }
}
