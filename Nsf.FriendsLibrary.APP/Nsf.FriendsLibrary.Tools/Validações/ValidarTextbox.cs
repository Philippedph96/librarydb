﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Precisamos dessas duas referências
using System.Windows.Forms;
using System.Drawing;
using Biblioteca.Utilitarios.Notificacoes;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;
using Bunifu.UI.WinForms;
using Bunifu.UI.WinForms.BunifuTextbox;

namespace Biblioteca.Validacoes
{
    public static class ValidarTextbox
    {
        #region Numbers or Letters
        public static void SoNumero(KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar) || e.KeyChar != (char)8))
            {
                e.Handled = false;
            }
            else
            {
                if (char.IsLetter(e.KeyChar) == true)
                {
                    e.Handled = true;
                }
            }
        }
        public static void SoLetra(KeyPressEventArgs e)
        {
            //Verficia se o valor é letra       //Habilita o BackSpace     //Habilita o espaço
            if (!(char.IsLetter(e.KeyChar) || char.IsControl(e.KeyChar) || e.KeyChar != (char)8))
            {
                e.Handled = false;
            }
            else
            {
                if (char.IsDigit(e.KeyChar) == true)
                {
                    e.Handled = true;
                }
            }
        }
        #endregion

        #region MascaraTextBox
        public static void MascaraData(BunifuTextBox text, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || e.KeyChar == (char)8)
            {
                if (e.KeyChar == (char)8)
                {
                    //Só para não cair do Switch kkkk Gambiarra pura
                }
                else
                {
                    switch (text.TextLength)
                    {
                        case 0: //Primeiro caso
                            text.Text = ""; //Para os primeiros valores, atribui-se o valor nulo
                            break;
                        case 2:
                            text.Text = text.Text + "/"; // Ou seja, atribui primeiro o "mesmo" valor encontrado no primeiro caso, e depois aumaticamente atribui a barra
                            text.SelectionStart = 4; //O ponteiro do mouse/tecla irá para posição 4
                            break;
                        case 5:
                            text.Text = text.Text + "/";
                            text.SelectionStart = 8;
                            break;
                    }
                }

            }
            else
            {
                //Podemos colocar uma notificação depois
                e.Handled = true;
            }
        }

        public static void MascaraCPF(BunifuTextBox text, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || e.KeyChar == (char)8)
            {
                if (e.KeyChar == (char)8)
                {
                    //Só para não cair do Switch kkkk Gambiarra pura
                }
                else
                {
                    switch (text.TextLength)
                    {
                        case 0:
                            text.Text = "";
                            break;
                        case 3:
                            text.Text = text.Text + ".";
                            text.SelectionStart = 4;
                            break;
                        case 7:
                            text.Text = text.Text + ".";
                            text.SelectionStart = 9;
                            break;
                        case 11:
                            text.Text = text.Text + "-";
                            text.SelectionStart = 13;
                            break;
                    }
                }
            }

            else
            {
                e.Handled = true;
            }
        }

        public static void MascaraCelular(BunifuTextBox text, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || e.KeyChar == (char)8)
            {
                if (e.KeyChar == (char)8)
                {
                    //Gambiarra
                }
                else
                {
                    switch (text.TextLength)
                    {
                        case 0:
                            text.Text = "(";
                            text.SelectionStart = 1;
                            break;

                        case 1:
                            text.Text = "";
                            break;

                        case 3:
                            text.Text = text.Text + ")";
                            text.SelectionStart = 4;
                            text.Text = text.Text + " ";
                            text.SelectionStart = 5;
                            break;

                        case 10:
                            text.Text = text.Text + "-";
                            text.SelectionStart = 11;
                            break;
                    }
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        public static void MascaraCEP(BunifuTextBox text, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) == true || e.KeyChar == (char)8)
            {
                if (e.KeyChar == (char)8)
                {
                    //Gambiarra
                }
                else
                {
                    switch (text.TextLength)
                    {
                        case 0:
                            text.Text = "";
                            break;
                        case 5:
                            text.Text = text.Text + "-";
                            text.SelectionStart = 6;
                            break;
                    }
                    
                }
            }
            else
            {
                e.Handled = true;  
            }

        }
        #endregion


    }
}
