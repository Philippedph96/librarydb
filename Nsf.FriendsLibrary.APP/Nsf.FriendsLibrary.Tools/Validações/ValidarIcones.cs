﻿using Nsf.FriendsLibrary.DB.Bibliotecario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Nsf.FriendsLibrary.Tools.Validações
{
    public class ValidarIcones
    {
        public static bool IsBunifuTextBoxEmpty(string  text)
        {
            return text.Trim().Length > 0;
        }

        public static bool IsBunifuSenhaValid (string senha)
        {
            if (senha.Length >= 8 && senha.Length <= 16)
                return true;
            else
                return false;
        }

        public static bool IsBunifuCPFValid (string cpf)
        {
            string verifyCPF = @"^\d{3}.\d{3}.\d{3}.\d{2}$";
            return new Regex(verifyCPF, RegexOptions.IgnoreCase).IsMatch(cpf);
        }

        public static bool IsBunifuCellPhoneValid(string cellphone)
        {
            string verifyCellPhone = @"^\([1-9]{2}\) (?:[2-8]|9[1-9])[0-9]{3}\-[0-9]{4}$";
            return new Regex(verifyCellPhone, RegexOptions.IgnoreCase).IsMatch(cellphone);
        }

        public static bool IsBunifuEmailValid (string email)
        {
            string verifyEmail = @"^([a-z\d\.-]+)@([a-z\d-]+)\.([a-z]{2,10})(|\.[a-z]{2,5})$";
            return new Regex(verifyEmail, RegexOptions.IgnoreCase).IsMatch(email);
        }

        public static bool IsBunifuCEPValid (string cep)
        {
            string verifyCep = @"^\d{5}-\d{3}$";
            return new Regex(verifyCep, RegexOptions.IgnoreCase).IsMatch(cep);
        }

        public static bool IsBunifuNumber(string number)
        {
            string verifyNumber = @"^\d+$";
            return new Regex(verifyNumber, RegexOptions.IgnoreCase).IsMatch(number);
        }

        public static bool VerifyLogin (string login)
        {
            BibliotecarioDatabase db = new BibliotecarioDatabase();
            return db.VerificarLogin(login);
        }

        public static bool IsFourteenYears (DateTime dataAniversario)
        {
            var hoje = DateTime.Now;
            var idade = hoje.Year - dataAniversario.Year;

            if (idade < 14)
                return false;
            else
                return true;
            
        }
    }
}

