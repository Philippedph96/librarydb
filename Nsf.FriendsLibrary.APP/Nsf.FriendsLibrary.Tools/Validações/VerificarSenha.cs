﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Biblioteca.Validacoes
{
    public class VerificarSenha
    {
        public bool SenhaForteSimples(string senha)
        {
            string ex = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[[:punct:]]).{8,14}$";
            Regex re = new Regex(ex);
            if (re.IsMatch(senha) == true)
                return true;
            else
                return true;
            

        }

        public bool[] SenhaForte(string presenha)
        {
            string senha = presenha.Trim();
            bool[] dadosSenha = new bool[6];

            //Tem Letra maiuscula
            string ex1 = @"^ (?=.*[A - Z]).{ 0,14}$";
            Regex re1 = new Regex(ex1);
            if (re1.IsMatch(senha) == true)
                dadosSenha[0] = true;
            else
                dadosSenha[0] = false;
            
            //Tem Letra Minuscula
            string ex2 = @"^(?=.*[a-z]).{0,14}$";
            Regex re2 = new Regex(ex2);
            if (re2.IsMatch(senha) == true)
                 dadosSenha[1] = true;
            else
                 dadosSenha[1] = false;
            
            //Tem caractere especial
            string ex3 = @"^(?=.*[a-z]).{0,14}$";
            Regex re3 = new Regex(ex3);
            if (re3.IsMatch(senha) == true)
                dadosSenha[2] = true;
            else
                dadosSenha[2] = false;
            
            //tem numero
            string ex4 = @"^(?=.*\d).{0,14}$";
            Regex re4 = new Regex(ex4);
            if (re4.IsMatch(senha) == true)
                dadosSenha[3] = true;
            else
                 dadosSenha[3] = false;
            
            //tamanho correto
            int tamanho = senha.Length;
            if (tamanho < 14 && tamanho > 6)
                dadosSenha[4] = true;
            else
                dadosSenha[5] = false;

           return dadosSenha;
        }
    }
}
