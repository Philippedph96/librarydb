﻿namespace Biblioteca
{
    partial class frmMain
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties3 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties5 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties6 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties7 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
            Bunifu.UI.WinForms.BunifuAnimatorNS.Animation animation2 = new Bunifu.UI.WinForms.BunifuAnimatorNS.Animation();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties4 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            this.pnMainMenu = new System.Windows.Forms.Panel();
            this.btnFuncionario = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.btnDashboard = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.btnEmprestimo = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.btnRelatorio = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.btnMembro = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.btnLivro = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.ElipseForm = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.AnimationPanel = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnBarra = new System.Windows.Forms.Panel();
            this.pnSuperior = new System.Windows.Forms.Panel();
            this.lblNome = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnConteiner = new System.Windows.Forms.Panel();
            this.pnMain = new System.Windows.Forms.Panel();
            this.TimerMouseDetect = new System.Windows.Forms.Timer(this.components);
            this.bunifuTransition = new Bunifu.UI.WinForms.BunifuTransition(this.components);
            this.bunifuButton1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.frmSubMain1 = new Biblioteca.Forms.Alternativas__Marcos_.frmSubMain();
            this.bunifuToolTip1 = new Bunifu.UI.WinForms.BunifuToolTip(this.components);
            this.pnMainMenu.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.pnSuperior.SuspendLayout();
            this.pnConteiner.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnMainMenu
            // 
            this.pnMainMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.pnMainMenu.Controls.Add(this.btnFuncionario);
            this.pnMainMenu.Controls.Add(this.btnDashboard);
            this.pnMainMenu.Controls.Add(this.btnEmprestimo);
            this.pnMainMenu.Controls.Add(this.bunifuButton1);
            this.pnMainMenu.Controls.Add(this.btnRelatorio);
            this.pnMainMenu.Controls.Add(this.btnMembro);
            this.pnMainMenu.Controls.Add(this.btnLivro);
            this.pnMainMenu.Controls.Add(this.panel2);
            this.AnimationPanel.SetDecoration(this.pnMainMenu, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition.SetDecoration(this.pnMainMenu, Bunifu.UI.WinForms.BunifuTransition.DecorationType.None);
            this.pnMainMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnMainMenu.Location = new System.Drawing.Point(0, 0);
            this.pnMainMenu.Name = "pnMainMenu";
            this.pnMainMenu.Size = new System.Drawing.Size(203, 683);
            this.pnMainMenu.TabIndex = 0;
            this.bunifuToolTip1.SetToolTip(this.pnMainMenu, "");
            this.bunifuToolTip1.SetToolTipIcon(this.pnMainMenu, null);
            this.bunifuToolTip1.SetToolTipTitle(this.pnMainMenu, "");
            // 
            // btnFuncionario
            // 
            this.btnFuncionario.BackColor = System.Drawing.Color.Transparent;
            this.btnFuncionario.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnFuncionario.BackgroundImage")));
            this.btnFuncionario.ButtonText = "Funcionários";
            this.btnFuncionario.ButtonTextMarginLeft = -18;
            this.btnFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AnimationPanel.SetDecoration(this.btnFuncionario, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition.SetDecoration(this.btnFuncionario, Bunifu.UI.WinForms.BunifuTransition.DecorationType.None);
            this.btnFuncionario.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnFuncionario.DisabledFillColor = System.Drawing.SystemColors.GrayText;
            this.btnFuncionario.DisabledForecolor = System.Drawing.Color.White;
            this.btnFuncionario.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFuncionario.ForeColor = System.Drawing.Color.White;
            this.btnFuncionario.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnFuncionario.IconPadding = 9;
            this.btnFuncionario.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnFuncionario.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnFuncionario.IdleBorderRadius = 1;
            this.btnFuncionario.IdleBorderThickness = 0;
            this.btnFuncionario.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnFuncionario.IdleIconLeftImage = global::Nsf.FriendsLibrary.APP.Properties.Resources.Funcionários;
            this.btnFuncionario.IdleIconRightImage = null;
            this.btnFuncionario.Location = new System.Drawing.Point(0, 302);
            this.btnFuncionario.Name = "btnFuncionario";
            stateProperties1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(75)))), ((int)(((byte)(152)))));
            stateProperties1.BorderRadius = 1;
            stateProperties1.BorderThickness = 0;
            stateProperties1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(75)))), ((int)(((byte)(152)))));
            stateProperties1.IconLeftImage = global::Nsf.FriendsLibrary.APP.Properties.Resources.Funcionários;
            stateProperties1.IconRightImage = null;
            this.btnFuncionario.onHoverState = stateProperties1;
            this.btnFuncionario.Size = new System.Drawing.Size(214, 55);
            this.btnFuncionario.TabIndex = 6;
            this.btnFuncionario.Tag = "5";
            this.btnFuncionario.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuToolTip1.SetToolTip(this.btnFuncionario, "");
            this.bunifuToolTip1.SetToolTipIcon(this.btnFuncionario, null);
            this.bunifuToolTip1.SetToolTipTitle(this.btnFuncionario, "");
            this.btnFuncionario.Click += new System.EventHandler(this.btnDashboard_Click_1);
            // 
            // btnDashboard
            // 
            this.btnDashboard.BackColor = System.Drawing.Color.Transparent;
            this.btnDashboard.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnDashboard.BackgroundImage")));
            this.btnDashboard.ButtonText = "Dashboard";
            this.btnDashboard.ButtonTextMarginLeft = -35;
            this.btnDashboard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AnimationPanel.SetDecoration(this.btnDashboard, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition.SetDecoration(this.btnDashboard, Bunifu.UI.WinForms.BunifuTransition.DecorationType.None);
            this.btnDashboard.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnDashboard.DisabledFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnDashboard.DisabledForecolor = System.Drawing.Color.White;
            this.btnDashboard.Enabled = false;
            this.btnDashboard.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDashboard.ForeColor = System.Drawing.Color.White;
            this.btnDashboard.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnDashboard.IconPadding = 9;
            this.btnDashboard.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnDashboard.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnDashboard.IdleBorderRadius = 1;
            this.btnDashboard.IdleBorderThickness = 0;
            this.btnDashboard.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnDashboard.IdleIconLeftImage = ((System.Drawing.Image)(resources.GetObject("btnDashboard.IdleIconLeftImage")));
            this.btnDashboard.IdleIconRightImage = null;
            this.btnDashboard.Location = new System.Drawing.Point(0, 110);
            this.btnDashboard.Name = "btnDashboard";
            stateProperties2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(75)))), ((int)(((byte)(152)))));
            stateProperties2.BorderRadius = 1;
            stateProperties2.BorderThickness = 0;
            stateProperties2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(75)))), ((int)(((byte)(152)))));
            stateProperties2.IconLeftImage = global::Nsf.FriendsLibrary.APP.Properties.Resources.Menu;
            stateProperties2.IconRightImage = null;
            this.btnDashboard.onHoverState = stateProperties2;
            this.btnDashboard.Size = new System.Drawing.Size(214, 55);
            this.btnDashboard.TabIndex = 5;
            this.btnDashboard.Tag = "0";
            this.btnDashboard.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuToolTip1.SetToolTip(this.btnDashboard, "");
            this.bunifuToolTip1.SetToolTipIcon(this.btnDashboard, null);
            this.bunifuToolTip1.SetToolTipTitle(this.btnDashboard, "");
            // 
            // btnEmprestimo
            // 
            this.btnEmprestimo.BackColor = System.Drawing.Color.Transparent;
            this.btnEmprestimo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEmprestimo.BackgroundImage")));
            this.btnEmprestimo.ButtonText = "Empréstimos";
            this.btnEmprestimo.ButtonTextMarginLeft = -18;
            this.btnEmprestimo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AnimationPanel.SetDecoration(this.btnEmprestimo, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition.SetDecoration(this.btnEmprestimo, Bunifu.UI.WinForms.BunifuTransition.DecorationType.None);
            this.btnEmprestimo.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnEmprestimo.DisabledFillColor = System.Drawing.SystemColors.GrayText;
            this.btnEmprestimo.DisabledForecolor = System.Drawing.Color.White;
            this.btnEmprestimo.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEmprestimo.ForeColor = System.Drawing.Color.White;
            this.btnEmprestimo.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnEmprestimo.IconPadding = 9;
            this.btnEmprestimo.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnEmprestimo.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnEmprestimo.IdleBorderRadius = 1;
            this.btnEmprestimo.IdleBorderThickness = 0;
            this.btnEmprestimo.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnEmprestimo.IdleIconLeftImage = global::Nsf.FriendsLibrary.APP.Properties.Resources.Empréstimos;
            this.btnEmprestimo.IdleIconRightImage = null;
            this.btnEmprestimo.Location = new System.Drawing.Point(0, 366);
            this.btnEmprestimo.Name = "btnEmprestimo";
            stateProperties3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(75)))), ((int)(((byte)(152)))));
            stateProperties3.BorderRadius = 1;
            stateProperties3.BorderThickness = 0;
            stateProperties3.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(75)))), ((int)(((byte)(152)))));
            stateProperties3.IconLeftImage = global::Nsf.FriendsLibrary.APP.Properties.Resources.Empréstimos;
            stateProperties3.IconRightImage = null;
            this.btnEmprestimo.onHoverState = stateProperties3;
            this.btnEmprestimo.Size = new System.Drawing.Size(214, 55);
            this.btnEmprestimo.TabIndex = 5;
            this.btnEmprestimo.Tag = "4";
            this.btnEmprestimo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuToolTip1.SetToolTip(this.btnEmprestimo, "");
            this.bunifuToolTip1.SetToolTipIcon(this.btnEmprestimo, null);
            this.bunifuToolTip1.SetToolTipTitle(this.btnEmprestimo, "");
            this.btnEmprestimo.Click += new System.EventHandler(this.btnDashboard_Click_1);
            // 
            // btnRelatorio
            // 
            this.btnRelatorio.BackColor = System.Drawing.Color.Transparent;
            this.btnRelatorio.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRelatorio.BackgroundImage")));
            this.btnRelatorio.ButtonText = "Relatórios";
            this.btnRelatorio.ButtonTextMarginLeft = -43;
            this.btnRelatorio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AnimationPanel.SetDecoration(this.btnRelatorio, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition.SetDecoration(this.btnRelatorio, Bunifu.UI.WinForms.BunifuTransition.DecorationType.None);
            this.btnRelatorio.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnRelatorio.DisabledFillColor = System.Drawing.SystemColors.GrayText;
            this.btnRelatorio.DisabledForecolor = System.Drawing.Color.White;
            this.btnRelatorio.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRelatorio.ForeColor = System.Drawing.Color.White;
            this.btnRelatorio.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnRelatorio.IconPadding = 9;
            this.btnRelatorio.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnRelatorio.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnRelatorio.IdleBorderRadius = 1;
            this.btnRelatorio.IdleBorderThickness = 0;
            this.btnRelatorio.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnRelatorio.IdleIconLeftImage = global::Nsf.FriendsLibrary.APP.Properties.Resources.Relatórios;
            this.btnRelatorio.IdleIconRightImage = null;
            this.btnRelatorio.Location = new System.Drawing.Point(0, 430);
            this.btnRelatorio.Name = "btnRelatorio";
            stateProperties5.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(75)))), ((int)(((byte)(152)))));
            stateProperties5.BorderRadius = 1;
            stateProperties5.BorderThickness = 0;
            stateProperties5.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(75)))), ((int)(((byte)(152)))));
            stateProperties5.IconLeftImage = global::Nsf.FriendsLibrary.APP.Properties.Resources.Relatórios;
            stateProperties5.IconRightImage = null;
            this.btnRelatorio.onHoverState = stateProperties5;
            this.btnRelatorio.Size = new System.Drawing.Size(214, 55);
            this.btnRelatorio.TabIndex = 5;
            this.btnRelatorio.Tag = "3";
            this.btnRelatorio.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuToolTip1.SetToolTip(this.btnRelatorio, "");
            this.bunifuToolTip1.SetToolTipIcon(this.btnRelatorio, null);
            this.bunifuToolTip1.SetToolTipTitle(this.btnRelatorio, "");
            this.btnRelatorio.Visible = false;
            this.btnRelatorio.Click += new System.EventHandler(this.btnDashboard_Click_1);
            // 
            // btnMembro
            // 
            this.btnMembro.BackColor = System.Drawing.Color.Transparent;
            this.btnMembro.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnMembro.BackgroundImage")));
            this.btnMembro.ButtonText = "Leitores";
            this.btnMembro.ButtonTextMarginLeft = -65;
            this.btnMembro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AnimationPanel.SetDecoration(this.btnMembro, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition.SetDecoration(this.btnMembro, Bunifu.UI.WinForms.BunifuTransition.DecorationType.None);
            this.btnMembro.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnMembro.DisabledFillColor = System.Drawing.SystemColors.GrayText;
            this.btnMembro.DisabledForecolor = System.Drawing.Color.White;
            this.btnMembro.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMembro.ForeColor = System.Drawing.Color.White;
            this.btnMembro.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnMembro.IconPadding = 9;
            this.btnMembro.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnMembro.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnMembro.IdleBorderRadius = 1;
            this.btnMembro.IdleBorderThickness = 0;
            this.btnMembro.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnMembro.IdleIconLeftImage = global::Nsf.FriendsLibrary.APP.Properties.Resources.Membros;
            this.btnMembro.IdleIconRightImage = null;
            this.btnMembro.Location = new System.Drawing.Point(0, 238);
            this.btnMembro.Name = "btnMembro";
            stateProperties6.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(75)))), ((int)(((byte)(152)))));
            stateProperties6.BorderRadius = 1;
            stateProperties6.BorderThickness = 0;
            stateProperties6.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(75)))), ((int)(((byte)(152)))));
            stateProperties6.IconLeftImage = global::Nsf.FriendsLibrary.APP.Properties.Resources.Membros;
            stateProperties6.IconRightImage = null;
            this.btnMembro.onHoverState = stateProperties6;
            this.btnMembro.Size = new System.Drawing.Size(214, 55);
            this.btnMembro.TabIndex = 5;
            this.btnMembro.Tag = "2";
            this.btnMembro.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuToolTip1.SetToolTip(this.btnMembro, "");
            this.bunifuToolTip1.SetToolTipIcon(this.btnMembro, null);
            this.bunifuToolTip1.SetToolTipTitle(this.btnMembro, "");
            this.btnMembro.Click += new System.EventHandler(this.btnDashboard_Click_1);
            // 
            // btnLivro
            // 
            this.btnLivro.BackColor = System.Drawing.Color.Transparent;
            this.btnLivro.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLivro.BackgroundImage")));
            this.btnLivro.ButtonText = "Livros";
            this.btnLivro.ButtonTextMarginLeft = -79;
            this.btnLivro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AnimationPanel.SetDecoration(this.btnLivro, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition.SetDecoration(this.btnLivro, Bunifu.UI.WinForms.BunifuTransition.DecorationType.None);
            this.btnLivro.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnLivro.DisabledFillColor = System.Drawing.SystemColors.GrayText;
            this.btnLivro.DisabledForecolor = System.Drawing.Color.White;
            this.btnLivro.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLivro.ForeColor = System.Drawing.Color.White;
            this.btnLivro.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnLivro.IconPadding = 9;
            this.btnLivro.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnLivro.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnLivro.IdleBorderRadius = 1;
            this.btnLivro.IdleBorderThickness = 0;
            this.btnLivro.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnLivro.IdleIconLeftImage = global::Nsf.FriendsLibrary.APP.Properties.Resources.Livros;
            this.btnLivro.IdleIconRightImage = null;
            this.btnLivro.Location = new System.Drawing.Point(0, 174);
            this.btnLivro.Name = "btnLivro";
            stateProperties7.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(75)))), ((int)(((byte)(152)))));
            stateProperties7.BorderRadius = 1;
            stateProperties7.BorderThickness = 0;
            stateProperties7.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(75)))), ((int)(((byte)(152)))));
            stateProperties7.IconLeftImage = global::Nsf.FriendsLibrary.APP.Properties.Resources.Livros;
            stateProperties7.IconRightImage = null;
            this.btnLivro.onHoverState = stateProperties7;
            this.btnLivro.Size = new System.Drawing.Size(214, 55);
            this.btnLivro.TabIndex = 5;
            this.btnLivro.Tag = "1";
            this.btnLivro.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuToolTip1.SetToolTip(this.btnLivro, "");
            this.bunifuToolTip1.SetToolTipIcon(this.btnLivro, null);
            this.bunifuToolTip1.SetToolTipTitle(this.btnLivro, "");
            this.btnLivro.Click += new System.EventHandler(this.btnDashboard_Click_1);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.pictureBox2);
            this.AnimationPanel.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition.SetDecoration(this.panel2, Bunifu.UI.WinForms.BunifuTransition.DecorationType.None);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(203, 90);
            this.panel2.TabIndex = 0;
            this.bunifuToolTip1.SetToolTip(this.panel2, "");
            this.bunifuToolTip1.SetToolTipIcon(this.panel2, null);
            this.bunifuToolTip1.SetToolTipTitle(this.panel2, "");
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.bunifuTransition.SetDecoration(this.pictureBox2, Bunifu.UI.WinForms.BunifuTransition.DecorationType.None);
            this.AnimationPanel.SetDecoration(this.pictureBox2, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(76, 15);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(51, 57);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            this.bunifuToolTip1.SetToolTip(this.pictureBox2, "");
            this.bunifuToolTip1.SetToolTipIcon(this.pictureBox2, null);
            this.bunifuToolTip1.SetToolTipTitle(this.pictureBox2, "");
            // 
            // ElipseForm
            // 
            this.ElipseForm.ElipseRadius = 25;
            this.ElipseForm.TargetControl = this;
            // 
            // AnimationPanel
            // 
            this.AnimationPanel.AnimationType = BunifuAnimatorNS.AnimationType.HorizSlide;
            this.AnimationPanel.Cursor = null;
            animation1.AnimateOnlyDifferences = true;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 0F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 0F;
            this.AnimationPanel.DefaultAnimation = animation1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.frmSubMain1);
            this.panel3.Controls.Add(this.panel4);
            this.AnimationPanel.SetDecoration(this.panel3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition.SetDecoration(this.panel3, Bunifu.UI.WinForms.BunifuTransition.DecorationType.None);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(203, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(218, 683);
            this.panel3.TabIndex = 0;
            this.bunifuToolTip1.SetToolTip(this.panel3, "");
            this.bunifuToolTip1.SetToolTipIcon(this.panel3, null);
            this.bunifuToolTip1.SetToolTipTitle(this.panel3, "");
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel4.Controls.Add(this.label3);
            this.panel4.Controls.Add(this.label2);
            this.AnimationPanel.SetDecoration(this.panel4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition.SetDecoration(this.panel4, Bunifu.UI.WinForms.BunifuTransition.DecorationType.None);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(218, 92);
            this.panel4.TabIndex = 0;
            this.bunifuToolTip1.SetToolTip(this.panel4, "");
            this.bunifuToolTip1.SetToolTipIcon(this.panel4, null);
            this.bunifuToolTip1.SetToolTipTitle(this.panel4, "");
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.AnimationPanel.SetDecoration(this.label3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition.SetDecoration(this.label3, Bunifu.UI.WinForms.BunifuTransition.DecorationType.None);
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(88)))));
            this.label3.Location = new System.Drawing.Point(67, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 18);
            this.label3.TabIndex = 3;
            this.label3.Text = "Sub-Menu";
            this.bunifuToolTip1.SetToolTip(this.label3, "");
            this.bunifuToolTip1.SetToolTipIcon(this.label3, null);
            this.bunifuToolTip1.SetToolTipTitle(this.label3, "");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.AnimationPanel.SetDecoration(this.label2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition.SetDecoration(this.label2, Bunifu.UI.WinForms.BunifuTransition.DecorationType.None);
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(88)))));
            this.label2.Location = new System.Drawing.Point(27, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(171, 26);
            this.label2.TabIndex = 3;
            this.label2.Text = "Friends Library";
            this.bunifuToolTip1.SetToolTip(this.label2, "");
            this.bunifuToolTip1.SetToolTipIcon(this.label2, null);
            this.bunifuToolTip1.SetToolTipTitle(this.label2, "");
            // 
            // pnBarra
            // 
            this.AnimationPanel.SetDecoration(this.pnBarra, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition.SetDecoration(this.pnBarra, Bunifu.UI.WinForms.BunifuTransition.DecorationType.None);
            this.pnBarra.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnBarra.Location = new System.Drawing.Point(421, 0);
            this.pnBarra.Name = "pnBarra";
            this.pnBarra.Size = new System.Drawing.Size(5, 683);
            this.pnBarra.TabIndex = 1;
            this.bunifuToolTip1.SetToolTip(this.pnBarra, "");
            this.bunifuToolTip1.SetToolTipIcon(this.pnBarra, null);
            this.bunifuToolTip1.SetToolTipTitle(this.pnBarra, "");
            // 
            // pnSuperior
            // 
            this.pnSuperior.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnSuperior.Controls.Add(this.lblNome);
            this.pnSuperior.Controls.Add(this.label1);
            this.AnimationPanel.SetDecoration(this.pnSuperior, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition.SetDecoration(this.pnSuperior, Bunifu.UI.WinForms.BunifuTransition.DecorationType.None);
            this.pnSuperior.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnSuperior.Location = new System.Drawing.Point(426, 0);
            this.pnSuperior.Name = "pnSuperior";
            this.pnSuperior.Size = new System.Drawing.Size(673, 41);
            this.pnSuperior.TabIndex = 2;
            this.bunifuToolTip1.SetToolTip(this.pnSuperior, "");
            this.bunifuToolTip1.SetToolTipIcon(this.pnSuperior, null);
            this.bunifuToolTip1.SetToolTipTitle(this.pnSuperior, "");
            // 
            // lblNome
            // 
            this.lblNome.AutoSize = true;
            this.AnimationPanel.SetDecoration(this.lblNome, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition.SetDecoration(this.lblNome, Bunifu.UI.WinForms.BunifuTransition.DecorationType.None);
            this.lblNome.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(88)))));
            this.lblNome.Location = new System.Drawing.Point(131, 9);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(16, 21);
            this.lblNome.TabIndex = 7;
            this.lblNome.Text = "-";
            this.bunifuToolTip1.SetToolTip(this.lblNome, "");
            this.bunifuToolTip1.SetToolTipIcon(this.lblNome, null);
            this.bunifuToolTip1.SetToolTipTitle(this.lblNome, "");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.AnimationPanel.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition.SetDecoration(this.label1, Bunifu.UI.WinForms.BunifuTransition.DecorationType.None);
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(36)))), ((int)(((byte)(88)))));
            this.label1.Location = new System.Drawing.Point(6, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 21);
            this.label1.TabIndex = 7;
            this.label1.Text = "Seja bem-vindo: ";
            this.bunifuToolTip1.SetToolTip(this.label1, "");
            this.bunifuToolTip1.SetToolTipIcon(this.label1, null);
            this.bunifuToolTip1.SetToolTipTitle(this.label1, "");
            // 
            // pnConteiner
            // 
            this.pnConteiner.BackColor = System.Drawing.Color.White;
            this.pnConteiner.Controls.Add(this.pnMain);
            this.AnimationPanel.SetDecoration(this.pnConteiner, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition.SetDecoration(this.pnConteiner, Bunifu.UI.WinForms.BunifuTransition.DecorationType.None);
            this.pnConteiner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnConteiner.Location = new System.Drawing.Point(426, 41);
            this.pnConteiner.Name = "pnConteiner";
            this.pnConteiner.Size = new System.Drawing.Size(673, 642);
            this.pnConteiner.TabIndex = 3;
            this.bunifuToolTip1.SetToolTip(this.pnConteiner, "");
            this.bunifuToolTip1.SetToolTipIcon(this.pnConteiner, null);
            this.bunifuToolTip1.SetToolTipTitle(this.pnConteiner, "");
            // 
            // pnMain
            // 
            this.pnMain.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.AnimationPanel.SetDecoration(this.pnMain, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition.SetDecoration(this.pnMain, Bunifu.UI.WinForms.BunifuTransition.DecorationType.None);
            this.pnMain.Location = new System.Drawing.Point(28, 5);
            this.pnMain.Name = "pnMain";
            this.pnMain.Size = new System.Drawing.Size(625, 632);
            this.pnMain.TabIndex = 210;
            this.bunifuToolTip1.SetToolTip(this.pnMain, "");
            this.bunifuToolTip1.SetToolTipIcon(this.pnMain, null);
            this.bunifuToolTip1.SetToolTipTitle(this.pnMain, "");
            this.pnMain.Visible = false;
            // 
            // TimerMouseDetect
            // 
            this.TimerMouseDetect.Enabled = true;
            this.TimerMouseDetect.Tick += new System.EventHandler(this.TimerMouseDetect_Tick);
            // 
            // bunifuTransition
            // 
            this.bunifuTransition.AnimationType = Bunifu.UI.WinForms.BunifuAnimatorNS.AnimationType.Scale;
            this.bunifuTransition.Cursor = null;
            animation2.AnimateOnlyDifferences = true;
            animation2.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.BlindCoeff")));
            animation2.LeafCoeff = 0F;
            animation2.MaxTime = 1F;
            animation2.MinTime = 0F;
            animation2.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.MosaicCoeff")));
            animation2.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation2.MosaicShift")));
            animation2.MosaicSize = 0;
            animation2.Padding = new System.Windows.Forms.Padding(0);
            animation2.RotateCoeff = 0F;
            animation2.RotateLimit = 0F;
            animation2.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.ScaleCoeff")));
            animation2.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.SlideCoeff")));
            animation2.TimeCoeff = 0F;
            animation2.TransparencyCoeff = 0F;
            this.bunifuTransition.DefaultAnimation = animation2;
            this.bunifuTransition.Interval = 50;
            // 
            // bunifuButton1
            // 
            this.bunifuButton1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuButton1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton1.BackgroundImage")));
            this.bunifuButton1.ButtonText = "";
            this.bunifuButton1.ButtonTextMarginLeft = 0;
            this.bunifuButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.AnimationPanel.SetDecoration(this.bunifuButton1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition.SetDecoration(this.bunifuButton1, Bunifu.UI.WinForms.BunifuTransition.DecorationType.None);
            this.bunifuButton1.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.bunifuButton1.DisabledFillColor = System.Drawing.SystemColors.GrayText;
            this.bunifuButton1.DisabledForecolor = System.Drawing.Color.White;
            this.bunifuButton1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuButton1.ForeColor = System.Drawing.Color.White;
            this.bunifuButton1.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuButton1.IconPadding = 5;
            this.bunifuButton1.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.bunifuButton1.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.bunifuButton1.IdleBorderRadius = 1;
            this.bunifuButton1.IdleBorderThickness = 0;
            this.bunifuButton1.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.bunifuButton1.IdleIconLeftImage = ((System.Drawing.Image)(resources.GetObject("bunifuButton1.IdleIconLeftImage")));
            this.bunifuButton1.IdleIconRightImage = null;
            this.bunifuButton1.Location = new System.Drawing.Point(79, 625);
            this.bunifuButton1.Name = "bunifuButton1";
            stateProperties4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(75)))), ((int)(((byte)(152)))));
            stateProperties4.BorderRadius = 1;
            stateProperties4.BorderThickness = 0;
            stateProperties4.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(75)))), ((int)(((byte)(152)))));
            stateProperties4.IconLeftImage = ((System.Drawing.Image)(resources.GetObject("stateProperties4.IconLeftImage")));
            stateProperties4.IconRightImage = null;
            this.bunifuButton1.onHoverState = stateProperties4;
            this.bunifuButton1.Size = new System.Drawing.Size(57, 55);
            this.bunifuButton1.TabIndex = 5;
            this.bunifuButton1.Tag = "3";
            this.bunifuButton1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuToolTip1.SetToolTip(this.bunifuButton1, "");
            this.bunifuToolTip1.SetToolTipIcon(this.bunifuButton1, null);
            this.bunifuToolTip1.SetToolTipTitle(this.bunifuButton1, "");
            this.bunifuButton1.Visible = false;
            this.bunifuButton1.Click += new System.EventHandler(this.bunifuButton1_Click_1);
            // 
            // frmSubMain1
            // 
            this.frmSubMain1.BackColor = System.Drawing.Color.White;
            this.AnimationPanel.SetDecoration(this.frmSubMain1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition.SetDecoration(this.frmSubMain1, Bunifu.UI.WinForms.BunifuTransition.DecorationType.None);
            this.frmSubMain1.Dock = System.Windows.Forms.DockStyle.Left;
            this.frmSubMain1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frmSubMain1.Location = new System.Drawing.Point(0, 92);
            this.frmSubMain1.Name = "frmSubMain1";
            this.frmSubMain1.Size = new System.Drawing.Size(234, 591);
            this.frmSubMain1.TabIndex = 1;
            this.bunifuToolTip1.SetToolTip(this.frmSubMain1, "");
            this.bunifuToolTip1.SetToolTipIcon(this.frmSubMain1, null);
            this.bunifuToolTip1.SetToolTipTitle(this.frmSubMain1, "");
            // 
            // bunifuToolTip1
            // 
            this.bunifuToolTip1.Active = true;
            this.bunifuToolTip1.AlignTextWithTitle = false;
            this.bunifuToolTip1.AllowAutoClose = false;
            this.bunifuToolTip1.AllowFading = true;
            this.bunifuToolTip1.AutoCloseDuration = 5000;
            this.bunifuToolTip1.BackColor = System.Drawing.SystemColors.Control;
            this.bunifuToolTip1.BorderColor = System.Drawing.Color.Gainsboro;
            this.bunifuToolTip1.ClickToShowDisplayControl = false;
            this.bunifuToolTip1.ConvertNewlinesToBreakTags = true;
            this.bunifuToolTip1.DisplayControl = null;
            this.bunifuToolTip1.EntryAnimationSpeed = 350;
            this.bunifuToolTip1.ExitAnimationSpeed = 200;
            this.bunifuToolTip1.GenerateAutoCloseDuration = false;
            this.bunifuToolTip1.IconMargin = 6;
            this.bunifuToolTip1.InitialDelay = 0;
            this.bunifuToolTip1.Name = "bunifuToolTip1";
            this.bunifuToolTip1.Opacity = 1D;
            this.bunifuToolTip1.OverrideToolTipTitles = false;
            this.bunifuToolTip1.Padding = new System.Windows.Forms.Padding(10);
            this.bunifuToolTip1.ReshowDelay = 100;
            this.bunifuToolTip1.ShowAlways = true;
            this.bunifuToolTip1.ShowBorders = false;
            this.bunifuToolTip1.ShowIcons = true;
            this.bunifuToolTip1.ShowShadows = true;
            this.bunifuToolTip1.Tag = null;
            this.bunifuToolTip1.TextFont = new System.Drawing.Font("Segoe UI", 9F);
            this.bunifuToolTip1.TextForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuToolTip1.TextMargin = 2;
            this.bunifuToolTip1.TitleFont = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.bunifuToolTip1.TitleForeColor = System.Drawing.Color.Black;
            this.bunifuToolTip1.ToolTipPosition = new System.Drawing.Point(0, 0);
            this.bunifuToolTip1.ToolTipTitle = null;
            // 
            // frmMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(1099, 683);
            this.Controls.Add(this.pnConteiner);
            this.Controls.Add(this.pnSuperior);
            this.Controls.Add(this.pnBarra);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.pnMainMenu);
            this.AnimationPanel.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition.SetDecoration(this, Bunifu.UI.WinForms.BunifuTransition.DecorationType.None);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Friends Library Management";
            this.pnMainMenu.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.pnSuperior.ResumeLayout(false);
            this.pnSuperior.PerformLayout();
            this.pnConteiner.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnMainMenu;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Bunifu.Framework.UI.BunifuElipse ElipseForm;
        private BunifuAnimatorNS.BunifuTransition AnimationPanel;
        private System.Windows.Forms.Timer TimerMouseDetect;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnBarra;
        private Forms.Alternativas__Marcos_.frmSubMain frmSubMain1;
        private System.Windows.Forms.Panel pnConteiner;
        private System.Windows.Forms.Panel pnSuperior;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnLivro;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnDashboard;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnEmprestimo;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnRelatorio;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnMembro;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnFuncionario;
        private System.Windows.Forms.Panel pnMain;
        private Bunifu.UI.WinForms.BunifuTransition bunifuTransition;
        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifuButton1;
        private Bunifu.UI.WinForms.BunifuToolTip bunifuToolTip1;
    }
}

