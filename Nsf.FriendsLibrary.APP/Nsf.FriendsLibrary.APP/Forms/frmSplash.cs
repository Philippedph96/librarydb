﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteca.Forms
{
    public partial class frmSplash : Form
    {
        Timer timeback = new Timer();
        Timer timerocket = new Timer();
        Timer timeball = new Timer();

        public frmSplash()
        {
            InitializeComponent();

            //Começa a contagem para o início da tela de Splash Screen
            Task.Factory.StartNew(() =>
            {
                //Espere cerca de 11/5 segundos 
                System.Threading.Thread.Sleep(11500);

                //Literalmente, INVOCA uma ação capaz de reconhecer os formulários
                Invoke(new Action(() =>
                {
                    frmLogin tela = new frmLogin();
                    tela.Show();
                    this.Hide();
                }));
            });

            timeback.Tick += new EventHandler(Resources);
            timerocket.Tick += new EventHandler(RocketUP);
            timeball.Tick += new EventHandler(Ball);

            Default();
        }

        void Default()
        {
            pnlLineRocket.Height = 1; pnlLineRocket.Width = 0; pnlLineRocket.Left = 341;
            imgBall.Top = 262; imgBall.Visible = false; imgBall.BringToFront();
            imgRockets.Height = 64; imgRockets.Top = 320; imgRockets.Visible = false;

            //Localização das imagens
            imgBall.Image = Nsf.FriendsLibrary.APP.Properties.Resources.BlueBall;
            imgRockets.Image = Nsf.FriendsLibrary.APP.Properties.Resources.WhiteRocket;
            AnimTransition.HideSync(lblFrase);

            //Intervalo de tempo
            timeback.Interval = 30;
            timeball.Interval = 40;
            timerocket.Interval = 10;

            timeback.Start(); //Primeiro da magia
        }

        //Valor inicial
        int a = 0;
        void Resources(object sender, EventArgs e)
        {
            pnlLineRocket.Width += a;

            if (pnlLineRocket.Width < 1)
            {
                a = 8;
            }

            if (pnlLineRocket.Width > 104)
            {
                timeback.Stop();
                timerocket.Start();
            }

            //
            AnimTransition.ShowSync(lblFrase);
        }

        int down = 1, lt = 5, rocktop = -1;
        void Ball(object sender, EventArgs e)
        {
            imgBall.Top += down;

            if (imgBall.Top < 265)
            {
                imgBall.Visible = true;
                down = 1;
            }
            if (imgBall.Top > 305)
            {
                timeball.Interval = 10; //Tornar a animação mais rápida
                imgRockets.Image = Nsf.FriendsLibrary.APP.Properties.Resources.BlueRocket;
                
                imgBall.Visible = false;
                down = 30;
                imgRockets.Top -= down;

                if (imgRockets.Top < -60)
                {
                    pnlLineRocket.Width -= lt;
                    pnlLineRocket.Left += lt;

                    if (pnlLineRocket.Left > 470)
                    {
                        down = 1;
                        timeball.Stop();
                        Default();
                    }
                }
            }
        }

        void RocketUP(object sender, EventArgs e)
        {
            imgRockets.Top += rocktop;

            if (imgRockets.Top > 318)
            {
                imgRockets.Visible = true;
                rocktop = -5;
            }
            if (imgRockets.Top < 250)
            {
                rocktop = -1;
                timerocket.Stop();
                timeball.Start();
            }
        }
    }
}
