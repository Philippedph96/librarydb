﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Biblioteca.Forms;
using KimtToo.VisualReactive;
using Nsf.FriendsLibrary.Tools.Utilitários.User_Session;

namespace Biblioteca
{
    public partial class frmMain : Form
    {
        public static frmMain UserControl;
        public frmMain()
        {
            InitializeComponent();
            UserControl = this;

            // lblNome.Text = UserSession.UsuarioLogado.nm_bibliotecario;
            this.AlterarCorFundo(btnLivro);
        }

        private void DashboadDisable()
        {
            var condicao = int.Parse(Tag.ToString());
            if (condicao == 0)
                frmSubMain1.Visible = true;
        }
        public void OpenUserControl(UserControl control)
        {
            if (pnMain.InvokeRequired)
            {
                pnMain.Invoke((MethodInvoker)delegate
                {
                    pnMain.Visible = false;
                    if (pnMain.Controls.Count == 1)
                        pnMain.Controls.RemoveAt(0);
                    pnMain.Controls.Add(control);

                    if (pnMain.Visible == false)
                        bunifuTransition.ShowSync(pnMain);
                });
            }
            else // Para User Controls sem método assíncrono (Testes de formulários)
            {
                pnMain.Visible = false;
                if (pnMain.Controls.Count == 1)
                    pnMain.Controls.RemoveAt(0);
                pnMain.Controls.Add(control);

                if (pnMain.Visible == false)
                    bunifuTransition.ShowSync(pnMain);
            }
        }

        public void CleanControl(UserControl control)
        {
            if (pnMain.Controls.Count == 1)
                pnMain.Controls.RemoveAt(0);
            pnMain.Controls.Add(control);
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        bool extended = false;
        private void TimerMouseDetect_Tick(object sender, EventArgs e)
        {
            //Quando finazar a Animação
            if (!AnimationPanel.IsCompleted) return;
            //Detecta se o mouse está sobre o Panel Principal
            if (pnMainMenu.ClientRectangle.Contains(PointToClient(Control.MousePosition)))
            {
                //Expand o Panel com a Animação
                if (!extended)
                {
                    extended = true;
                    pnMainMenu.Width = 203;
                }
            }
            else
            {
                //Fecha o Panel com a Animação
                if (extended)
                {
                    extended = false;
                    pnMainMenu.Visible = false;
                    pnMainMenu.Width = 53;
                    AnimationPanel.Show(pnMainMenu);
                }
            }
        }

        private void AlterarCorFundo(Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifu)
        {
            if (bunifu.InvokeRequired)
                bunifu.Invoke((MethodInvoker)delegate
                {
                    //Troca a cor do controle quando selecionado
                    bunifu.IdleBorderColor = Color.FromArgb(0, 36, 88);
                    bunifu.IdleFillColor = Color.FromArgb(0, 36, 88);
                });
        }
        private void NormalizarCorPadrao(Bunifu.UI.WinForms.BunifuButton.BunifuButton bunifu)
        {
            if (bunifu.InvokeRequired)
                bunifu.Invoke((MethodInvoker)delegate
                {
                    //Troca a cor do controle quando selecionado
                    bunifu.IdleBorderColor = Color.FromArgb(0, 82, 204);
                    bunifu.IdleFillColor = Color.FromArgb(0, 82, 204);
                });
        }

        //Este método altera a cor do botão de acordo com o número da Tag
        private void EscolherCorFundo(int numero)
        {
            //Se o número encontrado na Tag for correspondente ao número 0
            if (numero == 0)
                AlterarCorFundo(btnDashboard);
            else
                NormalizarCorPadrao(btnDashboard);
            //Se o número encontrado na Tag for correspondente ao número 1
            if (numero == 1)
                AlterarCorFundo(btnLivro);
            else
                NormalizarCorPadrao(btnLivro);

            //Se o número encontrado na Tag for correspondente ao número 2
            if (numero == 2)
                AlterarCorFundo(btnMembro);
            else
                NormalizarCorPadrao(btnMembro);

            //Se o número encontrado na Tag for correspondente ao número 3
            if (numero == 3)
                AlterarCorFundo(btnRelatorio);
            else
                NormalizarCorPadrao(btnRelatorio);

            //Se o número encontrado na Tag for correspondente ao número 4
            if (numero == 4)
                AlterarCorFundo(btnEmprestimo);
            else
                NormalizarCorPadrao(btnEmprestimo);

            //Se o número encontrado na Tag for correspondente ao número 5
            if (numero == 5)
                AlterarCorFundo(btnFuncionario);
            else
                NormalizarCorPadrao(btnFuncionario);

        }
        private async void btnDashboard_Click_1(object sender, EventArgs e)
        {
            //Pegando o número de TAG
            VSReactive<int>.SetState("menu", int.Parse(((Control)sender).Tag.ToString()));

            //Pegando o número da Tag para utilizar como parâmetro no método EscolherCorFundo
            var inteiro = int.Parse(((Control)sender).Tag.ToString());
            await Task.Run(() => EscolherCorFundo(inteiro));
        }

        private void bunifuButton1_Click_1(object sender, EventArgs e)
        {
            frmLogin login = new frmLogin();
            login.Show();
            this.Close();
        }

    }
}
