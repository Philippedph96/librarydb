﻿using Biblioteca.Utilitarios.Notificacoes;
using Nsf.FriendsLibrary.APP.Properties;
using Nsf.FriendsLibrary.Business.Bibliotecario;
using Nsf.FriendsLibrary.DB;
using Nsf.FriendsLibrary.Tools.Utilitários.User_Session;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Biblioteca.Forms
{
    public partial class frmLogin : Form
    {
        #region Animations and Fuctions
        Timer timer1 = new Timer();
        Timer timer2 = new Timer();
        Label label = new Label();

        public frmLogin()
        {
            InitializeComponent();

            this.Size = new Size(320, 480);
            imgPassword.Image = Nsf.FriendsLibrary.APP.Properties.Resources.Password;
            imgPassword.Enabled = false;

            //Variável verificadora
            ativo = true;
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            timer1.Tick += new EventHandler(Timer1);
            timer2.Tick += new EventHandler(Timer2);

            timer1.Interval = 10;
            timer2.Interval = 5;
            timer1.Start();
        }

        void Timer1(object sender, EventArgs e)
        {
            LogoPosition();
        }

        void Timer2(object sender, EventArgs e)
        {
            if (label.Text == "Login")
            {
                Login();
            }
        }

        int logo = 1;
        void LogoPosition()
        {
            if (panel1.Left == 0)
            {
                imgLogo.Top += logo;
                if (imgLogo.Top > 50)
                {
                    timer1.Stop();
                }
            }

        }

        int speed = 20;
        void Login()
        {
            if (panel1.Left < 0)
            {
                timer1.Start();
        
                panel1.Left += speed;

                if (panel1.Left == 0)
                {
                    timer2.Stop();
                }
            }
        }

        bool ativo;
        void Unlock()
        {
            if (ativo == true)
            {
                //Destranca
                txtPassword.UseSystemPasswordChar = false;
                imgPassword.Image = Nsf.FriendsLibrary.APP.Properties.Resources.Unlocked;
                ativo = false;
            }
            else
            {
                //Tranca
                txtPassword.UseSystemPasswordChar = true;
                imgPassword.Image = Nsf.FriendsLibrary.APP.Properties.Resources.Password;
                ativo = true;
            }
        }

        private void txtUserName_Enter(object sender, EventArgs e)
        {
            if (txtUserName.Text == "User Name")
            {
                txtUserName.Text = string.Empty;
                lnUserName.BackColor = Color.FromArgb(0, 82, 204);
            }
        }

        private void txtUserName_Leave(object sender, EventArgs e)
        {
            if (txtUserName.Text == string.Empty)
            {
                txtUserName.Text = "User Name";
                lnUserName.BackColor = Color.FromArgb(100, 192, 192, 192);
            }
        }

        private void txtPassword_Enter(object sender, EventArgs e)
        {
            if (txtPassword.Text == "Password")
            {
                txtPassword.UseSystemPasswordChar = true;
                txtPassword.Text = string.Empty;
                imgPassword.Enabled = true;
                lnPassword.BackColor = Color.FromArgb(0, 82, 204);
            }
        }

        private void txtPassword_Leave(object sender, EventArgs e)
        {
            if (txtPassword.Text == string.Empty)
            {
                txtPassword.UseSystemPasswordChar = false;
                txtPassword.Text = "Password";
                imgPassword.Enabled = false;
                lnPassword.BackColor = Color.FromArgb(100, 192, 192, 192);
            }
        }

        private void Forget(object sender, EventArgs e)
        {
            Label controle = (Label)sender;

            label = controle;
            timer2.Start();
        }

        private void Lock(object sender, EventArgs e)
        {
            Unlock();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        #endregion
        private async void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                //COLETA DE DADOS
                string password = txtPassword.Text;

                //PROCESSAMENTO DOS DADOS
                BibliotecarioBusiness db = new BibliotecarioBusiness();
                var funcionario = db.Login(password);

                //SAÍDA DOS DADOS
                if (funcionario != null)
                {
                    UserSession.UsuarioLogado = funcionario;

                    frmMain menu = new frmMain();
                    this.Hide();
                    menu.Show();
                }
                else
                {
                    //Criação de uma nova thread para ser executada em segundo plano de uma outra thread que está sendo executada kkk
                    await Task.Factory.StartNew(() =>
                    {
                    //Cria uma variável de texto com a seguinte informação
                    string text = "Credênciais inválidas!!!!";

                    //Pego a imagem da pasta resources
                    Image imagem = Resources.Delete;

                    //Passo para a classe que irá gerar uma notificação
                    this.SelectorClass(3, text, imagem);
                    });
                }

            }
            catch (ArgumentException business)
            {
                MessageBox.Show(business.Message,
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + "\n" + "Tente mais tarde.",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private void bunifuThinButton21_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnLogin_Click(null, null);
        }
    }
}
