﻿using Bunifu.UI.WinForms.BunifuTextbox;

namespace Nsf.FriendsLibrary.APP.User_Controls.Complemento_dos_Livros.Autor
{
    partial class frmAddEditCategory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAddEditCategory));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            this.lblTitle = new System.Windows.Forms.Label();
            this.pnConteiner = new System.Windows.Forms.Panel();
            this.pnControls = new System.Windows.Forms.Panel();
            this.txtDescricao = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnSeparator = new System.Windows.Forms.Panel();
            this.btnLimpar = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.btnSaveUpdate = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.txtCategoria = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.pnError = new System.Windows.Forms.Panel();
            this.lblError = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TimerError = new System.Windows.Forms.Timer(this.components);
            this.ToolTipForControls = new Bunifu.UI.WinForms.BunifuToolTip(this.components);
            this.pnTop = new System.Windows.Forms.Panel();
            this.cdsHearder = new Bunifu.Framework.UI.BunifuCards();
            this.btnClose = new Bunifu.Framework.UI.BunifuImageButton();
            this.dclForm = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.ElipseForm = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.ElipseTextBox = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.pnConteiner.SuspendLayout();
            this.pnControls.SuspendLayout();
            this.pnError.SuspendLayout();
            this.pnTop.SuspendLayout();
            this.cdsHearder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold);
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(36, 16);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(70, 30);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "{title} ";
            this.ToolTipForControls.SetToolTip(this.lblTitle, "");
            this.ToolTipForControls.SetToolTipIcon(this.lblTitle, null);
            this.ToolTipForControls.SetToolTipTitle(this.lblTitle, "");
            // 
            // pnConteiner
            // 
            this.pnConteiner.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnConteiner.Controls.Add(this.pnControls);
            this.pnConteiner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnConteiner.ForeColor = System.Drawing.Color.White;
            this.pnConteiner.Location = new System.Drawing.Point(0, 0);
            this.pnConteiner.Name = "pnConteiner";
            this.pnConteiner.Size = new System.Drawing.Size(432, 355);
            this.pnConteiner.TabIndex = 1;
            this.ToolTipForControls.SetToolTip(this.pnConteiner, "");
            this.ToolTipForControls.SetToolTipIcon(this.pnConteiner, null);
            this.ToolTipForControls.SetToolTipTitle(this.pnConteiner, "");
            // 
            // pnControls
            // 
            this.pnControls.BackColor = System.Drawing.Color.White;
            this.pnControls.Controls.Add(this.txtDescricao);
            this.pnControls.Controls.Add(this.panel1);
            this.pnControls.Controls.Add(this.pnSeparator);
            this.pnControls.Controls.Add(this.btnLimpar);
            this.pnControls.Controls.Add(this.btnSaveUpdate);
            this.pnControls.Controls.Add(this.txtCategoria);
            this.pnControls.Controls.Add(this.pnError);
            this.pnControls.Controls.Add(this.label1);
            this.pnControls.Location = new System.Drawing.Point(13, 67);
            this.pnControls.Name = "pnControls";
            this.pnControls.Size = new System.Drawing.Size(407, 276);
            this.pnControls.TabIndex = 181;
            this.ToolTipForControls.SetToolTip(this.pnControls, "");
            this.ToolTipForControls.SetToolTipIcon(this.pnControls, null);
            this.ToolTipForControls.SetToolTipTitle(this.pnControls, "");
            // 
            // txtDescricao
            // 
            this.txtDescricao.AcceptsReturn = false;
            this.txtDescricao.AcceptsTab = false;
            this.txtDescricao.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtDescricao.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtDescricao.BackColor = System.Drawing.Color.Transparent;
            this.txtDescricao.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtDescricao.BackgroundImage")));
            this.txtDescricao.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtDescricao.BorderColorDisabled = System.Drawing.Color.Tomato;
            this.txtDescricao.BorderColorHover = System.Drawing.Color.Crimson;
            this.txtDescricao.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtDescricao.BorderRadius = 35;
            this.txtDescricao.BorderThickness = 1;
            this.txtDescricao.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtDescricao.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescricao.DefaultText = "";
            this.txtDescricao.FillColor = System.Drawing.SystemColors.Window;
            this.txtDescricao.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtDescricao.HideSelection = true;
            this.txtDescricao.IconLeft = null;
            this.txtDescricao.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtDescricao.IconPadding = 6;
            this.txtDescricao.IconRight = null;
            this.txtDescricao.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.txtDescricao.Location = new System.Drawing.Point(23, 152);
            this.txtDescricao.MaxLength = 100;
            this.txtDescricao.MinimumSize = new System.Drawing.Size(100, 35);
            this.txtDescricao.Modified = false;
            this.txtDescricao.Name = "txtDescricao";
            this.txtDescricao.PasswordChar = '\0';
            this.txtDescricao.ReadOnly = false;
            this.txtDescricao.SelectedText = "";
            this.txtDescricao.SelectionLength = 0;
            this.txtDescricao.SelectionStart = 0;
            this.txtDescricao.ShortcutsEnabled = true;
            this.txtDescricao.Size = new System.Drawing.Size(352, 36);
            this.txtDescricao.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtDescricao.TabIndex = 3;
            this.txtDescricao.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtDescricao.TextMarginLeft = 10;
            this.txtDescricao.TextPlaceholder = "Breve descrição";
            this.ToolTipForControls.SetToolTip(this.txtDescricao, "Digite uma breve descrição da categoria podendo ter até no máximo 100 Caracteres");
            this.ToolTipForControls.SetToolTipIcon(this.txtDescricao, null);
            this.ToolTipForControls.SetToolTipTitle(this.txtDescricao, "");
            this.txtDescricao.UseSystemPasswordChar = false;
            this.txtDescricao.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown_1);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Location = new System.Drawing.Point(17, 213);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(375, 1);
            this.panel1.TabIndex = 4;
            this.ToolTipForControls.SetToolTip(this.panel1, "");
            this.ToolTipForControls.SetToolTipIcon(this.panel1, null);
            this.ToolTipForControls.SetToolTipTitle(this.panel1, "");
            // 
            // pnSeparator
            // 
            this.pnSeparator.BackColor = System.Drawing.Color.Gainsboro;
            this.pnSeparator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnSeparator.Location = new System.Drawing.Point(17, 73);
            this.pnSeparator.Name = "pnSeparator";
            this.pnSeparator.Size = new System.Drawing.Size(375, 1);
            this.pnSeparator.TabIndex = 1;
            this.ToolTipForControls.SetToolTip(this.pnSeparator, "");
            this.ToolTipForControls.SetToolTipIcon(this.pnSeparator, null);
            this.ToolTipForControls.SetToolTipTitle(this.pnSeparator, "");
            // 
            // btnLimpar
            // 
            this.btnLimpar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLimpar.BackColor = System.Drawing.Color.Transparent;
            this.btnLimpar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLimpar.BackgroundImage")));
            this.btnLimpar.ButtonText = "Limpar";
            this.btnLimpar.ButtonTextMarginLeft = 15;
            this.btnLimpar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLimpar.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnLimpar.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnLimpar.DisabledForecolor = System.Drawing.Color.White;
            this.btnLimpar.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold);
            this.btnLimpar.ForeColor = System.Drawing.Color.White;
            this.btnLimpar.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnLimpar.IconPadding = 8;
            this.btnLimpar.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.btnLimpar.IdleBorderColor = System.Drawing.Color.Gray;
            this.btnLimpar.IdleBorderRadius = 10;
            this.btnLimpar.IdleBorderThickness = 1;
            this.btnLimpar.IdleFillColor = System.Drawing.Color.Gray;
            this.btnLimpar.IdleIconLeftImage = null;
            this.btnLimpar.IdleIconRightImage = ((System.Drawing.Image)(resources.GetObject("btnLimpar.IdleIconRightImage")));
            this.btnLimpar.Location = new System.Drawing.Point(205, 227);
            this.btnLimpar.Name = "btnLimpar";
            stateProperties1.BorderColor = System.Drawing.Color.Gainsboro;
            stateProperties1.BorderRadius = 10;
            stateProperties1.BorderThickness = 1;
            stateProperties1.FillColor = System.Drawing.Color.Gainsboro;
            stateProperties1.IconLeftImage = null;
            stateProperties1.IconRightImage = ((System.Drawing.Image)(resources.GetObject("stateProperties1.IconRightImage")));
            this.btnLimpar.onHoverState = stateProperties1;
            this.btnLimpar.Size = new System.Drawing.Size(122, 35);
            this.btnLimpar.TabIndex = 6;
            this.btnLimpar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ToolTipForControls.SetToolTip(this.btnLimpar, "Ao pressionar esse botão, todos os campos do formulário serão limpados.\r\n");
            this.ToolTipForControls.SetToolTipIcon(this.btnLimpar, null);
            this.ToolTipForControls.SetToolTipTitle(this.btnLimpar, "");
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // btnSaveUpdate
            // 
            this.btnSaveUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveUpdate.BackColor = System.Drawing.Color.Transparent;
            this.btnSaveUpdate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSaveUpdate.BackgroundImage")));
            this.btnSaveUpdate.ButtonText = "Salvar";
            this.btnSaveUpdate.ButtonTextMarginLeft = 15;
            this.btnSaveUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSaveUpdate.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnSaveUpdate.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnSaveUpdate.DisabledForecolor = System.Drawing.Color.White;
            this.btnSaveUpdate.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold);
            this.btnSaveUpdate.ForeColor = System.Drawing.Color.White;
            this.btnSaveUpdate.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnSaveUpdate.IconPadding = 8;
            this.btnSaveUpdate.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.btnSaveUpdate.IdleBorderColor = System.Drawing.Color.Crimson;
            this.btnSaveUpdate.IdleBorderRadius = 10;
            this.btnSaveUpdate.IdleBorderThickness = 1;
            this.btnSaveUpdate.IdleFillColor = System.Drawing.Color.Crimson;
            this.btnSaveUpdate.IdleIconLeftImage = null;
            this.btnSaveUpdate.IdleIconRightImage = ((System.Drawing.Image)(resources.GetObject("btnSaveUpdate.IdleIconRightImage")));
            this.btnSaveUpdate.Location = new System.Drawing.Point(76, 227);
            this.btnSaveUpdate.Name = "btnSaveUpdate";
            stateProperties2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(20)))), ((int)(((byte)(58)))));
            stateProperties2.BorderRadius = 10;
            stateProperties2.BorderThickness = 1;
            stateProperties2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(20)))), ((int)(((byte)(58)))));
            stateProperties2.IconLeftImage = null;
            stateProperties2.IconRightImage = ((System.Drawing.Image)(resources.GetObject("stateProperties2.IconRightImage")));
            this.btnSaveUpdate.onHoverState = stateProperties2;
            this.btnSaveUpdate.Size = new System.Drawing.Size(125, 35);
            this.btnSaveUpdate.TabIndex = 5;
            this.btnSaveUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ToolTipForControls.SetToolTip(this.btnSaveUpdate, "Após preencher todos os campos acima pressione este botão para que a mágica acont" +
        "eça.");
            this.ToolTipForControls.SetToolTipIcon(this.btnSaveUpdate, null);
            this.ToolTipForControls.SetToolTipTitle(this.btnSaveUpdate, "");
            this.btnSaveUpdate.Click += new System.EventHandler(this.btnSaveUpdate_Click_1);
            // 
            // txtCategoria
            // 
            this.txtCategoria.AcceptsReturn = false;
            this.txtCategoria.AcceptsTab = false;
            this.txtCategoria.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtCategoria.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtCategoria.BackColor = System.Drawing.Color.Transparent;
            this.txtCategoria.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtCategoria.BackgroundImage")));
            this.txtCategoria.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtCategoria.BorderColorDisabled = System.Drawing.Color.Tomato;
            this.txtCategoria.BorderColorHover = System.Drawing.Color.Crimson;
            this.txtCategoria.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtCategoria.BorderRadius = 35;
            this.txtCategoria.BorderThickness = 1;
            this.txtCategoria.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtCategoria.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCategoria.DefaultText = "";
            this.txtCategoria.FillColor = System.Drawing.SystemColors.Window;
            this.txtCategoria.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtCategoria.HideSelection = true;
            this.txtCategoria.IconLeft = null;
            this.txtCategoria.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtCategoria.IconPadding = 6;
            this.txtCategoria.IconRight = null;
            this.txtCategoria.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.txtCategoria.Location = new System.Drawing.Point(23, 99);
            this.txtCategoria.MaxLength = 70;
            this.txtCategoria.MinimumSize = new System.Drawing.Size(100, 35);
            this.txtCategoria.Modified = false;
            this.txtCategoria.Name = "txtCategoria";
            this.txtCategoria.PasswordChar = '\0';
            this.txtCategoria.ReadOnly = false;
            this.txtCategoria.SelectedText = "";
            this.txtCategoria.SelectionLength = 0;
            this.txtCategoria.SelectionStart = 0;
            this.txtCategoria.ShortcutsEnabled = true;
            this.txtCategoria.Size = new System.Drawing.Size(352, 36);
            this.txtCategoria.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtCategoria.TabIndex = 2;
            this.txtCategoria.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCategoria.TextMarginLeft = 10;
            this.txtCategoria.TextPlaceholder = "Nome da Categoria";
            this.ToolTipForControls.SetToolTip(this.txtCategoria, "Digite o nome da categoria\r\n");
            this.ToolTipForControls.SetToolTipIcon(this.txtCategoria, null);
            this.ToolTipForControls.SetToolTipTitle(this.txtCategoria, "");
            this.txtCategoria.UseSystemPasswordChar = false;
            this.txtCategoria.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown_1);
            this.txtCategoria.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNome_KeyPress);
            // 
            // pnError
            // 
            this.pnError.Controls.Add(this.lblError);
            this.pnError.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnError.Location = new System.Drawing.Point(0, 0);
            this.pnError.Name = "pnError";
            this.pnError.Size = new System.Drawing.Size(407, 32);
            this.pnError.TabIndex = 190;
            this.ToolTipForControls.SetToolTip(this.pnError, "");
            this.ToolTipForControls.SetToolTipIcon(this.pnError, null);
            this.ToolTipForControls.SetToolTipTitle(this.pnError, "");
            this.pnError.Visible = false;
            // 
            // lblError
            // 
            this.lblError.BackColor = System.Drawing.Color.Tomato;
            this.lblError.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblError.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.White;
            this.lblError.Location = new System.Drawing.Point(0, 0);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(407, 32);
            this.lblError.TabIndex = 0;
            this.lblError.Text = "Nome";
            this.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ToolTipForControls.SetToolTip(this.lblError, "");
            this.ToolTipForControls.SetToolTipIcon(this.lblError, null);
            this.ToolTipForControls.SetToolTipTitle(this.lblError, "");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(34, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(199, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Informações da Categoria";
            this.ToolTipForControls.SetToolTip(this.label1, "");
            this.ToolTipForControls.SetToolTipIcon(this.label1, null);
            this.ToolTipForControls.SetToolTipTitle(this.label1, "");
            // 
            // TimerError
            // 
            this.TimerError.Interval = 2000;
            this.TimerError.Tick += new System.EventHandler(this.TimerError_Tick);
            // 
            // ToolTipForControls
            // 
            this.ToolTipForControls.Active = true;
            this.ToolTipForControls.AlignTextWithTitle = false;
            this.ToolTipForControls.AllowAutoClose = false;
            this.ToolTipForControls.AllowFading = true;
            this.ToolTipForControls.AutoCloseDuration = 5000;
            this.ToolTipForControls.BackColor = System.Drawing.SystemColors.Control;
            this.ToolTipForControls.BorderColor = System.Drawing.Color.Gainsboro;
            this.ToolTipForControls.ClickToShowDisplayControl = false;
            this.ToolTipForControls.ConvertNewlinesToBreakTags = true;
            this.ToolTipForControls.DisplayControl = null;
            this.ToolTipForControls.EntryAnimationSpeed = 350;
            this.ToolTipForControls.ExitAnimationSpeed = 200;
            this.ToolTipForControls.GenerateAutoCloseDuration = false;
            this.ToolTipForControls.IconMargin = 6;
            this.ToolTipForControls.InitialDelay = 0;
            this.ToolTipForControls.Name = "ToolTipForControls";
            this.ToolTipForControls.Opacity = 1D;
            this.ToolTipForControls.OverrideToolTipTitles = false;
            this.ToolTipForControls.Padding = new System.Windows.Forms.Padding(10);
            this.ToolTipForControls.ReshowDelay = 100;
            this.ToolTipForControls.ShowAlways = true;
            this.ToolTipForControls.ShowBorders = false;
            this.ToolTipForControls.ShowIcons = true;
            this.ToolTipForControls.ShowShadows = true;
            this.ToolTipForControls.Tag = null;
            this.ToolTipForControls.TextFont = new System.Drawing.Font("Segoe UI", 9F);
            this.ToolTipForControls.TextForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ToolTipForControls.TextMargin = 2;
            this.ToolTipForControls.TitleFont = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.ToolTipForControls.TitleForeColor = System.Drawing.Color.Black;
            this.ToolTipForControls.ToolTipPosition = new System.Drawing.Point(0, 0);
            this.ToolTipForControls.ToolTipTitle = null;
            // 
            // pnTop
            // 
            this.pnTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(182)))), ((int)(((byte)(78)))));
            this.pnTop.Controls.Add(this.cdsHearder);
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Size = new System.Drawing.Size(524, 55);
            this.pnTop.TabIndex = 0;
            this.ToolTipForControls.SetToolTip(this.pnTop, "");
            this.ToolTipForControls.SetToolTipIcon(this.pnTop, null);
            this.ToolTipForControls.SetToolTipTitle(this.pnTop, "");
            // 
            // cdsHearder
            // 
            this.cdsHearder.BackColor = System.Drawing.Color.Crimson;
            this.cdsHearder.BorderRadius = 5;
            this.cdsHearder.BottomSahddow = true;
            this.cdsHearder.color = System.Drawing.Color.White;
            this.cdsHearder.Controls.Add(this.lblTitle);
            this.cdsHearder.Controls.Add(this.btnClose);
            this.cdsHearder.LeftSahddow = false;
            this.cdsHearder.Location = new System.Drawing.Point(-8, -7);
            this.cdsHearder.Name = "cdsHearder";
            this.cdsHearder.RightSahddow = true;
            this.cdsHearder.ShadowDepth = 20;
            this.cdsHearder.Size = new System.Drawing.Size(486, 68);
            this.cdsHearder.TabIndex = 1;
            this.ToolTipForControls.SetToolTip(this.cdsHearder, "");
            this.ToolTipForControls.SetToolTipIcon(this.cdsHearder, null);
            this.ToolTipForControls.SetToolTipTitle(this.cdsHearder, "");
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.Crimson;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageActive = null;
            this.btnClose.Location = new System.Drawing.Point(405, 19);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(26, 26);
            this.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnClose.TabIndex = 177;
            this.btnClose.TabStop = false;
            this.ToolTipForControls.SetToolTip(this.btnClose, "Volte para página anterior");
            this.ToolTipForControls.SetToolTipIcon(this.btnClose, null);
            this.ToolTipForControls.SetToolTipTitle(this.btnClose, "");
            this.btnClose.Zoom = 10;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dclForm
            // 
            this.dclForm.Fixed = true;
            this.dclForm.Horizontal = true;
            this.dclForm.TargetControl = this.cdsHearder;
            this.dclForm.Vertical = true;
            // 
            // ElipseForm
            // 
            this.ElipseForm.ElipseRadius = 5;
            this.ElipseForm.TargetControl = this;
            // 
            // ElipseTextBox
            // 
            this.ElipseTextBox.ElipseRadius = 8;
            this.ElipseTextBox.TargetControl = this;
            // 
            // frmAddEditCategory
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(432, 355);
            this.Controls.Add(this.pnTop);
            this.Controls.Add(this.pnConteiner);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "frmAddEditCategory";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmAddEditAuthor";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmAddEditAuthor_KeyDown);
            this.pnConteiner.ResumeLayout(false);
            this.pnControls.ResumeLayout(false);
            this.pnControls.PerformLayout();
            this.pnError.ResumeLayout(false);
            this.pnTop.ResumeLayout(false);
            this.cdsHearder.ResumeLayout(false);
            this.cdsHearder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Panel pnConteiner;
        private System.Windows.Forms.Timer TimerError;
        private Bunifu.Framework.UI.BunifuImageButton btnClose;
        private Bunifu.UI.WinForms.BunifuToolTip ToolTipForControls;
        private Bunifu.Framework.UI.BunifuDragControl dclForm;
        private System.Windows.Forms.Panel pnTop;
        private Bunifu.Framework.UI.BunifuCards cdsHearder;
        private Bunifu.Framework.UI.BunifuElipse ElipseForm;
        private Bunifu.Framework.UI.BunifuElipse ElipseTextBox;
        private System.Windows.Forms.Panel pnControls;
        private BunifuTextBox txtDescricao;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnSeparator;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnLimpar;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnSaveUpdate;
        private BunifuTextBox txtCategoria;
        private System.Windows.Forms.Panel pnError;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.Label label1;
    }
}