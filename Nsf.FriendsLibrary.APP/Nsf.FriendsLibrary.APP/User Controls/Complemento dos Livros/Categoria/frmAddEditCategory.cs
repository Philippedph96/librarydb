﻿using Biblioteca.Utilitarios.ImagemPLugin;
using Biblioteca.Validacoes;
using Nsf.FriendsLibrary.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bunifu.UI.WinForms.BunifuTextbox;
using Nsf.FriendsLibrary.Tools.Validações;
using Bunifu.UI.WinForms;
using System.Threading;
using Biblioteca.Utilitarios.Notificacoes;
using Nsf.FriendsLibrary.Business.Complementos_dos_Livros.Categoria;
using Timer = System.Windows.Forms.Timer;
using Nsf.FriendsLibrary.Tools;

namespace Nsf.FriendsLibrary.APP.User_Controls.Complemento_dos_Livros.Autor
{
    public partial class frmAddEditCategory : Form
    {
        //Variável de escopo
        tb_categoria categoria = null;

        public frmAddEditCategory()
        {
            InitializeComponent();

            //Título já começa com Adicionar
            lblTitle.Text = "Adicone uma nova Categoria";
        }

        //Método que irá receber os valores passados pela gridview
        public void LoadScreen(tb_categoria dto)
        {
            //Altero de Salvar para alterar
            btnSaveUpdate.ButtonText = "Alterar";
            
            //Altero o título para (Novamente com o conceito de alterar e salvar)
            lblTitle.Text = "Altere as informações da Categoria";

            //Pego o valor passado por paramêtro e recebo na variável de escopo Categoria
            this.categoria = dto;

            //Recebo os valores encontrados no banco para os controles no formulário
            txtCategoria.Text = dto.nm_categoria;
            txtDescricao.Text = dto.ds_exemplo;
        }

        private void ShowError(string error)
        {
            //Crio um método anonimo para que outras thread tenham acesso a esse controle
            if (pnError.InvokeRequired)
                pnError.Invoke((MethodInvoker)delegate
                {
                     //O conteiner que está sobre o Label fica visível 
                     pnError.Visible = true;
                });

            //Para esse controle também kk
            if (lblError.InvokeRequired)
                lblError.Invoke((MethodInvoker)delegate
                {
                     //A label irá receber por parâmetro o valor do error
                     lblError.Text = error;
                });

            //E o controle Timer é ativado (Vale lembrar que o intervalo de aparição do Timer é de 2 segundos)
            TimerError.Start();
        }

        private void TimerError_Tick(object sender, EventArgs e)
        {
            //(Após os 2 segundos, o Timer é finalizado)
            TimerError.Stop();

            //E o panel fica invisível
            pnError.Visible = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            //Ao pressionar o X (inho) a tela é fechada
            this.Close();
        }

        //Esse método tem como função carregar os respectivos controles aos valores do banco de dados
        private tb_categoria Controles(tb_categoria controls)
        {
            controls.nm_categoria = txtCategoria.Text;
            controls.ds_exemplo = txtDescricao.Text;

            return controls;
        }

        //Caso esteja na condição de Salvar
        private tb_categoria CarregarControlesSave()
        {
            //Instancio o  objeto 
            categoria = new tb_categoria();

            //Chamo o evento controle com uma variável de retorno
            var retorno = Controles(categoria);

            //retorno os valores
            return retorno;
        }

        private tb_categoria CarregarControlesUpdate()
        {
            //Utilizo o Using para não dar error de Multiple Entities
            using (var context = new LibraryEntities())
            {
                //Faço uma procura no banco de dados a fim de saber ONDE ou em que valor o usuário está querendo alterar
                categoria = context.tb_categoria.Where(x => x.id_categoria == categoria.id_categoria).FirstOrDefault();

                //Uma vez encontrado, chamo o evento Controles para que se possa ser preenchido os campos
                var retorno = Controles(categoria);

                //retorno os valores
                return retorno;
            }
        }

        //Variável do tipo Image de escopo (Para não ficar criando uma variável com o mesmo papel várias vezes!)
        Image imagem;
        string text;

        //Esse botão tem a funcionabilidade de servir tanto para Salvar quando para Alterar, dependendo do critério utilizado
        private async void btnSaveUpdate_Click_1(object sender, EventArgs e)
        {
            await Task.Run(async () =>
            {
                //Evita buggs
                try
                {
                    //O critério utilizado para utilizar o Salvar está baseado no valor encontrado na lblTitle
                    if (lblTitle.Text == "Adicone uma nova Categoria")
                    {
                        //Retorno os valores carregados no método CarregarControlesSave
                        var save = CarregarControlesSave();

                        //Instacio o objeto Business (Regra de validação)
                        CategoriaBusiness db = new CategoriaBusiness();

                        //Chamando o método Save, passo o valor encontrado no váriavel Save por parâmetro
                        db.Save(save);

                        await Task.Factory.StartNew(() =>
                        {
                            //Se tudo der certo... Exibo uma imagem para o usuário (Pego a imagem armazenado no Resources do projeto).
                            imagem = Properties.Resources.ThumpUP;
                            text = "Nova Categoria registrada na base de dados com êxito!";
                            this.SelectorClass(1, text, imagem);
                        });
                    }
                    else
                    {
                        //Caso o If Retorno uma mudança na Label, significa que estamos na parte de UPDATE
                        //Então nesse caso, retorna-se os valores carregados no método CarregarControlesUpdate
                        var update = CarregarControlesUpdate();

                        //Instacio o objeto Business
                        CategoriaBusiness db = new CategoriaBusiness();

                        //Chamando o método Update, passo o valor encontrado no váriavel Update por parâmetro
                        db.Update(update);

                        await Task.Factory.StartNew(() =>
                        {
                            //Se tudo der certo... Exibo uma imagem para o usuário (Pego a imagem armazenado no Resources do projeto).
                            imagem = Properties.Resources.Warning;
                            text = "As informações da Categoria foram alteradas na base de dados com êxito!!";
                            this.SelectorClass(2, text, imagem);
                        });
                    }
                }
                //Caso ocorra algum erro...
                catch (ArgumentException ex)
                {
                    //Chama-se o método ShowError que já foi explicado acima!
                    ShowError(ex.Message);
                }
            });
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Faz com que o controle só aceite Letras (BackSpace, space, delete...)
            ValidarTextbox.SoLetra(e);
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            LimparControles();
        }

        private void LimparControles()
        {
            txtDescricao.Clear();
            txtCategoria.Clear();
        }

        //Implementação da opção de apertar o Enter
        private void txtNome_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnSaveUpdate_Click_1(null, null);
        }

        private void frmAddEditAuthor_KeyDown(object sender, KeyEventArgs e)
        {
            //Posição do ESQ
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

    }
}
