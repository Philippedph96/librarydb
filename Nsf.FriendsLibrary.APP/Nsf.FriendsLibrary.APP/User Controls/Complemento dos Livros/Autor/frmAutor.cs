﻿using Biblioteca.Utilitarios.Notificacoes;
using Biblioteca.Validacoes;
using Nsf.FriendsLibrary.Business.Complementos_dos_Livros.Autor;
using Nsf.FriendsLibrary.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf.FriendsLibrary.APP.User_Controls.Complemento_dos_Livros.Autor
{
    public partial class frmAutor : Form
    {
        public frmAutor()
        {
            InitializeComponent();

            //O formulário já abre com as informações no banco de dados
            LoadGridView();
        }

        //Esse método serve para que os dados que estão no banco de dados vão para o controle DataGridView
        private async void LoadGridView()
        {
            try
            {
                //Instacio o objeto
                AutorBusiness db = new AutorBusiness();

                //Chamo o método List que retorno todos os valores no banco de dados
                List<tb_autor> list = db.List();

                //Crio uma nova Thread para executar em segundo plano o método que irá configurar a griedview
                await Task.Run(() => ConfigGrid(dgvAutor, list));
            }
            catch (Exception exception)
            {
                MessageBox.Show("Error: " + exception.Message, "Friends Library", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Bem semelhante ao método anterior (Para não dizer igual) esse método retorna valores a partir do valor encontrado como paramêtro
        private async void LoadGridViewWithFilter(string filtro)
        {
            try
            {
                //Instancio o  objeto
                AutorBusiness db = new AutorBusiness();

                //Chamo o método Filter passando o valor digitado pelo usuário na caixa de texto, e o método retorno valores aproximados
                List<tb_autor> list = db.Filter(filtro);

                //Crio uma nova Thread para executar em segundo plano o método que irá configurar a griedview
                await Task.Run(() => ConfigGrid(dgvAutor, list));
            }
            catch (Exception exception)
            {
                MessageBox.Show("Error: " + exception.Message, "Friends Library", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Esse método configura algumas propriedades básicas do controle (É mais para não ficar repetindo o código)
        private void ConfigGrid(DataGridView dgv, List<tb_autor> valor)
        {
            //Como esse método está sendo chamado a partir de uma Thread separada da principal, faz-se necessário a utilização...
            //De um método anônimo para se conseguir ter acesso a thread principal
            if (dgv.InvokeRequired)
            {
                dgv.Invoke((MethodInvoker)delegate
                {
                    //Para não gerar colunas automagicamente
                    dgv.AutoGenerateColumns = false;

                    //Passo o valor encontrado no Filtro
                    dgv.DataSource = valor;
                });
            }

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            //Ao precionar o botão com um X(zinho), a tela é fechada
            this.Close();
        }

        private async void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                //Novamente, criando uma nova thread em segundo plano para executar o código abaixo (Para não sobrecarregar a thread principal
                await Task.Run(() =>
                {
                    //Instancio o formulário recenbo em uma variável que ao ser compilada interpreta e se torno o valor do retorno
                    frmAddEditAuthor tela = new frmAddEditAuthor();

                    //Abro uma faixa de diálogo com o formulário
                    tela.ShowDialog();

                    //Após os valores serem digitados, chamo o método para que o valor seja atualizado na grid
                    LoadGridView();
                });
            }
            catch (Exception exception)
            {
                MessageBox.Show("Error: " + exception.Message, "Friends Library", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private async void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                //É um conceito novo para mim ainda, mas novamente estamos utilizando uma thread que será executada em segundo plano..
                //Chamando o método para remover os dados encontrados na gridview
                await Task.Run(() => RemoveItens());

                //Criação de uma nova thread para ser executada em segundo plano de uma outra thread que está sendo executada kkk
                await Task.Factory.StartNew(() =>
                {
                //Cria uma variável de texto com a seguinte informação
                string text = "As informações do Autor foram removidas com êxito!!";

                //Pego a imagem da pasta resources
                Image imagem = Properties.Resources.Delete;

                //Passo para a classe que irá gerar uma notificação
                this.SelectorClass(3, text, imagem);
                });
            }
            catch (ArgumentException business)
            {
                MessageBox.Show(business.Message,
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro" + ex.Message + " Tente mais tarde.",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private void RemoveItens()
        {
            //Pego o valor da gridview
            var autor = PegarInformacaoGrid();

            //Instancio a classe
            AutorBusiness db = new AutorBusiness();

            //Passo o valor do ID
            db.Remove(autor.id_autor);

            //Recarrego a gridview
            LoadGridView();
        }

        //Esse método serve para pegar os dados selecionados da gridview (Para não ter redundância no código)
        private tb_autor PegarInformacaoGrid()
        {
            //Pego todo o valor encontrado em uma determinada linha da gried bem como todos os dados referente a mesma
            tb_autor autor = dgvAutor.CurrentRow.DataBoundItem as tb_autor;

            //Retorno o valor para ser utilizado nos métodos
            return autor;
        }

        private async void btnEditar_Click(object sender, EventArgs e)
        {
            //Uma vez mais, crio uma nova thread para executar em segundo plano o código abaixo
            await Task.Run(() =>
            {
                EditItens();
            });
        }

        private void EditItens()
        {
            //Pego as informações do retorno do método PegarInformacaoGrid
            var autor = PegarInformacaoGrid();

            //Instancio o objeto 
            var tela = new frmAddEditAuthor();

            //Chamo o método LoadScreen passando como paramêtro os valores retornados da grid
            tela.LoadScreen(autor);

            //Abro dialogo com esse outro formulário
            tela.ShowDialog();

            //E após o formulário ser fechado, carrego os dados salvos
            LoadGridView();
        }

        //Esse evento verifica os dados que estão sendo digitados em determinado controle
        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Recebo o valor digitado na BunifuTextbox
            var filtro = txtSearch.Text;

            //Esse método irá verificar se o que o usuário está digitando são letras, senão for o controle não aceita
            ValidarTextbox.SoLetra(e);

            //Chamo e passo o valor da varíavel criada acima por paramétro
            LoadGridViewWithFilter(filtro);
        }

        private void frmAutor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }
    }
}
