﻿namespace Nsf.FriendsLibrary.APP.User_Controls.Complemento_dos_Livros.Autor
{
    partial class frmAutor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAutor));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cdsHearder = new Bunifu.Framework.UI.BunifuCards();
            this.btnClose = new Bunifu.Framework.UI.BunifuImageButton();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnSeparator = new System.Windows.Forms.Panel();
            this.btnEditar = new Bunifu.Framework.UI.BunifuImageButton();
            this.dgvAutor = new Bunifu.UI.WinForms.BunifuDataGridView();
            this.idautorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nmautorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsnascimentoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsescritaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbautorBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label2 = new System.Windows.Forms.Label();
            this.btnPrint = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnExcel = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnRemove = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnAdd = new Bunifu.Framework.UI.BunifuImageButton();
            this.txtSearch = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.ElipseForms = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.dclForms = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.ToolTipForButtons = new Bunifu.UI.WinForms.BunifuToolTip(this.components);
            this.cdsHearder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAutor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbautorBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAdd)).BeginInit();
            this.SuspendLayout();
            // 
            // cdsHearder
            // 
            this.cdsHearder.BackColor = System.Drawing.Color.White;
            this.cdsHearder.BorderRadius = 5;
            this.cdsHearder.BottomSahddow = true;
            this.cdsHearder.color = System.Drawing.Color.White;
            this.cdsHearder.Controls.Add(this.btnClose);
            this.cdsHearder.Controls.Add(this.label1);
            this.cdsHearder.LeftSahddow = false;
            this.cdsHearder.Location = new System.Drawing.Point(-7, -4);
            this.cdsHearder.Name = "cdsHearder";
            this.cdsHearder.RightSahddow = true;
            this.cdsHearder.ShadowDepth = 20;
            this.cdsHearder.Size = new System.Drawing.Size(686, 59);
            this.cdsHearder.TabIndex = 0;
            this.ToolTipForButtons.SetToolTip(this.cdsHearder, "");
            this.ToolTipForButtons.SetToolTipIcon(this.cdsHearder, null);
            this.ToolTipForButtons.SetToolTipTitle(this.cdsHearder, "");
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.White;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageActive = null;
            this.btnClose.Location = new System.Drawing.Point(639, 15);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(26, 26);
            this.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnClose.TabIndex = 2;
            this.btnClose.TabStop = false;
            this.ToolTipForButtons.SetToolTip(this.btnClose, "Feche a tela");
            this.ToolTipForButtons.SetToolTipIcon(this.btnClose, null);
            this.ToolTipForButtons.SetToolTipTitle(this.btnClose, "");
            this.btnClose.Zoom = 10;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(20, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(257, 28);
            this.label1.TabIndex = 1;
            this.label1.Text = "Gerenciamento de Autores";
            this.ToolTipForButtons.SetToolTip(this.label1, "");
            this.ToolTipForButtons.SetToolTipIcon(this.label1, null);
            this.ToolTipForButtons.SetToolTipTitle(this.label1, "");
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.pnSeparator);
            this.panel1.Controls.Add(this.btnEditar);
            this.panel1.Controls.Add(this.dgvAutor);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btnPrint);
            this.panel1.Controls.Add(this.btnExcel);
            this.panel1.Controls.Add(this.btnRemove);
            this.panel1.Controls.Add(this.btnAdd);
            this.panel1.Controls.Add(this.txtSearch);
            this.panel1.Location = new System.Drawing.Point(13, 65);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(645, 421);
            this.panel1.TabIndex = 1;
            this.ToolTipForButtons.SetToolTip(this.panel1, "");
            this.ToolTipForButtons.SetToolTipIcon(this.panel1, null);
            this.ToolTipForButtons.SetToolTipTitle(this.panel1, "");
            // 
            // pnSeparator
            // 
            this.pnSeparator.BackColor = System.Drawing.Color.Gainsboro;
            this.pnSeparator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnSeparator.Location = new System.Drawing.Point(17, 46);
            this.pnSeparator.Name = "pnSeparator";
            this.pnSeparator.Size = new System.Drawing.Size(609, 1);
            this.pnSeparator.TabIndex = 193;
            this.ToolTipForButtons.SetToolTip(this.pnSeparator, "");
            this.ToolTipForButtons.SetToolTipIcon(this.pnSeparator, null);
            this.ToolTipForButtons.SetToolTipTitle(this.pnSeparator, "");
            // 
            // btnEditar
            // 
            this.btnEditar.BackColor = System.Drawing.Color.White;
            this.btnEditar.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.Image")));
            this.btnEditar.ImageActive = null;
            this.btnEditar.Location = new System.Drawing.Point(353, 66);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(26, 26);
            this.btnEditar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnEditar.TabIndex = 4;
            this.btnEditar.TabStop = false;
            this.ToolTipForButtons.SetToolTip(this.btnEditar, resources.GetString("btnEditar.ToolTip"));
            this.ToolTipForButtons.SetToolTipIcon(this.btnEditar, null);
            this.ToolTipForButtons.SetToolTipTitle(this.btnEditar, "Edite as informações dos Autores");
            this.btnEditar.Zoom = 10;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // dgvAutor
            // 
            this.dgvAutor.AllowCustomTheming = false;
            this.dgvAutor.AllowUserToAddRows = false;
            this.dgvAutor.AllowUserToDeleteRows = false;
            this.dgvAutor.AllowUserToResizeColumns = false;
            this.dgvAutor.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.dgvAutor.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAutor.AutoGenerateColumns = false;
            this.dgvAutor.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAutor.BackgroundColor = System.Drawing.Color.White;
            this.dgvAutor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvAutor.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvAutor.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAutor.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvAutor.ColumnHeadersHeight = 40;
            this.dgvAutor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idautorDataGridViewTextBoxColumn,
            this.nmautorDataGridViewTextBoxColumn,
            this.dsnascimentoDataGridViewTextBoxColumn,
            this.dsescritaDataGridViewTextBoxColumn});
            this.dgvAutor.CurrentTheme.AlternatingRowsStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.dgvAutor.CurrentTheme.AlternatingRowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.dgvAutor.CurrentTheme.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Black;
            this.dgvAutor.CurrentTheme.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.dgvAutor.CurrentTheme.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvAutor.CurrentTheme.BackColor = System.Drawing.Color.White;
            this.dgvAutor.CurrentTheme.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            this.dgvAutor.CurrentTheme.HeaderStyle.BackColor = System.Drawing.Color.DodgerBlue;
            this.dgvAutor.CurrentTheme.HeaderStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            this.dgvAutor.CurrentTheme.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgvAutor.CurrentTheme.Name = null;
            this.dgvAutor.CurrentTheme.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgvAutor.CurrentTheme.RowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.dgvAutor.CurrentTheme.RowsStyle.ForeColor = System.Drawing.Color.Black;
            this.dgvAutor.CurrentTheme.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.dgvAutor.CurrentTheme.RowsStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvAutor.DataSource = this.tbautorBindingSource;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAutor.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvAutor.EnableHeadersVisualStyles = false;
            this.dgvAutor.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            this.dgvAutor.HeaderBackColor = System.Drawing.Color.DodgerBlue;
            this.dgvAutor.HeaderBgColor = System.Drawing.Color.Empty;
            this.dgvAutor.HeaderForeColor = System.Drawing.Color.White;
            this.dgvAutor.Location = new System.Drawing.Point(17, 112);
            this.dgvAutor.MultiSelect = false;
            this.dgvAutor.Name = "dgvAutor";
            this.dgvAutor.ReadOnly = true;
            this.dgvAutor.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvAutor.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvAutor.RowHeadersVisible = false;
            this.dgvAutor.RowHeadersWidth = 35;
            this.dgvAutor.RowTemplate.DividerHeight = 1;
            this.dgvAutor.RowTemplate.Height = 40;
            this.dgvAutor.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvAutor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAutor.Size = new System.Drawing.Size(610, 308);
            this.dgvAutor.TabIndex = 0;
            this.dgvAutor.Theme = Bunifu.UI.WinForms.BunifuDataGridView.PresetThemes.Light;
            this.ToolTipForButtons.SetToolTip(this.dgvAutor, "");
            this.ToolTipForButtons.SetToolTipIcon(this.dgvAutor, null);
            this.ToolTipForButtons.SetToolTipTitle(this.dgvAutor, "");
            // 
            // idautorDataGridViewTextBoxColumn
            // 
            this.idautorDataGridViewTextBoxColumn.DataPropertyName = "id_autor";
            this.idautorDataGridViewTextBoxColumn.HeaderText = "id_autor";
            this.idautorDataGridViewTextBoxColumn.Name = "idautorDataGridViewTextBoxColumn";
            this.idautorDataGridViewTextBoxColumn.ReadOnly = true;
            this.idautorDataGridViewTextBoxColumn.Visible = false;
            // 
            // nmautorDataGridViewTextBoxColumn
            // 
            this.nmautorDataGridViewTextBoxColumn.DataPropertyName = "nm_autor";
            this.nmautorDataGridViewTextBoxColumn.HeaderText = "Nome";
            this.nmautorDataGridViewTextBoxColumn.Name = "nmautorDataGridViewTextBoxColumn";
            this.nmautorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dsnascimentoDataGridViewTextBoxColumn
            // 
            this.dsnascimentoDataGridViewTextBoxColumn.DataPropertyName = "ds_nascimento";
            this.dsnascimentoDataGridViewTextBoxColumn.HeaderText = "Nascimento";
            this.dsnascimentoDataGridViewTextBoxColumn.Name = "dsnascimentoDataGridViewTextBoxColumn";
            this.dsnascimentoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dsescritaDataGridViewTextBoxColumn
            // 
            this.dsescritaDataGridViewTextBoxColumn.DataPropertyName = "ds_escrita";
            this.dsescritaDataGridViewTextBoxColumn.HeaderText = "Tipo Escrita";
            this.dsescritaDataGridViewTextBoxColumn.Name = "dsescritaDataGridViewTextBoxColumn";
            this.dsescritaDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tbautorBindingSource
            // 
            this.tbautorBindingSource.DataSource = typeof(Nsf.FriendsLibrary.DB.tb_autor);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.Gray;
            this.label2.Location = new System.Drawing.Point(13, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(139, 21);
            this.label2.TabIndex = 2;
            this.label2.Text = "Todos os Autores";
            this.ToolTipForButtons.SetToolTip(this.label2, "");
            this.ToolTipForButtons.SetToolTipIcon(this.label2, null);
            this.ToolTipForButtons.SetToolTipTitle(this.label2, "");
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.Color.White;
            this.btnPrint.Image = ((System.Drawing.Image)(resources.GetObject("btnPrint.Image")));
            this.btnPrint.ImageActive = null;
            this.btnPrint.Location = new System.Drawing.Point(600, 65);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(26, 26);
            this.btnPrint.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnPrint.TabIndex = 4;
            this.btnPrint.TabStop = false;
            this.ToolTipForButtons.SetToolTip(this.btnPrint, "Pressionando esse botão, você irá imprimir as informações que constam nesse formu" +
        "lário\r\n");
            this.ToolTipForButtons.SetToolTipIcon(this.btnPrint, null);
            this.ToolTipForButtons.SetToolTipTitle(this.btnPrint, "Imprima as informações");
            this.btnPrint.Visible = false;
            this.btnPrint.Zoom = 10;
            // 
            // btnExcel
            // 
            this.btnExcel.BackColor = System.Drawing.Color.White;
            this.btnExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnExcel.Image")));
            this.btnExcel.ImageActive = null;
            this.btnExcel.Location = new System.Drawing.Point(568, 65);
            this.btnExcel.Name = "btnExcel";
            this.btnExcel.Size = new System.Drawing.Size(26, 26);
            this.btnExcel.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnExcel.TabIndex = 4;
            this.btnExcel.TabStop = false;
            this.ToolTipForButtons.SetToolTip(this.btnExcel, "Pressionando esse botão, você será redirecionado para o Microsoft Excel");
            this.ToolTipForButtons.SetToolTipIcon(this.btnExcel, null);
            this.ToolTipForButtons.SetToolTipTitle(this.btnExcel, "Exporte para Excel");
            this.btnExcel.Visible = false;
            this.btnExcel.Zoom = 10;
            // 
            // btnRemove
            // 
            this.btnRemove.BackColor = System.Drawing.Color.White;
            this.btnRemove.Image = ((System.Drawing.Image)(resources.GetObject("btnRemove.Image")));
            this.btnRemove.ImageActive = null;
            this.btnRemove.Location = new System.Drawing.Point(385, 66);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(26, 26);
            this.btnRemove.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnRemove.TabIndex = 4;
            this.btnRemove.TabStop = false;
            this.ToolTipForButtons.SetToolTip(this.btnRemove, "Pressione esse botão para excluir as informações dos Autores");
            this.ToolTipForButtons.SetToolTipIcon(this.btnRemove, null);
            this.ToolTipForButtons.SetToolTipTitle(this.btnRemove, "Remova um Autor\r\n");
            this.btnRemove.Zoom = 10;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.White;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageActive = null;
            this.btnAdd.Location = new System.Drawing.Point(321, 66);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(26, 26);
            this.btnAdd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnAdd.TabIndex = 4;
            this.btnAdd.TabStop = false;
            this.ToolTipForButtons.SetToolTip(this.btnAdd, "Pressione esse botão para cadastrar um novo Autor\r\n");
            this.ToolTipForButtons.SetToolTipIcon(this.btnAdd, null);
            this.ToolTipForButtons.SetToolTipTitle(this.btnAdd, "Adicione novos Autores");
            this.btnAdd.Zoom = 10;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.AcceptsReturn = false;
            this.txtSearch.AcceptsTab = false;
            this.txtSearch.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtSearch.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtSearch.BackColor = System.Drawing.Color.Transparent;
            this.txtSearch.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtSearch.BackgroundImage")));
            this.txtSearch.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtSearch.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtSearch.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.txtSearch.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtSearch.BorderRadius = 35;
            this.txtSearch.BorderThickness = 1;
            this.txtSearch.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtSearch.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.DefaultText = "";
            this.txtSearch.FillColor = System.Drawing.Color.White;
            this.txtSearch.HideSelection = true;
            this.txtSearch.IconLeft = null;
            this.txtSearch.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtSearch.IconPadding = 8;
            this.txtSearch.IconRight = ((System.Drawing.Image)(resources.GetObject("txtSearch.IconRight")));
            this.txtSearch.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtSearch.Location = new System.Drawing.Point(16, 61);
            this.txtSearch.MaxLength = 70;
            this.txtSearch.MinimumSize = new System.Drawing.Size(100, 35);
            this.txtSearch.Modified = false;
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.PasswordChar = '\0';
            this.txtSearch.ReadOnly = false;
            this.txtSearch.SelectedText = "";
            this.txtSearch.SelectionLength = 0;
            this.txtSearch.SelectionStart = 0;
            this.txtSearch.ShortcutsEnabled = true;
            this.txtSearch.Size = new System.Drawing.Size(299, 35);
            this.txtSearch.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtSearch.TabIndex = 1;
            this.txtSearch.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtSearch.TextMarginLeft = 10;
            this.txtSearch.TextPlaceholder = "Pesquise pelos autores";
            this.ToolTipForButtons.SetToolTip(this.txtSearch, "");
            this.ToolTipForButtons.SetToolTipIcon(this.txtSearch, null);
            this.ToolTipForButtons.SetToolTipTitle(this.txtSearch, "");
            this.txtSearch.UseSystemPasswordChar = false;
            this.txtSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSearch_KeyPress);
            // 
            // ElipseForms
            // 
            this.ElipseForms.ElipseRadius = 15;
            this.ElipseForms.TargetControl = this;
            // 
            // dclForms
            // 
            this.dclForms.Fixed = true;
            this.dclForms.Horizontal = true;
            this.dclForms.TargetControl = this.cdsHearder;
            this.dclForms.Vertical = true;
            // 
            // ToolTipForButtons
            // 
            this.ToolTipForButtons.Active = true;
            this.ToolTipForButtons.AlignTextWithTitle = false;
            this.ToolTipForButtons.AllowAutoClose = false;
            this.ToolTipForButtons.AllowFading = true;
            this.ToolTipForButtons.AutoCloseDuration = 5000;
            this.ToolTipForButtons.BackColor = System.Drawing.SystemColors.Control;
            this.ToolTipForButtons.BorderColor = System.Drawing.Color.Gainsboro;
            this.ToolTipForButtons.ClickToShowDisplayControl = false;
            this.ToolTipForButtons.ConvertNewlinesToBreakTags = true;
            this.ToolTipForButtons.DisplayControl = null;
            this.ToolTipForButtons.EntryAnimationSpeed = 350;
            this.ToolTipForButtons.ExitAnimationSpeed = 200;
            this.ToolTipForButtons.GenerateAutoCloseDuration = false;
            this.ToolTipForButtons.IconMargin = 6;
            this.ToolTipForButtons.InitialDelay = 0;
            this.ToolTipForButtons.Name = "ToolTipForButtons";
            this.ToolTipForButtons.Opacity = 1D;
            this.ToolTipForButtons.OverrideToolTipTitles = false;
            this.ToolTipForButtons.Padding = new System.Windows.Forms.Padding(10);
            this.ToolTipForButtons.ReshowDelay = 100;
            this.ToolTipForButtons.ShowAlways = true;
            this.ToolTipForButtons.ShowBorders = false;
            this.ToolTipForButtons.ShowIcons = true;
            this.ToolTipForButtons.ShowShadows = true;
            this.ToolTipForButtons.Tag = null;
            this.ToolTipForButtons.TextFont = new System.Drawing.Font("Segoe UI", 9F);
            this.ToolTipForButtons.TextForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ToolTipForButtons.TextMargin = 2;
            this.ToolTipForButtons.TitleFont = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.ToolTipForButtons.TitleForeColor = System.Drawing.Color.Black;
            this.ToolTipForButtons.ToolTipPosition = new System.Drawing.Point(0, 0);
            this.ToolTipForButtons.ToolTipTitle = null;
            // 
            // frmAutor
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(670, 499);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.cdsHearder);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "frmAutor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmAutor";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmAutor_KeyDown);
            this.cdsHearder.ResumeLayout(false);
            this.cdsHearder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAutor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbautorBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExcel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAdd)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuCards cdsHearder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuImageButton btnClose;
        private Bunifu.Framework.UI.BunifuElipse ElipseForms;
        private Bunifu.Framework.UI.BunifuDragControl dclForms;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtSearch;
        private Bunifu.Framework.UI.BunifuImageButton btnPrint;
        private Bunifu.Framework.UI.BunifuImageButton btnExcel;
        private Bunifu.Framework.UI.BunifuImageButton btnRemove;
        private Bunifu.Framework.UI.BunifuImageButton btnAdd;
        private System.Windows.Forms.Label label2;
        private Bunifu.UI.WinForms.BunifuToolTip ToolTipForButtons;
        private Bunifu.UI.WinForms.BunifuDataGridView dgvAutor;
        private System.Windows.Forms.BindingSource tbautorBindingSource;
        private Bunifu.Framework.UI.BunifuImageButton btnEditar;
        private System.Windows.Forms.DataGridViewTextBoxColumn idautorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nmautorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsnascimentoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsescritaDataGridViewTextBoxColumn;
        private System.Windows.Forms.Panel pnSeparator;
    }
}