﻿namespace Nsf.FriendsLibrary.APP.User_Controls.Complemento_dos_Livros.Autor
{
    partial class frmAddEditAuthor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAddEditAuthor));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties3 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            this.lblTitle = new System.Windows.Forms.Label();
            this.pnConteiner = new System.Windows.Forms.Panel();
            this.pnControls = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnSeparator = new System.Windows.Forms.Panel();
            this.imgAutor = new Bunifu.UI.WinForms.BunifuPictureBox();
            this.btnLimpar = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.btnSaveUpdate = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.btnProcurar = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.txtNome = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtEstilo = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtNascimento = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.pnError = new System.Windows.Forms.Panel();
            this.lblError = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TimerError = new System.Windows.Forms.Timer(this.components);
            this.ToolTipForControls = new Bunifu.UI.WinForms.BunifuToolTip(this.components);
            this.pnTop = new System.Windows.Forms.Panel();
            this.cdsHearder = new Bunifu.Framework.UI.BunifuCards();
            this.btnClose = new Bunifu.Framework.UI.BunifuImageButton();
            this.dclForm = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.ElipseForm = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.imageListIconValidator = new System.Windows.Forms.ImageList(this.components);
            this.pnConteiner.SuspendLayout();
            this.pnControls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgAutor)).BeginInit();
            this.pnError.SuspendLayout();
            this.pnTop.SuspendLayout();
            this.cdsHearder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold);
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(36, 16);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(70, 30);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "{title} ";
            this.ToolTipForControls.SetToolTip(this.lblTitle, "");
            this.ToolTipForControls.SetToolTipIcon(this.lblTitle, null);
            this.ToolTipForControls.SetToolTipTitle(this.lblTitle, "");
            // 
            // pnConteiner
            // 
            this.pnConteiner.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnConteiner.Controls.Add(this.pnControls);
            this.pnConteiner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnConteiner.ForeColor = System.Drawing.Color.White;
            this.pnConteiner.Location = new System.Drawing.Point(0, 55);
            this.pnConteiner.Name = "pnConteiner";
            this.pnConteiner.Size = new System.Drawing.Size(525, 365);
            this.pnConteiner.TabIndex = 1;
            this.ToolTipForControls.SetToolTip(this.pnConteiner, "");
            this.ToolTipForControls.SetToolTipIcon(this.pnConteiner, null);
            this.ToolTipForControls.SetToolTipTitle(this.pnConteiner, "");
            // 
            // pnControls
            // 
            this.pnControls.BackColor = System.Drawing.Color.White;
            this.pnControls.Controls.Add(this.panel1);
            this.pnControls.Controls.Add(this.pnSeparator);
            this.pnControls.Controls.Add(this.imgAutor);
            this.pnControls.Controls.Add(this.btnLimpar);
            this.pnControls.Controls.Add(this.btnSaveUpdate);
            this.pnControls.Controls.Add(this.btnProcurar);
            this.pnControls.Controls.Add(this.txtNome);
            this.pnControls.Controls.Add(this.txtEstilo);
            this.pnControls.Controls.Add(this.txtNascimento);
            this.pnControls.Controls.Add(this.pnError);
            this.pnControls.Controls.Add(this.label1);
            this.pnControls.Location = new System.Drawing.Point(16, 12);
            this.pnControls.Name = "pnControls";
            this.pnControls.Size = new System.Drawing.Size(492, 340);
            this.pnControls.TabIndex = 181;
            this.ToolTipForControls.SetToolTip(this.pnControls, "");
            this.ToolTipForControls.SetToolTipIcon(this.pnControls, null);
            this.ToolTipForControls.SetToolTipTitle(this.pnControls, "");
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Location = new System.Drawing.Point(17, 265);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(465, 1);
            this.panel1.TabIndex = 192;
            this.ToolTipForControls.SetToolTip(this.panel1, "");
            this.ToolTipForControls.SetToolTipIcon(this.panel1, null);
            this.ToolTipForControls.SetToolTipTitle(this.panel1, "");
            // 
            // pnSeparator
            // 
            this.pnSeparator.BackColor = System.Drawing.Color.Gainsboro;
            this.pnSeparator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnSeparator.Location = new System.Drawing.Point(17, 81);
            this.pnSeparator.Name = "pnSeparator";
            this.pnSeparator.Size = new System.Drawing.Size(465, 1);
            this.pnSeparator.TabIndex = 192;
            this.ToolTipForControls.SetToolTip(this.pnSeparator, "");
            this.ToolTipForControls.SetToolTipIcon(this.pnSeparator, null);
            this.ToolTipForControls.SetToolTipTitle(this.pnSeparator, "");
            // 
            // imgAutor
            // 
            this.imgAutor.AllowFocused = false;
            this.imgAutor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.imgAutor.BackColor = System.Drawing.Color.Gainsboro;
            this.imgAutor.BorderRadius = 50;
            this.imgAutor.Image = ((System.Drawing.Image)(resources.GetObject("imgAutor.Image")));
            this.imgAutor.IsCircle = true;
            this.imgAutor.Location = new System.Drawing.Point(373, 104);
            this.imgAutor.Name = "imgAutor";
            this.imgAutor.Size = new System.Drawing.Size(100, 100);
            this.imgAutor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAutor.TabIndex = 191;
            this.imgAutor.TabStop = false;
            this.ToolTipForControls.SetToolTip(this.imgAutor, "");
            this.ToolTipForControls.SetToolTipIcon(this.imgAutor, null);
            this.ToolTipForControls.SetToolTipTitle(this.imgAutor, "Imagem Padrão");
            this.imgAutor.Type = Bunifu.UI.WinForms.BunifuPictureBox.Types.Square;
            // 
            // btnLimpar
            // 
            this.btnLimpar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLimpar.BackColor = System.Drawing.Color.Transparent;
            this.btnLimpar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLimpar.BackgroundImage")));
            this.btnLimpar.ButtonText = "Limpar";
            this.btnLimpar.ButtonTextMarginLeft = 15;
            this.btnLimpar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLimpar.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnLimpar.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnLimpar.DisabledForecolor = System.Drawing.Color.White;
            this.btnLimpar.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold);
            this.btnLimpar.ForeColor = System.Drawing.Color.White;
            this.btnLimpar.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnLimpar.IconPadding = 8;
            this.btnLimpar.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.btnLimpar.IdleBorderColor = System.Drawing.Color.Gray;
            this.btnLimpar.IdleBorderRadius = 10;
            this.btnLimpar.IdleBorderThickness = 1;
            this.btnLimpar.IdleFillColor = System.Drawing.Color.Gray;
            this.btnLimpar.IdleIconLeftImage = null;
            this.btnLimpar.IdleIconRightImage = ((System.Drawing.Image)(resources.GetObject("btnLimpar.IdleIconRightImage")));
            this.btnLimpar.Location = new System.Drawing.Point(246, 285);
            this.btnLimpar.Name = "btnLimpar";
            stateProperties1.BorderColor = System.Drawing.Color.Gainsboro;
            stateProperties1.BorderRadius = 10;
            stateProperties1.BorderThickness = 1;
            stateProperties1.FillColor = System.Drawing.Color.Gainsboro;
            stateProperties1.IconLeftImage = null;
            stateProperties1.IconRightImage = ((System.Drawing.Image)(resources.GetObject("stateProperties1.IconRightImage")));
            this.btnLimpar.onHoverState = stateProperties1;
            this.btnLimpar.Size = new System.Drawing.Size(122, 35);
            this.btnLimpar.TabIndex = 7;
            this.btnLimpar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ToolTipForControls.SetToolTip(this.btnLimpar, "Ao pressionar esse botão, todos os campos do formulário serão limpados.\r\n");
            this.ToolTipForControls.SetToolTipIcon(this.btnLimpar, null);
            this.ToolTipForControls.SetToolTipTitle(this.btnLimpar, "");
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // btnSaveUpdate
            // 
            this.btnSaveUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveUpdate.BackColor = System.Drawing.Color.Transparent;
            this.btnSaveUpdate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSaveUpdate.BackgroundImage")));
            this.btnSaveUpdate.ButtonText = "Salvar";
            this.btnSaveUpdate.ButtonTextMarginLeft = 15;
            this.btnSaveUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSaveUpdate.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnSaveUpdate.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnSaveUpdate.DisabledForecolor = System.Drawing.Color.White;
            this.btnSaveUpdate.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold);
            this.btnSaveUpdate.ForeColor = System.Drawing.Color.White;
            this.btnSaveUpdate.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnSaveUpdate.IconPadding = 8;
            this.btnSaveUpdate.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.btnSaveUpdate.IdleBorderColor = System.Drawing.Color.DodgerBlue;
            this.btnSaveUpdate.IdleBorderRadius = 10;
            this.btnSaveUpdate.IdleBorderThickness = 1;
            this.btnSaveUpdate.IdleFillColor = System.Drawing.Color.DodgerBlue;
            this.btnSaveUpdate.IdleIconLeftImage = null;
            this.btnSaveUpdate.IdleIconRightImage = ((System.Drawing.Image)(resources.GetObject("btnSaveUpdate.IdleIconRightImage")));
            this.btnSaveUpdate.Location = new System.Drawing.Point(115, 285);
            this.btnSaveUpdate.Name = "btnSaveUpdate";
            stateProperties2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            stateProperties2.BorderRadius = 10;
            stateProperties2.BorderThickness = 1;
            stateProperties2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            stateProperties2.IconLeftImage = null;
            stateProperties2.IconRightImage = ((System.Drawing.Image)(resources.GetObject("stateProperties2.IconRightImage")));
            this.btnSaveUpdate.onHoverState = stateProperties2;
            this.btnSaveUpdate.Size = new System.Drawing.Size(125, 35);
            this.btnSaveUpdate.TabIndex = 6;
            this.btnSaveUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ToolTipForControls.SetToolTip(this.btnSaveUpdate, "Após preencher todos os campos acima pressione este botão para que a mágica acont" +
        "eça.");
            this.ToolTipForControls.SetToolTipIcon(this.btnSaveUpdate, null);
            this.ToolTipForControls.SetToolTipTitle(this.btnSaveUpdate, "");
            this.btnSaveUpdate.Click += new System.EventHandler(this.btnSaveUpdate_Click_1);
            // 
            // btnProcurar
            // 
            this.btnProcurar.BackColor = System.Drawing.Color.Transparent;
            this.btnProcurar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnProcurar.BackgroundImage")));
            this.btnProcurar.ButtonText = "   Importe";
            this.btnProcurar.ButtonTextMarginLeft = -13;
            this.btnProcurar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProcurar.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnProcurar.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnProcurar.DisabledForecolor = System.Drawing.Color.White;
            this.btnProcurar.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Bold);
            this.btnProcurar.ForeColor = System.Drawing.Color.White;
            this.btnProcurar.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnProcurar.IconPadding = 5;
            this.btnProcurar.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnProcurar.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(158)))), ((int)(((byte)(234)))));
            this.btnProcurar.IdleBorderRadius = 10;
            this.btnProcurar.IdleBorderThickness = 1;
            this.btnProcurar.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(123)))), ((int)(((byte)(158)))), ((int)(((byte)(234)))));
            this.btnProcurar.IdleIconLeftImage = null;
            this.btnProcurar.IdleIconRightImage = null;
            this.btnProcurar.Location = new System.Drawing.Point(370, 205);
            this.btnProcurar.Name = "btnProcurar";
            stateProperties3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(116)))), ((int)(((byte)(224)))));
            stateProperties3.BorderRadius = 10;
            stateProperties3.BorderThickness = 1;
            stateProperties3.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(67)))), ((int)(((byte)(116)))), ((int)(((byte)(224)))));
            stateProperties3.IconLeftImage = null;
            stateProperties3.IconRightImage = null;
            this.btnProcurar.onHoverState = stateProperties3;
            this.btnProcurar.Size = new System.Drawing.Size(106, 35);
            this.btnProcurar.TabIndex = 8;
            this.btnProcurar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ToolTipForControls.SetToolTip(this.btnProcurar, "Importe uma imagem do seu computador");
            this.ToolTipForControls.SetToolTipIcon(this.btnProcurar, null);
            this.ToolTipForControls.SetToolTipTitle(this.btnProcurar, "");
            this.btnProcurar.Click += new System.EventHandler(this.btnProcurar_Click);
            // 
            // txtNome
            // 
            this.txtNome.AcceptsReturn = false;
            this.txtNome.AcceptsTab = false;
            this.txtNome.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtNome.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtNome.BackColor = System.Drawing.Color.Transparent;
            this.txtNome.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtNome.BackgroundImage")));
            this.txtNome.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtNome.BorderColorDisabled = System.Drawing.Color.Tomato;
            this.txtNome.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.txtNome.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtNome.BorderRadius = 35;
            this.txtNome.BorderThickness = 1;
            this.txtNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtNome.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNome.DefaultText = "";
            this.txtNome.FillColor = System.Drawing.Color.White;
            this.txtNome.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtNome.HideSelection = true;
            this.txtNome.IconLeft = null;
            this.txtNome.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtNome.IconPadding = 6;
            this.txtNome.IconRight = null;
            this.txtNome.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.txtNome.Location = new System.Drawing.Point(17, 99);
            this.txtNome.MaxLength = 70;
            this.txtNome.MinimumSize = new System.Drawing.Size(100, 35);
            this.txtNome.Modified = false;
            this.txtNome.Name = "txtNome";
            this.txtNome.PasswordChar = '\0';
            this.txtNome.ReadOnly = false;
            this.txtNome.SelectedText = "";
            this.txtNome.SelectionLength = 0;
            this.txtNome.SelectionStart = 0;
            this.txtNome.ShortcutsEnabled = true;
            this.txtNome.Size = new System.Drawing.Size(333, 36);
            this.txtNome.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtNome.TabIndex = 2;
            this.txtNome.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtNome.TextMarginLeft = 10;
            this.txtNome.TextPlaceholder = "Nome do Autor";
            this.ToolTipForControls.SetToolTip(this.txtNome, "Digite o nome do autor\r\n");
            this.ToolTipForControls.SetToolTipIcon(this.txtNome, null);
            this.ToolTipForControls.SetToolTipTitle(this.txtNome, "");
            this.txtNome.UseSystemPasswordChar = false;
            this.txtNome.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown_1);
            // 
            // txtEstilo
            // 
            this.txtEstilo.AcceptsReturn = false;
            this.txtEstilo.AcceptsTab = false;
            this.txtEstilo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtEstilo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtEstilo.BackColor = System.Drawing.Color.White;
            this.txtEstilo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtEstilo.BackgroundImage")));
            this.txtEstilo.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtEstilo.BorderColorDisabled = System.Drawing.Color.Tomato;
            this.txtEstilo.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.txtEstilo.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtEstilo.BorderRadius = 35;
            this.txtEstilo.BorderThickness = 1;
            this.txtEstilo.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtEstilo.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEstilo.DefaultText = "";
            this.txtEstilo.FillColor = System.Drawing.Color.White;
            this.txtEstilo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtEstilo.HideSelection = true;
            this.txtEstilo.IconLeft = null;
            this.txtEstilo.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtEstilo.IconPadding = 10;
            this.txtEstilo.IconRight = ((System.Drawing.Image)(resources.GetObject("txtEstilo.IconRight")));
            this.txtEstilo.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtEstilo.Location = new System.Drawing.Point(17, 209);
            this.txtEstilo.MaxLength = 40;
            this.txtEstilo.MinimumSize = new System.Drawing.Size(100, 35);
            this.txtEstilo.Modified = false;
            this.txtEstilo.Name = "txtEstilo";
            this.txtEstilo.PasswordChar = '\0';
            this.txtEstilo.ReadOnly = false;
            this.txtEstilo.SelectedText = "";
            this.txtEstilo.SelectionLength = 0;
            this.txtEstilo.SelectionStart = 0;
            this.txtEstilo.ShortcutsEnabled = true;
            this.txtEstilo.Size = new System.Drawing.Size(333, 36);
            this.txtEstilo.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtEstilo.TabIndex = 4;
            this.txtEstilo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtEstilo.TextMarginLeft = 10;
            this.txtEstilo.TextPlaceholder = "Selecione";
            this.ToolTipForControls.SetToolTip(this.txtEstilo, "Selecione o estilo de escrita do autor");
            this.ToolTipForControls.SetToolTipIcon(this.txtEstilo, null);
            this.ToolTipForControls.SetToolTipTitle(this.txtEstilo, "");
            this.txtEstilo.UseSystemPasswordChar = false;
            this.txtEstilo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown_1);
            // 
            // txtNascimento
            // 
            this.txtNascimento.AcceptsReturn = false;
            this.txtNascimento.AcceptsTab = false;
            this.txtNascimento.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtNascimento.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtNascimento.BackColor = System.Drawing.Color.Transparent;
            this.txtNascimento.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtNascimento.BackgroundImage")));
            this.txtNascimento.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtNascimento.BorderColorDisabled = System.Drawing.Color.Tomato;
            this.txtNascimento.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.txtNascimento.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtNascimento.BorderRadius = 35;
            this.txtNascimento.BorderThickness = 1;
            this.txtNascimento.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtNascimento.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNascimento.DefaultText = "";
            this.txtNascimento.FillColor = System.Drawing.Color.White;
            this.txtNascimento.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtNascimento.HideSelection = true;
            this.txtNascimento.IconLeft = null;
            this.txtNascimento.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtNascimento.IconPadding = 6;
            this.txtNascimento.IconRight = null;
            this.txtNascimento.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.txtNascimento.Location = new System.Drawing.Point(17, 154);
            this.txtNascimento.MaxLength = 10;
            this.txtNascimento.MinimumSize = new System.Drawing.Size(100, 35);
            this.txtNascimento.Modified = false;
            this.txtNascimento.Name = "txtNascimento";
            this.txtNascimento.PasswordChar = '\0';
            this.txtNascimento.ReadOnly = false;
            this.txtNascimento.SelectedText = "";
            this.txtNascimento.SelectionLength = 0;
            this.txtNascimento.SelectionStart = 0;
            this.txtNascimento.ShortcutsEnabled = true;
            this.txtNascimento.Size = new System.Drawing.Size(333, 36);
            this.txtNascimento.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtNascimento.TabIndex = 3;
            this.txtNascimento.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtNascimento.TextMarginLeft = 10;
            this.txtNascimento.TextPlaceholder = "Data de Nascimento";
            this.ToolTipForControls.SetToolTip(this.txtNascimento, "DD/MM/YYYY");
            this.ToolTipForControls.SetToolTipIcon(this.txtNascimento, null);
            this.ToolTipForControls.SetToolTipTitle(this.txtNascimento, "");
            this.txtNascimento.UseSystemPasswordChar = false;
            this.txtNascimento.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown_1);
            this.txtNascimento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNascimento_KeyPress);
            // 
            // pnError
            // 
            this.pnError.Controls.Add(this.lblError);
            this.pnError.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnError.Location = new System.Drawing.Point(0, 0);
            this.pnError.Name = "pnError";
            this.pnError.Size = new System.Drawing.Size(492, 32);
            this.pnError.TabIndex = 190;
            this.ToolTipForControls.SetToolTip(this.pnError, "");
            this.ToolTipForControls.SetToolTipIcon(this.pnError, null);
            this.ToolTipForControls.SetToolTipTitle(this.pnError, "");
            this.pnError.Visible = false;
            // 
            // lblError
            // 
            this.lblError.BackColor = System.Drawing.Color.Tomato;
            this.lblError.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblError.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.White;
            this.lblError.Location = new System.Drawing.Point(0, 0);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(492, 32);
            this.lblError.TabIndex = 0;
            this.lblError.Text = "Nome";
            this.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ToolTipForControls.SetToolTip(this.lblError, "");
            this.ToolTipForControls.SetToolTipIcon(this.lblError, null);
            this.ToolTipForControls.SetToolTipTitle(this.lblError, "");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(34, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(172, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Informações do Autor";
            this.ToolTipForControls.SetToolTip(this.label1, "");
            this.ToolTipForControls.SetToolTipIcon(this.label1, null);
            this.ToolTipForControls.SetToolTipTitle(this.label1, "");
            // 
            // TimerError
            // 
            this.TimerError.Interval = 2000;
            this.TimerError.Tick += new System.EventHandler(this.TimerError_Tick);
            // 
            // ToolTipForControls
            // 
            this.ToolTipForControls.Active = true;
            this.ToolTipForControls.AlignTextWithTitle = false;
            this.ToolTipForControls.AllowAutoClose = false;
            this.ToolTipForControls.AllowFading = true;
            this.ToolTipForControls.AutoCloseDuration = 5000;
            this.ToolTipForControls.BackColor = System.Drawing.SystemColors.Control;
            this.ToolTipForControls.BorderColor = System.Drawing.Color.Gainsboro;
            this.ToolTipForControls.ClickToShowDisplayControl = false;
            this.ToolTipForControls.ConvertNewlinesToBreakTags = true;
            this.ToolTipForControls.DisplayControl = null;
            this.ToolTipForControls.EntryAnimationSpeed = 350;
            this.ToolTipForControls.ExitAnimationSpeed = 200;
            this.ToolTipForControls.GenerateAutoCloseDuration = false;
            this.ToolTipForControls.IconMargin = 6;
            this.ToolTipForControls.InitialDelay = 0;
            this.ToolTipForControls.Name = "ToolTipForControls";
            this.ToolTipForControls.Opacity = 1D;
            this.ToolTipForControls.OverrideToolTipTitles = false;
            this.ToolTipForControls.Padding = new System.Windows.Forms.Padding(10);
            this.ToolTipForControls.ReshowDelay = 100;
            this.ToolTipForControls.ShowAlways = true;
            this.ToolTipForControls.ShowBorders = false;
            this.ToolTipForControls.ShowIcons = true;
            this.ToolTipForControls.ShowShadows = true;
            this.ToolTipForControls.Tag = null;
            this.ToolTipForControls.TextFont = new System.Drawing.Font("Segoe UI", 9F);
            this.ToolTipForControls.TextForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ToolTipForControls.TextMargin = 2;
            this.ToolTipForControls.TitleFont = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.ToolTipForControls.TitleForeColor = System.Drawing.Color.Black;
            this.ToolTipForControls.ToolTipPosition = new System.Drawing.Point(0, 0);
            this.ToolTipForControls.ToolTipTitle = null;
            // 
            // pnTop
            // 
            this.pnTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(182)))), ((int)(((byte)(78)))));
            this.pnTop.Controls.Add(this.cdsHearder);
            this.pnTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Size = new System.Drawing.Size(525, 55);
            this.pnTop.TabIndex = 0;
            this.ToolTipForControls.SetToolTip(this.pnTop, "");
            this.ToolTipForControls.SetToolTipIcon(this.pnTop, null);
            this.ToolTipForControls.SetToolTipTitle(this.pnTop, "");
            // 
            // cdsHearder
            // 
            this.cdsHearder.BackColor = System.Drawing.Color.DodgerBlue;
            this.cdsHearder.BorderRadius = 5;
            this.cdsHearder.BottomSahddow = true;
            this.cdsHearder.color = System.Drawing.Color.White;
            this.cdsHearder.Controls.Add(this.lblTitle);
            this.cdsHearder.Controls.Add(this.btnClose);
            this.cdsHearder.LeftSahddow = false;
            this.cdsHearder.Location = new System.Drawing.Point(-8, -2);
            this.cdsHearder.Name = "cdsHearder";
            this.cdsHearder.RightSahddow = true;
            this.cdsHearder.ShadowDepth = 20;
            this.cdsHearder.Size = new System.Drawing.Size(545, 59);
            this.cdsHearder.TabIndex = 1;
            this.ToolTipForControls.SetToolTip(this.cdsHearder, "");
            this.ToolTipForControls.SetToolTipIcon(this.cdsHearder, null);
            this.ToolTipForControls.SetToolTipTitle(this.cdsHearder, "");
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.Transparent;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageActive = null;
            this.btnClose.Location = new System.Drawing.Point(490, 15);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(26, 26);
            this.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnClose.TabIndex = 177;
            this.btnClose.TabStop = false;
            this.ToolTipForControls.SetToolTip(this.btnClose, "Volte para página anterior");
            this.ToolTipForControls.SetToolTipIcon(this.btnClose, null);
            this.ToolTipForControls.SetToolTipTitle(this.btnClose, "");
            this.btnClose.Zoom = 10;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dclForm
            // 
            this.dclForm.Fixed = true;
            this.dclForm.Horizontal = true;
            this.dclForm.TargetControl = this.cdsHearder;
            this.dclForm.Vertical = true;
            // 
            // ElipseForm
            // 
            this.ElipseForm.ElipseRadius = 5;
            this.ElipseForm.TargetControl = this;
            // 
            // imageListIconValidator
            // 
            this.imageListIconValidator.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListIconValidator.ImageStream")));
            this.imageListIconValidator.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListIconValidator.Images.SetKeyName(0, "Checkmark.png");
            this.imageListIconValidator.Images.SetKeyName(1, "Error.png");
            // 
            // frmAddEditAuthor
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(525, 420);
            this.Controls.Add(this.pnConteiner);
            this.Controls.Add(this.pnTop);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "frmAddEditAuthor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmAddEditAuthor";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmAddEditAuthor_KeyDown);
            this.pnConteiner.ResumeLayout(false);
            this.pnControls.ResumeLayout(false);
            this.pnControls.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgAutor)).EndInit();
            this.pnError.ResumeLayout(false);
            this.pnTop.ResumeLayout(false);
            this.cdsHearder.ResumeLayout(false);
            this.cdsHearder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Panel pnConteiner;
        private System.Windows.Forms.Timer TimerError;
        private Bunifu.Framework.UI.BunifuImageButton btnClose;
        private Bunifu.UI.WinForms.BunifuToolTip ToolTipForControls;
        private System.Windows.Forms.Panel pnControls;
        private System.Windows.Forms.Label label1;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnProcurar;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtNome;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtEstilo;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtNascimento;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnSaveUpdate;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnLimpar;
        private System.Windows.Forms.Panel pnError;
        private System.Windows.Forms.Label lblError;
        private Bunifu.UI.WinForms.BunifuPictureBox imgAutor;
        private Bunifu.Framework.UI.BunifuDragControl dclForm;
        private System.Windows.Forms.Panel pnTop;
        private Bunifu.Framework.UI.BunifuCards cdsHearder;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnSeparator;
        private Bunifu.Framework.UI.BunifuElipse ElipseForm;
        private System.Windows.Forms.ImageList imageListIconValidator;
    }
}