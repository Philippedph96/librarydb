﻿using Biblioteca.Utilitarios.ImagemPLugin;
using Biblioteca.Validacoes;
using Nsf.FriendsLibrary.Business.Complementos_dos_Livros.Autor;
using Nsf.FriendsLibrary.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Bunifu.UI.WinForms.BunifuTextbox;
using Nsf.FriendsLibrary.Tools.Validações;
using Bunifu.UI.WinForms;
using System.Threading;
using Biblioteca.Utilitarios.Notificacoes;

namespace Nsf.FriendsLibrary.APP.User_Controls.Complemento_dos_Livros.Autor
{
    public partial class frmAddEditAuthor : Form
    {
        //Variável de escopo
        tb_autor autor = null;
        public frmAddEditAuthor()
        {
            InitializeComponent();

            CarregarTextBoxBunifu();

            //Título já começa com Adicionar
            lblTitle.Text = "Adicone um novo Autor";
        }

        //Método que irá receber os valores passados pela gridview
        public void LoadScreen(tb_autor dto)
        {
            //Altero de Salvar para alterar
            btnSaveUpdate.ButtonText = "Alterar";

            //Altero o título para (Novamente com o conceito de alterar e salvar)
            lblTitle.Text = "Altere as informações do Autor";

            //Pego o valor passado por paramêtro e recebo na variável de escopo Autor
            this.autor = dto;

            //Recebo os valores encontrados no banco para os controles no formulário
            txtNome.Text = dto.nm_autor;
            txtNascimento.Text = dto.ds_nascimento.ToString();
            txtEstilo.SelectedText = dto.ds_escrita.ToString();
            imgAutor.Image = ImagemPlugin.ConverterParaImagem(dto.img_autor);
        }

        private void CarregarTextBoxBunifu()
        {
            AutoCompleteStringCollection collection = new AutoCompleteStringCollection();

            //Pego os valores Salvos no banco de dados
            AutorBusiness db = new AutorBusiness();
            var retorno = db.ListStyle();

            //E para cada item encontrado na busca feito ao banco de dados...
            foreach (var item in retorno)
            {
                //Adiciona-se o valor na coleção em strings
                collection.Add(item.ToString());
            }

            //Método anônimo para poder ter acesso ao controle txtEstilo, já que as duas pertecem a threads diferentes
            if (txtEstilo.InvokeRequired)
                txtEstilo.Invoke((MethodInvoker)delegate
                {
                    //Crio uma instancia nessa coleção de dados em strings
                    txtEstilo.AutoCompleteCustomSource = collection;

                });
        }

        private void ShowError(string error)
        {
            //Crio um método anonimo para que outras thread tenham acesso a esse controle
            if (pnError.InvokeRequired)
                pnError.Invoke((MethodInvoker)delegate
                {
                    //O conteiner que está sobre o Label fica visível 
                    pnError.Visible = true;
                });

            //Para esse controle também kk
            if (lblError.InvokeRequired)
                lblError.Invoke((MethodInvoker)delegate
                {
                    //A label irá receber por parâmetro o valor do error
                    lblError.Text = error;
                });

            //E o controle Timer é ativado (Vale lembrar que o intervalo de aparição do Timer é de 2 segundos)
            TimerError.Start();
        }

        private void TimerError_Tick(object sender, EventArgs e)
        {
            //(Após os 2 segundos, o Timer é finalizado)
            TimerError.Stop();

            //E o panel fica invisível
            pnError.Visible = false;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            //Ao pressionar o X (inho) a tela é fechada
            this.Close();
        }

        //Esse método tem como função carregar os respectivos controles aos valores do banco de dados
        private tb_autor Controles(tb_autor controls)
        {
            controls.nm_autor = txtNome.Text;
            controls.ds_nascimento = Convert.ToDateTime(txtNascimento.Text);
            controls.ds_escrita = txtEstilo.Text;
            controls.img_autor = ImagemPlugin.ConverterParaString(imgAutor.Image);

            return controls;
        }

        //Caso esteja na condição de Salvar
        private tb_autor CarregarControlesSave()
        {
            //Instancio o  objeto 
            autor = new tb_autor();

            //Chamo o evento controle com uma variável de retorno
            var retorno = Controles(autor);

            //retorno os valores
            return retorno;
        }
        private tb_autor CarregarControlesUpdate()
        {
            //Utilizo o Using para não dar error de Multiple Entities
            using (var context = new LibraryEntities())
            {
                //Faço uma procura no banco de dados a fim de saber ONDE ou em que valor o usuário está querendo alterar
                autor = context.tb_autor.Where(x => x.id_autor == autor.id_autor).FirstOrDefault();

                //Uma vez encontrado, chamo o evento Controles para que se possa ser preenchido os campos
                var retorno = Controles(autor);

                //retorno os valores
                return retorno;
            }
        }

        //Variável do tipo Image de escopo (Para não ficar criando uma variável com o mesmo papel várias vezes!)
        Image imagem;
        string text;

        //Esse botão tem a funcionabilidade de servir tanto para Salvar quando para Alterar, dependendo do critério utilizado
        private async void btnSaveUpdate_Click_1(object sender, EventArgs e)
        {
            await Task.Run(async () =>
            {
                //Evita buggs
                try
                {
                    //O critério utilizado para utilizar o Salvar está baseado no valor encontrado na lblTitle
                    if (lblTitle.Text == "Adicone um novo Autor")
                    {
                        //Retorno os valores carregados no método CarregarControlesSave
                        var save = CarregarControlesSave();

                        //Instacio o objeto Business (Regra de validação)
                        AutorBusiness db = new AutorBusiness();

                        //Chamando o método Save, passo o valor encontrado no váriavel Save por parâmetro
                        db.Save(save);

                        await Task.Factory.StartNew(() =>
                        {
                            //Se tudo der certo... Exibo uma imagem para o usuário (Pego a imagem armazenado no Resources do projeto).
                            imagem = Properties.Resources.ThumpUP;
                            text = "Novo Autor registrado na base de dados com êxito!";
                            this.SelectorClass(1, text, imagem);
                        });
                    }
                    else
                    {
                        //Caso o If Retorno uma mudança na Label, significa que estamos na parte de UPDATE
                        //Então nesse caso, retorna-se os valores carregados no método CarregarControlesUpdate
                        var update = CarregarControlesUpdate();

                        //Instacio o objeto Business
                        AutorBusiness db = new AutorBusiness();

                        //Chamando o método Update, passo o valor encontrado no váriavel Update por parâmetro
                        db.Update(update);

                        await Task.Factory.StartNew(() =>
                        {
                            //Se tudo der certo... Exibo uma imagem para o usuário (Pego a imagem armazenado no Resources do projeto).
                            imagem = Properties.Resources.Warning;
                            text = "As informações do Autor foram alteradas na base de dados com êxito!!";
                            this.SelectorClass(2, text, imagem);
                        });
                    }
                }
                //Caso ocorra algum erro...
                catch (ArgumentException ex)
                {
                    //Chama-se o método ShowError que já foi explicado acima!
                    ShowError(ex.Message);
                }
            });
        }

        //Neste caso (Ainda são conceitos novos para mim), Este método cria uma Thread do tipo SAFE, pois como estamos utilizando MultiThreadings...
        //Ou seja, a partir desse método, outras threads conseguem acessar a minha thread principal (É como se fosse, "Agora vocês tem permissão para acessar esse formulário"
        public void AbrirDialogo()
        {
            //Tenho acesso a um método da thread principal
            Thread td = new Thread(new ThreadStart(this.EscolherArquivo));

            //Essa Thread começa a ser reconhecida como SAFE (SINGLE-THREADED-APARTMENT)   
            td.SetApartmentState(ApartmentState.STA);

            //Essa thread começa a ser executada em segundo plano
            td.IsBackground = true;

            //Ela começa a ser executada
            td.Start();
        }

        [STAThread] //Indica que os valores que serão manipulados nesse método é STA
        private void btnProcurar_Click(object sender, EventArgs e)
        {
            //Chamo o método
            AbrirDialogo();
        }

        [STAThread] //Indica que os valores que serão manipulados nesse método é STA
        private void EscolherArquivo()
        {
            try
            {
                //Eu utilizando o Using, pois como o usuário pode pegar arquivos grandes, nesse momento, só irá executar o que está no using e irá sair
                using (var dialog = new OpenFileDialog()) //Instancio o quê será a caixa de dialogo
                {
                    //Filtro como condiçao para procurar apenas arquivos com o filtro determinado
                    dialog.Filter = "JPG Files(*.jpg)|*.jpg|PNG Files (*.png)|*.png";

                    //Se o usuário apertar em OK
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        //Valor encontrado
                        imgAutor.ImageLocation = dialog.FileName;
                    }
                    else
                    {
                        //Esse IF é para se caso estiver na parte de alterar, e o usuário não colocar nada, a picture box fica com a imagem que ela já tinha
                        if (lblTitle.Text == "Altere as informações do Autor")
                            imgAutor.ImageLocation = dialog.FileName;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro" + ex.Message + " Tente mais tarde.",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private void txtNascimento_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Cria-se uma nova thread para executar o método MascaraData...
            ValidarTextbox.MascaraData(txtNascimento, e);
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            //CleanControls.LimparTextBoxBunifu(this.pnBunifu.Controls);
            LimparBunifu();
        }

        //Infelizmente, o controle BunifuTextBox tem algumas propriedades diferentes do TextBox, e logo não foi possível implementar um método capaz...
        //De limpar todos os controles em um critério
        private void LimparBunifu()
        {
            txtNascimento.Clear();
            txtEstilo.Clear();
            txtNome.Clear();
        }

        //Implementação da opção de apertar o Enter
        private void txtNome_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnSaveUpdate_Click_1(null, null);
        }

        private void frmAddEditAuthor_KeyDown(object sender, KeyEventArgs e)
        {
            //Posição do ESQ
            if (e.KeyCode == Keys.Escape)
                this.Close();

        }
    }
}
