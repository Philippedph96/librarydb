﻿using Bunifu.UI.WinForms.BunifuTextbox;

namespace Nsf.FriendsLibrary.APP.User_Controls.Complemento_dos_Livros.Autor
{
    partial class frmAddEditPublish
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAddEditPublish));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            this.lblTitle = new System.Windows.Forms.Label();
            this.pnConteiner = new System.Windows.Forms.Panel();
            this.pnControls = new System.Windows.Forms.Panel();
            this.txtSede = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnSeparator = new System.Windows.Forms.Panel();
            this.btnLimpar = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.btnSaveUpdate = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.txtEditora = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.pnError = new System.Windows.Forms.Panel();
            this.lblError = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TimerError = new System.Windows.Forms.Timer(this.components);
            this.ToolTipForControls = new Bunifu.UI.WinForms.BunifuToolTip(this.components);
            this.pnTop = new System.Windows.Forms.Panel();
            this.cdsHearder = new Bunifu.Framework.UI.BunifuCards();
            this.btnClose = new Bunifu.Framework.UI.BunifuImageButton();
            this.dclForm = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.ElipseForm = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.ElipseTextBox = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.pnConteiner.SuspendLayout();
            this.pnControls.SuspendLayout();
            this.pnError.SuspendLayout();
            this.pnTop.SuspendLayout();
            this.cdsHearder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold);
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(36, 16);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(70, 30);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "{title} ";
            this.ToolTipForControls.SetToolTip(this.lblTitle, "");
            this.ToolTipForControls.SetToolTipIcon(this.lblTitle, null);
            this.ToolTipForControls.SetToolTipTitle(this.lblTitle, "");
            // 
            // pnConteiner
            // 
            this.pnConteiner.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnConteiner.Controls.Add(this.pnControls);
            this.pnConteiner.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnConteiner.ForeColor = System.Drawing.Color.White;
            this.pnConteiner.Location = new System.Drawing.Point(0, 0);
            this.pnConteiner.Name = "pnConteiner";
            this.pnConteiner.Size = new System.Drawing.Size(432, 355);
            this.pnConteiner.TabIndex = 1;
            this.ToolTipForControls.SetToolTip(this.pnConteiner, "");
            this.ToolTipForControls.SetToolTipIcon(this.pnConteiner, null);
            this.ToolTipForControls.SetToolTipTitle(this.pnConteiner, "");
            // 
            // pnControls
            // 
            this.pnControls.BackColor = System.Drawing.Color.White;
            this.pnControls.Controls.Add(this.txtSede);
            this.pnControls.Controls.Add(this.panel1);
            this.pnControls.Controls.Add(this.pnSeparator);
            this.pnControls.Controls.Add(this.btnLimpar);
            this.pnControls.Controls.Add(this.btnSaveUpdate);
            this.pnControls.Controls.Add(this.txtEditora);
            this.pnControls.Controls.Add(this.pnError);
            this.pnControls.Controls.Add(this.label1);
            this.pnControls.Location = new System.Drawing.Point(13, 67);
            this.pnControls.Name = "pnControls";
            this.pnControls.Size = new System.Drawing.Size(407, 276);
            this.pnControls.TabIndex = 181;
            this.ToolTipForControls.SetToolTip(this.pnControls, "");
            this.ToolTipForControls.SetToolTipIcon(this.pnControls, null);
            this.ToolTipForControls.SetToolTipTitle(this.pnControls, "");
            // 
            // txtSede
            // 
            this.txtSede.AcceptsReturn = false;
            this.txtSede.AcceptsTab = false;
            this.txtSede.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtSede.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtSede.BackColor = System.Drawing.Color.Transparent;
            this.txtSede.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtSede.BackgroundImage")));
            this.txtSede.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtSede.BorderColorDisabled = System.Drawing.Color.Tomato;
            this.txtSede.BorderColorHover = System.Drawing.Color.DarkSlateGray;
            this.txtSede.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtSede.BorderRadius = 35;
            this.txtSede.BorderThickness = 1;
            this.txtSede.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtSede.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSede.DefaultText = "";
            this.txtSede.FillColor = System.Drawing.SystemColors.Window;
            this.txtSede.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSede.HideSelection = true;
            this.txtSede.IconLeft = null;
            this.txtSede.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtSede.IconPadding = 6;
            this.txtSede.IconRight = null;
            this.txtSede.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.txtSede.Location = new System.Drawing.Point(23, 149);
            this.txtSede.MaxLength = 100;
            this.txtSede.MinimumSize = new System.Drawing.Size(100, 35);
            this.txtSede.Modified = false;
            this.txtSede.Name = "txtSede";
            this.txtSede.PasswordChar = '\0';
            this.txtSede.ReadOnly = false;
            this.txtSede.SelectedText = "";
            this.txtSede.SelectionLength = 0;
            this.txtSede.SelectionStart = 0;
            this.txtSede.ShortcutsEnabled = true;
            this.txtSede.Size = new System.Drawing.Size(352, 36);
            this.txtSede.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtSede.TabIndex = 3;
            this.txtSede.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtSede.TextMarginLeft = 10;
            this.txtSede.TextPlaceholder = "Sede da empresa";
            this.ToolTipForControls.SetToolTip(this.txtSede, "Digite a sede da editora\r\n\r\n");
            this.ToolTipForControls.SetToolTipIcon(this.txtSede, null);
            this.ToolTipForControls.SetToolTipTitle(this.txtSede, "");
            this.txtSede.UseSystemPasswordChar = false;
            this.txtSede.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown_1);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Location = new System.Drawing.Point(17, 213);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(375, 1);
            this.panel1.TabIndex = 4;
            this.ToolTipForControls.SetToolTip(this.panel1, "");
            this.ToolTipForControls.SetToolTipIcon(this.panel1, null);
            this.ToolTipForControls.SetToolTipTitle(this.panel1, "");
            // 
            // pnSeparator
            // 
            this.pnSeparator.BackColor = System.Drawing.Color.Gainsboro;
            this.pnSeparator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnSeparator.Location = new System.Drawing.Point(17, 73);
            this.pnSeparator.Name = "pnSeparator";
            this.pnSeparator.Size = new System.Drawing.Size(375, 1);
            this.pnSeparator.TabIndex = 1;
            this.ToolTipForControls.SetToolTip(this.pnSeparator, "");
            this.ToolTipForControls.SetToolTipIcon(this.pnSeparator, null);
            this.ToolTipForControls.SetToolTipTitle(this.pnSeparator, "");
            // 
            // btnLimpar
            // 
            this.btnLimpar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLimpar.BackColor = System.Drawing.Color.Transparent;
            this.btnLimpar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnLimpar.BackgroundImage")));
            this.btnLimpar.ButtonText = "Limpar";
            this.btnLimpar.ButtonTextMarginLeft = 15;
            this.btnLimpar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLimpar.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnLimpar.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnLimpar.DisabledForecolor = System.Drawing.Color.White;
            this.btnLimpar.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold);
            this.btnLimpar.ForeColor = System.Drawing.Color.White;
            this.btnLimpar.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnLimpar.IconPadding = 8;
            this.btnLimpar.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.btnLimpar.IdleBorderColor = System.Drawing.Color.Gray;
            this.btnLimpar.IdleBorderRadius = 10;
            this.btnLimpar.IdleBorderThickness = 1;
            this.btnLimpar.IdleFillColor = System.Drawing.Color.Gray;
            this.btnLimpar.IdleIconLeftImage = null;
            this.btnLimpar.IdleIconRightImage = ((System.Drawing.Image)(resources.GetObject("btnLimpar.IdleIconRightImage")));
            this.btnLimpar.Location = new System.Drawing.Point(205, 227);
            this.btnLimpar.Name = "btnLimpar";
            stateProperties1.BorderColor = System.Drawing.Color.Gainsboro;
            stateProperties1.BorderRadius = 10;
            stateProperties1.BorderThickness = 1;
            stateProperties1.FillColor = System.Drawing.Color.Gainsboro;
            stateProperties1.IconLeftImage = null;
            stateProperties1.IconRightImage = ((System.Drawing.Image)(resources.GetObject("stateProperties1.IconRightImage")));
            this.btnLimpar.onHoverState = stateProperties1;
            this.btnLimpar.Size = new System.Drawing.Size(122, 35);
            this.btnLimpar.TabIndex = 6;
            this.btnLimpar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ToolTipForControls.SetToolTip(this.btnLimpar, "Ao pressionar esse botão, todos os campos do formulário serão limpados.\r\n");
            this.ToolTipForControls.SetToolTipIcon(this.btnLimpar, null);
            this.ToolTipForControls.SetToolTipTitle(this.btnLimpar, "");
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // btnSaveUpdate
            // 
            this.btnSaveUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveUpdate.BackColor = System.Drawing.Color.Transparent;
            this.btnSaveUpdate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSaveUpdate.BackgroundImage")));
            this.btnSaveUpdate.ButtonText = "Salvar";
            this.btnSaveUpdate.ButtonTextMarginLeft = 15;
            this.btnSaveUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSaveUpdate.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnSaveUpdate.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnSaveUpdate.DisabledForecolor = System.Drawing.Color.White;
            this.btnSaveUpdate.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold);
            this.btnSaveUpdate.ForeColor = System.Drawing.Color.White;
            this.btnSaveUpdate.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnSaveUpdate.IconPadding = 8;
            this.btnSaveUpdate.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.btnSaveUpdate.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(26)))), ((int)(((byte)(26)))));
            this.btnSaveUpdate.IdleBorderRadius = 10;
            this.btnSaveUpdate.IdleBorderThickness = 1;
            this.btnSaveUpdate.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(26)))), ((int)(((byte)(26)))));
            this.btnSaveUpdate.IdleIconLeftImage = null;
            this.btnSaveUpdate.IdleIconRightImage = ((System.Drawing.Image)(resources.GetObject("btnSaveUpdate.IdleIconRightImage")));
            this.btnSaveUpdate.Location = new System.Drawing.Point(76, 227);
            this.btnSaveUpdate.Name = "btnSaveUpdate";
            stateProperties2.BorderColor = System.Drawing.Color.Maroon;
            stateProperties2.BorderRadius = 10;
            stateProperties2.BorderThickness = 1;
            stateProperties2.FillColor = System.Drawing.Color.Maroon;
            stateProperties2.IconLeftImage = null;
            stateProperties2.IconRightImage = ((System.Drawing.Image)(resources.GetObject("stateProperties2.IconRightImage")));
            this.btnSaveUpdate.onHoverState = stateProperties2;
            this.btnSaveUpdate.Size = new System.Drawing.Size(125, 35);
            this.btnSaveUpdate.TabIndex = 5;
            this.btnSaveUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ToolTipForControls.SetToolTip(this.btnSaveUpdate, "Após preencher todos os campos acima pressione este botão para que a mágica acont" +
        "eça.");
            this.ToolTipForControls.SetToolTipIcon(this.btnSaveUpdate, null);
            this.ToolTipForControls.SetToolTipTitle(this.btnSaveUpdate, "");
            this.btnSaveUpdate.Click += new System.EventHandler(this.btnSaveUpdate_Click_1);
            // 
            // txtEditora
            // 
            this.txtEditora.AcceptsReturn = false;
            this.txtEditora.AcceptsTab = false;
            this.txtEditora.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtEditora.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtEditora.BackColor = System.Drawing.Color.Transparent;
            this.txtEditora.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtEditora.BackgroundImage")));
            this.txtEditora.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtEditora.BorderColorDisabled = System.Drawing.Color.Tomato;
            this.txtEditora.BorderColorHover = System.Drawing.Color.DarkSlateGray;
            this.txtEditora.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtEditora.BorderRadius = 35;
            this.txtEditora.BorderThickness = 1;
            this.txtEditora.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtEditora.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEditora.DefaultText = "";
            this.txtEditora.FillColor = System.Drawing.SystemColors.Window;
            this.txtEditora.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtEditora.HideSelection = true;
            this.txtEditora.IconLeft = null;
            this.txtEditora.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtEditora.IconPadding = 6;
            this.txtEditora.IconRight = null;
            this.txtEditora.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.txtEditora.Location = new System.Drawing.Point(23, 96);
            this.txtEditora.MaxLength = 70;
            this.txtEditora.MinimumSize = new System.Drawing.Size(100, 35);
            this.txtEditora.Modified = false;
            this.txtEditora.Name = "txtEditora";
            this.txtEditora.PasswordChar = '\0';
            this.txtEditora.ReadOnly = false;
            this.txtEditora.SelectedText = "";
            this.txtEditora.SelectionLength = 0;
            this.txtEditora.SelectionStart = 0;
            this.txtEditora.ShortcutsEnabled = true;
            this.txtEditora.Size = new System.Drawing.Size(352, 36);
            this.txtEditora.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtEditora.TabIndex = 2;
            this.txtEditora.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtEditora.TextMarginLeft = 10;
            this.txtEditora.TextPlaceholder = "Nome da Editora";
            this.ToolTipForControls.SetToolTip(this.txtEditora, "Digite o nome da editora\r\n\r\n");
            this.ToolTipForControls.SetToolTipIcon(this.txtEditora, null);
            this.ToolTipForControls.SetToolTipTitle(this.txtEditora, "");
            this.txtEditora.UseSystemPasswordChar = false;
            this.txtEditora.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown_1);
            this.txtEditora.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNome_KeyPress);
            // 
            // pnError
            // 
            this.pnError.Controls.Add(this.lblError);
            this.pnError.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnError.Location = new System.Drawing.Point(0, 0);
            this.pnError.Name = "pnError";
            this.pnError.Size = new System.Drawing.Size(407, 32);
            this.pnError.TabIndex = 190;
            this.ToolTipForControls.SetToolTip(this.pnError, "");
            this.ToolTipForControls.SetToolTipIcon(this.pnError, null);
            this.ToolTipForControls.SetToolTipTitle(this.pnError, "");
            this.pnError.Visible = false;
            // 
            // lblError
            // 
            this.lblError.BackColor = System.Drawing.Color.Tomato;
            this.lblError.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblError.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.White;
            this.lblError.Location = new System.Drawing.Point(0, 0);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(407, 32);
            this.lblError.TabIndex = 0;
            this.lblError.Text = "Nome";
            this.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ToolTipForControls.SetToolTip(this.lblError, "");
            this.ToolTipForControls.SetToolTipIcon(this.lblError, null);
            this.ToolTipForControls.SetToolTipTitle(this.lblError, "");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Gray;
            this.label1.Location = new System.Drawing.Point(34, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Informações da Editora";
            this.ToolTipForControls.SetToolTip(this.label1, "");
            this.ToolTipForControls.SetToolTipIcon(this.label1, null);
            this.ToolTipForControls.SetToolTipTitle(this.label1, "");
            // 
            // TimerError
            // 
            this.TimerError.Interval = 2000;
            this.TimerError.Tick += new System.EventHandler(this.TimerError_Tick);
            // 
            // ToolTipForControls
            // 
            this.ToolTipForControls.Active = true;
            this.ToolTipForControls.AlignTextWithTitle = false;
            this.ToolTipForControls.AllowAutoClose = false;
            this.ToolTipForControls.AllowFading = true;
            this.ToolTipForControls.AutoCloseDuration = 5000;
            this.ToolTipForControls.BackColor = System.Drawing.SystemColors.Control;
            this.ToolTipForControls.BorderColor = System.Drawing.Color.Gainsboro;
            this.ToolTipForControls.ClickToShowDisplayControl = false;
            this.ToolTipForControls.ConvertNewlinesToBreakTags = true;
            this.ToolTipForControls.DisplayControl = null;
            this.ToolTipForControls.EntryAnimationSpeed = 350;
            this.ToolTipForControls.ExitAnimationSpeed = 200;
            this.ToolTipForControls.GenerateAutoCloseDuration = false;
            this.ToolTipForControls.IconMargin = 6;
            this.ToolTipForControls.InitialDelay = 0;
            this.ToolTipForControls.Name = "ToolTipForControls";
            this.ToolTipForControls.Opacity = 1D;
            this.ToolTipForControls.OverrideToolTipTitles = false;
            this.ToolTipForControls.Padding = new System.Windows.Forms.Padding(10);
            this.ToolTipForControls.ReshowDelay = 100;
            this.ToolTipForControls.ShowAlways = true;
            this.ToolTipForControls.ShowBorders = false;
            this.ToolTipForControls.ShowIcons = true;
            this.ToolTipForControls.ShowShadows = true;
            this.ToolTipForControls.Tag = null;
            this.ToolTipForControls.TextFont = new System.Drawing.Font("Segoe UI", 9F);
            this.ToolTipForControls.TextForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ToolTipForControls.TextMargin = 2;
            this.ToolTipForControls.TitleFont = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.ToolTipForControls.TitleForeColor = System.Drawing.Color.Black;
            this.ToolTipForControls.ToolTipPosition = new System.Drawing.Point(0, 0);
            this.ToolTipForControls.ToolTipTitle = null;
            // 
            // pnTop
            // 
            this.pnTop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(182)))), ((int)(((byte)(78)))));
            this.pnTop.Controls.Add(this.cdsHearder);
            this.pnTop.Location = new System.Drawing.Point(0, 0);
            this.pnTop.Name = "pnTop";
            this.pnTop.Size = new System.Drawing.Size(524, 55);
            this.pnTop.TabIndex = 0;
            this.ToolTipForControls.SetToolTip(this.pnTop, "");
            this.ToolTipForControls.SetToolTipIcon(this.pnTop, null);
            this.ToolTipForControls.SetToolTipTitle(this.pnTop, "");
            // 
            // cdsHearder
            // 
            this.cdsHearder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(26)))), ((int)(((byte)(26)))));
            this.cdsHearder.BorderRadius = 5;
            this.cdsHearder.BottomSahddow = true;
            this.cdsHearder.color = System.Drawing.Color.White;
            this.cdsHearder.Controls.Add(this.lblTitle);
            this.cdsHearder.Controls.Add(this.btnClose);
            this.cdsHearder.LeftSahddow = false;
            this.cdsHearder.Location = new System.Drawing.Point(-8, -7);
            this.cdsHearder.Name = "cdsHearder";
            this.cdsHearder.RightSahddow = true;
            this.cdsHearder.ShadowDepth = 20;
            this.cdsHearder.Size = new System.Drawing.Size(486, 68);
            this.cdsHearder.TabIndex = 1;
            this.ToolTipForControls.SetToolTip(this.cdsHearder, "");
            this.ToolTipForControls.SetToolTipIcon(this.cdsHearder, null);
            this.ToolTipForControls.SetToolTipTitle(this.cdsHearder, "");
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(26)))), ((int)(((byte)(26)))));
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageActive = null;
            this.btnClose.Location = new System.Drawing.Point(405, 19);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(26, 26);
            this.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnClose.TabIndex = 177;
            this.btnClose.TabStop = false;
            this.ToolTipForControls.SetToolTip(this.btnClose, "Volte para página anterior");
            this.ToolTipForControls.SetToolTipIcon(this.btnClose, null);
            this.ToolTipForControls.SetToolTipTitle(this.btnClose, "");
            this.btnClose.Zoom = 10;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // dclForm
            // 
            this.dclForm.Fixed = true;
            this.dclForm.Horizontal = true;
            this.dclForm.TargetControl = this.cdsHearder;
            this.dclForm.Vertical = true;
            // 
            // ElipseForm
            // 
            this.ElipseForm.ElipseRadius = 5;
            this.ElipseForm.TargetControl = this;
            // 
            // ElipseTextBox
            // 
            this.ElipseTextBox.ElipseRadius = 8;
            this.ElipseTextBox.TargetControl = this;
            // 
            // frmAddEditPublish
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(432, 355);
            this.Controls.Add(this.pnTop);
            this.Controls.Add(this.pnConteiner);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "frmAddEditPublish";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmAddEditAuthor";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmAddEditAuthor_KeyDown);
            this.pnConteiner.ResumeLayout(false);
            this.pnControls.ResumeLayout(false);
            this.pnControls.PerformLayout();
            this.pnError.ResumeLayout(false);
            this.pnTop.ResumeLayout(false);
            this.cdsHearder.ResumeLayout(false);
            this.cdsHearder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Panel pnConteiner;
        private System.Windows.Forms.Timer TimerError;
        private Bunifu.Framework.UI.BunifuImageButton btnClose;
        private Bunifu.UI.WinForms.BunifuToolTip ToolTipForControls;
        private Bunifu.Framework.UI.BunifuDragControl dclForm;
        private System.Windows.Forms.Panel pnTop;
        private Bunifu.Framework.UI.BunifuCards cdsHearder;
        private Bunifu.Framework.UI.BunifuElipse ElipseForm;
        private Bunifu.Framework.UI.BunifuElipse ElipseTextBox;
        private System.Windows.Forms.Panel pnControls;
        private BunifuTextBox txtSede;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnSeparator;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnLimpar;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnSaveUpdate;
        private BunifuTextBox txtEditora;
        private System.Windows.Forms.Panel pnError;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.Label label1;
    }
}