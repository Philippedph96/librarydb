﻿namespace Nsf.FriendsLibrary.APP.User_Controls.Usuario
{
    partial class frmAddUpdateUser
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAddUpdateUser));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            this.pnSeparator1 = new System.Windows.Forms.Panel();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblCEP = new System.Windows.Forms.Label();
            this.lblAdress = new System.Windows.Forms.Label();
            this.lblNumero = new System.Windows.Forms.Label();
            this.lblProfessor = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblDados = new System.Windows.Forms.Label();
            this.lblCondicao = new System.Windows.Forms.Label();
            this.lblAluno = new System.Windows.Forms.Label();
            this.lblData = new System.Windows.Forms.Label();
            this.lblCPF = new System.Windows.Forms.Label();
            this.pnSeparatorDados = new System.Windows.Forms.Panel();
            this.pnSeparatorCondicao = new System.Windows.Forms.Panel();
            this.pnSeparatorEndereco = new System.Windows.Forms.Panel();
            this.lblEndereco = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCancelar = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.txtCidade = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtNumero = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtCPF = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtCEP = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtEndereco = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtEmail = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtUsuario = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.btnSaveUpdate = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.chkAluno = new Bunifu.UI.WinForms.BunifuCheckBox();
            this.chkProfessor = new Bunifu.UI.WinForms.BunifuCheckBox();
            this.txtData = new Bunifu.UI.WinForms.BunifuDatePicker();
            this.imageListIconValidator = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // pnSeparator1
            // 
            this.pnSeparator1.BackColor = System.Drawing.Color.Gainsboro;
            this.pnSeparator1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnSeparator1.Location = new System.Drawing.Point(18, 64);
            this.pnSeparator1.Name = "pnSeparator1";
            this.pnSeparator1.Size = new System.Drawing.Size(590, 1);
            this.pnSeparator1.TabIndex = 1;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTitulo.Location = new System.Drawing.Point(39, 22);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(78, 32);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "{Title}";
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblUsuario.Location = new System.Drawing.Point(68, 121);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(108, 20);
            this.lblUsuario.TabIndex = 4;
            this.lblUsuario.Text = "Nome Usuário";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblEmail.Location = new System.Drawing.Point(68, 238);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(46, 20);
            this.lblEmail.TabIndex = 10;
            this.lblEmail.Text = "Email";
            // 
            // lblCEP
            // 
            this.lblCEP.AutoSize = true;
            this.lblCEP.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCEP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCEP.Location = new System.Drawing.Point(68, 335);
            this.lblCEP.Name = "lblCEP";
            this.lblCEP.Size = new System.Drawing.Size(35, 20);
            this.lblCEP.TabIndex = 14;
            this.lblCEP.Text = "CEP";
            // 
            // lblAdress
            // 
            this.lblAdress.AutoSize = true;
            this.lblAdress.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAdress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblAdress.Location = new System.Drawing.Point(68, 400);
            this.lblAdress.Name = "lblAdress";
            this.lblAdress.Size = new System.Drawing.Size(73, 20);
            this.lblAdress.TabIndex = 18;
            this.lblAdress.Text = "Endereço";
            // 
            // lblNumero
            // 
            this.lblNumero.AutoSize = true;
            this.lblNumero.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblNumero.Location = new System.Drawing.Point(419, 400);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(101, 20);
            this.lblNumero.TabIndex = 20;
            this.lblNumero.Text = "Número Casa";
            // 
            // lblProfessor
            // 
            this.lblProfessor.AccessibleRole = System.Windows.Forms.AccessibleRole.CheckButton;
            this.lblProfessor.AutoSize = true;
            this.lblProfessor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblProfessor.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProfessor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblProfessor.Location = new System.Drawing.Point(101, 512);
            this.lblProfessor.Name = "lblProfessor";
            this.lblProfessor.Size = new System.Drawing.Size(77, 20);
            this.lblProfessor.TabIndex = 25;
            this.lblProfessor.Text = "Professor ";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Location = new System.Drawing.Point(18, 544);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(590, 1);
            this.panel1.TabIndex = 3;
            // 
            // lblDados
            // 
            this.lblDados.AutoSize = true;
            this.lblDados.Font = new System.Drawing.Font("Segoe UI Semibold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDados.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblDados.Location = new System.Drawing.Point(40, 85);
            this.lblDados.Name = "lblDados";
            this.lblDados.Size = new System.Drawing.Size(139, 25);
            this.lblDados.TabIndex = 2;
            this.lblDados.Text = "Dados Pessoais";
            // 
            // lblCondicao
            // 
            this.lblCondicao.AutoSize = true;
            this.lblCondicao.Font = new System.Drawing.Font("Segoe UI Semibold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCondicao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCondicao.Location = new System.Drawing.Point(40, 466);
            this.lblCondicao.Name = "lblCondicao";
            this.lblCondicao.Size = new System.Drawing.Size(206, 25);
            this.lblCondicao.TabIndex = 22;
            this.lblCondicao.Text = "Condição na Instituição";
            // 
            // lblAluno
            // 
            this.lblAluno.AccessibleRole = System.Windows.Forms.AccessibleRole.CheckButton;
            this.lblAluno.AutoSize = true;
            this.lblAluno.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblAluno.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAluno.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblAluno.Location = new System.Drawing.Point(231, 512);
            this.lblAluno.Name = "lblAluno";
            this.lblAluno.Size = new System.Drawing.Size(50, 20);
            this.lblAluno.TabIndex = 27;
            this.lblAluno.Text = "Aluno";
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            this.lblData.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblData.Location = new System.Drawing.Point(306, 180);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(126, 20);
            this.lblData.TabIndex = 8;
            this.lblData.Text = "Data Nascimento";
            // 
            // lblCPF
            // 
            this.lblCPF.AutoSize = true;
            this.lblCPF.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCPF.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCPF.Location = new System.Drawing.Point(68, 180);
            this.lblCPF.Name = "lblCPF";
            this.lblCPF.Size = new System.Drawing.Size(35, 20);
            this.lblCPF.TabIndex = 6;
            this.lblCPF.Text = "CPF";
            // 
            // pnSeparatorDados
            // 
            this.pnSeparatorDados.BackColor = System.Drawing.Color.Gainsboro;
            this.pnSeparatorDados.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnSeparatorDados.Location = new System.Drawing.Point(35, 113);
            this.pnSeparatorDados.Name = "pnSeparatorDados";
            this.pnSeparatorDados.Size = new System.Drawing.Size(530, 1);
            this.pnSeparatorDados.TabIndex = 3;
            // 
            // pnSeparatorCondicao
            // 
            this.pnSeparatorCondicao.BackColor = System.Drawing.Color.Gainsboro;
            this.pnSeparatorCondicao.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnSeparatorCondicao.Location = new System.Drawing.Point(35, 494);
            this.pnSeparatorCondicao.Name = "pnSeparatorCondicao";
            this.pnSeparatorCondicao.Size = new System.Drawing.Size(530, 1);
            this.pnSeparatorCondicao.TabIndex = 23;
            // 
            // pnSeparatorEndereco
            // 
            this.pnSeparatorEndereco.BackColor = System.Drawing.Color.Gainsboro;
            this.pnSeparatorEndereco.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnSeparatorEndereco.Location = new System.Drawing.Point(35, 326);
            this.pnSeparatorEndereco.Name = "pnSeparatorEndereco";
            this.pnSeparatorEndereco.Size = new System.Drawing.Size(530, 1);
            this.pnSeparatorEndereco.TabIndex = 13;
            // 
            // lblEndereco
            // 
            this.lblEndereco.AutoSize = true;
            this.lblEndereco.Font = new System.Drawing.Font("Segoe UI Semibold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndereco.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblEndereco.Location = new System.Drawing.Point(40, 297);
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.Size = new System.Drawing.Size(89, 25);
            this.lblEndereco.TabIndex = 12;
            this.lblEndereco.Text = "Endereço";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(306, 335);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 20);
            this.label1.TabIndex = 16;
            this.label1.Text = "Cidade";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancelar.BackColor = System.Drawing.Color.Transparent;
            this.btnCancelar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancelar.BackgroundImage")));
            this.btnCancelar.ButtonText = "Limpar";
            this.btnCancelar.ButtonTextMarginLeft = 0;
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnCancelar.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnCancelar.DisabledForecolor = System.Drawing.Color.White;
            this.btnCancelar.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnCancelar.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnCancelar.IconPadding = 10;
            this.btnCancelar.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnCancelar.IdleBorderColor = System.Drawing.Color.Silver;
            this.btnCancelar.IdleBorderRadius = 40;
            this.btnCancelar.IdleBorderThickness = 0;
            this.btnCancelar.IdleFillColor = System.Drawing.Color.Silver;
            this.btnCancelar.IdleIconLeftImage = null;
            this.btnCancelar.IdleIconRightImage = null;
            this.btnCancelar.Location = new System.Drawing.Point(332, 566);
            this.btnCancelar.Name = "btnCancelar";
            stateProperties1.BorderColor = System.Drawing.SystemColors.Control;
            stateProperties1.BorderRadius = 40;
            stateProperties1.BorderThickness = 0;
            stateProperties1.FillColor = System.Drawing.SystemColors.Control;
            stateProperties1.IconLeftImage = null;
            stateProperties1.IconRightImage = null;
            this.btnCancelar.onHoverState = stateProperties1;
            this.btnCancelar.Size = new System.Drawing.Size(136, 45);
            this.btnCancelar.TabIndex = 28;
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // txtCidade
            // 
            this.txtCidade.AcceptsReturn = false;
            this.txtCidade.AcceptsTab = false;
            this.txtCidade.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtCidade.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtCidade.BackColor = System.Drawing.Color.Gainsboro;
            this.txtCidade.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtCidade.BackgroundImage")));
            this.txtCidade.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtCidade.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtCidade.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(38)))), ((int)(((byte)(157)))));
            this.txtCidade.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtCidade.BorderRadius = 1;
            this.txtCidade.BorderThickness = 1;
            this.txtCidade.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtCidade.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCidade.DefaultText = "";
            this.txtCidade.FillColor = System.Drawing.Color.Gainsboro;
            this.txtCidade.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtCidade.HideSelection = true;
            this.txtCidade.IconLeft = global::Nsf.FriendsLibrary.APP.Properties.Resources.WorldWide;
            this.txtCidade.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtCidade.IconPadding = 4;
            this.txtCidade.IconRight = null;
            this.txtCidade.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtCidade.Location = new System.Drawing.Point(310, 357);
            this.txtCidade.MaxLength = 50;
            this.txtCidade.MinimumSize = new System.Drawing.Size(100, 30);
            this.txtCidade.Modified = false;
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.PasswordChar = '\0';
            this.txtCidade.ReadOnly = false;
            this.txtCidade.SelectedText = "";
            this.txtCidade.SelectionLength = 0;
            this.txtCidade.SelectionStart = 0;
            this.txtCidade.ShortcutsEnabled = true;
            this.txtCidade.Size = new System.Drawing.Size(229, 30);
            this.txtCidade.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtCidade.TabIndex = 17;
            this.txtCidade.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCidade.TextMarginLeft = 10;
            this.txtCidade.TextPlaceholder = "";
            this.txtCidade.UseSystemPasswordChar = false;
            this.txtCidade.TextChange += new System.EventHandler(this.txtCidade_TextChange);
            this.txtCidade.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUsuario_KeyDown);
            // 
            // txtNumero
            // 
            this.txtNumero.AcceptsReturn = false;
            this.txtNumero.AcceptsTab = false;
            this.txtNumero.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtNumero.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtNumero.BackColor = System.Drawing.Color.Gainsboro;
            this.txtNumero.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtNumero.BackgroundImage")));
            this.txtNumero.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtNumero.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtNumero.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(38)))), ((int)(((byte)(157)))));
            this.txtNumero.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtNumero.BorderRadius = 1;
            this.txtNumero.BorderThickness = 1;
            this.txtNumero.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtNumero.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumero.DefaultText = "";
            this.txtNumero.FillColor = System.Drawing.Color.Gainsboro;
            this.txtNumero.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtNumero.HideSelection = true;
            this.txtNumero.IconLeft = global::Nsf.FriendsLibrary.APP.Properties.Resources.Número;
            this.txtNumero.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtNumero.IconPadding = 4;
            this.txtNumero.IconRight = null;
            this.txtNumero.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtNumero.Location = new System.Drawing.Point(423, 422);
            this.txtNumero.MaxLength = 32767;
            this.txtNumero.MinimumSize = new System.Drawing.Size(100, 30);
            this.txtNumero.Modified = false;
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.PasswordChar = '\0';
            this.txtNumero.ReadOnly = false;
            this.txtNumero.SelectedText = "";
            this.txtNumero.SelectionLength = 0;
            this.txtNumero.SelectionStart = 0;
            this.txtNumero.ShortcutsEnabled = true;
            this.txtNumero.Size = new System.Drawing.Size(116, 30);
            this.txtNumero.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtNumero.TabIndex = 21;
            this.txtNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtNumero.TextMarginLeft = 10;
            this.txtNumero.TextPlaceholder = "";
            this.txtNumero.UseSystemPasswordChar = false;
            this.txtNumero.TextChange += new System.EventHandler(this.txtNumero_TextChange);
            this.txtNumero.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUsuario_KeyDown);
            this.txtNumero.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumero_KeyPress);
            // 
            // txtCPF
            // 
            this.txtCPF.AcceptsReturn = false;
            this.txtCPF.AcceptsTab = false;
            this.txtCPF.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtCPF.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtCPF.BackColor = System.Drawing.Color.Gainsboro;
            this.txtCPF.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtCPF.BackgroundImage")));
            this.txtCPF.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtCPF.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtCPF.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(38)))), ((int)(((byte)(157)))));
            this.txtCPF.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtCPF.BorderRadius = 1;
            this.txtCPF.BorderThickness = 1;
            this.txtCPF.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtCPF.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCPF.DefaultText = "";
            this.txtCPF.FillColor = System.Drawing.Color.Gainsboro;
            this.txtCPF.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtCPF.HideSelection = true;
            this.txtCPF.IconLeft = global::Nsf.FriendsLibrary.APP.Properties.Resources.Identification;
            this.txtCPF.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtCPF.IconPadding = 4;
            this.txtCPF.IconRight = null;
            this.txtCPF.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtCPF.Location = new System.Drawing.Point(72, 200);
            this.txtCPF.MaxLength = 14;
            this.txtCPF.MinimumSize = new System.Drawing.Size(100, 30);
            this.txtCPF.Modified = false;
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.PasswordChar = '\0';
            this.txtCPF.ReadOnly = false;
            this.txtCPF.SelectedText = "";
            this.txtCPF.SelectionLength = 0;
            this.txtCPF.SelectionStart = 0;
            this.txtCPF.ShortcutsEnabled = true;
            this.txtCPF.Size = new System.Drawing.Size(220, 30);
            this.txtCPF.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtCPF.TabIndex = 7;
            this.txtCPF.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCPF.TextMarginLeft = 10;
            this.txtCPF.TextPlaceholder = "";
            this.txtCPF.UseSystemPasswordChar = false;
            this.txtCPF.TextChange += new System.EventHandler(this.txtCPF_TextChange);
            this.txtCPF.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUsuario_KeyDown);
            this.txtCPF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCPF_KeyPress);
            // 
            // txtCEP
            // 
            this.txtCEP.AcceptsReturn = false;
            this.txtCEP.AcceptsTab = false;
            this.txtCEP.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtCEP.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtCEP.BackColor = System.Drawing.Color.Gainsboro;
            this.txtCEP.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtCEP.BackgroundImage")));
            this.txtCEP.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtCEP.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtCEP.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(38)))), ((int)(((byte)(157)))));
            this.txtCEP.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtCEP.BorderRadius = 1;
            this.txtCEP.BorderThickness = 1;
            this.txtCEP.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtCEP.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCEP.DefaultText = "";
            this.txtCEP.FillColor = System.Drawing.Color.Gainsboro;
            this.txtCEP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtCEP.HideSelection = true;
            this.txtCEP.IconLeft = global::Nsf.FriendsLibrary.APP.Properties.Resources.Map;
            this.txtCEP.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtCEP.IconPadding = 4;
            this.txtCEP.IconRight = null;
            this.txtCEP.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtCEP.Location = new System.Drawing.Point(72, 357);
            this.txtCEP.MaxLength = 9;
            this.txtCEP.MinimumSize = new System.Drawing.Size(100, 30);
            this.txtCEP.Modified = false;
            this.txtCEP.Name = "txtCEP";
            this.txtCEP.PasswordChar = '\0';
            this.txtCEP.ReadOnly = false;
            this.txtCEP.SelectedText = "";
            this.txtCEP.SelectionLength = 0;
            this.txtCEP.SelectionStart = 0;
            this.txtCEP.ShortcutsEnabled = true;
            this.txtCEP.Size = new System.Drawing.Size(220, 30);
            this.txtCEP.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtCEP.TabIndex = 15;
            this.txtCEP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCEP.TextMarginLeft = 10;
            this.txtCEP.TextPlaceholder = "";
            this.txtCEP.UseSystemPasswordChar = false;
            this.txtCEP.TextChange += new System.EventHandler(this.txtCEP_TextChange);
            this.txtCEP.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUsuario_KeyDown);
            this.txtCEP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCEP_KeyPress);
            this.txtCEP.Leave += new System.EventHandler(this.txtCEP_Leave);
            // 
            // txtEndereco
            // 
            this.txtEndereco.AcceptsReturn = false;
            this.txtEndereco.AcceptsTab = false;
            this.txtEndereco.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtEndereco.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtEndereco.BackColor = System.Drawing.Color.Gainsboro;
            this.txtEndereco.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtEndereco.BackgroundImage")));
            this.txtEndereco.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtEndereco.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtEndereco.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(38)))), ((int)(((byte)(157)))));
            this.txtEndereco.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtEndereco.BorderRadius = 1;
            this.txtEndereco.BorderThickness = 1;
            this.txtEndereco.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtEndereco.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEndereco.DefaultText = "";
            this.txtEndereco.FillColor = System.Drawing.Color.Gainsboro;
            this.txtEndereco.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtEndereco.HideSelection = true;
            this.txtEndereco.IconLeft = global::Nsf.FriendsLibrary.APP.Properties.Resources.Home;
            this.txtEndereco.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtEndereco.IconPadding = 4;
            this.txtEndereco.IconRight = null;
            this.txtEndereco.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.txtEndereco.Location = new System.Drawing.Point(72, 422);
            this.txtEndereco.MaxLength = 100;
            this.txtEndereco.MinimumSize = new System.Drawing.Size(100, 30);
            this.txtEndereco.Modified = false;
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.PasswordChar = '\0';
            this.txtEndereco.ReadOnly = false;
            this.txtEndereco.SelectedText = "";
            this.txtEndereco.SelectionLength = 0;
            this.txtEndereco.SelectionStart = 0;
            this.txtEndereco.ShortcutsEnabled = true;
            this.txtEndereco.Size = new System.Drawing.Size(333, 30);
            this.txtEndereco.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtEndereco.TabIndex = 19;
            this.txtEndereco.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtEndereco.TextMarginLeft = 10;
            this.txtEndereco.TextPlaceholder = "";
            this.txtEndereco.UseSystemPasswordChar = false;
            this.txtEndereco.TextChange += new System.EventHandler(this.txtEndereco_TextChange);
            this.txtEndereco.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUsuario_KeyDown);
            // 
            // txtEmail
            // 
            this.txtEmail.AcceptsReturn = false;
            this.txtEmail.AcceptsTab = false;
            this.txtEmail.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtEmail.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtEmail.BackColor = System.Drawing.Color.Gainsboro;
            this.txtEmail.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtEmail.BackgroundImage")));
            this.txtEmail.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtEmail.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtEmail.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(38)))), ((int)(((byte)(157)))));
            this.txtEmail.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtEmail.BorderRadius = 1;
            this.txtEmail.BorderThickness = 1;
            this.txtEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtEmail.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.DefaultText = "";
            this.txtEmail.FillColor = System.Drawing.Color.Gainsboro;
            this.txtEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtEmail.HideSelection = true;
            this.txtEmail.IconLeft = global::Nsf.FriendsLibrary.APP.Properties.Resources.Email1;
            this.txtEmail.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtEmail.IconPadding = 4;
            this.txtEmail.IconRight = null;
            this.txtEmail.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.txtEmail.Location = new System.Drawing.Point(72, 258);
            this.txtEmail.MaxLength = 80;
            this.txtEmail.MinimumSize = new System.Drawing.Size(100, 30);
            this.txtEmail.Modified = false;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.PasswordChar = '\0';
            this.txtEmail.ReadOnly = false;
            this.txtEmail.SelectedText = "";
            this.txtEmail.SelectionLength = 0;
            this.txtEmail.SelectionStart = 0;
            this.txtEmail.ShortcutsEnabled = true;
            this.txtEmail.Size = new System.Drawing.Size(467, 30);
            this.txtEmail.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtEmail.TabIndex = 11;
            this.txtEmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtEmail.TextMarginLeft = 10;
            this.txtEmail.TextPlaceholder = "";
            this.txtEmail.UseSystemPasswordChar = false;
            this.txtEmail.TextChange += new System.EventHandler(this.txtEmail_TextChange);
            this.txtEmail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUsuario_KeyDown);
            // 
            // txtUsuario
            // 
            this.txtUsuario.AcceptsReturn = false;
            this.txtUsuario.AcceptsTab = false;
            this.txtUsuario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtUsuario.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtUsuario.BackColor = System.Drawing.Color.Gainsboro;
            this.txtUsuario.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtUsuario.BackgroundImage")));
            this.txtUsuario.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtUsuario.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtUsuario.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(38)))), ((int)(((byte)(157)))));
            this.txtUsuario.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtUsuario.BorderRadius = 1;
            this.txtUsuario.BorderThickness = 1;
            this.txtUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtUsuario.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuario.DefaultText = "";
            this.txtUsuario.FillColor = System.Drawing.Color.Gainsboro;
            this.txtUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtUsuario.HideSelection = true;
            this.txtUsuario.IconLeft = global::Nsf.FriendsLibrary.APP.Properties.Resources.User;
            this.txtUsuario.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtUsuario.IconPadding = 4;
            this.txtUsuario.IconRight = null;
            this.txtUsuario.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.txtUsuario.Location = new System.Drawing.Point(72, 142);
            this.txtUsuario.MaxLength = 100;
            this.txtUsuario.MinimumSize = new System.Drawing.Size(100, 30);
            this.txtUsuario.Modified = false;
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.PasswordChar = '\0';
            this.txtUsuario.ReadOnly = false;
            this.txtUsuario.SelectedText = "";
            this.txtUsuario.SelectionLength = 0;
            this.txtUsuario.SelectionStart = 0;
            this.txtUsuario.ShortcutsEnabled = true;
            this.txtUsuario.Size = new System.Drawing.Size(467, 30);
            this.txtUsuario.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtUsuario.TabIndex = 5;
            this.txtUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtUsuario.TextMarginLeft = 10;
            this.txtUsuario.TextPlaceholder = "";
            this.txtUsuario.UseSystemPasswordChar = false;
            this.txtUsuario.TextChange += new System.EventHandler(this.txtUsuario_TextChange);
            this.txtUsuario.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUsuario_KeyDown);
            // 
            // btnSaveUpdate
            // 
            this.btnSaveUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveUpdate.BackColor = System.Drawing.Color.Transparent;
            this.btnSaveUpdate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSaveUpdate.BackgroundImage")));
            this.btnSaveUpdate.ButtonText = "Salvar";
            this.btnSaveUpdate.ButtonTextMarginLeft = 0;
            this.btnSaveUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSaveUpdate.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnSaveUpdate.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnSaveUpdate.DisabledForecolor = System.Drawing.Color.White;
            this.btnSaveUpdate.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveUpdate.ForeColor = System.Drawing.Color.White;
            this.btnSaveUpdate.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnSaveUpdate.IconPadding = 10;
            this.btnSaveUpdate.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnSaveUpdate.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnSaveUpdate.IdleBorderRadius = 40;
            this.btnSaveUpdate.IdleBorderThickness = 0;
            this.btnSaveUpdate.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnSaveUpdate.IdleIconLeftImage = null;
            this.btnSaveUpdate.IdleIconRightImage = null;
            this.btnSaveUpdate.Location = new System.Drawing.Point(474, 566);
            this.btnSaveUpdate.Name = "btnSaveUpdate";
            stateProperties2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(133)))), ((int)(((byte)(255)))));
            stateProperties2.BorderRadius = 40;
            stateProperties2.BorderThickness = 0;
            stateProperties2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(133)))), ((int)(((byte)(255)))));
            stateProperties2.IconLeftImage = null;
            stateProperties2.IconRightImage = null;
            this.btnSaveUpdate.onHoverState = stateProperties2;
            this.btnSaveUpdate.Size = new System.Drawing.Size(136, 45);
            this.btnSaveUpdate.TabIndex = 29;
            this.btnSaveUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSaveUpdate.Click += new System.EventHandler(this.btnSaveUpdate_Click);
            // 
            // chkAluno
            // 
            this.chkAluno.AllowBindingControlAnimation = true;
            this.chkAluno.AllowBindingControlColorChanges = false;
            this.chkAluno.AllowBindingControlLocation = true;
            this.chkAluno.AllowCheckBoxAnimation = true;
            this.chkAluno.AllowCheckmarkAnimation = true;
            this.chkAluno.AllowOnHoverStates = true;
            this.chkAluno.BackColor = System.Drawing.Color.Transparent;
            this.chkAluno.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("chkAluno.BackgroundImage")));
            this.chkAluno.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.chkAluno.BindingControl = this.lblAluno;
            this.chkAluno.BindingControlPosition = Bunifu.UI.WinForms.BunifuCheckBox.BindingControlPositions.Right;
            this.chkAluno.Checked = false;
            this.chkAluno.CheckState = Bunifu.UI.WinForms.BunifuCheckBox.CheckStates.Unchecked;
            this.chkAluno.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAluno.CustomCheckmarkImage = null;
            this.chkAluno.Location = new System.Drawing.Point(202, 507);
            this.chkAluno.MinimumSize = new System.Drawing.Size(17, 17);
            this.chkAluno.Name = "chkAluno";
            this.chkAluno.OnCheck.BorderColor = System.Drawing.Color.Crimson;
            this.chkAluno.OnCheck.BorderRadius = 2;
            this.chkAluno.OnCheck.BorderThickness = 2;
            this.chkAluno.OnCheck.CheckBoxColor = System.Drawing.Color.Crimson;
            this.chkAluno.OnCheck.CheckmarkColor = System.Drawing.Color.White;
            this.chkAluno.OnCheck.CheckmarkThickness = 2;
            this.chkAluno.OnDisable.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkAluno.OnDisable.BorderRadius = 2;
            this.chkAluno.OnDisable.BorderThickness = 2;
            this.chkAluno.OnDisable.CheckBoxColor = System.Drawing.Color.Transparent;
            this.chkAluno.OnDisable.CheckmarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkAluno.OnDisable.CheckmarkThickness = 2;
            this.chkAluno.OnHoverChecked.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(68)))), ((int)(((byte)(102)))));
            this.chkAluno.OnHoverChecked.BorderRadius = 2;
            this.chkAluno.OnHoverChecked.BorderThickness = 2;
            this.chkAluno.OnHoverChecked.CheckBoxColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(68)))), ((int)(((byte)(102)))));
            this.chkAluno.OnHoverChecked.CheckmarkColor = System.Drawing.Color.White;
            this.chkAluno.OnHoverChecked.CheckmarkThickness = 2;
            this.chkAluno.OnHoverUnchecked.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(45)))), ((int)(((byte)(83)))));
            this.chkAluno.OnHoverUnchecked.BorderRadius = 2;
            this.chkAluno.OnHoverUnchecked.BorderThickness = 2;
            this.chkAluno.OnHoverUnchecked.CheckBoxColor = System.Drawing.Color.Transparent;
            this.chkAluno.OnUncheck.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkAluno.OnUncheck.BorderRadius = 2;
            this.chkAluno.OnUncheck.BorderThickness = 2;
            this.chkAluno.OnUncheck.CheckBoxColor = System.Drawing.Color.Gainsboro;
            this.chkAluno.Size = new System.Drawing.Size(26, 26);
            this.chkAluno.Style = Bunifu.UI.WinForms.BunifuCheckBox.CheckBoxStyles.Bunifu;
            this.chkAluno.TabIndex = 26;
            this.chkAluno.ThreeState = false;
            this.chkAluno.ToolTipText = null;
            this.chkAluno.Click += new System.EventHandler(this.chkProfessor_Click);
            this.chkAluno.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUsuario_KeyDown);
            // 
            // chkProfessor
            // 
            this.chkProfessor.AllowBindingControlAnimation = true;
            this.chkProfessor.AllowBindingControlColorChanges = false;
            this.chkProfessor.AllowBindingControlLocation = true;
            this.chkProfessor.AllowCheckBoxAnimation = true;
            this.chkProfessor.AllowCheckmarkAnimation = true;
            this.chkProfessor.AllowOnHoverStates = true;
            this.chkProfessor.BackColor = System.Drawing.Color.Transparent;
            this.chkProfessor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("chkProfessor.BackgroundImage")));
            this.chkProfessor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.chkProfessor.BindingControl = this.lblProfessor;
            this.chkProfessor.BindingControlPosition = Bunifu.UI.WinForms.BunifuCheckBox.BindingControlPositions.Right;
            this.chkProfessor.Checked = false;
            this.chkProfessor.CheckState = Bunifu.UI.WinForms.BunifuCheckBox.CheckStates.Unchecked;
            this.chkProfessor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkProfessor.CustomCheckmarkImage = null;
            this.chkProfessor.Location = new System.Drawing.Point(72, 507);
            this.chkProfessor.MinimumSize = new System.Drawing.Size(17, 17);
            this.chkProfessor.Name = "chkProfessor";
            this.chkProfessor.OnCheck.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(113)))), ((int)(((byte)(242)))));
            this.chkProfessor.OnCheck.BorderRadius = 2;
            this.chkProfessor.OnCheck.BorderThickness = 2;
            this.chkProfessor.OnCheck.CheckBoxColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(113)))), ((int)(((byte)(242)))));
            this.chkProfessor.OnCheck.CheckmarkColor = System.Drawing.Color.White;
            this.chkProfessor.OnCheck.CheckmarkThickness = 2;
            this.chkProfessor.OnDisable.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkProfessor.OnDisable.BorderRadius = 2;
            this.chkProfessor.OnDisable.BorderThickness = 2;
            this.chkProfessor.OnDisable.CheckBoxColor = System.Drawing.Color.Transparent;
            this.chkProfessor.OnDisable.CheckmarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkProfessor.OnDisable.CheckmarkThickness = 2;
            this.chkProfessor.OnHoverChecked.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(151)))), ((int)(((byte)(246)))));
            this.chkProfessor.OnHoverChecked.BorderRadius = 2;
            this.chkProfessor.OnHoverChecked.BorderThickness = 2;
            this.chkProfessor.OnHoverChecked.CheckBoxColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(151)))), ((int)(((byte)(246)))));
            this.chkProfessor.OnHoverChecked.CheckmarkColor = System.Drawing.Color.White;
            this.chkProfessor.OnHoverChecked.CheckmarkThickness = 2;
            this.chkProfessor.OnHoverUnchecked.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(138)))), ((int)(((byte)(245)))));
            this.chkProfessor.OnHoverUnchecked.BorderRadius = 2;
            this.chkProfessor.OnHoverUnchecked.BorderThickness = 2;
            this.chkProfessor.OnHoverUnchecked.CheckBoxColor = System.Drawing.Color.Transparent;
            this.chkProfessor.OnUncheck.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkProfessor.OnUncheck.BorderRadius = 2;
            this.chkProfessor.OnUncheck.BorderThickness = 2;
            this.chkProfessor.OnUncheck.CheckBoxColor = System.Drawing.Color.Gainsboro;
            this.chkProfessor.Size = new System.Drawing.Size(26, 26);
            this.chkProfessor.Style = Bunifu.UI.WinForms.BunifuCheckBox.CheckBoxStyles.Bunifu;
            this.chkProfessor.TabIndex = 24;
            this.chkProfessor.ThreeState = false;
            this.chkProfessor.ToolTipText = null;
            this.chkProfessor.Click += new System.EventHandler(this.chkProfessor_Click);
            this.chkProfessor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUsuario_KeyDown);
            // 
            // txtData
            // 
            this.txtData.BorderRadius = 1;
            this.txtData.Color = System.Drawing.Color.Gainsboro;
            this.txtData.CustomFormat = "dd/MM/yyyy";
            this.txtData.DateBorderThickness = Bunifu.UI.WinForms.BunifuDatePicker.BorderThickness.Thick;
            this.txtData.DateTextAlign = Bunifu.UI.WinForms.BunifuDatePicker.TextAlign.Left;
            this.txtData.DisabledColor = System.Drawing.Color.Gray;
            this.txtData.DisplayWeekNumbers = false;
            this.txtData.DPHeight = 0;
            this.txtData.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.txtData.FillDatePicker = true;
            this.txtData.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtData.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtData.Icon = ((System.Drawing.Image)(resources.GetObject("txtData.Icon")));
            this.txtData.IconColor = System.Drawing.Color.Gainsboro;
            this.txtData.IconLocation = Bunifu.UI.WinForms.BunifuDatePicker.Indicator.Right;
            this.txtData.Location = new System.Drawing.Point(310, 200);
            this.txtData.MinDate = new System.DateTime(1960, 1, 1, 0, 0, 0, 0);
            this.txtData.MinimumSize = new System.Drawing.Size(220, 30);
            this.txtData.Name = "txtData";
            this.txtData.Size = new System.Drawing.Size(229, 30);
            this.txtData.TabIndex = 230;
            this.txtData.ValueChanged += new System.EventHandler(this.txtData_ValueChanged);
            this.txtData.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUsuario_KeyDown);
            // 
            // imageListIconValidator
            // 
            this.imageListIconValidator.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListIconValidator.ImageStream")));
            this.imageListIconValidator.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListIconValidator.Images.SetKeyName(0, "Checkmark.png");
            this.imageListIconValidator.Images.SetKeyName(1, "Error.png");
            // 
            // frmAddUpdateUser
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.txtData);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.txtCidade);
            this.Controls.Add(this.txtNumero);
            this.Controls.Add(this.txtCPF);
            this.Controls.Add(this.txtCEP);
            this.Controls.Add(this.txtEndereco);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtUsuario);
            this.Controls.Add(this.btnSaveUpdate);
            this.Controls.Add(this.chkAluno);
            this.Controls.Add(this.chkProfessor);
            this.Controls.Add(this.lblCPF);
            this.Controls.Add(this.lblData);
            this.Controls.Add(this.lblCEP);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblNumero);
            this.Controls.Add(this.lblCondicao);
            this.Controls.Add(this.lblAluno);
            this.Controls.Add(this.lblEndereco);
            this.Controls.Add(this.lblDados);
            this.Controls.Add(this.lblProfessor);
            this.Controls.Add(this.lblAdress);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblUsuario);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnSeparatorEndereco);
            this.Controls.Add(this.pnSeparatorCondicao);
            this.Controls.Add(this.pnSeparatorDados);
            this.Controls.Add(this.pnSeparator1);
            this.Controls.Add(this.lblTitulo);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmAddUpdateUser";
            this.Size = new System.Drawing.Size(625, 632);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnSeparator1;
        private System.Windows.Forms.Label lblTitulo;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnSaveUpdate;
        private Bunifu.UI.WinForms.BunifuCheckBox chkProfessor;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblCEP;
        private System.Windows.Forms.Label lblAdress;
        private System.Windows.Forms.Label lblNumero;
        private System.Windows.Forms.Label lblProfessor;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtUsuario;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtEmail;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtCEP;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtNumero;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtEndereco;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnCancelar;
        private System.Windows.Forms.Label lblDados;
        private System.Windows.Forms.Label lblCondicao;
        private System.Windows.Forms.Label lblAluno;
        private Bunifu.UI.WinForms.BunifuCheckBox chkAluno;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.Label lblCPF;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtCPF;
        private System.Windows.Forms.Panel pnSeparatorDados;
        private System.Windows.Forms.Panel pnSeparatorCondicao;
        private System.Windows.Forms.Panel pnSeparatorEndereco;
        private System.Windows.Forms.Label lblEndereco;
        private System.Windows.Forms.Label label1;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtCidade;
        private Bunifu.UI.WinForms.BunifuDatePicker txtData;
        private System.Windows.Forms.ImageList imageListIconValidator;
    }
}
