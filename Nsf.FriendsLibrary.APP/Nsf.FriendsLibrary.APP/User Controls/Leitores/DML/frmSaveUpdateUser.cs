﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf.FriendsLibrary.DB;
using Bunifu.UI.WinForms;
using Biblioteca.Banco.Database.Usuario;
using Biblioteca.Utilitarios.Notificacoes;
using Nsf.FriendsLibrary.Tools.Validações;
using Bunifu.UI.WinForms.BunifuTextbox;
using Nsf.FriendsLibrary.Tools.Utilitários.Corrreios;
using Biblioteca.Utilitarios.Email;
using Biblioteca.Validacoes;
using Biblioteca;

namespace Nsf.FriendsLibrary.APP.User_Controls.Usuario
{
    public partial class frmAddUpdateUser : UserControl
    {
        public frmAddUpdateUser()
        {
            InitializeComponent();
            lblTitulo.Text = "Registre novos Leitores";
        }

        #region LOADSCREEN - Alteração dos Dados

        //Objeto de escopo de classe
        tb_leitor user = null;
        public void LoadScreen(tb_leitor dto)
        {
            //Passando o valor encotrando no paramêtro para o obejto de escopo
            this.user = dto;
            lblTitulo.Text = "Altere as informações dos Leitores";
            btnSaveUpdate.ButtonText = "Editar";

            //Dados Pessoais
            txtUsuario.Text = dto.nm_leitor;
            txtEmail.Text = dto.ds_email;
            txtCPF.Text = dto.ds_cpf;
            txtData.Value = dto.dt_nascimento;

            //Endereço
            txtCEP.Text = dto.ds_cep;
            txtCidade.Text = dto.ds_cidade;
            txtEndereco.Text = dto.ds_endereco;
            txtNumero.Text = dto.nr_numero.ToString();

            //Status na Instituição
            chkAluno.Checked = dto.bl_estudante;
            chkProfessor.Checked = dto.bl_professor;
        }
        #endregion

        #region CRUD - Carregando dados e executando no banco
        private tb_leitor LoadControlsFromForm(tb_leitor controls)
        {
            //Dados Pessoais
            controls.ds_cpf = txtCPF.Text;
            controls.ds_email = txtEmail.Text;
            controls.nm_leitor = txtUsuario.Text;
            controls.dt_nascimento = txtData.Value;

            //Endereço
            controls.ds_cep = txtCEP.Text;
            controls.ds_cidade = txtCidade.Text;
            controls.ds_endereco = txtEndereco.Text;
            controls.nr_numero = Convert.ToInt32(txtNumero.Text);

            //Status na Instituição
            controls.bl_estudante = chkAluno.Checked;
            controls.bl_professor = chkProfessor.Checked;

            //Retorno com os valores já carregados
            return controls;
        }

        private tb_leitor SaveControls()
        {
            //Chamo o objeto de espoco de tela
            user = new tb_leitor();

            //Chamo o método que irá carregar todos os controles presentes na tela
            var retorno = this.LoadControlsFromForm(user);

            //Retorno os valores já carregados
            return retorno;
        }

        private tb_leitor UpdateControls()
        {
            using (LibraryEntities context = new LibraryEntities())
            {
                //Faço uma procura pelo o ID passado pela variável de escopo
                user = context.tb_leitor.Where(c => c.id_leitor == user.id_leitor).FirstOrDefault();

                //Carregos os controles com os seus respectivos dados
                var retorno = this.LoadControlsFromForm(user);

                //Retorno os valroes já carregados
                return retorno;
            }
        }

        //Variáveis de escopo que serão utilizadas para mostrar a notificação ao usário
        Image imagem;
        string texto;
        private async void btnSaveUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (lblTitulo.Text == "Registre novos Leitores") //MÉTODO DE SALVAR
                {
                    //Chamo o método que carregará os dados nos controles, e recupero o valor em uma variável
                    var saveControls = SaveControls();

                    //Instancio a Business
                    LeitorBusiness db = new LeitorBusiness();
                    db.Save(saveControls);

                    //PopUp para notificação
                    texto = @"Novo leitor registrado na base de dados com êxito!";
                    imagem = Properties.Resources.ThumpUP;
                    await Task.Run(() =>  this.SelectorClass(1, texto, imagem));

                    //Envio um Email para o récem-leitor
                    EnvioEmail sendingEmail = new EnvioEmail();
                    string leitor = txtUsuario.Text;
                    string email = txtEmail.Text;
                    sendingEmail.EnviarEmail(email, leitor, 2);
                }
                else//MÉTODO DE ALTERAR
                {
                    //Carrego os dados / Entrada de dados
                    var updateControls = UpdateControls();

                    //Processamento das informações. Aqui ocorre a parte de manipulação com o banco de dados
                    LeitorBusiness db = new LeitorBusiness();
                    db.Update(updateControls);

                    //PopUp para notificação / Saída de dados
                    texto = @"As informações do leitor foram atualizadas na base de dados com êxito!";
                    imagem = Properties.Resources.Warning;
                    this.SelectorClass(2, texto, imagem);
                }

            }
            catch (ArgumentException business)
            {
                MessageBox.Show(business.Message,
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro" + "/n" + ex.Message + "/n" + "Tente mais tarde.",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }
        #endregion

        #region LIMPAR CONTROLES

        bool validar = false;
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                validar = true;

                frmAddUpdateUser cleanControl = new frmAddUpdateUser();
                frmMain.UserControl.CleanControl(cleanControl);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro" + ex.Message + "/n" + "Tente mais tarde.",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }
        #endregion

        #region CHECKBOX - Utilizando uma condição para desabilitar um dos dois Checkbox
        private void chkProfessor_Click(object sender, EventArgs e)
        {
            if (chkProfessor.Checked == false && chkAluno.Checked == false)
            {
                chkAluno.Enabled = true;
                chkProfessor.Enabled = true;
            }

            if (chkProfessor.Checked == true)
            {
                chkAluno.Enabled = false;
                chkProfessor.Enabled = true;
            }
            else if (chkAluno.Checked == true)
            {
                chkProfessor.Enabled = false;
                chkAluno.Enabled = true;
            }

        }
        #endregion

        #region CEP - Conversando com a API do correios afim de pegar informações
        private void txtCEP_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtCEP.Text.Length > 0)
                {
                    //Instancio o objeto que fará contanto com a Api do correios
                    CorreiosAPI correiosApi = new CorreiosAPI();

                    //Pego o cep digitado pelo usuário
                    string cep = txtCEP.Text;

                    // Chama função BuscarAPICorreio
                    CorreiosResponse correio = correiosApi.BuscarDados(cep);

                    // Altera os valores dos textbox com a resposta do correio
                    txtEndereco.Text = correio.Logradouro;
                    txtCidade.Text = correio.Localidade;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + "\n" + ex.Message + "\n" + "Verique a internet ou tente mais tarde!!",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        #endregion

        #region VALIDAÇÃO BUNIFUTEXTBOX 
        private Image GetImage(bool valid)
        {
            if (valid)
                return imageListIconValidator.Images[0];
            else
                return imageListIconValidator.Images[1];
        }

        private void txtUsuario_TextChange(object sender, EventArgs e)
        {
            validar = false;
            if (validar == false)
                txtUsuario.IconRight = GetImage(ValidarIcones.IsBunifuTextBoxEmpty(txtUsuario.Text));
        }

        private void txtCPF_TextChange(object sender, EventArgs e)
        {
            validar = false;
            if (validar == false)
                txtCPF.IconRight = GetImage(ValidarIcones.IsBunifuCPFValid(txtCPF.Text));
        }

        private void txtEmail_TextChange(object sender, EventArgs e)
        {
            validar = false;
            if (validar == false)
                txtEmail.IconRight = GetImage(ValidarIcones.IsBunifuEmailValid(txtEmail.Text));
        }

        private void txtCEP_TextChange(object sender, EventArgs e)
        {
            validar = false;
            if (validar == false)
                txtCEP.IconRight = GetImage(ValidarIcones.IsBunifuCEPValid(txtCEP.Text));
        }

        private void txtCidade_TextChange(object sender, EventArgs e)
        {
            validar = false;
            if (validar == false)
                txtCidade.IconRight = GetImage(ValidarIcones.IsBunifuTextBoxEmpty(txtCidade.Text));
        }

        private void txtEndereco_TextChange(object sender, EventArgs e)
        {
            validar = false;
            if (validar == false)
                txtEndereco.IconRight = GetImage(ValidarIcones.IsBunifuTextBoxEmpty(txtEndereco.Text));
        }

        private void txtNumero_TextChange(object sender, EventArgs e)
        {
            validar = false;
            if (validar == false)
                txtNumero.IconRight = GetImage(ValidarIcones.IsBunifuNumber(txtNumero.Text));
        }

        private void txtCPF_KeyPress(object sender, KeyPressEventArgs e)
        {
            validar = false;
            if (validar == false)
                ValidarTextbox.MascaraCPF(txtCPF, e);
        }

        private void txtCEP_KeyPress(object sender, KeyPressEventArgs e)
        {
            validar = false;
            if (validar == false)
                ValidarTextbox.MascaraCEP(txtCEP, e);
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            validar = false;
            if (validar == false)
                ValidarTextbox.SoNumero(e);
        }

        private void txtData_ValueChanged(object sender, EventArgs e)
        {
            validar = false;
            if (validar == false)
                txtData.Icon = GetImage(ValidarIcones.IsFourteenYears(txtData.Value));
        }

        #endregion

        private void txtUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnSaveUpdate_Click(null, null);
        }
    }
}
