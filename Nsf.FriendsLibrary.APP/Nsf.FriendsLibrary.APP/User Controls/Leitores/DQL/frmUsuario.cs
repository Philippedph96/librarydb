﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KimtToo.VisualReactive;
using Biblioteca.Banco.Database.Usuario;
using Nsf.FriendsLibrary.DB;
using Biblioteca;
using Biblioteca.Utilitarios.Notificacoes;
using PagedList;

namespace Nsf.FriendsLibrary.APP.User_Controls.Usuario
{
    public partial class frmUsuario : UserControl
    {
        public static frmUsuario User;

        int pageNumber = 1;
        IPagedList<tb_leitor> list;

        public frmUsuario()
        {
            User = this;
            InitializeComponent();
            this.PopulateGrid();
        }

        //Habilita a navegação do TAB CONTROL a partir do CHECKBOX selecionado
        private void chkTitulo_Click(object sender, EventArgs e)
        {
            VSReactive<int>.SetState("user", int.Parse(((Control)sender).Tag.ToString()));
        }

        private void ConfigGrid(DataGridView dgv, List<tb_leitor> valor)
        {
            if (dgv.InvokeRequired)
                dgv.Invoke((MethodInvoker)delegate
                {
                    dgv.AutoGenerateColumns = false;
                    dgv.DataSource = valor;
                });
        }

        private async Task<IPagedList<tb_leitor>> GetPagedListAsync(int pageNumber = 1, int pageSize = 8)
        {
            return await Task.Factory.StartNew(() =>
            {
                LeitorBusiness db = new LeitorBusiness();
                var valor = db.List();
                return valor.ToPagedList(pageNumber, pageSize);
            });

        }

        private void PopulateGrid()
        {
            try
            {
                LeitorBusiness db = new LeitorBusiness();
                var list = db.List();

                dgvUsuario.AutoGenerateColumns = false;
                dgvUsuario.DataSource = list;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + "\n\n" + "Verique a internet ou tente mais tarde!!",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        public void PopulateGridWithReaderFilter(string nome)
        {
            LeitorBusiness db = new LeitorBusiness();
            var filter = db.FilterReader(nome);
            this.ConfigGrid(dgvUsuario, filter);
        }

        public void PopulateGridWithCPFFilter(string cpf)
        {
            LeitorBusiness db = new LeitorBusiness();
            var filter = db.FilterCPF(cpf);
            this.ConfigGrid(dgvUsuario, filter);
        }

        private tb_leitor CatchGridRegisters()
        {
            var leitor = dgvUsuario.CurrentRow.DataBoundItem as tb_leitor;
            return leitor;
        }

        private void RemoveRegistersFromGrid()
        {
            var delete = CatchGridRegisters();

            LeitorBusiness db = new LeitorBusiness();
            db.Remove(delete.id_leitor);

            this.PopulateGrid();
        }

        private async void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                var update = this.CatchGridRegisters();

                frmAddUpdateUser usercontrol = new frmAddUpdateUser();
                usercontrol.LoadScreen(update);

                frmMain.UserControl.OpenUserControl(usercontrol);
                await GetPagedListAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + "\n\n" + "Verique a internet ou tente mais tarde!!",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private async void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                
                //Processamento dos dados
                this.RemoveRegistersFromGrid();

                //Saída de dados
                Image imagem = Properties.Resources.Delete;
                string texto = "As informações do Usuário foram removidas com êxito!";
                await Task.Run(() =>  this.SelectorClass(3, texto, imagem));

                //Atualização da gridview
                await GetPagedListAsync();
            }
            catch (ArgumentException argumentException)
            {
                MessageBox.Show(argumentException.Message,
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + "\n\n" + "Verique a internet ou tente mais tarde!!",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private async void frmUsuario_Load(object sender, EventArgs e)
        {
            try
            {
                list = await GetPagedListAsync();

                btnProximo.Enabled = list.HasNextPage;
                btnAnterior.Enabled = list.HasPreviousPage;

                dgvUsuario.DataSource = list.ToList();
                lblNumberPages.Text = string.Format("{0}/{1}", pageNumber, list.PageCount);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + "\n\n" + "Verique a internet ou tente mais tarde!!",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }

        }

        private async void btnAnterior_Click(object sender, EventArgs e)
        {
            try
            {
                list = await GetPagedListAsync(--pageNumber);

                btnProximo.Enabled = list.HasNextPage;
                btnAnterior.Enabled = list.HasPreviousPage;

                dgvUsuario.DataSource = list.ToList();
                lblNumberPages.Text = string.Format("{0}/{1}", pageNumber, list.PageCount);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + "\n\n" + "Verique a internet ou tente mais tarde!!",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private async void btnProximo_Click(object sender, EventArgs e)
        {
            try
            {
                list = await GetPagedListAsync(++pageNumber);

                btnProximo.Enabled = list.HasNextPage;
                btnAnterior.Enabled = list.HasPreviousPage;

                dgvUsuario.DataSource = list.ToList();
                lblNumberPages.Text = string.Format("{0}/{1}", pageNumber, list.PageCount);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + "\n\n" + "Verique a internet ou tente mais tarde!!",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }

}
    }
}
