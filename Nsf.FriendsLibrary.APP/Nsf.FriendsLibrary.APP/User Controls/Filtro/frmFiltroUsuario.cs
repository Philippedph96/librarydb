﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KimtToo.VisualReactive;
using Biblioteca;
using Nsf.FriendsLibrary.Tools.Validações;
using Nsf.FriendsLibrary.APP.User_Controls.Usuario;

namespace Nsf.FriendsLibrary.APP.User_Controls.Filtro
{
    public partial class frmFiltroUsuario : UserControl
    {
        public frmFiltroUsuario()
        {
            InitializeComponent();

            if (Program.IsInDesignMode()) return;
            pnHider.Height = 28;

            VSReactive<int>.Subscribe("user", e => tcbFiltroBanido.SelectedIndex = e);
        }

        private Image GetImage (bool valid)
        {
            if (valid)
                return imageListIconValidator.Images[0];
            else
                return imageListIconValidator.Images[1];
        }
        //Validação com ícone a direita
        private void txtPesquisaUsuario_TextChange(object sender, EventArgs e)
        {
            txtCPF.IconRight = GetImage(ValidarIcones.IsBunifuTextBoxEmpty(txtCPF.Text));
        }

        //Validação com ícone a direita
        private void txtUsuario_TextChange(object sender, EventArgs e)
        {
            txtUsuario.IconRight = GetImage(ValidarIcones.IsBunifuTextBoxEmpty(txtUsuario.Text));
        }

        //Pesquisa a partir das informações inseridas na BunifuTextBox
        private void txtUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            frmUsuario.User.PopulateGridWithReaderFilter(txtUsuario.Text);
        }

        private void txtCPF_KeyPress(object sender, KeyPressEventArgs e)
        {
            frmUsuario.User.PopulateGridWithReaderFilter(txtCPF.Text);
        }
    }
}
