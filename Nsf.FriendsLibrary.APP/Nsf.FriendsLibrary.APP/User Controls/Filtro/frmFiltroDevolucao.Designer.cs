﻿namespace Nsf.FriendsLibrary.APP.User_Controls.Filtro
{
    partial class frmFiltroDevolucao
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFiltroDevolucao));
            this.imageListIconValidator = new System.Windows.Forms.ImageList(this.components);
            this.pnHider = new System.Windows.Forms.Panel();
            this.tpgLeitor = new System.Windows.Forms.TabPage();
            this.txtPesquisaLeitor = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.tpgLivro = new System.Windows.Forms.TabPage();
            this.txtPesquisaLivro = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.tcbFiltroEmprestimo = new System.Windows.Forms.TabControl();
            this.tpgEmprestimo = new System.Windows.Forms.TabPage();
            this.btnRemove = new Bunifu.Framework.UI.BunifuImageButton();
            this.dtpDevolucao = new Bunifu.UI.WinForms.BunifuDatePicker();
            this.lblEmprestimo = new System.Windows.Forms.Label();
            this.tpgLeitor.SuspendLayout();
            this.tpgLivro.SuspendLayout();
            this.tcbFiltroEmprestimo.SuspendLayout();
            this.tpgEmprestimo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemove)).BeginInit();
            this.SuspendLayout();
            // 
            // imageListIconValidator
            // 
            this.imageListIconValidator.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListIconValidator.ImageStream")));
            this.imageListIconValidator.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListIconValidator.Images.SetKeyName(0, "Checkmark.png");
            this.imageListIconValidator.Images.SetKeyName(1, "Error.png");
            // 
            // pnHider
            // 
            this.pnHider.BackColor = System.Drawing.Color.White;
            this.pnHider.Location = new System.Drawing.Point(-1, 1);
            this.pnHider.Name = "pnHider";
            this.pnHider.Size = new System.Drawing.Size(409, 1);
            this.pnHider.TabIndex = 1;
            // 
            // tpgLeitor
            // 
            this.tpgLeitor.Controls.Add(this.txtPesquisaLeitor);
            this.tpgLeitor.Location = new System.Drawing.Point(4, 26);
            this.tpgLeitor.Name = "tpgLeitor";
            this.tpgLeitor.Padding = new System.Windows.Forms.Padding(3);
            this.tpgLeitor.Size = new System.Drawing.Size(405, 60);
            this.tpgLeitor.TabIndex = 2;
            this.tpgLeitor.Tag = "";
            this.tpgLeitor.Text = "1";
            this.tpgLeitor.UseVisualStyleBackColor = true;
            // 
            // txtPesquisaLeitor
            // 
            this.txtPesquisaLeitor.AcceptsReturn = false;
            this.txtPesquisaLeitor.AcceptsTab = false;
            this.txtPesquisaLeitor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtPesquisaLeitor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtPesquisaLeitor.BackColor = System.Drawing.Color.Transparent;
            this.txtPesquisaLeitor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtPesquisaLeitor.BackgroundImage")));
            this.txtPesquisaLeitor.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.txtPesquisaLeitor.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtPesquisaLeitor.BorderColorHover = System.Drawing.Color.DodgerBlue;
            this.txtPesquisaLeitor.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtPesquisaLeitor.BorderRadius = 10;
            this.txtPesquisaLeitor.BorderThickness = 1;
            this.txtPesquisaLeitor.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtPesquisaLeitor.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPesquisaLeitor.DefaultText = "";
            this.txtPesquisaLeitor.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txtPesquisaLeitor.HideSelection = true;
            this.txtPesquisaLeitor.IconLeft = null;
            this.txtPesquisaLeitor.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtPesquisaLeitor.IconPadding = 8;
            this.txtPesquisaLeitor.IconRight = null;
            this.txtPesquisaLeitor.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtPesquisaLeitor.Location = new System.Drawing.Point(8, 3);
            this.txtPesquisaLeitor.MaxLength = 70;
            this.txtPesquisaLeitor.MinimumSize = new System.Drawing.Size(100, 25);
            this.txtPesquisaLeitor.Modified = false;
            this.txtPesquisaLeitor.Name = "txtPesquisaLeitor";
            this.txtPesquisaLeitor.PasswordChar = '\0';
            this.txtPesquisaLeitor.ReadOnly = false;
            this.txtPesquisaLeitor.SelectedText = "";
            this.txtPesquisaLeitor.SelectionLength = 0;
            this.txtPesquisaLeitor.SelectionStart = 0;
            this.txtPesquisaLeitor.ShortcutsEnabled = true;
            this.txtPesquisaLeitor.Size = new System.Drawing.Size(376, 35);
            this.txtPesquisaLeitor.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtPesquisaLeitor.TabIndex = 14;
            this.txtPesquisaLeitor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtPesquisaLeitor.TextMarginLeft = 10;
            this.txtPesquisaLeitor.TextPlaceholder = "Pesquise pelos leitores";
            this.txtPesquisaLeitor.UseSystemPasswordChar = false;
            this.txtPesquisaLeitor.TextChange += new System.EventHandler(this.txtPesquisaLeitor_TextChange);
            this.txtPesquisaLeitor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPesquisaLeitor_KeyPress);
            // 
            // tpgLivro
            // 
            this.tpgLivro.Controls.Add(this.txtPesquisaLivro);
            this.tpgLivro.Location = new System.Drawing.Point(4, 26);
            this.tpgLivro.Name = "tpgLivro";
            this.tpgLivro.Padding = new System.Windows.Forms.Padding(3);
            this.tpgLivro.Size = new System.Drawing.Size(405, 60);
            this.tpgLivro.TabIndex = 1;
            this.tpgLivro.Tag = "";
            this.tpgLivro.Text = "0";
            this.tpgLivro.UseVisualStyleBackColor = true;
            // 
            // txtPesquisaLivro
            // 
            this.txtPesquisaLivro.AcceptsReturn = false;
            this.txtPesquisaLivro.AcceptsTab = false;
            this.txtPesquisaLivro.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtPesquisaLivro.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtPesquisaLivro.BackColor = System.Drawing.Color.Transparent;
            this.txtPesquisaLivro.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtPesquisaLivro.BackgroundImage")));
            this.txtPesquisaLivro.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.txtPesquisaLivro.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtPesquisaLivro.BorderColorHover = System.Drawing.Color.DodgerBlue;
            this.txtPesquisaLivro.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtPesquisaLivro.BorderRadius = 10;
            this.txtPesquisaLivro.BorderThickness = 1;
            this.txtPesquisaLivro.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtPesquisaLivro.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPesquisaLivro.DefaultText = "";
            this.txtPesquisaLivro.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txtPesquisaLivro.HideSelection = true;
            this.txtPesquisaLivro.IconLeft = null;
            this.txtPesquisaLivro.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtPesquisaLivro.IconPadding = 8;
            this.txtPesquisaLivro.IconRight = null;
            this.txtPesquisaLivro.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtPesquisaLivro.Location = new System.Drawing.Point(8, 3);
            this.txtPesquisaLivro.MaxLength = 70;
            this.txtPesquisaLivro.MinimumSize = new System.Drawing.Size(100, 25);
            this.txtPesquisaLivro.Modified = false;
            this.txtPesquisaLivro.Name = "txtPesquisaLivro";
            this.txtPesquisaLivro.PasswordChar = '\0';
            this.txtPesquisaLivro.ReadOnly = false;
            this.txtPesquisaLivro.SelectedText = "";
            this.txtPesquisaLivro.SelectionLength = 0;
            this.txtPesquisaLivro.SelectionStart = 0;
            this.txtPesquisaLivro.ShortcutsEnabled = true;
            this.txtPesquisaLivro.Size = new System.Drawing.Size(378, 35);
            this.txtPesquisaLivro.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtPesquisaLivro.TabIndex = 11;
            this.txtPesquisaLivro.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtPesquisaLivro.TextMarginLeft = 10;
            this.txtPesquisaLivro.TextPlaceholder = "Pesquise pelos dos livros";
            this.txtPesquisaLivro.UseSystemPasswordChar = false;
            this.txtPesquisaLivro.TextChange += new System.EventHandler(this.txtPesquisaLivro_TextChange);
            this.txtPesquisaLivro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPesquisaLivro_KeyPress_1);
            // 
            // tcbFiltroEmprestimo
            // 
            this.tcbFiltroEmprestimo.Controls.Add(this.tpgLivro);
            this.tcbFiltroEmprestimo.Controls.Add(this.tpgLeitor);
            this.tcbFiltroEmprestimo.Controls.Add(this.tpgEmprestimo);
            this.tcbFiltroEmprestimo.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcbFiltroEmprestimo.Location = new System.Drawing.Point(-6, 3);
            this.tcbFiltroEmprestimo.Name = "tcbFiltroEmprestimo";
            this.tcbFiltroEmprestimo.SelectedIndex = 0;
            this.tcbFiltroEmprestimo.Size = new System.Drawing.Size(413, 90);
            this.tcbFiltroEmprestimo.TabIndex = 2;
            // 
            // tpgEmprestimo
            // 
            this.tpgEmprestimo.Controls.Add(this.btnRemove);
            this.tpgEmprestimo.Controls.Add(this.dtpDevolucao);
            this.tpgEmprestimo.Controls.Add(this.lblEmprestimo);
            this.tpgEmprestimo.Location = new System.Drawing.Point(4, 26);
            this.tpgEmprestimo.Name = "tpgEmprestimo";
            this.tpgEmprestimo.Padding = new System.Windows.Forms.Padding(3);
            this.tpgEmprestimo.Size = new System.Drawing.Size(405, 60);
            this.tpgEmprestimo.TabIndex = 3;
            this.tpgEmprestimo.Text = "2";
            this.tpgEmprestimo.UseVisualStyleBackColor = true;
            // 
            // btnRemove
            // 
            this.btnRemove.BackColor = System.Drawing.Color.White;
            this.btnRemove.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRemove.Image = global::Nsf.FriendsLibrary.APP.Properties.Resources.Search;
            this.btnRemove.ImageActive = null;
            this.btnRemove.Location = new System.Drawing.Point(354, 6);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(36, 41);
            this.btnRemove.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnRemove.TabIndex = 230;
            this.btnRemove.TabStop = false;
            this.btnRemove.Zoom = 10;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // dtpDevolucao
            // 
            this.dtpDevolucao.BorderRadius = 1;
            this.dtpDevolucao.Color = System.Drawing.Color.WhiteSmoke;
            this.dtpDevolucao.DateBorderThickness = Bunifu.UI.WinForms.BunifuDatePicker.BorderThickness.Thick;
            this.dtpDevolucao.DateTextAlign = Bunifu.UI.WinForms.BunifuDatePicker.TextAlign.Left;
            this.dtpDevolucao.DisabledColor = System.Drawing.Color.Gray;
            this.dtpDevolucao.DisplayWeekNumbers = false;
            this.dtpDevolucao.DPHeight = 0;
            this.dtpDevolucao.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpDevolucao.FillDatePicker = true;
            this.dtpDevolucao.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDevolucao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dtpDevolucao.Icon = ((System.Drawing.Image)(resources.GetObject("dtpDevolucao.Icon")));
            this.dtpDevolucao.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dtpDevolucao.IconLocation = Bunifu.UI.WinForms.BunifuDatePicker.Indicator.Right;
            this.dtpDevolucao.Location = new System.Drawing.Point(24, 24);
            this.dtpDevolucao.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtpDevolucao.MinimumSize = new System.Drawing.Size(324, 30);
            this.dtpDevolucao.Name = "dtpDevolucao";
            this.dtpDevolucao.Size = new System.Drawing.Size(324, 30);
            this.dtpDevolucao.TabIndex = 229;
            // 
            // lblEmprestimo
            // 
            this.lblEmprestimo.AutoSize = true;
            this.lblEmprestimo.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmprestimo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblEmprestimo.Location = new System.Drawing.Point(20, 2);
            this.lblEmprestimo.Name = "lblEmprestimo";
            this.lblEmprestimo.Size = new System.Drawing.Size(111, 19);
            this.lblEmprestimo.TabIndex = 228;
            this.lblEmprestimo.Text = "Data Devolução:";
            // 
            // frmFiltroDevolucao
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.pnHider);
            this.Controls.Add(this.tcbFiltroEmprestimo);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmFiltroDevolucao";
            this.Size = new System.Drawing.Size(402, 76);
            this.tpgLeitor.ResumeLayout(false);
            this.tpgLivro.ResumeLayout(false);
            this.tcbFiltroEmprestimo.ResumeLayout(false);
            this.tpgEmprestimo.ResumeLayout(false);
            this.tpgEmprestimo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemove)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ImageList imageListIconValidator;
        private System.Windows.Forms.Panel pnHider;
        private System.Windows.Forms.TabPage tpgLeitor;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtPesquisaLeitor;
        private System.Windows.Forms.TabPage tpgLivro;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtPesquisaLivro;
        private System.Windows.Forms.TabControl tcbFiltroEmprestimo;
        private System.Windows.Forms.TabPage tpgEmprestimo;
        private System.Windows.Forms.Label lblEmprestimo;
        private Bunifu.UI.WinForms.BunifuDatePicker dtpDevolucao;
        private Bunifu.Framework.UI.BunifuImageButton btnRemove;
    }
}
