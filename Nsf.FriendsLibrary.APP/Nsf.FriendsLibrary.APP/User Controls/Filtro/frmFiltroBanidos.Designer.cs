﻿namespace Nsf.FriendsLibrary.APP.User_Controls.Filtro
{
    partial class frmFiltroBanidos
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFiltroBanidos));
            this.tcbFiltroBanido = new System.Windows.Forms.TabControl();
            this.tgbCodigo = new System.Windows.Forms.TabPage();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.txtCodigoUsuario = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.tpgNome = new System.Windows.Forms.TabPage();
            this.lblNomeUsuario = new System.Windows.Forms.Label();
            this.txtPesquisaUsuario = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.tpgEmail = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPesquisaEmail = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.pnHider = new System.Windows.Forms.Panel();
            this.imageListIconValidator = new System.Windows.Forms.ImageList(this.components);
            this.tcbFiltroBanido.SuspendLayout();
            this.tgbCodigo.SuspendLayout();
            this.tpgNome.SuspendLayout();
            this.tpgEmail.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcbFiltroBanido
            // 
            this.tcbFiltroBanido.Controls.Add(this.tgbCodigo);
            this.tcbFiltroBanido.Controls.Add(this.tpgNome);
            this.tcbFiltroBanido.Controls.Add(this.tpgEmail);
            this.tcbFiltroBanido.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcbFiltroBanido.Location = new System.Drawing.Point(-7, -2);
            this.tcbFiltroBanido.Name = "tcbFiltroBanido";
            this.tcbFiltroBanido.SelectedIndex = 0;
            this.tcbFiltroBanido.Size = new System.Drawing.Size(414, 110);
            this.tcbFiltroBanido.TabIndex = 1;
            // 
            // tgbCodigo
            // 
            this.tgbCodigo.BackColor = System.Drawing.Color.White;
            this.tgbCodigo.Controls.Add(this.lblUsuario);
            this.tgbCodigo.Controls.Add(this.txtCodigoUsuario);
            this.tgbCodigo.Location = new System.Drawing.Point(4, 26);
            this.tgbCodigo.Name = "tgbCodigo";
            this.tgbCodigo.Padding = new System.Windows.Forms.Padding(3);
            this.tgbCodigo.Size = new System.Drawing.Size(406, 80);
            this.tgbCodigo.TabIndex = 0;
            this.tgbCodigo.Tag = "";
            this.tgbCodigo.Text = "0";
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblUsuario.Location = new System.Drawing.Point(8, 5);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(97, 19);
            this.lblUsuario.TabIndex = 14;
            this.lblUsuario.Text = "Código Leitor:";
            // 
            // txtCodigoUsuario
            // 
            this.txtCodigoUsuario.AcceptsReturn = false;
            this.txtCodigoUsuario.AcceptsTab = false;
            this.txtCodigoUsuario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtCodigoUsuario.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtCodigoUsuario.BackColor = System.Drawing.Color.Transparent;
            this.txtCodigoUsuario.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtCodigoUsuario.BackgroundImage")));
            this.txtCodigoUsuario.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.txtCodigoUsuario.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtCodigoUsuario.BorderColorHover = System.Drawing.Color.DodgerBlue;
            this.txtCodigoUsuario.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtCodigoUsuario.BorderRadius = 10;
            this.txtCodigoUsuario.BorderThickness = 1;
            this.txtCodigoUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtCodigoUsuario.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoUsuario.DefaultText = "";
            this.txtCodigoUsuario.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txtCodigoUsuario.HideSelection = true;
            this.txtCodigoUsuario.IconLeft = null;
            this.txtCodigoUsuario.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoUsuario.IconPadding = 8;
            this.txtCodigoUsuario.IconRight = null;
            this.txtCodigoUsuario.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtCodigoUsuario.Location = new System.Drawing.Point(12, 26);
            this.txtCodigoUsuario.MaxLength = 70;
            this.txtCodigoUsuario.MinimumSize = new System.Drawing.Size(100, 25);
            this.txtCodigoUsuario.Modified = false;
            this.txtCodigoUsuario.Name = "txtCodigoUsuario";
            this.txtCodigoUsuario.PasswordChar = '\0';
            this.txtCodigoUsuario.ReadOnly = false;
            this.txtCodigoUsuario.SelectedText = "";
            this.txtCodigoUsuario.SelectionLength = 0;
            this.txtCodigoUsuario.SelectionStart = 0;
            this.txtCodigoUsuario.ShortcutsEnabled = true;
            this.txtCodigoUsuario.Size = new System.Drawing.Size(217, 35);
            this.txtCodigoUsuario.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtCodigoUsuario.TabIndex = 13;
            this.txtCodigoUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCodigoUsuario.TextMarginLeft = 10;
            this.txtCodigoUsuario.TextPlaceholder = "Pesquise pelo código";
            this.txtCodigoUsuario.UseSystemPasswordChar = false;
            this.txtCodigoUsuario.TextChange += new System.EventHandler(this.txtCodigoUsuario_TextChange);
            // 
            // tpgNome
            // 
            this.tpgNome.Controls.Add(this.lblNomeUsuario);
            this.tpgNome.Controls.Add(this.txtPesquisaUsuario);
            this.tpgNome.Location = new System.Drawing.Point(4, 26);
            this.tpgNome.Name = "tpgNome";
            this.tpgNome.Padding = new System.Windows.Forms.Padding(3);
            this.tpgNome.Size = new System.Drawing.Size(406, 80);
            this.tpgNome.TabIndex = 1;
            this.tpgNome.Tag = "";
            this.tpgNome.Text = "1";
            this.tpgNome.UseVisualStyleBackColor = true;
            // 
            // lblNomeUsuario
            // 
            this.lblNomeUsuario.AutoSize = true;
            this.lblNomeUsuario.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblNomeUsuario.Location = new System.Drawing.Point(8, 5);
            this.lblNomeUsuario.Name = "lblNomeUsuario";
            this.lblNomeUsuario.Size = new System.Drawing.Size(90, 19);
            this.lblNomeUsuario.TabIndex = 12;
            this.lblNomeUsuario.Text = "Nome Leitor:";
            // 
            // txtPesquisaUsuario
            // 
            this.txtPesquisaUsuario.AcceptsReturn = false;
            this.txtPesquisaUsuario.AcceptsTab = false;
            this.txtPesquisaUsuario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtPesquisaUsuario.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtPesquisaUsuario.BackColor = System.Drawing.Color.Transparent;
            this.txtPesquisaUsuario.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtPesquisaUsuario.BackgroundImage")));
            this.txtPesquisaUsuario.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.txtPesquisaUsuario.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtPesquisaUsuario.BorderColorHover = System.Drawing.Color.DodgerBlue;
            this.txtPesquisaUsuario.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtPesquisaUsuario.BorderRadius = 10;
            this.txtPesquisaUsuario.BorderThickness = 1;
            this.txtPesquisaUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtPesquisaUsuario.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPesquisaUsuario.DefaultText = "";
            this.txtPesquisaUsuario.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txtPesquisaUsuario.HideSelection = true;
            this.txtPesquisaUsuario.IconLeft = null;
            this.txtPesquisaUsuario.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtPesquisaUsuario.IconPadding = 8;
            this.txtPesquisaUsuario.IconRight = null;
            this.txtPesquisaUsuario.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtPesquisaUsuario.Location = new System.Drawing.Point(12, 26);
            this.txtPesquisaUsuario.MaxLength = 70;
            this.txtPesquisaUsuario.MinimumSize = new System.Drawing.Size(100, 25);
            this.txtPesquisaUsuario.Modified = false;
            this.txtPesquisaUsuario.Name = "txtPesquisaUsuario";
            this.txtPesquisaUsuario.PasswordChar = '\0';
            this.txtPesquisaUsuario.ReadOnly = false;
            this.txtPesquisaUsuario.SelectedText = "";
            this.txtPesquisaUsuario.SelectionLength = 0;
            this.txtPesquisaUsuario.SelectionStart = 0;
            this.txtPesquisaUsuario.ShortcutsEnabled = true;
            this.txtPesquisaUsuario.Size = new System.Drawing.Size(378, 35);
            this.txtPesquisaUsuario.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtPesquisaUsuario.TabIndex = 11;
            this.txtPesquisaUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtPesquisaUsuario.TextMarginLeft = 10;
            this.txtPesquisaUsuario.TextPlaceholder = "Pesquise pelos dos usuários";
            this.txtPesquisaUsuario.UseSystemPasswordChar = false;
            this.txtPesquisaUsuario.TextChange += new System.EventHandler(this.txtPesquisaUsuario_TextChange);
            // 
            // tpgEmail
            // 
            this.tpgEmail.Controls.Add(this.label2);
            this.tpgEmail.Controls.Add(this.txtPesquisaEmail);
            this.tpgEmail.Location = new System.Drawing.Point(4, 26);
            this.tpgEmail.Name = "tpgEmail";
            this.tpgEmail.Padding = new System.Windows.Forms.Padding(3);
            this.tpgEmail.Size = new System.Drawing.Size(406, 80);
            this.tpgEmail.TabIndex = 2;
            this.tpgEmail.Tag = "";
            this.tpgEmail.Text = "2";
            this.tpgEmail.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(8, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 19);
            this.label2.TabIndex = 15;
            this.label2.Text = "Email:";
            // 
            // txtPesquisaEmail
            // 
            this.txtPesquisaEmail.AcceptsReturn = false;
            this.txtPesquisaEmail.AcceptsTab = false;
            this.txtPesquisaEmail.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtPesquisaEmail.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtPesquisaEmail.BackColor = System.Drawing.Color.Transparent;
            this.txtPesquisaEmail.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtPesquisaEmail.BackgroundImage")));
            this.txtPesquisaEmail.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.txtPesquisaEmail.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtPesquisaEmail.BorderColorHover = System.Drawing.Color.DodgerBlue;
            this.txtPesquisaEmail.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtPesquisaEmail.BorderRadius = 10;
            this.txtPesquisaEmail.BorderThickness = 1;
            this.txtPesquisaEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtPesquisaEmail.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPesquisaEmail.DefaultText = "";
            this.txtPesquisaEmail.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txtPesquisaEmail.HideSelection = true;
            this.txtPesquisaEmail.IconLeft = null;
            this.txtPesquisaEmail.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtPesquisaEmail.IconPadding = 8;
            this.txtPesquisaEmail.IconRight = null;
            this.txtPesquisaEmail.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtPesquisaEmail.Location = new System.Drawing.Point(12, 26);
            this.txtPesquisaEmail.MaxLength = 70;
            this.txtPesquisaEmail.MinimumSize = new System.Drawing.Size(100, 25);
            this.txtPesquisaEmail.Modified = false;
            this.txtPesquisaEmail.Name = "txtPesquisaEmail";
            this.txtPesquisaEmail.PasswordChar = '\0';
            this.txtPesquisaEmail.ReadOnly = false;
            this.txtPesquisaEmail.SelectedText = "";
            this.txtPesquisaEmail.SelectionLength = 0;
            this.txtPesquisaEmail.SelectionStart = 0;
            this.txtPesquisaEmail.ShortcutsEnabled = true;
            this.txtPesquisaEmail.Size = new System.Drawing.Size(376, 35);
            this.txtPesquisaEmail.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtPesquisaEmail.TabIndex = 14;
            this.txtPesquisaEmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtPesquisaEmail.TextMarginLeft = 10;
            this.txtPesquisaEmail.TextPlaceholder = "Pesquise pelos e-mails";
            this.txtPesquisaEmail.UseSystemPasswordChar = false;
            this.txtPesquisaEmail.TextChange += new System.EventHandler(this.txtPesquisaEmail_TextChange);
            // 
            // pnHider
            // 
            this.pnHider.BackColor = System.Drawing.Color.White;
            this.pnHider.Location = new System.Drawing.Point(-1, -4);
            this.pnHider.Name = "pnHider";
            this.pnHider.Size = new System.Drawing.Size(409, 1);
            this.pnHider.TabIndex = 2;
            // 
            // imageListIconValidator
            // 
            this.imageListIconValidator.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListIconValidator.ImageStream")));
            this.imageListIconValidator.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListIconValidator.Images.SetKeyName(0, "Checkmark.png");
            this.imageListIconValidator.Images.SetKeyName(1, "Error.png");
            // 
            // frmFiltroBanidos
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.pnHider);
            this.Controls.Add(this.tcbFiltroBanido);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmFiltroBanidos";
            this.Size = new System.Drawing.Size(401, 99);
            this.tcbFiltroBanido.ResumeLayout(false);
            this.tgbCodigo.ResumeLayout(false);
            this.tgbCodigo.PerformLayout();
            this.tpgNome.ResumeLayout(false);
            this.tpgNome.PerformLayout();
            this.tpgEmail.ResumeLayout(false);
            this.tpgEmail.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcbFiltroBanido;
        private System.Windows.Forms.TabPage tgbCodigo;
        private System.Windows.Forms.TabPage tpgNome;
        private System.Windows.Forms.Label lblNomeUsuario;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtPesquisaUsuario;
        private System.Windows.Forms.Label lblUsuario;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtCodigoUsuario;
        private System.Windows.Forms.TabPage tpgEmail;
        private System.Windows.Forms.Label label2;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtPesquisaEmail;
        private System.Windows.Forms.Panel pnHider;
        private System.Windows.Forms.ImageList imageListIconValidator;
    }
}
