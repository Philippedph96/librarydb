﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KimtToo.VisualReactive;
using Nsf.FriendsLibrary.Business.Emprestimo;
using Nsf.FriendsLibrary.APP.User_Controls.Emprestimos;
using Nsf.FriendsLibrary.APP.User_Controls.Devolução;
using Nsf.FriendsLibrary.Tools.Validações;

namespace Nsf.FriendsLibrary.APP.User_Controls.Filtro
{
    public partial class frmFiltroDevolucao : UserControl
    {
        public frmFiltroDevolucao()
        {
            InitializeComponent();

            if (Program.IsInDesignMode()) return;
            pnHider.Height = 30;

            VSReactive<int>.Subscribe("devolucao", e => tcbFiltroEmprestimo.SelectedIndex = e);
        }

        private Image GetImage (bool valid)
        {
            if (valid)
                return imageListIconValidator.Images[0];
            else
                return imageListIconValidator.Images[1];
        }

        private void txtPesquisaLivro_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            string livro = txtPesquisaLivro.Text;
            frmDevolucao.GridView.PopulateGridWithTitleFilter(livro);
        }

        private void txtPesquisaLeitor_KeyPress(object sender, KeyPressEventArgs e)
        {
            string leitor = txtPesquisaLeitor.Text;
            frmDevolucao.GridView.PopulateGridWithReaderFilter(leitor);
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            DateTime data = dtpDevolucao.Value.Date;
            frmDevolucao.GridView.PopulateGridWithDateFilter(data);
        }

        private void txtPesquisaLivro_TextChange(object sender, EventArgs e)
        {
            txtPesquisaLivro.IconRight = GetImage(ValidarIcones.IsBunifuTextBoxEmpty(txtPesquisaLivro.Text));
        }

        private void txtPesquisaLeitor_TextChange(object sender, EventArgs e)
        {
            txtPesquisaLeitor.IconRight = GetImage(ValidarIcones.IsBunifuTextBoxEmpty(txtPesquisaLeitor.Text));
        }
    }
}
