﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KimtToo.VisualReactive;

namespace Nsf.FriendsLibrary.APP.User_Controls.Filtro
{
    public partial class frmFiltroEspera : UserControl
    {
        public frmFiltroEspera()
        {
            InitializeComponent();

            if (Program.IsInDesignMode()) return;
            pnHider.Height = 30;

            VSReactive<int>.Subscribe("espera", e => tcbFiltroAtraso.SelectedIndex = e);
        }
    }
}
