﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KimtToo.VisualReactive;
using Nsf.FriendsLibrary.Tools.Validações;

namespace Nsf.FriendsLibrary.APP.User_Controls.Filtro
{
    public partial class frmFiltroAtrasos : UserControl
    {
        public static frmFiltroAtrasos Metodos;
        public frmFiltroAtrasos()
        {
            Metodos = this;
            InitializeComponent();

            if (Program.IsInDesignMode()) return;
            pnHider.Height = 28;

            VSReactive<int>.Subscribe("atraso", e => tcbFiltroAtraso.SelectedIndex = e);
        }

        public Image GetImage(bool valid)
        {
            if (valid)
                return imageListIconValidator.Images[0];
            else
                return imageListIconValidator.Images[1];
        }

        private void txtPesquisaUsuario_TextChange(object sender, EventArgs e)
        {
            txtPesquisaUsuario.IconRight = GetImage(ValidarIcones.IsBunifuTextBoxEmpty(txtPesquisaUsuario.Text));
        }

        private void txtCodigoUsuario_TextChange(object sender, EventArgs e)
        {
            txtCodigoUsuario.IconRight = GetImage(ValidarIcones.IsBunifuTextBoxEmpty(txtCodigoUsuario.Text));
        }

        private void txtPesquisaLivro_TextChange(object sender, EventArgs e)
        {
            txtPesquisaLivro.IconRight = GetImage(ValidarIcones.IsBunifuTextBoxEmpty(txtPesquisaLivro.Text));
        }

        private void txtCodigoLivro_TextChange(object sender, EventArgs e)
        {
            txtCodigoLivro.IconRight = GetImage(ValidarIcones.IsBunifuTextBoxEmpty(txtCodigoLivro.Text));
        }

    }
}
