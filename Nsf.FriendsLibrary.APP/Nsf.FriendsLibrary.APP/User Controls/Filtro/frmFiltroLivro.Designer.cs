﻿namespace Nsf.FriendsLibrary.APP.User_Controls.Filtro
{
    partial class frmFiltroLivro
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFiltroLivro));
            this.tcbFiltroLivro = new System.Windows.Forms.TabControl();
            this.tpgLivros = new System.Windows.Forms.TabPage();
            this.txtLivros = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.tgpCategorias = new System.Windows.Forms.TabPage();
            this.txtCategoria = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.tpgAutor = new System.Windows.Forms.TabPage();
            this.txtAutor = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.pnHider = new System.Windows.Forms.Panel();
            this.imageListIconValidator = new System.Windows.Forms.ImageList(this.components);
            this.tcbFiltroLivro.SuspendLayout();
            this.tpgLivros.SuspendLayout();
            this.tgpCategorias.SuspendLayout();
            this.tpgAutor.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcbFiltroLivro
            // 
            this.tcbFiltroLivro.Controls.Add(this.tpgLivros);
            this.tcbFiltroLivro.Controls.Add(this.tgpCategorias);
            this.tcbFiltroLivro.Controls.Add(this.tpgAutor);
            this.tcbFiltroLivro.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcbFiltroLivro.Location = new System.Drawing.Point(-6, 3);
            this.tcbFiltroLivro.Name = "tcbFiltroLivro";
            this.tcbFiltroLivro.SelectedIndex = 0;
            this.tcbFiltroLivro.Size = new System.Drawing.Size(413, 77);
            this.tcbFiltroLivro.TabIndex = 1;
            // 
            // tpgLivros
            // 
            this.tpgLivros.BackColor = System.Drawing.Color.White;
            this.tpgLivros.Controls.Add(this.txtLivros);
            this.tpgLivros.Location = new System.Drawing.Point(4, 26);
            this.tpgLivros.Name = "tpgLivros";
            this.tpgLivros.Padding = new System.Windows.Forms.Padding(3);
            this.tpgLivros.Size = new System.Drawing.Size(405, 47);
            this.tpgLivros.TabIndex = 0;
            this.tpgLivros.Tag = "";
            this.tpgLivros.Text = "0";
            // 
            // txtLivros
            // 
            this.txtLivros.AcceptsReturn = false;
            this.txtLivros.AcceptsTab = false;
            this.txtLivros.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtLivros.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtLivros.BackColor = System.Drawing.Color.Transparent;
            this.txtLivros.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtLivros.BackgroundImage")));
            this.txtLivros.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.txtLivros.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtLivros.BorderColorHover = System.Drawing.Color.DodgerBlue;
            this.txtLivros.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtLivros.BorderRadius = 10;
            this.txtLivros.BorderThickness = 1;
            this.txtLivros.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtLivros.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLivros.DefaultText = "";
            this.txtLivros.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txtLivros.HideSelection = true;
            this.txtLivros.IconLeft = null;
            this.txtLivros.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtLivros.IconPadding = 8;
            this.txtLivros.IconRight = null;
            this.txtLivros.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtLivros.Location = new System.Drawing.Point(8, 3);
            this.txtLivros.MaxLength = 70;
            this.txtLivros.MinimumSize = new System.Drawing.Size(100, 25);
            this.txtLivros.Modified = false;
            this.txtLivros.Name = "txtLivros";
            this.txtLivros.PasswordChar = '\0';
            this.txtLivros.ReadOnly = false;
            this.txtLivros.SelectedText = "";
            this.txtLivros.SelectionLength = 0;
            this.txtLivros.SelectionStart = 0;
            this.txtLivros.ShortcutsEnabled = true;
            this.txtLivros.Size = new System.Drawing.Size(392, 35);
            this.txtLivros.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtLivros.TabIndex = 12;
            this.txtLivros.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtLivros.TextMarginLeft = 10;
            this.txtLivros.TextPlaceholder = "Pesquise pelos títulos dos livros";
            this.txtLivros.UseSystemPasswordChar = false;
            this.txtLivros.TextChange += new System.EventHandler(this.txtLivros_TextChange);
            this.txtLivros.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLivros_KeyPress_1);
            // 
            // tgpCategorias
            // 
            this.tgpCategorias.Controls.Add(this.txtCategoria);
            this.tgpCategorias.Location = new System.Drawing.Point(4, 26);
            this.tgpCategorias.Name = "tgpCategorias";
            this.tgpCategorias.Padding = new System.Windows.Forms.Padding(3);
            this.tgpCategorias.Size = new System.Drawing.Size(405, 47);
            this.tgpCategorias.TabIndex = 1;
            this.tgpCategorias.Tag = "";
            this.tgpCategorias.Text = "1";
            this.tgpCategorias.UseVisualStyleBackColor = true;
            // 
            // txtCategoria
            // 
            this.txtCategoria.AcceptsReturn = false;
            this.txtCategoria.AcceptsTab = false;
            this.txtCategoria.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtCategoria.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtCategoria.BackColor = System.Drawing.Color.Transparent;
            this.txtCategoria.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtCategoria.BackgroundImage")));
            this.txtCategoria.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.txtCategoria.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtCategoria.BorderColorHover = System.Drawing.Color.DodgerBlue;
            this.txtCategoria.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtCategoria.BorderRadius = 10;
            this.txtCategoria.BorderThickness = 1;
            this.txtCategoria.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtCategoria.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCategoria.DefaultText = "";
            this.txtCategoria.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txtCategoria.HideSelection = true;
            this.txtCategoria.IconLeft = null;
            this.txtCategoria.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtCategoria.IconPadding = 8;
            this.txtCategoria.IconRight = null;
            this.txtCategoria.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtCategoria.Location = new System.Drawing.Point(8, 3);
            this.txtCategoria.MaxLength = 70;
            this.txtCategoria.MinimumSize = new System.Drawing.Size(100, 25);
            this.txtCategoria.Modified = false;
            this.txtCategoria.Name = "txtCategoria";
            this.txtCategoria.PasswordChar = '\0';
            this.txtCategoria.ReadOnly = false;
            this.txtCategoria.SelectedText = "";
            this.txtCategoria.SelectionLength = 0;
            this.txtCategoria.SelectionStart = 0;
            this.txtCategoria.ShortcutsEnabled = true;
            this.txtCategoria.Size = new System.Drawing.Size(392, 35);
            this.txtCategoria.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtCategoria.TabIndex = 11;
            this.txtCategoria.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCategoria.TextMarginLeft = 10;
            this.txtCategoria.TextPlaceholder = "Pesquise pelas Categorias";
            this.txtCategoria.UseSystemPasswordChar = false;
            this.txtCategoria.TextChange += new System.EventHandler(this.txtCategoria_TextChange);
            this.txtCategoria.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCategoria_KeyPress);
            // 
            // tpgAutor
            // 
            this.tpgAutor.Controls.Add(this.txtAutor);
            this.tpgAutor.Location = new System.Drawing.Point(4, 26);
            this.tpgAutor.Name = "tpgAutor";
            this.tpgAutor.Padding = new System.Windows.Forms.Padding(3);
            this.tpgAutor.Size = new System.Drawing.Size(405, 47);
            this.tpgAutor.TabIndex = 2;
            this.tpgAutor.Tag = "";
            this.tpgAutor.Text = "2";
            this.tpgAutor.UseVisualStyleBackColor = true;
            // 
            // txtAutor
            // 
            this.txtAutor.AcceptsReturn = false;
            this.txtAutor.AcceptsTab = false;
            this.txtAutor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtAutor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtAutor.BackColor = System.Drawing.Color.Transparent;
            this.txtAutor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtAutor.BackgroundImage")));
            this.txtAutor.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.txtAutor.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtAutor.BorderColorHover = System.Drawing.Color.DodgerBlue;
            this.txtAutor.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtAutor.BorderRadius = 10;
            this.txtAutor.BorderThickness = 1;
            this.txtAutor.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtAutor.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAutor.DefaultText = "";
            this.txtAutor.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txtAutor.HideSelection = true;
            this.txtAutor.IconLeft = null;
            this.txtAutor.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtAutor.IconPadding = 8;
            this.txtAutor.IconRight = null;
            this.txtAutor.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtAutor.Location = new System.Drawing.Point(8, 3);
            this.txtAutor.MaxLength = 70;
            this.txtAutor.MinimumSize = new System.Drawing.Size(100, 25);
            this.txtAutor.Modified = false;
            this.txtAutor.Name = "txtAutor";
            this.txtAutor.PasswordChar = '\0';
            this.txtAutor.ReadOnly = false;
            this.txtAutor.SelectedText = "";
            this.txtAutor.SelectionLength = 0;
            this.txtAutor.SelectionStart = 0;
            this.txtAutor.ShortcutsEnabled = true;
            this.txtAutor.Size = new System.Drawing.Size(392, 35);
            this.txtAutor.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtAutor.TabIndex = 14;
            this.txtAutor.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtAutor.TextMarginLeft = 10;
            this.txtAutor.TextPlaceholder = "Pesquise pelos Autores";
            this.txtAutor.UseSystemPasswordChar = false;
            this.txtAutor.TextChange += new System.EventHandler(this.txtAutor_TextChange);
            this.txtAutor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAutor_KeyPress);
            // 
            // pnHider
            // 
            this.pnHider.BackColor = System.Drawing.Color.White;
            this.pnHider.Location = new System.Drawing.Point(-1, 1);
            this.pnHider.Name = "pnHider";
            this.pnHider.Size = new System.Drawing.Size(409, 1);
            this.pnHider.TabIndex = 2;
            // 
            // imageListIconValidator
            // 
            this.imageListIconValidator.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListIconValidator.ImageStream")));
            this.imageListIconValidator.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListIconValidator.Images.SetKeyName(0, "Checkmark.png");
            this.imageListIconValidator.Images.SetKeyName(1, "Error.png");
            // 
            // frmFiltroLivro
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.Controls.Add(this.pnHider);
            this.Controls.Add(this.tcbFiltroLivro);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmFiltroLivro";
            this.Size = new System.Drawing.Size(402, 70);
            this.tcbFiltroLivro.ResumeLayout(false);
            this.tpgLivros.ResumeLayout(false);
            this.tgpCategorias.ResumeLayout(false);
            this.tpgAutor.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcbFiltroLivro;
        private System.Windows.Forms.TabPage tpgLivros;
        private System.Windows.Forms.TabPage tgpCategorias;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtCategoria;
        private System.Windows.Forms.TabPage tpgAutor;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtAutor;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtLivros;
        private System.Windows.Forms.Panel pnHider;
        private System.Windows.Forms.ImageList imageListIconValidator;
    }
}
