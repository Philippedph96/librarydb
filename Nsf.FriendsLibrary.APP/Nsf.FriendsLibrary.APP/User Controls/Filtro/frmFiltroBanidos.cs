﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KimtToo.VisualReactive;
using Biblioteca;
using Nsf.FriendsLibrary.Tools.Validações;

namespace Nsf.FriendsLibrary.APP.User_Controls.Filtro
{
    public partial class frmFiltroBanidos : UserControl
    {
        public frmFiltroBanidos()
        {
            InitializeComponent();

            if (Program.IsInDesignMode()) return;
            pnHider.Height = 28;

            VSReactive<int>.Subscribe("banido", e => tcbFiltroBanido.SelectedIndex = e);
        }

        private Image GetImage (bool valid)
        {
            if (valid)
                return imageListIconValidator.Images[0];
            else
                return imageListIconValidator.Images[1];
        }

        private void txtCodigoUsuario_TextChange(object sender, EventArgs e)
        {
            txtCodigoUsuario.IconRight = GetImage(ValidarIcones.IsBunifuTextBoxEmpty(txtCodigoUsuario.Text));
        }

        private void txtPesquisaEmail_TextChange(object sender, EventArgs e)
        {
            txtPesquisaEmail.IconRight = GetImage(ValidarIcones.IsBunifuTextBoxEmpty(txtPesquisaEmail.Text));
        }

        private void txtPesquisaUsuario_TextChange(object sender, EventArgs e)
        {
            txtPesquisaUsuario.IconRight = GetImage(ValidarIcones.IsBunifuTextBoxEmpty(txtPesquisaUsuario.Text));
        }
    }
}
