﻿namespace Nsf.FriendsLibrary.APP.User_Controls.Filtro
{
    partial class frmFiltroEspera
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFiltroEspera));
            this.imageListIconValidator = new System.Windows.Forms.ImageList(this.components);
            this.pnHider = new System.Windows.Forms.Panel();
            this.tpgLivro = new System.Windows.Forms.TabPage();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.lblCodigoLivro = new System.Windows.Forms.Label();
            this.txtCodigoLivro = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtPesquisaLivro = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lblNomeUsuario = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.txtCodigoUsuario = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtPesquisaUsuario = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.tgpData = new System.Windows.Forms.TabPage();
            this.lblAte = new System.Windows.Forms.Label();
            this.lblDe = new System.Windows.Forms.Label();
            this.dtpAte = new Bunifu.UI.WinForms.BunifuDatePicker();
            this.dtpDe = new Bunifu.UI.WinForms.BunifuDatePicker();
            this.tcbFiltroAtraso = new System.Windows.Forms.TabControl();
            this.tpgLivro.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tgpData.SuspendLayout();
            this.tcbFiltroAtraso.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageListIconValidator
            // 
            this.imageListIconValidator.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListIconValidator.ImageStream")));
            this.imageListIconValidator.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListIconValidator.Images.SetKeyName(0, "Checkmark.png");
            this.imageListIconValidator.Images.SetKeyName(1, "Error.png");
            // 
            // pnHider
            // 
            this.pnHider.BackColor = System.Drawing.Color.White;
            this.pnHider.Location = new System.Drawing.Point(-1, 1);
            this.pnHider.Name = "pnHider";
            this.pnHider.Size = new System.Drawing.Size(409, 1);
            this.pnHider.TabIndex = 1;
            // 
            // tpgLivro
            // 
            this.tpgLivro.Controls.Add(this.lblTitulo);
            this.tpgLivro.Controls.Add(this.lblCodigoLivro);
            this.tpgLivro.Controls.Add(this.txtCodigoLivro);
            this.tpgLivro.Controls.Add(this.txtPesquisaLivro);
            this.tpgLivro.Location = new System.Drawing.Point(4, 26);
            this.tpgLivro.Name = "tpgLivro";
            this.tpgLivro.Padding = new System.Windows.Forms.Padding(3);
            this.tpgLivro.Size = new System.Drawing.Size(406, 129);
            this.tpgLivro.TabIndex = 2;
            this.tpgLivro.Tag = "";
            this.tpgLivro.Text = "2";
            this.tpgLivro.UseVisualStyleBackColor = true;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTitulo.Location = new System.Drawing.Point(8, 65);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(84, 19);
            this.lblTitulo.TabIndex = 15;
            this.lblTitulo.Text = "Título Livro:";
            // 
            // lblCodigoLivro
            // 
            this.lblCodigoLivro.AutoSize = true;
            this.lblCodigoLivro.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigoLivro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCodigoLivro.Location = new System.Drawing.Point(8, 3);
            this.lblCodigoLivro.Name = "lblCodigoLivro";
            this.lblCodigoLivro.Size = new System.Drawing.Size(92, 19);
            this.lblCodigoLivro.TabIndex = 16;
            this.lblCodigoLivro.Text = "Código Livro:";
            // 
            // txtCodigoLivro
            // 
            this.txtCodigoLivro.AcceptsReturn = false;
            this.txtCodigoLivro.AcceptsTab = false;
            this.txtCodigoLivro.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtCodigoLivro.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtCodigoLivro.BackColor = System.Drawing.Color.Transparent;
            this.txtCodigoLivro.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtCodigoLivro.BackgroundImage")));
            this.txtCodigoLivro.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.txtCodigoLivro.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtCodigoLivro.BorderColorHover = System.Drawing.Color.DodgerBlue;
            this.txtCodigoLivro.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtCodigoLivro.BorderRadius = 10;
            this.txtCodigoLivro.BorderThickness = 1;
            this.txtCodigoLivro.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtCodigoLivro.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoLivro.DefaultText = "";
            this.txtCodigoLivro.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txtCodigoLivro.HideSelection = true;
            this.txtCodigoLivro.IconLeft = null;
            this.txtCodigoLivro.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoLivro.IconPadding = 8;
            this.txtCodigoLivro.IconRight = null;
            this.txtCodigoLivro.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtCodigoLivro.Location = new System.Drawing.Point(12, 24);
            this.txtCodigoLivro.MaxLength = 70;
            this.txtCodigoLivro.MinimumSize = new System.Drawing.Size(100, 25);
            this.txtCodigoLivro.Modified = false;
            this.txtCodigoLivro.Name = "txtCodigoLivro";
            this.txtCodigoLivro.PasswordChar = '\0';
            this.txtCodigoLivro.ReadOnly = false;
            this.txtCodigoLivro.SelectedText = "";
            this.txtCodigoLivro.SelectionLength = 0;
            this.txtCodigoLivro.SelectionStart = 0;
            this.txtCodigoLivro.ShortcutsEnabled = true;
            this.txtCodigoLivro.Size = new System.Drawing.Size(170, 30);
            this.txtCodigoLivro.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtCodigoLivro.TabIndex = 13;
            this.txtCodigoLivro.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCodigoLivro.TextMarginLeft = 10;
            this.txtCodigoLivro.TextPlaceholder = "Pesquise pelo Código";
            this.txtCodigoLivro.UseSystemPasswordChar = false;
            // 
            // txtPesquisaLivro
            // 
            this.txtPesquisaLivro.AcceptsReturn = false;
            this.txtPesquisaLivro.AcceptsTab = false;
            this.txtPesquisaLivro.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtPesquisaLivro.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtPesquisaLivro.BackColor = System.Drawing.Color.Transparent;
            this.txtPesquisaLivro.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtPesquisaLivro.BackgroundImage")));
            this.txtPesquisaLivro.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.txtPesquisaLivro.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtPesquisaLivro.BorderColorHover = System.Drawing.Color.DodgerBlue;
            this.txtPesquisaLivro.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtPesquisaLivro.BorderRadius = 10;
            this.txtPesquisaLivro.BorderThickness = 1;
            this.txtPesquisaLivro.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtPesquisaLivro.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPesquisaLivro.DefaultText = "";
            this.txtPesquisaLivro.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txtPesquisaLivro.HideSelection = true;
            this.txtPesquisaLivro.IconLeft = null;
            this.txtPesquisaLivro.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtPesquisaLivro.IconPadding = 8;
            this.txtPesquisaLivro.IconRight = null;
            this.txtPesquisaLivro.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtPesquisaLivro.Location = new System.Drawing.Point(12, 86);
            this.txtPesquisaLivro.MaxLength = 70;
            this.txtPesquisaLivro.MinimumSize = new System.Drawing.Size(100, 25);
            this.txtPesquisaLivro.Modified = false;
            this.txtPesquisaLivro.Name = "txtPesquisaLivro";
            this.txtPesquisaLivro.PasswordChar = '\0';
            this.txtPesquisaLivro.ReadOnly = false;
            this.txtPesquisaLivro.SelectedText = "";
            this.txtPesquisaLivro.SelectionLength = 0;
            this.txtPesquisaLivro.SelectionStart = 0;
            this.txtPesquisaLivro.ShortcutsEnabled = true;
            this.txtPesquisaLivro.Size = new System.Drawing.Size(376, 35);
            this.txtPesquisaLivro.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtPesquisaLivro.TabIndex = 14;
            this.txtPesquisaLivro.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtPesquisaLivro.TextMarginLeft = 10;
            this.txtPesquisaLivro.TextPlaceholder = "Pesquise pelos dos livros";
            this.txtPesquisaLivro.UseSystemPasswordChar = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.lblNomeUsuario);
            this.tabPage2.Controls.Add(this.lblUsuario);
            this.tabPage2.Controls.Add(this.txtCodigoUsuario);
            this.tabPage2.Controls.Add(this.txtPesquisaUsuario);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(406, 129);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Tag = "";
            this.tabPage2.Text = "1";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // lblNomeUsuario
            // 
            this.lblNomeUsuario.AutoSize = true;
            this.lblNomeUsuario.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomeUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblNomeUsuario.Location = new System.Drawing.Point(8, 65);
            this.lblNomeUsuario.Name = "lblNomeUsuario";
            this.lblNomeUsuario.Size = new System.Drawing.Size(90, 19);
            this.lblNomeUsuario.TabIndex = 12;
            this.lblNomeUsuario.Text = "Nome Leitor:";
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblUsuario.Location = new System.Drawing.Point(8, 3);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(97, 19);
            this.lblUsuario.TabIndex = 12;
            this.lblUsuario.Text = "Código Leitor:";
            // 
            // txtCodigoUsuario
            // 
            this.txtCodigoUsuario.AcceptsReturn = false;
            this.txtCodigoUsuario.AcceptsTab = false;
            this.txtCodigoUsuario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtCodigoUsuario.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtCodigoUsuario.BackColor = System.Drawing.Color.Transparent;
            this.txtCodigoUsuario.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtCodigoUsuario.BackgroundImage")));
            this.txtCodigoUsuario.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.txtCodigoUsuario.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtCodigoUsuario.BorderColorHover = System.Drawing.Color.DodgerBlue;
            this.txtCodigoUsuario.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtCodigoUsuario.BorderRadius = 10;
            this.txtCodigoUsuario.BorderThickness = 1;
            this.txtCodigoUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtCodigoUsuario.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigoUsuario.DefaultText = "";
            this.txtCodigoUsuario.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txtCodigoUsuario.HideSelection = true;
            this.txtCodigoUsuario.IconLeft = null;
            this.txtCodigoUsuario.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtCodigoUsuario.IconPadding = 8;
            this.txtCodigoUsuario.IconRight = null;
            this.txtCodigoUsuario.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtCodigoUsuario.Location = new System.Drawing.Point(12, 24);
            this.txtCodigoUsuario.MaxLength = 70;
            this.txtCodigoUsuario.MinimumSize = new System.Drawing.Size(100, 25);
            this.txtCodigoUsuario.Modified = false;
            this.txtCodigoUsuario.Name = "txtCodigoUsuario";
            this.txtCodigoUsuario.PasswordChar = '\0';
            this.txtCodigoUsuario.ReadOnly = false;
            this.txtCodigoUsuario.SelectedText = "";
            this.txtCodigoUsuario.SelectionLength = 0;
            this.txtCodigoUsuario.SelectionStart = 0;
            this.txtCodigoUsuario.ShortcutsEnabled = true;
            this.txtCodigoUsuario.Size = new System.Drawing.Size(170, 30);
            this.txtCodigoUsuario.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtCodigoUsuario.TabIndex = 11;
            this.txtCodigoUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCodigoUsuario.TextMarginLeft = 10;
            this.txtCodigoUsuario.TextPlaceholder = "Pesquise pelo código";
            this.txtCodigoUsuario.UseSystemPasswordChar = false;
            // 
            // txtPesquisaUsuario
            // 
            this.txtPesquisaUsuario.AcceptsReturn = false;
            this.txtPesquisaUsuario.AcceptsTab = false;
            this.txtPesquisaUsuario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtPesquisaUsuario.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtPesquisaUsuario.BackColor = System.Drawing.Color.Transparent;
            this.txtPesquisaUsuario.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtPesquisaUsuario.BackgroundImage")));
            this.txtPesquisaUsuario.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.txtPesquisaUsuario.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtPesquisaUsuario.BorderColorHover = System.Drawing.Color.DodgerBlue;
            this.txtPesquisaUsuario.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtPesquisaUsuario.BorderRadius = 10;
            this.txtPesquisaUsuario.BorderThickness = 1;
            this.txtPesquisaUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtPesquisaUsuario.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPesquisaUsuario.DefaultText = "";
            this.txtPesquisaUsuario.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txtPesquisaUsuario.HideSelection = true;
            this.txtPesquisaUsuario.IconLeft = null;
            this.txtPesquisaUsuario.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtPesquisaUsuario.IconPadding = 8;
            this.txtPesquisaUsuario.IconRight = null;
            this.txtPesquisaUsuario.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtPesquisaUsuario.Location = new System.Drawing.Point(12, 86);
            this.txtPesquisaUsuario.MaxLength = 70;
            this.txtPesquisaUsuario.MinimumSize = new System.Drawing.Size(100, 25);
            this.txtPesquisaUsuario.Modified = false;
            this.txtPesquisaUsuario.Name = "txtPesquisaUsuario";
            this.txtPesquisaUsuario.PasswordChar = '\0';
            this.txtPesquisaUsuario.ReadOnly = false;
            this.txtPesquisaUsuario.SelectedText = "";
            this.txtPesquisaUsuario.SelectionLength = 0;
            this.txtPesquisaUsuario.SelectionStart = 0;
            this.txtPesquisaUsuario.ShortcutsEnabled = true;
            this.txtPesquisaUsuario.Size = new System.Drawing.Size(378, 35);
            this.txtPesquisaUsuario.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtPesquisaUsuario.TabIndex = 11;
            this.txtPesquisaUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtPesquisaUsuario.TextMarginLeft = 10;
            this.txtPesquisaUsuario.TextPlaceholder = "Pesquise pelos leitores";
            this.txtPesquisaUsuario.UseSystemPasswordChar = false;
            // 
            // tgpData
            // 
            this.tgpData.BackColor = System.Drawing.Color.White;
            this.tgpData.Controls.Add(this.lblAte);
            this.tgpData.Controls.Add(this.lblDe);
            this.tgpData.Controls.Add(this.dtpAte);
            this.tgpData.Controls.Add(this.dtpDe);
            this.tgpData.Location = new System.Drawing.Point(4, 26);
            this.tgpData.Name = "tgpData";
            this.tgpData.Padding = new System.Windows.Forms.Padding(3);
            this.tgpData.Size = new System.Drawing.Size(406, 129);
            this.tgpData.TabIndex = 0;
            this.tgpData.Tag = "";
            this.tgpData.Text = "0";
            // 
            // lblAte
            // 
            this.lblAte.AutoSize = true;
            this.lblAte.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAte.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblAte.Location = new System.Drawing.Point(8, 63);
            this.lblAte.Name = "lblAte";
            this.lblAte.Size = new System.Drawing.Size(36, 20);
            this.lblAte.TabIndex = 223;
            this.lblAte.Text = "Até:";
            // 
            // lblDe
            // 
            this.lblDe.AutoSize = true;
            this.lblDe.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDe.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblDe.Location = new System.Drawing.Point(8, 3);
            this.lblDe.Name = "lblDe";
            this.lblDe.Size = new System.Drawing.Size(32, 20);
            this.lblDe.TabIndex = 224;
            this.lblDe.Text = "De:";
            // 
            // dtpAte
            // 
            this.dtpAte.BorderRadius = 1;
            this.dtpAte.Color = System.Drawing.Color.WhiteSmoke;
            this.dtpAte.DateBorderThickness = Bunifu.UI.WinForms.BunifuDatePicker.BorderThickness.Thick;
            this.dtpAte.DateTextAlign = Bunifu.UI.WinForms.BunifuDatePicker.TextAlign.Left;
            this.dtpAte.DisabledColor = System.Drawing.Color.Gray;
            this.dtpAte.DisplayWeekNumbers = false;
            this.dtpAte.DPHeight = 0;
            this.dtpAte.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpAte.FillDatePicker = true;
            this.dtpAte.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpAte.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dtpAte.Icon = ((System.Drawing.Image)(resources.GetObject("dtpAte.Icon")));
            this.dtpAte.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dtpAte.IconLocation = Bunifu.UI.WinForms.BunifuDatePicker.Indicator.Right;
            this.dtpAte.Location = new System.Drawing.Point(12, 84);
            this.dtpAte.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtpAte.MinimumSize = new System.Drawing.Size(360, 30);
            this.dtpAte.Name = "dtpAte";
            this.dtpAte.Size = new System.Drawing.Size(360, 30);
            this.dtpAte.TabIndex = 221;
            // 
            // dtpDe
            // 
            this.dtpDe.BorderRadius = 1;
            this.dtpDe.Color = System.Drawing.Color.WhiteSmoke;
            this.dtpDe.DateBorderThickness = Bunifu.UI.WinForms.BunifuDatePicker.BorderThickness.Thick;
            this.dtpDe.DateTextAlign = Bunifu.UI.WinForms.BunifuDatePicker.TextAlign.Left;
            this.dtpDe.DisabledColor = System.Drawing.Color.Gray;
            this.dtpDe.DisplayWeekNumbers = false;
            this.dtpDe.DPHeight = 0;
            this.dtpDe.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpDe.FillDatePicker = true;
            this.dtpDe.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDe.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dtpDe.Icon = ((System.Drawing.Image)(resources.GetObject("dtpDe.Icon")));
            this.dtpDe.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dtpDe.IconLocation = Bunifu.UI.WinForms.BunifuDatePicker.Indicator.Right;
            this.dtpDe.Location = new System.Drawing.Point(12, 25);
            this.dtpDe.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtpDe.MinimumSize = new System.Drawing.Size(360, 30);
            this.dtpDe.Name = "dtpDe";
            this.dtpDe.Size = new System.Drawing.Size(360, 30);
            this.dtpDe.TabIndex = 222;
            // 
            // tcbFiltroAtraso
            // 
            this.tcbFiltroAtraso.Controls.Add(this.tgpData);
            this.tcbFiltroAtraso.Controls.Add(this.tabPage2);
            this.tcbFiltroAtraso.Controls.Add(this.tpgLivro);
            this.tcbFiltroAtraso.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcbFiltroAtraso.Location = new System.Drawing.Point(-6, 3);
            this.tcbFiltroAtraso.Name = "tcbFiltroAtraso";
            this.tcbFiltroAtraso.SelectedIndex = 0;
            this.tcbFiltroAtraso.Size = new System.Drawing.Size(414, 159);
            this.tcbFiltroAtraso.TabIndex = 2;
            // 
            // frmFiltroEspera
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.pnHider);
            this.Controls.Add(this.tcbFiltroAtraso);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmFiltroEspera";
            this.Size = new System.Drawing.Size(401, 154);
            this.tpgLivro.ResumeLayout(false);
            this.tpgLivro.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tgpData.ResumeLayout(false);
            this.tgpData.PerformLayout();
            this.tcbFiltroAtraso.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ImageList imageListIconValidator;
        private System.Windows.Forms.Panel pnHider;
        private System.Windows.Forms.TabPage tpgLivro;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Label lblCodigoLivro;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtCodigoLivro;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtPesquisaLivro;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label lblNomeUsuario;
        private System.Windows.Forms.Label lblUsuario;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtCodigoUsuario;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtPesquisaUsuario;
        private System.Windows.Forms.TabPage tgpData;
        private System.Windows.Forms.Label lblAte;
        private System.Windows.Forms.Label lblDe;
        private Bunifu.UI.WinForms.BunifuDatePicker dtpAte;
        private Bunifu.UI.WinForms.BunifuDatePicker dtpDe;
        private System.Windows.Forms.TabControl tcbFiltroAtraso;
    }
}
