﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KimtToo.VisualReactive;
using Nsf.FriendsLibrary.Business.Livro;
using Nsf.FriendsLibrary.APP.User_Controls.Livro;
using Nsf.FriendsLibrary.Tools.Validações;

namespace Nsf.FriendsLibrary.APP.User_Controls.Filtro
{
    public partial class frmFiltroLivro : UserControl
    {
        public frmFiltroLivro()
        {
            InitializeComponent();

            if (Program.IsInDesignMode()) return;
            pnHider.Height = 28;

            VSReactive<int>.Subscribe("livro", e => tcbFiltroLivro.SelectedIndex = e);
        }

        private Image GetImageIcon (bool validate)
        {
            if (validate)
                return imageListIconValidator.Images[0];
            else
                return imageListIconValidator.Images[1];
        }

        private void txtLivros_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            string livros = txtLivros.Text;
            frmLivro.GridView.PopulateGridWithTitleFilter(livros);
        }

        private void txtCategoria_KeyPress(object sender, KeyPressEventArgs e)
        {
            string categoria = txtCategoria.Text;
            frmLivro.GridView.PopulateGridWithCategoryFilter(categoria);
        }

        private void txtAutor_KeyPress(object sender, KeyPressEventArgs e)
        {
            string autor = txtAutor.Text;
            frmLivro.GridView.PopulateGridWithAuthorFilter(autor);
        }

        private void txtLivros_TextChange(object sender, EventArgs e)
        {
            txtLivros.IconRight = this.GetImageIcon(ValidarIcones.IsBunifuTextBoxEmpty(txtLivros.Text));
        }

        private void txtCategoria_TextChange(object sender, EventArgs e)
        {
            txtCategoria.IconRight = this.GetImageIcon(ValidarIcones.IsBunifuTextBoxEmpty(txtCategoria.Text));
        }

        private void txtAutor_TextChange(object sender, EventArgs e)
        {
            txtAutor.IconRight = this.GetImageIcon(ValidarIcones.IsBunifuTextBoxEmpty(txtAutor.Text));
        }
    }
}
