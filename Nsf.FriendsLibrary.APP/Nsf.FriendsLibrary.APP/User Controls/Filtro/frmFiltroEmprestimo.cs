﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KimtToo.VisualReactive;
using Nsf.FriendsLibrary.Business.Emprestimo;
using Nsf.FriendsLibrary.APP.User_Controls.Emprestimos;
using Nsf.FriendsLibrary.Tools.Validações;

namespace Nsf.FriendsLibrary.APP.User_Controls.Filtro
{
    public partial class frmFiltroEmprestimo : UserControl
    {
        public frmFiltroEmprestimo()
        {
            InitializeComponent();

            if (Program.IsInDesignMode()) return;
            pnHider.Height = 30;

            VSReactive<int>.Subscribe("emprestimo", e => tcbFiltroEmprestimo.SelectedIndex = e);
        }

        private Image GetImage (bool validate)
        {
            if (validate)
                return imageListIconValidator.Images[0];
            else
                return imageListIconValidator.Images[1];
        }

        private void txtPesquisaLivro_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            string livro = txtPesquisaLivro.Text;
            frmEmprestimo.Emprestimo.PopulateGridWithBookFilter(livro);
        }

        private void txtPesquisaLeitor_KeyPress(object sender, KeyPressEventArgs e)
        {
            string leitor = txtPesquisaLeitor.Text;
            frmEmprestimo.Emprestimo.PopulateGridWithReaderFilter(leitor);
        }

        private void btnPesquisa_Click(object sender, EventArgs e)
        {
            DateTime emprestimo = dtpEmprestimo.Value.Date;
            DateTime devolucao = dtpDevolucao.Value.Date;

            frmEmprestimo.Emprestimo.PopulateGridWithLoanFilter(emprestimo, devolucao);
        }
        
        private void txtPesquisaLivro_TextChange(object sender, EventArgs e)
        {
            txtPesquisaLivro.IconRight = this.GetImage(ValidarIcones.IsBunifuTextBoxEmpty(txtPesquisaLivro.Text));
        }

        private void txtPesquisaLeitor_TextChange(object sender, EventArgs e)
        {
            txtPesquisaLeitor.IconRight = this.GetImage(ValidarIcones.IsBunifuTextBoxEmpty(txtPesquisaLeitor.Text));
        }

    }
}
