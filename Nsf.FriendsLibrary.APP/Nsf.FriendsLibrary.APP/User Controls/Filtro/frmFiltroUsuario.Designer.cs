﻿namespace Nsf.FriendsLibrary.APP.User_Controls.Filtro
{
    partial class frmFiltroUsuario
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFiltroUsuario));
            this.tcbFiltroBanido = new System.Windows.Forms.TabControl();
            this.tgbNomeUsuario = new System.Windows.Forms.TabPage();
            this.txtUsuario = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.tpgNome = new System.Windows.Forms.TabPage();
            this.txtCPF = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.pnHider = new System.Windows.Forms.Panel();
            this.imageListIconValidator = new System.Windows.Forms.ImageList(this.components);
            this.tcbFiltroBanido.SuspendLayout();
            this.tgbNomeUsuario.SuspendLayout();
            this.tpgNome.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcbFiltroBanido
            // 
            this.tcbFiltroBanido.Controls.Add(this.tgbNomeUsuario);
            this.tcbFiltroBanido.Controls.Add(this.tpgNome);
            this.tcbFiltroBanido.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tcbFiltroBanido.Location = new System.Drawing.Point(-7, -2);
            this.tcbFiltroBanido.Name = "tcbFiltroBanido";
            this.tcbFiltroBanido.SelectedIndex = 0;
            this.tcbFiltroBanido.Size = new System.Drawing.Size(413, 77);
            this.tcbFiltroBanido.TabIndex = 1;
            // 
            // tgbNomeUsuario
            // 
            this.tgbNomeUsuario.BackColor = System.Drawing.Color.White;
            this.tgbNomeUsuario.Controls.Add(this.txtUsuario);
            this.tgbNomeUsuario.Location = new System.Drawing.Point(4, 26);
            this.tgbNomeUsuario.Name = "tgbNomeUsuario";
            this.tgbNomeUsuario.Padding = new System.Windows.Forms.Padding(3);
            this.tgbNomeUsuario.Size = new System.Drawing.Size(405, 47);
            this.tgbNomeUsuario.TabIndex = 0;
            this.tgbNomeUsuario.Tag = "";
            this.tgbNomeUsuario.Text = "0";
            // 
            // txtUsuario
            // 
            this.txtUsuario.AcceptsReturn = false;
            this.txtUsuario.AcceptsTab = false;
            this.txtUsuario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtUsuario.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtUsuario.BackColor = System.Drawing.Color.Transparent;
            this.txtUsuario.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtUsuario.BackgroundImage")));
            this.txtUsuario.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.txtUsuario.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtUsuario.BorderColorHover = System.Drawing.Color.DodgerBlue;
            this.txtUsuario.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtUsuario.BorderRadius = 10;
            this.txtUsuario.BorderThickness = 1;
            this.txtUsuario.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtUsuario.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuario.DefaultText = "";
            this.txtUsuario.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txtUsuario.HideSelection = true;
            this.txtUsuario.IconLeft = null;
            this.txtUsuario.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtUsuario.IconPadding = 8;
            this.txtUsuario.IconRight = null;
            this.txtUsuario.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtUsuario.Location = new System.Drawing.Point(8, 3);
            this.txtUsuario.MaxLength = 70;
            this.txtUsuario.MinimumSize = new System.Drawing.Size(100, 25);
            this.txtUsuario.Modified = false;
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.PasswordChar = '\0';
            this.txtUsuario.ReadOnly = false;
            this.txtUsuario.SelectedText = "";
            this.txtUsuario.SelectionLength = 0;
            this.txtUsuario.SelectionStart = 0;
            this.txtUsuario.ShortcutsEnabled = true;
            this.txtUsuario.Size = new System.Drawing.Size(378, 35);
            this.txtUsuario.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtUsuario.TabIndex = 15;
            this.txtUsuario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtUsuario.TextMarginLeft = 10;
            this.txtUsuario.TextPlaceholder = "Pesquise pelos leitores";
            this.txtUsuario.UseSystemPasswordChar = false;
            this.txtUsuario.TextChange += new System.EventHandler(this.txtUsuario_TextChange);
            this.txtUsuario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUsuario_KeyPress);
            // 
            // tpgNome
            // 
            this.tpgNome.Controls.Add(this.txtCPF);
            this.tpgNome.Location = new System.Drawing.Point(4, 26);
            this.tpgNome.Name = "tpgNome";
            this.tpgNome.Padding = new System.Windows.Forms.Padding(3);
            this.tpgNome.Size = new System.Drawing.Size(405, 47);
            this.tpgNome.TabIndex = 1;
            this.tpgNome.Tag = "";
            this.tpgNome.Text = "1";
            this.tpgNome.UseVisualStyleBackColor = true;
            // 
            // txtCPF
            // 
            this.txtCPF.AcceptsReturn = false;
            this.txtCPF.AcceptsTab = false;
            this.txtCPF.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtCPF.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtCPF.BackColor = System.Drawing.Color.Transparent;
            this.txtCPF.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtCPF.BackgroundImage")));
            this.txtCPF.BorderColorActive = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(142)))), ((int)(((byte)(60)))));
            this.txtCPF.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtCPF.BorderColorHover = System.Drawing.Color.DodgerBlue;
            this.txtCPF.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtCPF.BorderRadius = 10;
            this.txtCPF.BorderThickness = 1;
            this.txtCPF.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtCPF.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCPF.DefaultText = "";
            this.txtCPF.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txtCPF.HideSelection = true;
            this.txtCPF.IconLeft = null;
            this.txtCPF.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtCPF.IconPadding = 8;
            this.txtCPF.IconRight = null;
            this.txtCPF.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtCPF.Location = new System.Drawing.Point(8, 3);
            this.txtCPF.MaxLength = 70;
            this.txtCPF.MinimumSize = new System.Drawing.Size(100, 25);
            this.txtCPF.Modified = false;
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.PasswordChar = '\0';
            this.txtCPF.ReadOnly = false;
            this.txtCPF.SelectedText = "";
            this.txtCPF.SelectionLength = 0;
            this.txtCPF.SelectionStart = 0;
            this.txtCPF.ShortcutsEnabled = true;
            this.txtCPF.Size = new System.Drawing.Size(378, 35);
            this.txtCPF.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Bunifu;
            this.txtCPF.TabIndex = 11;
            this.txtCPF.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCPF.TextMarginLeft = 10;
            this.txtCPF.TextPlaceholder = "Pesquise pelo CPF";
            this.txtCPF.UseSystemPasswordChar = false;
            this.txtCPF.TextChange += new System.EventHandler(this.txtPesquisaUsuario_TextChange);
            this.txtCPF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCPF_KeyPress);
            // 
            // pnHider
            // 
            this.pnHider.BackColor = System.Drawing.Color.White;
            this.pnHider.Location = new System.Drawing.Point(-1, -4);
            this.pnHider.Name = "pnHider";
            this.pnHider.Size = new System.Drawing.Size(409, 1);
            this.pnHider.TabIndex = 2;
            // 
            // imageListIconValidator
            // 
            this.imageListIconValidator.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListIconValidator.ImageStream")));
            this.imageListIconValidator.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListIconValidator.Images.SetKeyName(0, "Checkmark.png");
            this.imageListIconValidator.Images.SetKeyName(1, "Error.png");
            // 
            // frmFiltroUsuario
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.pnHider);
            this.Controls.Add(this.tcbFiltroBanido);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmFiltroUsuario";
            this.Size = new System.Drawing.Size(402, 70);
            this.tcbFiltroBanido.ResumeLayout(false);
            this.tgbNomeUsuario.ResumeLayout(false);
            this.tpgNome.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcbFiltroBanido;
        private System.Windows.Forms.TabPage tgbNomeUsuario;
        private System.Windows.Forms.TabPage tpgNome;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtCPF;
        private System.Windows.Forms.Panel pnHider;
        private System.Windows.Forms.ImageList imageListIconValidator;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtUsuario;
    }
}
