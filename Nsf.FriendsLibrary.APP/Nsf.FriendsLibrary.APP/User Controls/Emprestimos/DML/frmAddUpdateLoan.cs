﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Biblioteca.Banco.Database.Usuario;
using Nsf.FriendsLibrary.DB;
using Nsf.FriendsLibrary.Business.Livro;
using Bunifu.UI.WinForms;
using Biblioteca.Utilitarios.ImagemPLugin;
using Nsf.FriendsLibrary.Tools.Utilitários.User_Session;
using Nsf.FriendsLibrary.Business.Emprestimo;
using Biblioteca.Utilitarios.Notificacoes;
using System.Threading;
using Biblioteca.Utilitarios.Email;
using Biblioteca.Validacoes;
using Nsf.FriendsLibrary.Tools.Validações;
using Biblioteca;

namespace Nsf.FriendsLibrary.APP.User_Controls.Emprestimos
{
    public partial class frmAddUpdateLoan : UserControl
    {
        public frmAddUpdateLoan()
        {
            InitializeComponent();

            dtpEmprestimo.Value = DateTime.Now;
            lblTitulo.Text = "Registrar novo Empréstimo";

            txtCPF.TextPlaceholder = "Digite o CPF do Leitor";
            cboTitulo.Items.Add("Selecione");
            cboNome.Items.Add("Selecione");
        }

        #region CARREGANDO COMBO - Leitor e Título do Livro

        private void LoadCPFReader()
        {
            txtCPF.Text = string.Empty;
            AutoCompleteStringCollection collection = new AutoCompleteStringCollection();

            LeitorBusiness db = new LeitorBusiness();
            var retorno = db.ListReaderCPF();

            foreach (var item in retorno)
            {
                collection.Add(item.ToString());
            }

            txtCPF.AutoCompleteCustomSource = collection;
        }

        private void LoadUserCombo()
        {
            cboNome.Items.Remove("Selecione");

            LeitorBusiness db = new LeitorBusiness();
            var list = db.List();

            cboNome.ValueMember = nameof(tb_leitor.id_leitor);
            cboNome.DisplayMember = nameof(tb_leitor.nm_leitor);

            cboNome.DataSource = list;
        }

        private void LoadTitleCombo()
        {
            cboTitulo.Items.Remove("Selecione");

            LivroBusiness db = new LivroBusiness();
            var list = db.List();

            cboTitulo.ValueMember = nameof(tb_livro.id_livro);
            cboTitulo.DisplayMember = nameof(tb_livro.nm_livro);

            cboTitulo.DataSource = list;
        }

        private void txtCPF_Enter_1(object sender, EventArgs e)
        {
            this.LoadCPFReader();
        }

        private void cboNome_Click_1(object sender, EventArgs e)
        {
            this.LoadUserCombo();
        }

        private void cboTitulo_Click_1(object sender, EventArgs e)
        {
            this.LoadTitleCombo();
        }

        #endregion

        #region CONFIGURAÇÃO DAS DATAS - Empréstimo e Devolução
        private void ConfigCHKs()
        {
            chk7.Enabled = true;
            chk15.Enabled = true;
        }

        private void VerificarEnabledFalso(bool chk1, bool chk2, DateTime valor, DateTimePicker dtp)
        {
            if (chk1 == false || chk2 == true)
                dtp.Value = valor;
            else if (chk1 == true || chk2 == false)
                dtp.Value = valor;
        }

        private DateTime SevenDaysChecked(DateTime data, DateTimePicker dtp, int days)
        {
            var sevenDays = data.AddDays(days);
            dtpDataPrevista.Value = sevenDays;

            chk7.Enabled = true;
            chk15.Enabled = false;

            return sevenDays;
        }

        private DateTime FifteenDaysChecked(DateTime data, DateTimePicker dtp, int days)
        {
            var fifteenDays = data.AddDays(days);
            dtpDataPrevista.Value = fifteenDays;

            chk7.Enabled = false;
            chk15.Enabled = true;

            return fifteenDays;
        }

        DateTime data;
        private DateTime SevenDaysOrFifteenDays(tb_emprestimo emprestimo, DateTimePicker dtp, DateTime date, BunifuCheckBox chk1, BunifuCheckBox chk2)
        {
            if (chk1.Checked == true)
            {
                data = this.SevenDaysChecked(date, dtp, 7);
            }
            else if (chk2.Checked == true)
            {
                data = this.FifteenDaysChecked(date, dtp, 15);
            }

            return data;
        }

        //return emprestimo.dt_devolucao = valor;
        #endregion

        #region LOADSCREEN - Alterando Valores
        tb_emprestimo_item item = null;
        tb_emprestimo loan = null;
        tb_leitor reader = null;
        tb_livro book = null;

        public void LoadScreen(tb_livro livro, tb_leitor leitor, tb_emprestimo emprestimo, tb_emprestimo_item item)
        {
            this.loan = emprestimo;
            this.reader = leitor;
            this.book = livro;
            this.item = item;

            this.LoadTitleCombo();
            this.LoadUserCombo();
            this.LoadCPFReader();

            //Controles
            lblTitulo.Text = "Alterar as informações do empréstimo";
            btnSaveUpdate.ButtonText = "Editar";

            //Livro
            cboTitulo.SelectedValue = livro.id_livro;

            //Leitor
            txtCPF.Text = reader.ds_cpf;
            cboNome.SelectedValue = leitor.id_leitor;

            //Empréstimo
            lblCodigo.Text = emprestimo.id_emprestimo.ToString();
            dtpEmprestimo.Value = emprestimo.dt_emprestimo;
            dtpDataPrevista.Value = emprestimo.dt_devolucao;

            //Empréstimo Item
            lblCodigoItem.Text = item.id_emprestimo_item.ToString();

            chk7.Enabled = false;
            chk15.Enabled = false;
        }
        #endregion

        #region EVENTOS - ControlsChanged

        DateTime valorRetorno;
        private void chk7_CheckedChanged(object sender, BunifuCheckBox.CheckedChangedEventArgs e)
        {
            //CARREGANDO OS DADOS - Objeto da tabela
            tb_emprestimo loan = new tb_emprestimo();

            //Váriaveis de escopo dentro do evento
            DateTime valorInicial = dtpEmprestimo.Value;
            DateTime data = new DateTime(dtpEmprestimo.Value.Year, dtpEmprestimo.Value.Month, dtpEmprestimo.Value.Day);

            //PROCESSAMENTO DOS DADOS - Métodos e mais Métodos
            this.ConfigCHKs(); // Seta a forma que eles sempre irão começar
            this.VerificarEnabledFalso(chk7.Checked, chk15.Checked, valorInicial, dtpDataPrevista);

            //SAÍDA DE DADOS - Mostrar para o User
            valorRetorno = this.SevenDaysOrFifteenDays(loan, dtpDataPrevista, data, chk7, chk15);
        }

        bool verificadora = true;
        private void cboTitulo_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            //CARREGANDO OS DADOS - Objeto da tabela
            tb_livro livro = cboTitulo.SelectedItem as tb_livro;

            //VARIÁVEIS = Variáveis para guardar as informações
            int autor = livro.id_autor;
            int categoria = livro.id_categoria;
            Image imagem = ImagemPlugin.ConverterParaImagem(livro.img_livro);

            //PROCESSAMENTO DOS DADOS - Instanciondo e chamando os métodos dentro dos mesmos.
            LivroBusiness db = new LivroBusiness();
            var nomeAutor = db.FilterAuthorID(autor); //A partir do que está sendo selecionado na combobox, eu pego o valor do ID que me retorna o nome do autor
            var nomeCategoria = db.FilterCategoryID(categoria);//A partir do que está sendo selecionado na combobox, eu pego o valor do ID que me retorna o nome da Categoria

            //SAÍDA DE DADOS = Mostrar para o User ficar Feliz com um pouco de mágia
            verificadora = true;
            TimerAddLeft.Start();

            imgNathalia.Image = imagem;
            lblNomeAutor.Text = nomeAutor;
            lblNomeCategoria.Text = nomeCategoria;

        }
        #endregion

        #region SAVE OR UPDATE - Depedendo do critério abordado
        private async void btnSaveUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (lblTitulo.Text == "Registrar novo Empréstimo")
                {
                    //CARREGANDO OS DADOS - Objeto da tabela
                    tb_livro livro = cboTitulo.SelectedItem as tb_livro;
                    tb_leitor leitor = cboNome.SelectedItem as tb_leitor;

                    loan = new tb_emprestimo();
                    loan.id_leitor = leitor.id_leitor;
                    loan.id_bibliotecario = UserSession.UsuarioLogado.id_bibliotecario;

                    loan.dt_emprestimo = dtpEmprestimo.Value;
                    loan.dt_devolucao = dtpDataPrevista.Value;

                    //PROCESSAMENTO DOS DADOS - Instanciondo e chamando os métodos dentro dos mesmos.
                    EmprestimoBusiness db = new EmprestimoBusiness();
                    db.Save(loan, livro);

                    //SAÍDA DE DADOS - Mostrar o resultado para o User ficar Happy
                    await Task.Factory.StartNew(() =>
                    {
                        Image imagem = Properties.Resources.ThumpUP;
                        string text = "Novo Empréstimo registrado na base de dados com êxito!";
                        this.SelectorClass(1, text, imagem);
                    });

                    //Envio de email explicando um pouco sobre o empréstimo
                    EnvioEmail sendingEmail = new EnvioEmail();
                    string paraleitor = leitor.ds_email;
                    string nomeLeitor = leitor.nm_leitor;

                    sendingEmail.DataDevolucao = loan.dt_devolucao;
                    sendingEmail.DataEmprestimo = loan.dt_emprestimo;
                    sendingEmail.EnviarEmail(paraleitor, nomeLeitor, 3);
                }
                else
                {
                    using (LibraryEntities context = new LibraryEntities())
                    {
                        //Encontrar o ID a ser alterado
                        context.tb_emprestimo.Where(c => c.id_emprestimo == loan.id_emprestimo).FirstOrDefault();

                        //CARREGANDO OS DADOS - Objeto da tabela
                        tb_livro book = cboTitulo.SelectedItem as tb_livro;
                        tb_leitor reader = cboNome.SelectedItem as tb_leitor;

                        loan = new tb_emprestimo();
                        loan.id_leitor = reader.id_leitor;
                        loan.id_bibliotecario = UserSession.UsuarioLogado.id_bibliotecario;

                        loan.id_emprestimo = int.Parse(lblCodigo.Text);
                        loan.dt_devolucao = dtpDataPrevista.Value;
                        loan.dt_emprestimo = dtpEmprestimo.Value;

                        item.id_emprestimo_item = int.Parse(lblCodigoItem.Text);

                        //PROCESSAMENTO DOS DADOS - Instanciondo e chamando os métodos dentro dos mesmos.
                        EmprestimoBusiness db = new EmprestimoBusiness();
                        db.Update(loan, book, item);

                        //SAÍDA DE DADOS - Mostrar o resultado para o User ficar Happy
                        await Task.Factory.StartNew(() =>
                        {
                            Image imagem = Properties.Resources.Warning;
                            string text = "As informações do empréstimo foram alteradas na base de dados com êxito!";
                            this.SelectorClass(2, text, imagem);
                        });
                    }
                }
            }
            catch (ArgumentException business)
            {
                MessageBox.Show(business.Message,
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + "\n" + "Tente mais tarde.",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }


        }
        #endregion

        #region LIMPAR CONTROLES
        bool validar = false;
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            validar = true;

            frmAddUpdateLoan loan = new frmAddUpdateLoan();
            frmMain.UserControl.CleanControl(loan);
        }
        #endregion

        #region TIMEREFFECT - Redução no Panel
        int valor1 = 106;
        private void TimerAddLeft_Tick(object sender, EventArgs e)
        {
            if (valor1 > 163)
            {
                TimerAddLeft.Stop();
                verificadora = false;

            }
            else
            {
                //Fecha o Panel com a Animação
                imgNathalia.Visible = true;

                lblTituloAutor.Visible = true;
                lblNomeAutor.Visible = true;

                lblTituloCategoria.Visible = true;
                lblNomeCategoria.Visible = true;

                pnFirstLeft.Visible = true;

                pnFirstLeft.Size = new Size(valor1, pnFirstLeft.Height);
                valor1 += 10;
            }
        }

        #endregion

        #region VALIDAÇÃO RUNTIME - Validador de BunifuTextBox
        private Image GetImage(bool valid)
        {
            if (valid)
                return imageListIconValidator.Images[0];
            else
                return imageListIconValidator.Images[1];
        }

        private void cboNome_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            var leitor = cboNome.SelectedItem as tb_leitor;

            LeitorBusiness db = new LeitorBusiness();
            var retorno = db.GetCPFbyName(leitor.nm_leitor);

            txtCPF.Text = retorno;

        }

        private void txtCPF_TextChange_1(object sender, EventArgs e)
        {
            try
            {
                validar = false;
                if (validar == false)
                    txtCPF.IconRight = GetImage(ValidarIcones.IsBunifuCPFValid(txtCPF.Text));

                EmprestimoBusiness db = new EmprestimoBusiness();
                var retorno = db.GetCombobox(txtCPF.Text);

                cboNome.SelectedValue = retorno;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + "\n\n" + "Verique a internet ou tente mais tarde!!",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private void txtCPF_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            validar = false;
            if (validar == false)
                ValidarTextbox.MascaraCPF(txtCPF, e);
        }

        #endregion

        private void txtCPF_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnSaveUpdate_Click(null, null);
        }
    }
}
