﻿namespace Nsf.FriendsLibrary.APP.User_Controls.Emprestimos
{
    partial class frmAddUpdateLoan
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAddUpdateLoan));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            this.pnSeparator1 = new System.Windows.Forms.Panel();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.lblEmprestimo = new System.Windows.Forms.Label();
            this.lbl7Dias = new System.Windows.Forms.Label();
            this.lbl15Dias = new System.Windows.Forms.Label();
            this.lblDevoulução = new System.Windows.Forms.Label();
            this.pnSeparator2 = new System.Windows.Forms.Panel();
            this.pnSeparator3 = new System.Windows.Forms.Panel();
            this.lblTituloAutor = new System.Windows.Forms.Label();
            this.lblTituloCategoria = new System.Windows.Forms.Label();
            this.lblNomeAutor = new System.Windows.Forms.Label();
            this.lblNomeCategoria = new System.Windows.Forms.Label();
            this.lblDataPrevista = new System.Windows.Forms.Label();
            this.btnCancelar = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.btnSaveUpdate = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.imgNathalia = new Bunifu.UI.WinForms.BunifuPictureBox();
            this.dtpDataPrevista = new Bunifu.UI.WinForms.BunifuDatePicker();
            this.dtpEmprestimo = new Bunifu.UI.WinForms.BunifuDatePicker();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.lblCodigoItem = new System.Windows.Forms.Label();
            this.imageListIconValidator = new System.Windows.Forms.ImageList(this.components);
            this.pnConteiner = new System.Windows.Forms.Panel();
            this.pnSecondPanel = new System.Windows.Forms.Panel();
            this.txtCPF = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.cboNome = new System.Windows.Forms.ComboBox();
            this.lblTituloCombo = new System.Windows.Forms.Label();
            this.cboTitulo = new System.Windows.Forms.ComboBox();
            this.lblLeitor = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pnFirstLeft = new System.Windows.Forms.Panel();
            this.TimerAddLeft = new System.Windows.Forms.Timer(this.components);
            this.TimerRemoveRight = new System.Windows.Forms.Timer(this.components);
            this.chk7 = new Bunifu.UI.WinForms.BunifuCheckBox();
            this.chk15 = new Bunifu.UI.WinForms.BunifuCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgNathalia)).BeginInit();
            this.pnConteiner.SuspendLayout();
            this.pnSecondPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnSeparator1
            // 
            this.pnSeparator1.BackColor = System.Drawing.Color.Gainsboro;
            this.pnSeparator1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnSeparator1.Location = new System.Drawing.Point(18, 64);
            this.pnSeparator1.Name = "pnSeparator1";
            this.pnSeparator1.Size = new System.Drawing.Size(590, 1);
            this.pnSeparator1.TabIndex = 3;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTitulo.Location = new System.Drawing.Point(31, 22);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(78, 32);
            this.lblTitulo.TabIndex = 2;
            this.lblTitulo.Text = "{Title}";
            // 
            // lblEmprestimo
            // 
            this.lblEmprestimo.AutoSize = true;
            this.lblEmprestimo.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold);
            this.lblEmprestimo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblEmprestimo.Location = new System.Drawing.Point(76, 340);
            this.lblEmprestimo.Name = "lblEmprestimo";
            this.lblEmprestimo.Size = new System.Drawing.Size(126, 20);
            this.lblEmprestimo.TabIndex = 212;
            this.lblEmprestimo.Text = "Data Empréstimo";
            // 
            // lbl7Dias
            // 
            this.lbl7Dias.AutoSize = true;
            this.lbl7Dias.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl7Dias.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl7Dias.Location = new System.Drawing.Point(113, 455);
            this.lbl7Dias.Name = "lbl7Dias";
            this.lbl7Dias.Size = new System.Drawing.Size(54, 20);
            this.lbl7Dias.TabIndex = 217;
            this.lbl7Dias.Text = "7 Dias ";
            // 
            // lbl15Dias
            // 
            this.lbl15Dias.AutoSize = true;
            this.lbl15Dias.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl15Dias.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbl15Dias.Location = new System.Drawing.Point(113, 484);
            this.lbl15Dias.Name = "lbl15Dias";
            this.lbl15Dias.Size = new System.Drawing.Size(62, 20);
            this.lbl15Dias.TabIndex = 217;
            this.lbl15Dias.Text = "15 Dias ";
            // 
            // lblDevoulução
            // 
            this.lblDevoulução.AutoSize = true;
            this.lblDevoulução.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDevoulução.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblDevoulução.Location = new System.Drawing.Point(76, 424);
            this.lblDevoulução.Name = "lblDevoulução";
            this.lblDevoulução.Size = new System.Drawing.Size(127, 20);
            this.lblDevoulução.TabIndex = 217;
            this.lblDevoulução.Text = "Data Devoulução";
            // 
            // pnSeparator2
            // 
            this.pnSeparator2.BackColor = System.Drawing.Color.Gainsboro;
            this.pnSeparator2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnSeparator2.Location = new System.Drawing.Point(18, 308);
            this.pnSeparator2.Name = "pnSeparator2";
            this.pnSeparator2.Size = new System.Drawing.Size(590, 1);
            this.pnSeparator2.TabIndex = 3;
            // 
            // pnSeparator3
            // 
            this.pnSeparator3.BackColor = System.Drawing.Color.Gainsboro;
            this.pnSeparator3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnSeparator3.Location = new System.Drawing.Point(18, 530);
            this.pnSeparator3.Name = "pnSeparator3";
            this.pnSeparator3.Size = new System.Drawing.Size(590, 1);
            this.pnSeparator3.TabIndex = 3;
            // 
            // lblTituloAutor
            // 
            this.lblTituloAutor.AutoSize = true;
            this.lblTituloAutor.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold);
            this.lblTituloAutor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTituloAutor.Location = new System.Drawing.Point(5, 216);
            this.lblTituloAutor.Name = "lblTituloAutor";
            this.lblTituloAutor.Size = new System.Drawing.Size(52, 20);
            this.lblTituloAutor.TabIndex = 212;
            this.lblTituloAutor.Text = "Autor:";
            this.lblTituloAutor.Visible = false;
            // 
            // lblTituloCategoria
            // 
            this.lblTituloCategoria.AutoSize = true;
            this.lblTituloCategoria.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold);
            this.lblTituloCategoria.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTituloCategoria.Location = new System.Drawing.Point(5, 255);
            this.lblTituloCategoria.Name = "lblTituloCategoria";
            this.lblTituloCategoria.Size = new System.Drawing.Size(79, 20);
            this.lblTituloCategoria.TabIndex = 212;
            this.lblTituloCategoria.Text = "Categoria:";
            this.lblTituloCategoria.Visible = false;
            // 
            // lblNomeAutor
            // 
            this.lblNomeAutor.AutoSize = true;
            this.lblNomeAutor.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblNomeAutor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblNomeAutor.Location = new System.Drawing.Point(15, 234);
            this.lblNomeAutor.Name = "lblNomeAutor";
            this.lblNomeAutor.Size = new System.Drawing.Size(41, 17);
            this.lblNomeAutor.TabIndex = 212;
            this.lblNomeAutor.Text = "None";
            this.lblNomeAutor.Visible = false;
            // 
            // lblNomeCategoria
            // 
            this.lblNomeCategoria.AutoSize = true;
            this.lblNomeCategoria.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblNomeCategoria.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblNomeCategoria.Location = new System.Drawing.Point(15, 275);
            this.lblNomeCategoria.Name = "lblNomeCategoria";
            this.lblNomeCategoria.Size = new System.Drawing.Size(41, 17);
            this.lblNomeCategoria.TabIndex = 212;
            this.lblNomeCategoria.Text = "None";
            this.lblNomeCategoria.Visible = false;
            // 
            // lblDataPrevista
            // 
            this.lblDataPrevista.AutoSize = true;
            this.lblDataPrevista.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataPrevista.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblDataPrevista.Location = new System.Drawing.Point(403, 424);
            this.lblDataPrevista.Name = "lblDataPrevista";
            this.lblDataPrevista.Size = new System.Drawing.Size(99, 20);
            this.lblDataPrevista.TabIndex = 217;
            this.lblDataPrevista.Text = "Data Prevista";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancelar.BackColor = System.Drawing.Color.Transparent;
            this.btnCancelar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancelar.BackgroundImage")));
            this.btnCancelar.ButtonText = "Limpar";
            this.btnCancelar.ButtonTextMarginLeft = 0;
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnCancelar.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnCancelar.DisabledForecolor = System.Drawing.Color.White;
            this.btnCancelar.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnCancelar.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnCancelar.IconPadding = 10;
            this.btnCancelar.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnCancelar.IdleBorderColor = System.Drawing.Color.Silver;
            this.btnCancelar.IdleBorderRadius = 40;
            this.btnCancelar.IdleBorderThickness = 0;
            this.btnCancelar.IdleFillColor = System.Drawing.Color.Silver;
            this.btnCancelar.IdleIconLeftImage = null;
            this.btnCancelar.IdleIconRightImage = null;
            this.btnCancelar.Location = new System.Drawing.Point(330, 557);
            this.btnCancelar.Name = "btnCancelar";
            stateProperties1.BorderColor = System.Drawing.SystemColors.Control;
            stateProperties1.BorderRadius = 40;
            stateProperties1.BorderThickness = 0;
            stateProperties1.FillColor = System.Drawing.SystemColors.Control;
            stateProperties1.IconLeftImage = null;
            stateProperties1.IconRightImage = null;
            this.btnCancelar.onHoverState = stateProperties1;
            this.btnCancelar.Size = new System.Drawing.Size(136, 45);
            this.btnCancelar.TabIndex = 219;
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSaveUpdate
            // 
            this.btnSaveUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveUpdate.BackColor = System.Drawing.Color.Transparent;
            this.btnSaveUpdate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSaveUpdate.BackgroundImage")));
            this.btnSaveUpdate.ButtonText = "Salvar";
            this.btnSaveUpdate.ButtonTextMarginLeft = 0;
            this.btnSaveUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSaveUpdate.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnSaveUpdate.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnSaveUpdate.DisabledForecolor = System.Drawing.Color.White;
            this.btnSaveUpdate.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveUpdate.ForeColor = System.Drawing.Color.White;
            this.btnSaveUpdate.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnSaveUpdate.IconPadding = 10;
            this.btnSaveUpdate.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnSaveUpdate.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnSaveUpdate.IdleBorderRadius = 40;
            this.btnSaveUpdate.IdleBorderThickness = 0;
            this.btnSaveUpdate.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnSaveUpdate.IdleIconLeftImage = null;
            this.btnSaveUpdate.IdleIconRightImage = null;
            this.btnSaveUpdate.Location = new System.Drawing.Point(472, 557);
            this.btnSaveUpdate.Name = "btnSaveUpdate";
            stateProperties2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(133)))), ((int)(((byte)(255)))));
            stateProperties2.BorderRadius = 40;
            stateProperties2.BorderThickness = 0;
            stateProperties2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(133)))), ((int)(((byte)(255)))));
            stateProperties2.IconLeftImage = null;
            stateProperties2.IconRightImage = null;
            this.btnSaveUpdate.onHoverState = stateProperties2;
            this.btnSaveUpdate.Size = new System.Drawing.Size(136, 45);
            this.btnSaveUpdate.TabIndex = 220;
            this.btnSaveUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSaveUpdate.Click += new System.EventHandler(this.btnSaveUpdate_Click);
            // 
            // imgNathalia
            // 
            this.imgNathalia.AllowFocused = false;
            this.imgNathalia.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.imgNathalia.BackColor = System.Drawing.Color.Gainsboro;
            this.imgNathalia.BorderRadius = 51;
            this.imgNathalia.Image = ((System.Drawing.Image)(resources.GetObject("imgNathalia.Image")));
            this.imgNathalia.IsCircle = false;
            this.imgNathalia.Location = new System.Drawing.Point(9, 107);
            this.imgNathalia.Name = "imgNathalia";
            this.imgNathalia.Size = new System.Drawing.Size(103, 103);
            this.imgNathalia.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgNathalia.TabIndex = 218;
            this.imgNathalia.TabStop = false;
            this.imgNathalia.Type = Bunifu.UI.WinForms.BunifuPictureBox.Types.Custom;
            this.imgNathalia.Visible = false;
            // 
            // dtpDataPrevista
            // 
            this.dtpDataPrevista.BorderRadius = 1;
            this.dtpDataPrevista.Color = System.Drawing.Color.Gainsboro;
            this.dtpDataPrevista.DateBorderThickness = Bunifu.UI.WinForms.BunifuDatePicker.BorderThickness.Thick;
            this.dtpDataPrevista.DateTextAlign = Bunifu.UI.WinForms.BunifuDatePicker.TextAlign.Left;
            this.dtpDataPrevista.DisabledColor = System.Drawing.Color.Gainsboro;
            this.dtpDataPrevista.DisplayWeekNumbers = true;
            this.dtpDataPrevista.DPHeight = 0;
            this.dtpDataPrevista.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpDataPrevista.FillDatePicker = true;
            this.dtpDataPrevista.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDataPrevista.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dtpDataPrevista.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDataPrevista.Icon = ((System.Drawing.Image)(resources.GetObject("dtpDataPrevista.Icon")));
            this.dtpDataPrevista.IconColor = System.Drawing.Color.Transparent;
            this.dtpDataPrevista.IconLocation = Bunifu.UI.WinForms.BunifuDatePicker.Indicator.Right;
            this.dtpDataPrevista.Location = new System.Drawing.Point(407, 453);
            this.dtpDataPrevista.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtpDataPrevista.MinimumSize = new System.Drawing.Size(140, 42);
            this.dtpDataPrevista.Name = "dtpDataPrevista";
            this.dtpDataPrevista.Size = new System.Drawing.Size(140, 42);
            this.dtpDataPrevista.TabIndex = 215;
            this.dtpDataPrevista.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCPF_KeyDown);
            // 
            // dtpEmprestimo
            // 
            this.dtpEmprestimo.BorderRadius = 1;
            this.dtpEmprestimo.Color = System.Drawing.Color.Gainsboro;
            this.dtpEmprestimo.DateBorderThickness = Bunifu.UI.WinForms.BunifuDatePicker.BorderThickness.Thin;
            this.dtpEmprestimo.DateTextAlign = Bunifu.UI.WinForms.BunifuDatePicker.TextAlign.Left;
            this.dtpEmprestimo.DisabledColor = System.Drawing.Color.Gray;
            this.dtpEmprestimo.DisplayWeekNumbers = false;
            this.dtpEmprestimo.DPHeight = 0;
            this.dtpEmprestimo.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.dtpEmprestimo.FillDatePicker = true;
            this.dtpEmprestimo.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpEmprestimo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dtpEmprestimo.Icon = ((System.Drawing.Image)(resources.GetObject("dtpEmprestimo.Icon")));
            this.dtpEmprestimo.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dtpEmprestimo.IconLocation = Bunifu.UI.WinForms.BunifuDatePicker.Indicator.Right;
            this.dtpEmprestimo.Location = new System.Drawing.Point(80, 363);
            this.dtpEmprestimo.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dtpEmprestimo.MinimumSize = new System.Drawing.Size(467, 35);
            this.dtpEmprestimo.Name = "dtpEmprestimo";
            this.dtpEmprestimo.Size = new System.Drawing.Size(467, 35);
            this.dtpEmprestimo.TabIndex = 215;
            this.dtpEmprestimo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCPF_KeyDown);
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblCodigo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCodigo.Location = new System.Drawing.Point(567, 22);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(12, 17);
            this.lblCodigo.TabIndex = 212;
            this.lblCodigo.Text = " ";
            this.lblCodigo.Visible = false;
            // 
            // lblCodigoItem
            // 
            this.lblCodigoItem.AutoSize = true;
            this.lblCodigoItem.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblCodigoItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCodigoItem.Location = new System.Drawing.Point(535, 22);
            this.lblCodigoItem.Name = "lblCodigoItem";
            this.lblCodigoItem.Size = new System.Drawing.Size(12, 17);
            this.lblCodigoItem.TabIndex = 223;
            this.lblCodigoItem.Text = " ";
            this.lblCodigoItem.Visible = false;
            // 
            // imageListIconValidator
            // 
            this.imageListIconValidator.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListIconValidator.ImageStream")));
            this.imageListIconValidator.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListIconValidator.Images.SetKeyName(0, "Checkmark.png");
            this.imageListIconValidator.Images.SetKeyName(1, "Error.png");
            // 
            // pnConteiner
            // 
            this.pnConteiner.Controls.Add(this.pnSecondPanel);
            this.pnConteiner.Controls.Add(this.pnFirstLeft);
            this.pnConteiner.Location = new System.Drawing.Point(0, 78);
            this.pnConteiner.Name = "pnConteiner";
            this.pnConteiner.Size = new System.Drawing.Size(622, 224);
            this.pnConteiner.TabIndex = 224;
            // 
            // pnSecondPanel
            // 
            this.pnSecondPanel.Controls.Add(this.txtCPF);
            this.pnSecondPanel.Controls.Add(this.cboNome);
            this.pnSecondPanel.Controls.Add(this.lblTituloCombo);
            this.pnSecondPanel.Controls.Add(this.cboTitulo);
            this.pnSecondPanel.Controls.Add(this.lblLeitor);
            this.pnSecondPanel.Controls.Add(this.label6);
            this.pnSecondPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnSecondPanel.Location = new System.Drawing.Point(106, 0);
            this.pnSecondPanel.Name = "pnSecondPanel";
            this.pnSecondPanel.Size = new System.Drawing.Size(458, 224);
            this.pnSecondPanel.TabIndex = 1;
            // 
            // txtCPF
            // 
            this.txtCPF.AcceptsReturn = false;
            this.txtCPF.AcceptsTab = false;
            this.txtCPF.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtCPF.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtCPF.BackColor = System.Drawing.Color.Gainsboro;
            this.txtCPF.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtCPF.BackgroundImage")));
            this.txtCPF.BorderColorActive = System.Drawing.Color.Gainsboro;
            this.txtCPF.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtCPF.BorderColorHover = System.Drawing.Color.Gainsboro;
            this.txtCPF.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtCPF.BorderRadius = 1;
            this.txtCPF.BorderThickness = 1;
            this.txtCPF.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtCPF.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCPF.DefaultText = "";
            this.txtCPF.FillColor = System.Drawing.Color.Gainsboro;
            this.txtCPF.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtCPF.HideSelection = true;
            this.txtCPF.IconLeft = null;
            this.txtCPF.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtCPF.IconPadding = 5;
            this.txtCPF.IconRight = null;
            this.txtCPF.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.txtCPF.Location = new System.Drawing.Point(22, 51);
            this.txtCPF.MaxLength = 14;
            this.txtCPF.MinimumSize = new System.Drawing.Size(100, 29);
            this.txtCPF.Modified = false;
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.PasswordChar = '\0';
            this.txtCPF.ReadOnly = false;
            this.txtCPF.SelectedText = "";
            this.txtCPF.SelectionLength = 0;
            this.txtCPF.SelectionStart = 0;
            this.txtCPF.ShortcutsEnabled = true;
            this.txtCPF.Size = new System.Drawing.Size(371, 29);
            this.txtCPF.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtCPF.TabIndex = 230;
            this.txtCPF.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCPF.TextMarginLeft = 5;
            this.txtCPF.TextPlaceholder = "Digite o CPF do Leitor";
            this.txtCPF.UseSystemPasswordChar = false;
            this.txtCPF.TextChange += new System.EventHandler(this.txtCPF_TextChange_1);
            this.txtCPF.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCPF_KeyDown);
            this.txtCPF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCPF_KeyPress_1);
            this.txtCPF.Enter += new System.EventHandler(this.txtCPF_Enter_1);
            // 
            // cboNome
            // 
            this.cboNome.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboNome.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboNome.BackColor = System.Drawing.Color.Gainsboro;
            this.cboNome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboNome.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboNome.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cboNome.FormatString = "  ";
            this.cboNome.FormattingEnabled = true;
            this.cboNome.Location = new System.Drawing.Point(22, 110);
            this.cboNome.Name = "cboNome";
            this.cboNome.Size = new System.Drawing.Size(371, 29);
            this.cboNome.TabIndex = 228;
            this.cboNome.Text = "  Selecionar";
            this.cboNome.SelectedIndexChanged += new System.EventHandler(this.cboNome_SelectedIndexChanged_1);
            this.cboNome.Click += new System.EventHandler(this.cboNome_Click_1);
            this.cboNome.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCPF_KeyDown);
            // 
            // lblTituloCombo
            // 
            this.lblTituloCombo.AutoSize = true;
            this.lblTituloCombo.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold);
            this.lblTituloCombo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTituloCombo.Location = new System.Drawing.Point(18, 147);
            this.lblTituloCombo.Name = "lblTituloCombo";
            this.lblTituloCombo.Size = new System.Drawing.Size(86, 20);
            this.lblTituloCombo.TabIndex = 227;
            this.lblTituloCombo.Text = "Título Livro";
            // 
            // cboTitulo
            // 
            this.cboTitulo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboTitulo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboTitulo.BackColor = System.Drawing.Color.Gainsboro;
            this.cboTitulo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboTitulo.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboTitulo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cboTitulo.FormatString = "  ";
            this.cboTitulo.FormattingEnabled = true;
            this.cboTitulo.Location = new System.Drawing.Point(22, 169);
            this.cboTitulo.Name = "cboTitulo";
            this.cboTitulo.Size = new System.Drawing.Size(371, 29);
            this.cboTitulo.TabIndex = 229;
            this.cboTitulo.Text = "  Selecionar";
            this.cboTitulo.SelectedIndexChanged += new System.EventHandler(this.cboTitulo_SelectedIndexChanged_1);
            this.cboTitulo.Click += new System.EventHandler(this.cboTitulo_Click_1);
            this.cboTitulo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCPF_KeyDown);
            // 
            // lblLeitor
            // 
            this.lblLeitor.AutoSize = true;
            this.lblLeitor.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold);
            this.lblLeitor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblLeitor.Location = new System.Drawing.Point(18, 88);
            this.lblLeitor.Name = "lblLeitor";
            this.lblLeitor.Size = new System.Drawing.Size(94, 20);
            this.lblLeitor.TabIndex = 226;
            this.lblLeitor.Text = "Nome Leitor";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold);
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label6.Location = new System.Drawing.Point(18, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 20);
            this.label6.TabIndex = 225;
            this.label6.Text = "CPF: ";
            // 
            // pnFirstLeft
            // 
            this.pnFirstLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnFirstLeft.Location = new System.Drawing.Point(0, 0);
            this.pnFirstLeft.Name = "pnFirstLeft";
            this.pnFirstLeft.Size = new System.Drawing.Size(106, 224);
            this.pnFirstLeft.TabIndex = 0;
            // 
            // TimerAddLeft
            // 
            this.TimerAddLeft.Tick += new System.EventHandler(this.TimerAddLeft_Tick);
            // 
            // chk7
            // 
            this.chk7.AllowBindingControlAnimation = true;
            this.chk7.AllowBindingControlColorChanges = false;
            this.chk7.AllowBindingControlLocation = true;
            this.chk7.AllowCheckBoxAnimation = true;
            this.chk7.AllowCheckmarkAnimation = true;
            this.chk7.AllowOnHoverStates = true;
            this.chk7.BackColor = System.Drawing.Color.Transparent;
            this.chk7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("chk7.BackgroundImage")));
            this.chk7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.chk7.BindingControl = null;
            this.chk7.BindingControlPosition = Bunifu.UI.WinForms.BunifuCheckBox.BindingControlPositions.Right;
            this.chk7.Checked = false;
            this.chk7.CheckState = Bunifu.UI.WinForms.BunifuCheckBox.CheckStates.Unchecked;
            this.chk7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chk7.CustomCheckmarkImage = null;
            this.chk7.Location = new System.Drawing.Point(80, 453);
            this.chk7.MinimumSize = new System.Drawing.Size(17, 17);
            this.chk7.Name = "chk7";
            this.chk7.OnCheck.BorderColor = System.Drawing.Color.Crimson;
            this.chk7.OnCheck.BorderRadius = 2;
            this.chk7.OnCheck.BorderThickness = 2;
            this.chk7.OnCheck.CheckBoxColor = System.Drawing.Color.Crimson;
            this.chk7.OnCheck.CheckmarkColor = System.Drawing.Color.White;
            this.chk7.OnCheck.CheckmarkThickness = 2;
            this.chk7.OnDisable.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chk7.OnDisable.BorderRadius = 2;
            this.chk7.OnDisable.BorderThickness = 2;
            this.chk7.OnDisable.CheckBoxColor = System.Drawing.Color.Transparent;
            this.chk7.OnDisable.CheckmarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chk7.OnDisable.CheckmarkThickness = 2;
            this.chk7.OnHoverChecked.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(68)))), ((int)(((byte)(102)))));
            this.chk7.OnHoverChecked.BorderRadius = 2;
            this.chk7.OnHoverChecked.BorderThickness = 2;
            this.chk7.OnHoverChecked.CheckBoxColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(68)))), ((int)(((byte)(102)))));
            this.chk7.OnHoverChecked.CheckmarkColor = System.Drawing.Color.White;
            this.chk7.OnHoverChecked.CheckmarkThickness = 2;
            this.chk7.OnHoverUnchecked.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(45)))), ((int)(((byte)(83)))));
            this.chk7.OnHoverUnchecked.BorderRadius = 2;
            this.chk7.OnHoverUnchecked.BorderThickness = 2;
            this.chk7.OnHoverUnchecked.CheckBoxColor = System.Drawing.Color.Transparent;
            this.chk7.OnUncheck.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chk7.OnUncheck.BorderRadius = 2;
            this.chk7.OnUncheck.BorderThickness = 2;
            this.chk7.OnUncheck.CheckBoxColor = System.Drawing.Color.Gainsboro;
            this.chk7.Size = new System.Drawing.Size(26, 26);
            this.chk7.Style = Bunifu.UI.WinForms.BunifuCheckBox.CheckBoxStyles.Bunifu;
            this.chk7.TabIndex = 226;
            this.chk7.ThreeState = false;
            this.chk7.ToolTipText = null;
            this.chk7.CheckedChanged += new System.EventHandler<Bunifu.UI.WinForms.BunifuCheckBox.CheckedChangedEventArgs>(this.chk7_CheckedChanged);
            this.chk7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCPF_KeyDown);
            // 
            // chk15
            // 
            this.chk15.AllowBindingControlAnimation = true;
            this.chk15.AllowBindingControlColorChanges = false;
            this.chk15.AllowBindingControlLocation = true;
            this.chk15.AllowCheckBoxAnimation = true;
            this.chk15.AllowCheckmarkAnimation = true;
            this.chk15.AllowOnHoverStates = true;
            this.chk15.BackColor = System.Drawing.Color.Transparent;
            this.chk15.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("chk15.BackgroundImage")));
            this.chk15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.chk15.BindingControl = null;
            this.chk15.BindingControlPosition = Bunifu.UI.WinForms.BunifuCheckBox.BindingControlPositions.Right;
            this.chk15.Checked = false;
            this.chk15.CheckState = Bunifu.UI.WinForms.BunifuCheckBox.CheckStates.Unchecked;
            this.chk15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chk15.CustomCheckmarkImage = null;
            this.chk15.Location = new System.Drawing.Point(80, 480);
            this.chk15.MinimumSize = new System.Drawing.Size(17, 17);
            this.chk15.Name = "chk15";
            this.chk15.OnCheck.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(113)))), ((int)(((byte)(242)))));
            this.chk15.OnCheck.BorderRadius = 2;
            this.chk15.OnCheck.BorderThickness = 2;
            this.chk15.OnCheck.CheckBoxColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(113)))), ((int)(((byte)(242)))));
            this.chk15.OnCheck.CheckmarkColor = System.Drawing.Color.White;
            this.chk15.OnCheck.CheckmarkThickness = 2;
            this.chk15.OnDisable.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chk15.OnDisable.BorderRadius = 2;
            this.chk15.OnDisable.BorderThickness = 2;
            this.chk15.OnDisable.CheckBoxColor = System.Drawing.Color.Transparent;
            this.chk15.OnDisable.CheckmarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chk15.OnDisable.CheckmarkThickness = 2;
            this.chk15.OnHoverChecked.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(151)))), ((int)(((byte)(246)))));
            this.chk15.OnHoverChecked.BorderRadius = 2;
            this.chk15.OnHoverChecked.BorderThickness = 2;
            this.chk15.OnHoverChecked.CheckBoxColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(151)))), ((int)(((byte)(246)))));
            this.chk15.OnHoverChecked.CheckmarkColor = System.Drawing.Color.White;
            this.chk15.OnHoverChecked.CheckmarkThickness = 2;
            this.chk15.OnHoverUnchecked.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(138)))), ((int)(((byte)(245)))));
            this.chk15.OnHoverUnchecked.BorderRadius = 2;
            this.chk15.OnHoverUnchecked.BorderThickness = 2;
            this.chk15.OnHoverUnchecked.CheckBoxColor = System.Drawing.Color.Transparent;
            this.chk15.OnUncheck.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chk15.OnUncheck.BorderRadius = 2;
            this.chk15.OnUncheck.BorderThickness = 2;
            this.chk15.OnUncheck.CheckBoxColor = System.Drawing.Color.Gainsboro;
            this.chk15.Size = new System.Drawing.Size(26, 26);
            this.chk15.Style = Bunifu.UI.WinForms.BunifuCheckBox.CheckBoxStyles.Bunifu;
            this.chk15.TabIndex = 225;
            this.chk15.ThreeState = false;
            this.chk15.ToolTipText = null;
            this.chk15.CheckedChanged += new System.EventHandler<Bunifu.UI.WinForms.BunifuCheckBox.CheckedChangedEventArgs>(this.chk7_CheckedChanged);
            this.chk15.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCPF_KeyDown);
            // 
            // frmAddUpdateLoan
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.chk7);
            this.Controls.Add(this.chk15);
            this.Controls.Add(this.imgNathalia);
            this.Controls.Add(this.lblNomeCategoria);
            this.Controls.Add(this.lblNomeAutor);
            this.Controls.Add(this.lblTituloCategoria);
            this.Controls.Add(this.lblTituloAutor);
            this.Controls.Add(this.lblCodigoItem);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnSaveUpdate);
            this.Controls.Add(this.lblDataPrevista);
            this.Controls.Add(this.lblDevoulução);
            this.Controls.Add(this.lbl15Dias);
            this.Controls.Add(this.lbl7Dias);
            this.Controls.Add(this.dtpDataPrevista);
            this.Controls.Add(this.dtpEmprestimo);
            this.Controls.Add(this.lblCodigo);
            this.Controls.Add(this.lblEmprestimo);
            this.Controls.Add(this.pnSeparator3);
            this.Controls.Add(this.pnSeparator2);
            this.Controls.Add(this.pnSeparator1);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.pnConteiner);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmAddUpdateLoan";
            this.Size = new System.Drawing.Size(625, 632);
            ((System.ComponentModel.ISupportInitialize)(this.imgNathalia)).EndInit();
            this.pnConteiner.ResumeLayout(false);
            this.pnSecondPanel.ResumeLayout(false);
            this.pnSecondPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnSeparator1;
        private System.Windows.Forms.Label lblTitulo;
        private Bunifu.UI.WinForms.BunifuDatePicker dtpEmprestimo;
        private System.Windows.Forms.Label lblEmprestimo;
        private System.Windows.Forms.Label lbl7Dias;
        private System.Windows.Forms.Label lbl15Dias;
        private Bunifu.UI.WinForms.BunifuCheckBox chk15Dias;
        private System.Windows.Forms.Label lblDevoulução;
        private Bunifu.UI.WinForms.BunifuPictureBox imgNathalia;
        private System.Windows.Forms.Panel pnSeparator2;
        private System.Windows.Forms.Panel pnSeparator3;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnCancelar;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnSaveUpdate;
        private System.Windows.Forms.Label lblTituloAutor;
        private System.Windows.Forms.Label lblTituloCategoria;
        private System.Windows.Forms.Label lblNomeAutor;
        private System.Windows.Forms.Label lblNomeCategoria;
        private Bunifu.UI.WinForms.BunifuDatePicker dtpDataPrevista;
        private System.Windows.Forms.Label lblDataPrevista;
        private Bunifu.UI.WinForms.BunifuCheckBox chk7Dias;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.Label lblCodigoItem;
        private System.Windows.Forms.ImageList imageListIconValidator;
        private System.Windows.Forms.Panel pnConteiner;
        private System.Windows.Forms.Panel pnSecondPanel;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtCPF;
        private System.Windows.Forms.ComboBox cboNome;
        private System.Windows.Forms.Label lblTituloCombo;
        private System.Windows.Forms.ComboBox cboTitulo;
        private System.Windows.Forms.Label lblLeitor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel pnFirstLeft;
        private System.Windows.Forms.Timer TimerAddLeft;
        private System.Windows.Forms.Timer TimerRemoveRight;
        private Bunifu.UI.WinForms.BunifuCheckBox chk7;
        private Bunifu.UI.WinForms.BunifuCheckBox chk15;
    }
}
