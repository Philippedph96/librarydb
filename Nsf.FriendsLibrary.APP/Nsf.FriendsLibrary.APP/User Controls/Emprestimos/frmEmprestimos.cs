﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Biblioteca.Banco.Database.Usuario;
using Nsf.FriendsLibrary.DB;
using Biblioteca.Utilitarios.Notificacoes;
using Nsf.FriendsLibrary.Business.Bibliotecario;

namespace Biblioteca._User_Controls.Emprestimos
{
    public partial class frmEmprestimos : UserControl
    {
        public frmEmprestimos()
        {
            InitializeComponent();
        }


        protected void RealizarEmprestimo(tb_emprestimo dto) {

            #region DataDia
            int dDevolucao, mDevolucao, dEmprestimo, mEmprestimo;
            DateTime devolucao, emprestimo;
            devolucao = dtdevolucao.Value;
            dDevolucao = devolucao.Day;
            mDevolucao = devolucao.Month;
            emprestimo = dtemprestimo.Value;
            dEmprestimo = emprestimo.Day;
            mEmprestimo = emprestimo.Month;

            if (dDevolucao <= dEmprestimo && mDevolucao <= mEmprestimo)
            MessageBox.Show("A data de devolução está incorreta", "Library Friends", MessageBoxButtons.OK,
                               MessageBoxIcon.Error);
            else
            {
                dto.dt_devolucao = dtdevolucao.Value;
                dto.dt_emprestimo = dtemprestimo.Value;
            }

            #endregion
            tb_bibliotecario bibliotecario = cbobibliotecario.SelectedItem as tb_bibliotecario;
            dto.id_bibliotecario = bibliotecario.id_bibliotecario;

            tb_leitor leitor = cboleitor.SelectedItem as tb_leitor;
            dto.id_leitor = leitor.id_leitor;

           // tb_livro livro = cbolivro.SelectedItem as tb_livro;
            

        }

        protected void CarregarCombos()
        {
            BibliotecarioBusiness bibliotecarioBussines = new BibliotecarioBusiness();
            List<tb_bibliotecario> ListaBibliotecario = bibliotecarioBussines.List();
            cbobibliotecario.ValueMember = nameof(tb_bibliotecario.id_bibliotecario);
            cbobibliotecario.DisplayMember = nameof(tb_bibliotecario.nm_bibliotecario);
            cbobibliotecario.DataSource = ListaBibliotecario;

            LeitorBusiness leitorBusiness = new LeitorBusiness();
            List<tb_leitor> ListaLeitor = leitorBusiness.List();
            cboleitor.ValueMember = nameof(tb_leitor.id_leitor);
            cboleitor.DisplayMember = nameof(tb_leitor.nm_leitor);
            cboleitor.DataSource = ListaLeitor;

            //LivroBusiness livroBusiness = new LivroBusiness();
            //List<tb_livro> ListaLivro = livroBusiness.List();
            //cbolivro.ValueMember = nameof(tb_livro.id_livro);
            //cbolivro.DisplayMember = nameof(tb_livro.nm_livro);
            //cbolivro.DataSource = ListaLivro;

        }

    }
}
