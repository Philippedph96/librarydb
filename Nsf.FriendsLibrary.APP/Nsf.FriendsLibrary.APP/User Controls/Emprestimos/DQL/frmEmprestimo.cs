﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf.FriendsLibrary.Business.Emprestimo;
using Nsf.FriendsLibrary.DB;
using KimtToo.VisualReactive;
using Biblioteca;
using PagedList;

namespace Nsf.FriendsLibrary.APP.User_Controls.Emprestimos
{
    public partial class frmEmprestimo : UserControl
    {
        public static frmEmprestimo Emprestimo;

        int pageNumber = 1;
        IPagedList<vw_consultar_emprestimo> list;
        public frmEmprestimo()
        {
            InitializeComponent();

            Emprestimo = this;
            this.PopulateGrid();
        }


        private void ConfigGrid(DataGridView dgv, List<vw_consultar_emprestimo> valores)
        {
            dgv.AutoGenerateColumns = false;
            dgv.DataSource = valores;
        }

        private async Task<IPagedList<vw_consultar_emprestimo>> GetPagedListAsync(int pageNumber = 1, int pageSize = 8)
        {
            return await Task.Factory.StartNew(() =>
            {
                EmprestimoBusiness db = new EmprestimoBusiness();
                var valor = db.ListLoan();
                return valor.ToPagedList(pageNumber, pageSize);
            });
        }

        private void PopulateGrid()
        {
            EmprestimoBusiness db = new EmprestimoBusiness();
            var valoresGrid = db.ListLoan();

            dgvEmprestimo.AutoGenerateColumns = false;
            dgvEmprestimo.DataSource = valoresGrid;
        }

        public void PopulateGridWithReaderFilter(string readers)
        {
            EmprestimoBusiness db = new EmprestimoBusiness();
            var valoresGrid = db.FilterReader(readers);

            this.ConfigGrid(dgvEmprestimo, valoresGrid);
        }

        public void PopulateGridWithBookFilter(string book)
        {
            EmprestimoBusiness db = new EmprestimoBusiness();
            var valoresGrid = db.FilterBook(book);

            this.ConfigGrid(dgvEmprestimo, valoresGrid);
        }

        public void PopulateGridWithLoanFilter(DateTime start, DateTime end)
        {
            EmprestimoBusiness db = new EmprestimoBusiness();
            var valoresGrid = db.FilterLoan(start, end);

            this.ConfigGrid(dgvEmprestimo, valoresGrid);
        }

        private void chkTitulo_Click(object sender, EventArgs e)
        {
            VSReactive<int>.SetState("emprestimo", int.Parse(((Control)sender).Tag.ToString()));
        }

        private async void frmEmprestimo_Load(object sender, EventArgs e)
        {
            list = await GetPagedListAsync();

            btnProximo.Enabled = list.HasNextPage;
            btnAnterior.Enabled = list.HasPreviousPage;

            dgvEmprestimo.DataSource = list.ToList();
            lblNumberPages.Text = string.Format("{0}/{1}", pageNumber, list.PageCount);
        }

        private async void btnAnterior_Click(object sender, EventArgs e)
        {
            list = await GetPagedListAsync(-- pageNumber);

            btnProximo.Enabled = list.HasNextPage;
            btnAnterior.Enabled = list.HasPreviousPage;

            dgvEmprestimo.DataSource = list.ToList();
            lblNumberPages.Text = string.Format("{0}/{1}", pageNumber, list.PageCount);
        }

        private async void btnProximo_Click(object sender, EventArgs e)
        {
            list = await GetPagedListAsync(++ pageNumber);

            btnProximo.Enabled = list.HasNextPage;
            btnAnterior.Enabled = list.HasPreviousPage;

            dgvEmprestimo.DataSource = list.ToList();
            lblNumberPages.Text = string.Format("{0}/{1}", pageNumber, list.PageCount);
        }
    }
}
