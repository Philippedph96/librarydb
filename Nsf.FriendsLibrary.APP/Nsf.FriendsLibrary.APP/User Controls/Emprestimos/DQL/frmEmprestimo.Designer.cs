﻿namespace Nsf.FriendsLibrary.APP.User_Controls.Emprestimos
{
    partial class frmEmprestimo
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEmprestimo));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnProximo = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.lblNumberPages = new System.Windows.Forms.Label();
            this.btnAnterior = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.lblDados = new System.Windows.Forms.Label();
            this.dgvEmprestimo = new Bunifu.UI.WinForms.BunifuDataGridView();
            this.nmleitorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nmlivroDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtemprestimoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtdevolucaoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nmbibliotecarioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vwconsultaremprestimoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lblTitulo = new System.Windows.Forms.Label();
            this.cdsHearder = new Bunifu.Framework.UI.BunifuCards();
            this.pnSeparatorDados = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblTituloLivro = new System.Windows.Forms.Label();
            this.lblCategoria = new System.Windows.Forms.Label();
            this.lblAutor = new System.Windows.Forms.Label();
            this.chkAutor = new Bunifu.UI.WinForms.BunifuRadioButton();
            this.chkCategoria = new Bunifu.UI.WinForms.BunifuRadioButton();
            this.chkTitulo = new Bunifu.UI.WinForms.BunifuRadioButton();
            this.frmFiltroEmprestimo1 = new Nsf.FriendsLibrary.APP.User_Controls.Filtro.frmFiltroEmprestimo();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmprestimo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwconsultaremprestimoBindingSource)).BeginInit();
            this.cdsHearder.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnProximo
            // 
            this.btnProximo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnProximo.BackColor = System.Drawing.Color.Transparent;
            this.btnProximo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnProximo.BackgroundImage")));
            this.btnProximo.ButtonText = "Próxima";
            this.btnProximo.ButtonTextMarginLeft = 10;
            this.btnProximo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProximo.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnProximo.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnProximo.DisabledForecolor = System.Drawing.Color.White;
            this.btnProximo.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProximo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnProximo.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnProximo.IconPadding = 10;
            this.btnProximo.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnProximo.IdleBorderColor = System.Drawing.Color.Gainsboro;
            this.btnProximo.IdleBorderRadius = 10;
            this.btnProximo.IdleBorderThickness = 0;
            this.btnProximo.IdleFillColor = System.Drawing.Color.Gainsboro;
            this.btnProximo.IdleIconLeftImage = null;
            this.btnProximo.IdleIconRightImage = ((System.Drawing.Image)(resources.GetObject("btnProximo.IdleIconRightImage")));
            this.btnProximo.Location = new System.Drawing.Point(338, 588);
            this.btnProximo.Name = "btnProximo";
            stateProperties1.BorderColor = System.Drawing.Color.Silver;
            stateProperties1.BorderRadius = 10;
            stateProperties1.BorderThickness = 0;
            stateProperties1.FillColor = System.Drawing.Color.Silver;
            stateProperties1.IconLeftImage = null;
            stateProperties1.IconRightImage = ((System.Drawing.Image)(resources.GetObject("stateProperties1.IconRightImage")));
            this.btnProximo.onHoverState = stateProperties1;
            this.btnProximo.Size = new System.Drawing.Size(111, 41);
            this.btnProximo.TabIndex = 51;
            this.btnProximo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnProximo.Click += new System.EventHandler(this.btnProximo_Click);
            // 
            // lblNumberPages
            // 
            this.lblNumberPages.AutoSize = true;
            this.lblNumberPages.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumberPages.Location = new System.Drawing.Point(275, 599);
            this.lblNumberPages.Name = "lblNumberPages";
            this.lblNumberPages.Size = new System.Drawing.Size(47, 19);
            this.lblNumberPages.TabIndex = 49;
            this.lblNumberPages.Text = "34/42";
            // 
            // btnAnterior
            // 
            this.btnAnterior.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAnterior.BackColor = System.Drawing.Color.Transparent;
            this.btnAnterior.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAnterior.BackgroundImage")));
            this.btnAnterior.ButtonText = "Anterior";
            this.btnAnterior.ButtonTextMarginLeft = -22;
            this.btnAnterior.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnterior.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnAnterior.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnAnterior.DisabledForecolor = System.Drawing.Color.White;
            this.btnAnterior.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnterior.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAnterior.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnAnterior.IconPadding = 10;
            this.btnAnterior.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnAnterior.IdleBorderColor = System.Drawing.Color.Gainsboro;
            this.btnAnterior.IdleBorderRadius = 10;
            this.btnAnterior.IdleBorderThickness = 0;
            this.btnAnterior.IdleFillColor = System.Drawing.Color.Gainsboro;
            this.btnAnterior.IdleIconLeftImage = ((System.Drawing.Image)(resources.GetObject("btnAnterior.IdleIconLeftImage")));
            this.btnAnterior.IdleIconRightImage = null;
            this.btnAnterior.Location = new System.Drawing.Point(147, 588);
            this.btnAnterior.Name = "btnAnterior";
            stateProperties2.BorderColor = System.Drawing.Color.Silver;
            stateProperties2.BorderRadius = 10;
            stateProperties2.BorderThickness = 0;
            stateProperties2.FillColor = System.Drawing.Color.Silver;
            stateProperties2.IconLeftImage = ((System.Drawing.Image)(resources.GetObject("stateProperties2.IconLeftImage")));
            stateProperties2.IconRightImage = null;
            this.btnAnterior.onHoverState = stateProperties2;
            this.btnAnterior.Size = new System.Drawing.Size(111, 41);
            this.btnAnterior.TabIndex = 52;
            this.btnAnterior.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAnterior.Click += new System.EventHandler(this.btnAnterior_Click);
            // 
            // lblDados
            // 
            this.lblDados.AutoSize = true;
            this.lblDados.BackColor = System.Drawing.Color.White;
            this.lblDados.Font = new System.Drawing.Font("Segoe UI Semibold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDados.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblDados.Location = new System.Drawing.Point(34, 59);
            this.lblDados.Name = "lblDados";
            this.lblDados.Size = new System.Drawing.Size(191, 25);
            this.lblDados.TabIndex = 55;
            this.lblDados.Text = "Filtros para Pesquisas";
            // 
            // dgvEmprestimo
            // 
            this.dgvEmprestimo.AllowCustomTheming = true;
            this.dgvEmprestimo.AllowUserToAddRows = false;
            this.dgvEmprestimo.AllowUserToDeleteRows = false;
            this.dgvEmprestimo.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.dgvEmprestimo.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvEmprestimo.AutoGenerateColumns = false;
            this.dgvEmprestimo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvEmprestimo.BackgroundColor = System.Drawing.Color.White;
            this.dgvEmprestimo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvEmprestimo.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvEmprestimo.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEmprestimo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvEmprestimo.ColumnHeadersHeight = 40;
            this.dgvEmprestimo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nmleitorDataGridViewTextBoxColumn,
            this.nmlivroDataGridViewTextBoxColumn,
            this.dtemprestimoDataGridViewTextBoxColumn,
            this.dtdevolucaoDataGridViewTextBoxColumn,
            this.nmbibliotecarioDataGridViewTextBoxColumn});
            this.dgvEmprestimo.CurrentTheme.AlternatingRowsStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.dgvEmprestimo.CurrentTheme.AlternatingRowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.dgvEmprestimo.CurrentTheme.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Black;
            this.dgvEmprestimo.CurrentTheme.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.dgvEmprestimo.CurrentTheme.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEmprestimo.CurrentTheme.BackColor = System.Drawing.Color.White;
            this.dgvEmprestimo.CurrentTheme.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            this.dgvEmprestimo.CurrentTheme.HeaderStyle.BackColor = System.Drawing.Color.DodgerBlue;
            this.dgvEmprestimo.CurrentTheme.HeaderStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            this.dgvEmprestimo.CurrentTheme.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgvEmprestimo.CurrentTheme.Name = null;
            this.dgvEmprestimo.CurrentTheme.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgvEmprestimo.CurrentTheme.RowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.dgvEmprestimo.CurrentTheme.RowsStyle.ForeColor = System.Drawing.Color.Black;
            this.dgvEmprestimo.CurrentTheme.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.dgvEmprestimo.CurrentTheme.RowsStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEmprestimo.DataSource = this.vwconsultaremprestimoBindingSource;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEmprestimo.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvEmprestimo.EnableHeadersVisualStyles = false;
            this.dgvEmprestimo.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            this.dgvEmprestimo.HeaderBackColor = System.Drawing.Color.DodgerBlue;
            this.dgvEmprestimo.HeaderBgColor = System.Drawing.Color.Empty;
            this.dgvEmprestimo.HeaderForeColor = System.Drawing.Color.White;
            this.dgvEmprestimo.Location = new System.Drawing.Point(3, 211);
            this.dgvEmprestimo.MultiSelect = false;
            this.dgvEmprestimo.Name = "dgvEmprestimo";
            this.dgvEmprestimo.ReadOnly = true;
            this.dgvEmprestimo.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEmprestimo.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvEmprestimo.RowHeadersVisible = false;
            this.dgvEmprestimo.RowHeadersWidth = 35;
            this.dgvEmprestimo.RowTemplate.DividerHeight = 1;
            this.dgvEmprestimo.RowTemplate.Height = 40;
            this.dgvEmprestimo.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEmprestimo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEmprestimo.Size = new System.Drawing.Size(619, 371);
            this.dgvEmprestimo.TabIndex = 50;
            this.dgvEmprestimo.Theme = Bunifu.UI.WinForms.BunifuDataGridView.PresetThemes.Light;
            // 
            // nmleitorDataGridViewTextBoxColumn
            // 
            this.nmleitorDataGridViewTextBoxColumn.DataPropertyName = "nm_leitor";
            this.nmleitorDataGridViewTextBoxColumn.FillWeight = 90F;
            this.nmleitorDataGridViewTextBoxColumn.HeaderText = "Leitor";
            this.nmleitorDataGridViewTextBoxColumn.Name = "nmleitorDataGridViewTextBoxColumn";
            this.nmleitorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nmlivroDataGridViewTextBoxColumn
            // 
            this.nmlivroDataGridViewTextBoxColumn.DataPropertyName = "nm_livro";
            this.nmlivroDataGridViewTextBoxColumn.FillWeight = 120F;
            this.nmlivroDataGridViewTextBoxColumn.HeaderText = "Título Livro";
            this.nmlivroDataGridViewTextBoxColumn.Name = "nmlivroDataGridViewTextBoxColumn";
            this.nmlivroDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dtemprestimoDataGridViewTextBoxColumn
            // 
            this.dtemprestimoDataGridViewTextBoxColumn.DataPropertyName = "dt_emprestimo";
            this.dtemprestimoDataGridViewTextBoxColumn.FillWeight = 70F;
            this.dtemprestimoDataGridViewTextBoxColumn.HeaderText = "Empréstimo";
            this.dtemprestimoDataGridViewTextBoxColumn.Name = "dtemprestimoDataGridViewTextBoxColumn";
            this.dtemprestimoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dtdevolucaoDataGridViewTextBoxColumn
            // 
            this.dtdevolucaoDataGridViewTextBoxColumn.DataPropertyName = "dt_devolucao";
            this.dtdevolucaoDataGridViewTextBoxColumn.FillWeight = 62F;
            this.dtdevolucaoDataGridViewTextBoxColumn.HeaderText = "Devolução";
            this.dtdevolucaoDataGridViewTextBoxColumn.Name = "dtdevolucaoDataGridViewTextBoxColumn";
            this.dtdevolucaoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nmbibliotecarioDataGridViewTextBoxColumn
            // 
            this.nmbibliotecarioDataGridViewTextBoxColumn.DataPropertyName = "nm_bibliotecario";
            this.nmbibliotecarioDataGridViewTextBoxColumn.FillWeight = 110F;
            this.nmbibliotecarioDataGridViewTextBoxColumn.HeaderText = "Funcionário";
            this.nmbibliotecarioDataGridViewTextBoxColumn.Name = "nmbibliotecarioDataGridViewTextBoxColumn";
            this.nmbibliotecarioDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // vwconsultaremprestimoBindingSource
            // 
            this.vwconsultaremprestimoBindingSource.DataSource = typeof(Nsf.FriendsLibrary.DB.vw_consultar_emprestimo);
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Segoe UI Semibold", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTitulo.Location = new System.Drawing.Point(20, 15);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(314, 28);
            this.lblTitulo.TabIndex = 1;
            this.lblTitulo.Text = "Gerenciamento dos Empréstimos";
            // 
            // cdsHearder
            // 
            this.cdsHearder.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cdsHearder.BorderRadius = 5;
            this.cdsHearder.BottomSahddow = true;
            this.cdsHearder.color = System.Drawing.Color.White;
            this.cdsHearder.Controls.Add(this.lblTitulo);
            this.cdsHearder.LeftSahddow = false;
            this.cdsHearder.Location = new System.Drawing.Point(-8, -5);
            this.cdsHearder.Name = "cdsHearder";
            this.cdsHearder.RightSahddow = true;
            this.cdsHearder.ShadowDepth = 20;
            this.cdsHearder.Size = new System.Drawing.Size(686, 54);
            this.cdsHearder.TabIndex = 53;
            // 
            // pnSeparatorDados
            // 
            this.pnSeparatorDados.BackColor = System.Drawing.Color.Gainsboro;
            this.pnSeparatorDados.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnSeparatorDados.Location = new System.Drawing.Point(-6, 94);
            this.pnSeparatorDados.Name = "pnSeparatorDados";
            this.pnSeparatorDados.Size = new System.Drawing.Size(590, 1);
            this.pnSeparatorDados.TabIndex = 56;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.lblTituloLivro);
            this.panel2.Controls.Add(this.lblCategoria);
            this.panel2.Controls.Add(this.lblAutor);
            this.panel2.Controls.Add(this.chkAutor);
            this.panel2.Controls.Add(this.chkCategoria);
            this.panel2.Controls.Add(this.chkTitulo);
            this.panel2.Location = new System.Drawing.Point(39, 100);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(127, 101);
            this.panel2.TabIndex = 58;
            // 
            // lblTituloLivro
            // 
            this.lblTituloLivro.AutoSize = true;
            this.lblTituloLivro.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloLivro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTituloLivro.Location = new System.Drawing.Point(38, 11);
            this.lblTituloLivro.Name = "lblTituloLivro";
            this.lblTituloLivro.Size = new System.Drawing.Size(81, 19);
            this.lblTituloLivro.TabIndex = 8;
            this.lblTituloLivro.Text = "Título Livro";
            // 
            // lblCategoria
            // 
            this.lblCategoria.AutoSize = true;
            this.lblCategoria.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCategoria.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCategoria.Location = new System.Drawing.Point(38, 41);
            this.lblCategoria.Name = "lblCategoria";
            this.lblCategoria.Size = new System.Drawing.Size(45, 19);
            this.lblCategoria.TabIndex = 8;
            this.lblCategoria.Text = "Leitor";
            // 
            // lblAutor
            // 
            this.lblAutor.AutoSize = true;
            this.lblAutor.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAutor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblAutor.Location = new System.Drawing.Point(38, 72);
            this.lblAutor.Name = "lblAutor";
            this.lblAutor.Size = new System.Drawing.Size(83, 19);
            this.lblAutor.TabIndex = 8;
            this.lblAutor.Text = "Empréstimo";
            // 
            // chkAutor
            // 
            this.chkAutor.Checked = false;
            this.chkAutor.Location = new System.Drawing.Point(12, 71);
            this.chkAutor.Name = "chkAutor";
            this.chkAutor.OutlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkAutor.RadioColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkAutor.Size = new System.Drawing.Size(20, 20);
            this.chkAutor.TabIndex = 6;
            this.chkAutor.Tag = "2";
            this.chkAutor.Text = null;
            this.chkAutor.Click += new System.EventHandler(this.chkTitulo_Click);
            // 
            // chkCategoria
            // 
            this.chkCategoria.Checked = false;
            this.chkCategoria.Location = new System.Drawing.Point(12, 41);
            this.chkCategoria.Name = "chkCategoria";
            this.chkCategoria.OutlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkCategoria.RadioColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkCategoria.Size = new System.Drawing.Size(20, 20);
            this.chkCategoria.TabIndex = 6;
            this.chkCategoria.Tag = "1";
            this.chkCategoria.Text = null;
            this.chkCategoria.Click += new System.EventHandler(this.chkTitulo_Click);
            // 
            // chkTitulo
            // 
            this.chkTitulo.Checked = true;
            this.chkTitulo.ForeColor = System.Drawing.Color.White;
            this.chkTitulo.Location = new System.Drawing.Point(12, 11);
            this.chkTitulo.Name = "chkTitulo";
            this.chkTitulo.OutlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkTitulo.RadioColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkTitulo.Size = new System.Drawing.Size(20, 20);
            this.chkTitulo.TabIndex = 6;
            this.chkTitulo.Tag = "0";
            this.chkTitulo.Text = null;
            this.chkTitulo.Click += new System.EventHandler(this.chkTitulo_Click);
            // 
            // frmFiltroEmprestimo1
            // 
            this.frmFiltroEmprestimo1.BackColor = System.Drawing.Color.White;
            this.frmFiltroEmprestimo1.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frmFiltroEmprestimo1.Location = new System.Drawing.Point(182, 68);
            this.frmFiltroEmprestimo1.Name = "frmFiltroEmprestimo1";
            this.frmFiltroEmprestimo1.Size = new System.Drawing.Size(402, 137);
            this.frmFiltroEmprestimo1.TabIndex = 59;
            // 
            // frmEmprestimo
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnProximo);
            this.Controls.Add(this.lblNumberPages);
            this.Controls.Add(this.btnAnterior);
            this.Controls.Add(this.lblDados);
            this.Controls.Add(this.dgvEmprestimo);
            this.Controls.Add(this.cdsHearder);
            this.Controls.Add(this.pnSeparatorDados);
            this.Controls.Add(this.frmFiltroEmprestimo1);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmEmprestimo";
            this.Size = new System.Drawing.Size(625, 632);
            this.Load += new System.EventHandler(this.frmEmprestimo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmprestimo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vwconsultaremprestimoBindingSource)).EndInit();
            this.cdsHearder.ResumeLayout(false);
            this.cdsHearder.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnProximo;
        private System.Windows.Forms.Label lblNumberPages;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnAnterior;
        private System.Windows.Forms.Label lblDados;
        private Bunifu.UI.WinForms.BunifuDataGridView dgvEmprestimo;
        private System.Windows.Forms.Label lblTitulo;
        private Bunifu.Framework.UI.BunifuCards cdsHearder;
        private System.Windows.Forms.Panel pnSeparatorDados;
        private System.Windows.Forms.BindingSource vwconsultaremprestimoBindingSource;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblTituloLivro;
        private System.Windows.Forms.Label lblCategoria;
        private System.Windows.Forms.Label lblAutor;
        private Bunifu.UI.WinForms.BunifuRadioButton chkAutor;
        private Bunifu.UI.WinForms.BunifuRadioButton chkCategoria;
        private Bunifu.UI.WinForms.BunifuRadioButton chkTitulo;
        private System.Windows.Forms.DataGridViewTextBoxColumn nmleitorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nmlivroDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtemprestimoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dtdevolucaoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nmbibliotecarioDataGridViewTextBoxColumn;
        private Filtro.frmFiltroEmprestimo frmFiltroEmprestimo1;
    }
}
