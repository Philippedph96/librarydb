﻿namespace Nsf.FriendsLibrary.APP.User_Controls.Usuario
{
    partial class frmBibliotecario
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBibliotecario));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            this.pnSeparatorDados = new System.Windows.Forms.Panel();
            this.lblCategoria = new System.Windows.Forms.Label();
            this.chkCategoria = new Bunifu.UI.WinForms.BunifuRadioButton();
            this.chkTitulo = new Bunifu.UI.WinForms.BunifuRadioButton();
            this.cdsHearder = new Bunifu.Framework.UI.BunifuCards();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.lblTituloLivro = new System.Windows.Forms.Label();
            this.lblDados = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgvBibliotecario = new Bunifu.UI.WinForms.BunifuDataGridView();
            this.nmbibliotecarioDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsemailDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dscpfDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dscelularDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tbbibliotecarioBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lblNumberPages = new System.Windows.Forms.Label();
            this.btnEditar = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnRemove = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnProximo = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.btnAnterior = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.frmFiltroBibliotecario1 = new Nsf.FriendsLibrary.APP.User_Controls.Filtro.frmFiltroBibliotecario();
            this.cdsHearder.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBibliotecario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbbibliotecarioBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemove)).BeginInit();
            this.SuspendLayout();
            // 
            // pnSeparatorDados
            // 
            this.pnSeparatorDados.BackColor = System.Drawing.Color.Gainsboro;
            this.pnSeparatorDados.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnSeparatorDados.Location = new System.Drawing.Point(17, 90);
            this.pnSeparatorDados.Name = "pnSeparatorDados";
            this.pnSeparatorDados.Size = new System.Drawing.Size(590, 1);
            this.pnSeparatorDados.TabIndex = 45;
            // 
            // lblCategoria
            // 
            this.lblCategoria.AutoSize = true;
            this.lblCategoria.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCategoria.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCategoria.Location = new System.Drawing.Point(38, 41);
            this.lblCategoria.Name = "lblCategoria";
            this.lblCategoria.Size = new System.Drawing.Size(33, 19);
            this.lblCategoria.TabIndex = 8;
            this.lblCategoria.Text = "CPF";
            // 
            // chkCategoria
            // 
            this.chkCategoria.Checked = false;
            this.chkCategoria.Location = new System.Drawing.Point(12, 41);
            this.chkCategoria.Name = "chkCategoria";
            this.chkCategoria.OutlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkCategoria.RadioColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkCategoria.Size = new System.Drawing.Size(20, 20);
            this.chkCategoria.TabIndex = 6;
            this.chkCategoria.Tag = "1";
            this.chkCategoria.Text = null;
            this.chkCategoria.Click += new System.EventHandler(this.chkTitulo_Click);
            // 
            // chkTitulo
            // 
            this.chkTitulo.Checked = true;
            this.chkTitulo.ForeColor = System.Drawing.Color.White;
            this.chkTitulo.Location = new System.Drawing.Point(12, 11);
            this.chkTitulo.Name = "chkTitulo";
            this.chkTitulo.OutlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkTitulo.RadioColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkTitulo.Size = new System.Drawing.Size(20, 20);
            this.chkTitulo.TabIndex = 6;
            this.chkTitulo.Tag = "0";
            this.chkTitulo.Text = null;
            this.chkTitulo.Click += new System.EventHandler(this.chkTitulo_Click);
            // 
            // cdsHearder
            // 
            this.cdsHearder.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cdsHearder.BorderRadius = 5;
            this.cdsHearder.BottomSahddow = true;
            this.cdsHearder.color = System.Drawing.Color.White;
            this.cdsHearder.Controls.Add(this.lblTitulo);
            this.cdsHearder.LeftSahddow = false;
            this.cdsHearder.Location = new System.Drawing.Point(-8, -5);
            this.cdsHearder.Name = "cdsHearder";
            this.cdsHearder.RightSahddow = true;
            this.cdsHearder.ShadowDepth = 20;
            this.cdsHearder.Size = new System.Drawing.Size(686, 54);
            this.cdsHearder.TabIndex = 42;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Segoe UI Semibold", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTitulo.Location = new System.Drawing.Point(20, 15);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(311, 28);
            this.lblTitulo.TabIndex = 1;
            this.lblTitulo.Text = "Gerenciamento dos Funcionários";
            // 
            // lblTituloLivro
            // 
            this.lblTituloLivro.AutoSize = true;
            this.lblTituloLivro.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloLivro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTituloLivro.Location = new System.Drawing.Point(38, 11);
            this.lblTituloLivro.Name = "lblTituloLivro";
            this.lblTituloLivro.Size = new System.Drawing.Size(83, 19);
            this.lblTituloLivro.TabIndex = 8;
            this.lblTituloLivro.Text = "Funcionário";
            // 
            // lblDados
            // 
            this.lblDados.AutoSize = true;
            this.lblDados.BackColor = System.Drawing.Color.White;
            this.lblDados.Font = new System.Drawing.Font("Segoe UI Semibold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDados.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblDados.Location = new System.Drawing.Point(34, 59);
            this.lblDados.Name = "lblDados";
            this.lblDados.Size = new System.Drawing.Size(191, 25);
            this.lblDados.TabIndex = 44;
            this.lblDados.Text = "Filtros para Pesquisas";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.lblTituloLivro);
            this.panel2.Controls.Add(this.lblCategoria);
            this.panel2.Controls.Add(this.chkCategoria);
            this.panel2.Controls.Add(this.chkTitulo);
            this.panel2.Location = new System.Drawing.Point(39, 100);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(127, 75);
            this.panel2.TabIndex = 43;
            // 
            // dgvBibliotecario
            // 
            this.dgvBibliotecario.AllowCustomTheming = true;
            this.dgvBibliotecario.AllowUserToAddRows = false;
            this.dgvBibliotecario.AllowUserToDeleteRows = false;
            this.dgvBibliotecario.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.dgvBibliotecario.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvBibliotecario.AutoGenerateColumns = false;
            this.dgvBibliotecario.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvBibliotecario.BackgroundColor = System.Drawing.Color.White;
            this.dgvBibliotecario.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvBibliotecario.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvBibliotecario.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBibliotecario.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvBibliotecario.ColumnHeadersHeight = 40;
            this.dgvBibliotecario.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nmbibliotecarioDataGridViewTextBoxColumn,
            this.Column1,
            this.dsemailDataGridViewTextBoxColumn,
            this.dscpfDataGridViewTextBoxColumn,
            this.dscelularDataGridViewTextBoxColumn});
            this.dgvBibliotecario.CurrentTheme.AlternatingRowsStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.dgvBibliotecario.CurrentTheme.AlternatingRowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.dgvBibliotecario.CurrentTheme.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Black;
            this.dgvBibliotecario.CurrentTheme.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.dgvBibliotecario.CurrentTheme.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvBibliotecario.CurrentTheme.BackColor = System.Drawing.Color.White;
            this.dgvBibliotecario.CurrentTheme.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            this.dgvBibliotecario.CurrentTheme.HeaderStyle.BackColor = System.Drawing.Color.DodgerBlue;
            this.dgvBibliotecario.CurrentTheme.HeaderStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            this.dgvBibliotecario.CurrentTheme.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgvBibliotecario.CurrentTheme.Name = null;
            this.dgvBibliotecario.CurrentTheme.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgvBibliotecario.CurrentTheme.RowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.dgvBibliotecario.CurrentTheme.RowsStyle.ForeColor = System.Drawing.Color.Black;
            this.dgvBibliotecario.CurrentTheme.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.dgvBibliotecario.CurrentTheme.RowsStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvBibliotecario.DataSource = this.tbbibliotecarioBindingSource;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBibliotecario.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvBibliotecario.EnableHeadersVisualStyles = false;
            this.dgvBibliotecario.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            this.dgvBibliotecario.HeaderBackColor = System.Drawing.Color.DodgerBlue;
            this.dgvBibliotecario.HeaderBgColor = System.Drawing.Color.Empty;
            this.dgvBibliotecario.HeaderForeColor = System.Drawing.Color.White;
            this.dgvBibliotecario.Location = new System.Drawing.Point(3, 192);
            this.dgvBibliotecario.MultiSelect = false;
            this.dgvBibliotecario.Name = "dgvBibliotecario";
            this.dgvBibliotecario.ReadOnly = true;
            this.dgvBibliotecario.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBibliotecario.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvBibliotecario.RowHeadersVisible = false;
            this.dgvBibliotecario.RowHeadersWidth = 35;
            this.dgvBibliotecario.RowTemplate.DividerHeight = 1;
            this.dgvBibliotecario.RowTemplate.Height = 40;
            this.dgvBibliotecario.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBibliotecario.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBibliotecario.Size = new System.Drawing.Size(619, 390);
            this.dgvBibliotecario.TabIndex = 39;
            this.dgvBibliotecario.Theme = Bunifu.UI.WinForms.BunifuDataGridView.PresetThemes.Light;
            // 
            // nmbibliotecarioDataGridViewTextBoxColumn
            // 
            this.nmbibliotecarioDataGridViewTextBoxColumn.DataPropertyName = "nm_bibliotecario";
            this.nmbibliotecarioDataGridViewTextBoxColumn.HeaderText = "Bibliotecario";
            this.nmbibliotecarioDataGridViewTextBoxColumn.Name = "nmbibliotecarioDataGridViewTextBoxColumn";
            this.nmbibliotecarioDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "ds_endereco";
            this.Column1.HeaderText = "Endereço";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // dsemailDataGridViewTextBoxColumn
            // 
            this.dsemailDataGridViewTextBoxColumn.DataPropertyName = "ds_email";
            this.dsemailDataGridViewTextBoxColumn.HeaderText = "E-mail";
            this.dsemailDataGridViewTextBoxColumn.Name = "dsemailDataGridViewTextBoxColumn";
            this.dsemailDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dscpfDataGridViewTextBoxColumn
            // 
            this.dscpfDataGridViewTextBoxColumn.DataPropertyName = "ds_cpf";
            this.dscpfDataGridViewTextBoxColumn.FillWeight = 80F;
            this.dscpfDataGridViewTextBoxColumn.HeaderText = "CPF";
            this.dscpfDataGridViewTextBoxColumn.Name = "dscpfDataGridViewTextBoxColumn";
            this.dscpfDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // dscelularDataGridViewTextBoxColumn
            // 
            this.dscelularDataGridViewTextBoxColumn.DataPropertyName = "ds_celular";
            this.dscelularDataGridViewTextBoxColumn.FillWeight = 80F;
            this.dscelularDataGridViewTextBoxColumn.HeaderText = "Celular";
            this.dscelularDataGridViewTextBoxColumn.Name = "dscelularDataGridViewTextBoxColumn";
            this.dscelularDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tbbibliotecarioBindingSource
            // 
            this.tbbibliotecarioBindingSource.DataSource = typeof(Nsf.FriendsLibrary.DB.tb_bibliotecario);
            // 
            // lblNumberPages
            // 
            this.lblNumberPages.AutoSize = true;
            this.lblNumberPages.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumberPages.Location = new System.Drawing.Point(275, 599);
            this.lblNumberPages.Name = "lblNumberPages";
            this.lblNumberPages.Size = new System.Drawing.Size(47, 19);
            this.lblNumberPages.TabIndex = 38;
            this.lblNumberPages.Text = "34/42";
            // 
            // btnEditar
            // 
            this.btnEditar.BackColor = System.Drawing.Color.White;
            this.btnEditar.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.Image")));
            this.btnEditar.ImageActive = null;
            this.btnEditar.Location = new System.Drawing.Point(540, 58);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(26, 26);
            this.btnEditar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnEditar.TabIndex = 46;
            this.btnEditar.TabStop = false;
            this.btnEditar.Zoom = 10;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.BackColor = System.Drawing.Color.White;
            this.btnRemove.Image = ((System.Drawing.Image)(resources.GetObject("btnRemove.Image")));
            this.btnRemove.ImageActive = null;
            this.btnRemove.Location = new System.Drawing.Point(572, 58);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(26, 26);
            this.btnRemove.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnRemove.TabIndex = 47;
            this.btnRemove.TabStop = false;
            this.btnRemove.Zoom = 10;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnProximo
            // 
            this.btnProximo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnProximo.BackColor = System.Drawing.Color.Transparent;
            this.btnProximo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnProximo.BackgroundImage")));
            this.btnProximo.ButtonText = "Próxima";
            this.btnProximo.ButtonTextMarginLeft = 10;
            this.btnProximo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProximo.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnProximo.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnProximo.DisabledForecolor = System.Drawing.Color.White;
            this.btnProximo.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProximo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnProximo.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnProximo.IconPadding = 10;
            this.btnProximo.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnProximo.IdleBorderColor = System.Drawing.Color.Gainsboro;
            this.btnProximo.IdleBorderRadius = 10;
            this.btnProximo.IdleBorderThickness = 0;
            this.btnProximo.IdleFillColor = System.Drawing.Color.Gainsboro;
            this.btnProximo.IdleIconLeftImage = null;
            this.btnProximo.IdleIconRightImage = ((System.Drawing.Image)(resources.GetObject("btnProximo.IdleIconRightImage")));
            this.btnProximo.Location = new System.Drawing.Point(338, 588);
            this.btnProximo.Name = "btnProximo";
            stateProperties1.BorderColor = System.Drawing.Color.Silver;
            stateProperties1.BorderRadius = 10;
            stateProperties1.BorderThickness = 0;
            stateProperties1.FillColor = System.Drawing.Color.Silver;
            stateProperties1.IconLeftImage = null;
            stateProperties1.IconRightImage = ((System.Drawing.Image)(resources.GetObject("stateProperties1.IconRightImage")));
            this.btnProximo.onHoverState = stateProperties1;
            this.btnProximo.Size = new System.Drawing.Size(111, 41);
            this.btnProximo.TabIndex = 40;
            this.btnProximo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnProximo.Click += new System.EventHandler(this.btnProximo_Click);
            // 
            // btnAnterior
            // 
            this.btnAnterior.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAnterior.BackColor = System.Drawing.Color.Transparent;
            this.btnAnterior.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAnterior.BackgroundImage")));
            this.btnAnterior.ButtonText = "Anterior";
            this.btnAnterior.ButtonTextMarginLeft = -22;
            this.btnAnterior.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnterior.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnAnterior.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnAnterior.DisabledForecolor = System.Drawing.Color.White;
            this.btnAnterior.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnterior.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAnterior.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnAnterior.IconPadding = 10;
            this.btnAnterior.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnAnterior.IdleBorderColor = System.Drawing.Color.Gainsboro;
            this.btnAnterior.IdleBorderRadius = 10;
            this.btnAnterior.IdleBorderThickness = 0;
            this.btnAnterior.IdleFillColor = System.Drawing.Color.Gainsboro;
            this.btnAnterior.IdleIconLeftImage = ((System.Drawing.Image)(resources.GetObject("btnAnterior.IdleIconLeftImage")));
            this.btnAnterior.IdleIconRightImage = null;
            this.btnAnterior.Location = new System.Drawing.Point(147, 588);
            this.btnAnterior.Name = "btnAnterior";
            stateProperties2.BorderColor = System.Drawing.Color.Silver;
            stateProperties2.BorderRadius = 10;
            stateProperties2.BorderThickness = 0;
            stateProperties2.FillColor = System.Drawing.Color.Silver;
            stateProperties2.IconLeftImage = ((System.Drawing.Image)(resources.GetObject("stateProperties2.IconLeftImage")));
            stateProperties2.IconRightImage = null;
            this.btnAnterior.onHoverState = stateProperties2;
            this.btnAnterior.Size = new System.Drawing.Size(111, 41);
            this.btnAnterior.TabIndex = 41;
            this.btnAnterior.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAnterior.Click += new System.EventHandler(this.btnAnterior_Click);
            // 
            // frmFiltroBibliotecario1
            // 
            this.frmFiltroBibliotecario1.BackColor = System.Drawing.Color.White;
            this.frmFiltroBibliotecario1.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frmFiltroBibliotecario1.Location = new System.Drawing.Point(189, 96);
            this.frmFiltroBibliotecario1.Name = "frmFiltroBibliotecario1";
            this.frmFiltroBibliotecario1.Size = new System.Drawing.Size(401, 73);
            this.frmFiltroBibliotecario1.TabIndex = 48;
            // 
            // frmBibliotecario
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.pnSeparatorDados);
            this.Controls.Add(this.cdsHearder);
            this.Controls.Add(this.lblDados);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnProximo);
            this.Controls.Add(this.btnAnterior);
            this.Controls.Add(this.dgvBibliotecario);
            this.Controls.Add(this.lblNumberPages);
            this.Controls.Add(this.frmFiltroBibliotecario1);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmBibliotecario";
            this.Size = new System.Drawing.Size(625, 632);
            this.Load += new System.EventHandler(this.frmBibliotecario_Load);
            this.cdsHearder.ResumeLayout(false);
            this.cdsHearder.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBibliotecario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbbibliotecarioBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemove)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuImageButton btnEditar;
        private Bunifu.Framework.UI.BunifuImageButton btnRemove;
        private System.Windows.Forms.Panel pnSeparatorDados;
        private System.Windows.Forms.Label lblCategoria;
        private Bunifu.UI.WinForms.BunifuRadioButton chkCategoria;
        private Bunifu.UI.WinForms.BunifuRadioButton chkTitulo;
        private Bunifu.Framework.UI.BunifuCards cdsHearder;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Label lblTituloLivro;
        private System.Windows.Forms.Label lblDados;
        private System.Windows.Forms.Panel panel2;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnProximo;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnAnterior;
        private Bunifu.UI.WinForms.BunifuDataGridView dgvBibliotecario;
        private System.Windows.Forms.Label lblNumberPages;
        private System.Windows.Forms.BindingSource tbbibliotecarioBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn nmbibliotecarioDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dsemailDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dscpfDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dscelularDataGridViewTextBoxColumn;
        private Filtro.frmFiltroBibliotecario frmFiltroBibliotecario1;
    }
}
