﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KimtToo.VisualReactive;
using Biblioteca.Banco.Database.Usuario;
using Nsf.FriendsLibrary.DB;
using Biblioteca;
using Biblioteca.Utilitarios.Notificacoes;
using Nsf.FriendsLibrary.Business.Bibliotecario;
using Nsf.FriendsLibrary.APP.User_Controls.Bibliotecario;
using PagedList;

namespace Nsf.FriendsLibrary.APP.User_Controls.Usuario
{
    public partial class frmBibliotecario : UserControl
    {
        public static frmBibliotecario Librarian;

        int pageNumber = 1;
        IPagedList<tb_bibliotecario> list;

        public frmBibliotecario()
        {
            Librarian = this;
            InitializeComponent();

            this.PopulateGrid();
        }

        //Habilita a navegação do TAB CONTROL a partir do CHECKBOX selecionado
        private void chkTitulo_Click(object sender, EventArgs e)
        {
            VSReactive<int>.SetState("librarian", int.Parse(((Control)sender).Tag.ToString()));
        }

        private void ConfigGrid(DataGridView dgv, List<tb_bibliotecario> valor)
        {
            dgv.AutoGenerateColumns = false;
            dgv.DataSource = valor;
        }

        private async Task<IPagedList<tb_bibliotecario>> GetPageListAsync(int pageNumber = 1, int pageSize = 8)
        {
            return await Task.Factory.StartNew(() =>
            {
                BibliotecarioBusiness db = new BibliotecarioBusiness();
                var valor = db.List();
                return valor.ToPagedList(pageNumber, pageSize);
            });
        }

        private void PopulateGrid()
        {
            try
            {
                BibliotecarioBusiness db = new BibliotecarioBusiness();
                var list = db.List();

                dgvBibliotecario.AutoGenerateColumns = false;
                dgvBibliotecario.DataSource = list;
            }
            catch (Exception exception)
            {
                MessageBox.Show("Error: " + exception.Message, "Friends Library", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void PopulateGridWithReaderFilter(string nome)
        {
            try
            {
                BibliotecarioBusiness db = new BibliotecarioBusiness();
                var filter = db.FilterLibrarian(nome);
                this.ConfigGrid(dgvBibliotecario, filter);
            }
            catch (Exception exception)
            {
                MessageBox.Show("Error: " + exception.Message, "Friends Library", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void PopulateGridWithCPFFilter(string cpf)
        {
            try
            {
                BibliotecarioBusiness db = new BibliotecarioBusiness();
                var filter = db.FilterCPF(cpf);
                this.ConfigGrid(dgvBibliotecario, filter);
            }
            catch (Exception exception)
            {
                MessageBox.Show("Error: " + exception.Message, "Friends Library", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private tb_bibliotecario CatchGridRegisters()
        {
            var bibliotecario = dgvBibliotecario.CurrentRow.DataBoundItem as tb_bibliotecario;
            return bibliotecario;
        }

        private void RemoveRegistersFromGrid()
        {
            var delete = CatchGridRegisters();

            BibliotecarioBusiness db = new BibliotecarioBusiness();
            db.Remove(delete.id_bibliotecario);

            this.PopulateGrid();
        }

        private async void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                var update = this.CatchGridRegisters();

                frmAddUpdateLibrarian usercontrol = new frmAddUpdateLibrarian();
                usercontrol.LoadScreen(update);

                frmMain.UserControl.OpenUserControl(usercontrol);
                await GetPageListAsync();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Error: " + exception.Message, "Friends Library", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private async void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                this.RemoveRegistersFromGrid();

                //Saída de dados
                Image imagem = Properties.Resources.Delete;
                string texto = "As informações do Usuário foram removidas com êxito!";
                await Task.Run(() =>  this.SelectorClass(3, texto, imagem));

                //Atualização da gridview
                await GetPageListAsync();
            }
            catch (ArgumentException argumentException)
            {
                MessageBox.Show(argumentException.Message, "Friends Library", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception exception)
            {
                MessageBox.Show("Error: " + exception.Message, "Friends Library", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private async void frmBibliotecario_Load(object sender, EventArgs e)
        {
            try
            {
                list = await GetPageListAsync();

                btnProximo.Enabled = list.HasNextPage;
                btnAnterior.Enabled = list.HasPreviousPage;

                dgvBibliotecario.DataSource = list.ToList();
                lblNumberPages.Text = string.Format("{0}/{1}", pageNumber, list.PageCount);
            }
            catch (Exception exception)
            {
                MessageBox.Show("Error: " + exception.Message, "Friends Library", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private async void btnAnterior_Click(object sender, EventArgs e)
        {
            try
            {
                if (list.HasPreviousPage)
                {
                    list = await GetPageListAsync(--pageNumber);

                    btnAnterior.Enabled = list.HasPreviousPage;
                    btnProximo.Enabled = list.HasNextPage;

                    dgvBibliotecario.DataSource = list.ToList();
                    lblNumberPages.Text = string.Format("{0}/{1}", pageNumber, list.PageCount);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("Error: " + exception.Message, "Friends Library", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private async void btnProximo_Click(object sender, EventArgs e)
        {
            try
            {
                if (list.HasNextPage)
                {
                    list = await GetPageListAsync(++pageNumber);

                    btnAnterior.Enabled = list.HasPreviousPage;
                    btnProximo.Enabled = list.HasNextPage;

                    dgvBibliotecario.DataSource = list.ToList();
                    lblNumberPages.Text = string.Format("{0}/{1}", pageNumber, list.PageCount);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("Error: " + exception.Message, "Friends Library", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
