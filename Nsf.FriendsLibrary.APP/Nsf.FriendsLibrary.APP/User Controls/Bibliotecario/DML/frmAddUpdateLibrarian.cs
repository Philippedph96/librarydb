﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf.FriendsLibrary.DB;
using Nsf.FriendsLibrary.Business.Bibliotecario;
using Biblioteca.Utilitarios.Notificacoes;
using Nsf.FriendsLibrary.Tools.Utilitários.Corrreios;
using Biblioteca.Utilitarios.Email;
using Nsf.FriendsLibrary.Tools.Validações;
using Biblioteca.Validacoes;
using Biblioteca;

namespace Nsf.FriendsLibrary.APP.User_Controls.Bibliotecario
{
    public partial class frmAddUpdateLibrarian : UserControl
    {
        public frmAddUpdateLibrarian()
        {
            InitializeComponent();
            lblTitulo.Text = "Registrar novo Funcionário";

            txtSenha.UseSystemPasswordChar = true;
            ativo = true;
        }

        #region LOADSCREEN - Alteração dos Dados
        tb_bibliotecario librarian = null;
        public void LoadScreen(tb_bibliotecario dto)
        {
            this.librarian = dto;
            lblTitulo.Text = "Atualização das informações dos Funcionários";
            btnSaveUpdate.ButtonText = "Editar";
            btnEditar.Image = Properties.Resources.Password;

            //Dados Pessoais
            txtFuncionario.Text = dto.nm_bibliotecario;
            txtCelular.Text = dto.ds_celular;
            txtEmail.Text = dto.ds_email;
            txtCPF.Text = dto.ds_cpf;

            //Endereço
            txtCEP.Text = dto.ds_cep;
            txtCidade.Text = dto.ds_cidade;
            txtEndereco.Text = dto.ds_endereco;
            txtNumero.Text = dto.nr_numero.ToString();

            //Acesso ao Programa
            txtLogin.Text = dto.ds_login;
            txtSenha.Text = dto.ds_senha;
        }
        #endregion

        #region CRUD - Carregando dados e executando no banco
        private tb_bibliotecario LoadControlsFromForm(tb_bibliotecario control)
        {
            //Dados Pessoais
            control.nm_bibliotecario = txtFuncionario.Text;
            control.ds_celular = txtCelular.Text;
            control.ds_email = txtEmail.Text;
            control.ds_cpf = txtCPF.Text;

            //Endereço
            control.ds_cep = txtCEP.Text;
            control.ds_cidade = txtCidade.Text;
            control.ds_endereco = txtEndereco.Text;
            control.nr_numero = Convert.ToInt32(txtNumero.Text);

            //Acesso ao Programa
            control.ds_login = txtLogin.Text;
            control.ds_senha = txtSenha.Text;

            return control;
        }

        private tb_bibliotecario SaveControls()
        {
            librarian = new tb_bibliotecario();
            var saveControls = this.LoadControlsFromForm(librarian);
            return saveControls;
        }

        private tb_bibliotecario UpdateControls()
        {
            using (LibraryEntities context = new LibraryEntities())
            {
                librarian = context.tb_bibliotecario.Where(c => c.id_bibliotecario == librarian.id_bibliotecario).FirstOrDefault();

                var updateControls = this.LoadControlsFromForm(librarian);
                return updateControls;
            }
        }

        private void btnSaveUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (lblTitulo.Text == "Registrar novo Funcionário")
                {
                    var saveControls = SaveControls();

                    BibliotecarioBusiness db = new BibliotecarioBusiness();
                    db.Save(saveControls);

                    Image imagem = Properties.Resources.ThumpUP;
                    string texto = @"Novo Funcionário registrado na base de dados com êxito!";
                    this.SelectorClass(1, texto, imagem);

                    EnvioEmail acessEmail = new EnvioEmail();
                    string email = txtEmail.Text;
                    string nome = txtFuncionario.Text;
                    acessEmail.EnviarEmail(email, nome, 1);
                }
                else
                {
                    var updateControls = UpdateControls();

                    BibliotecarioBusiness db = new BibliotecarioBusiness();
                    db.Update(updateControls);

                    Image imagem = Properties.Resources.Warning;
                    string texto = @"As informações do funcionário foram atualizadas na base de dados com êxito!";
                    this.SelectorClass(2, texto, imagem);
                }
            }
            catch (ArgumentException business)
            {
                MessageBox.Show(business.Message,
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + "\n" +  "Tente mais tarde.",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }
        #endregion

        #region API CORREIOS - A partir de um valor do formulário (CEP), traz-se mais dois valores que compoeem o endereço
        private void txtCEP_Leave(object sender, EventArgs e)
        {
            try
            {
                if (txtCEP.Text.Length > 0)
                {
                    CorreiosAPI correioApi = new CorreiosAPI();
                    string cep = txtCEP.Text;

                    CorreiosResponse correios = correioApi.BuscarDados(cep);

                    txtEndereco.Text = correios.Logradouro;
                    txtCidade.Text = correios.Localidade;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + "\n\n" + "Verique a internet ou tente mais tarde!!",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }
        #endregion

        #region MOSTRAR E ESCONDER SENHA
        bool ativo;
        private void Unlock()
        {
            if (ativo == true)
            {
                //Destranca
                txtSenha.UseSystemPasswordChar = false;
                btnEditar.Image = Nsf.FriendsLibrary.APP.Properties.Resources.Unlocked;
                ativo = false;
            }
            else
            {
                //Tranca
                txtSenha.UseSystemPasswordChar = true;
                btnEditar.Image = Nsf.FriendsLibrary.APP.Properties.Resources.Password;
                ativo = true;
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            this.Unlock();
        }
        #endregion

        #region VALIDAÇÃO RUNTIME - Validador de BunifuTextBox
        private Image GetImage(bool valid)
        {
            if (valid)
                return imageListIconValidator.Images[0];
            else
                return imageListIconValidator.Images[1];
        }

        private void txtSenha_TextChange(object sender, EventArgs e)
        {
            if (txtSenha.Text.Length > 0)
            {
                btnEditar.Visible = true;
                btnEditar.Image = Properties.Resources.Password;
            }
            else
            {
                btnEditar.Visible = false;
            }

            txtSenha.IconRight = GetImage(ValidarIcones.IsBunifuSenhaValid(txtSenha.Text));
        }

        private void txtFuncionario_TextChange(object sender, EventArgs e)
        {
            limpar = false;

            if (limpar == false)
                txtFuncionario.IconRight = this.GetImage(ValidarIcones.IsBunifuTextBoxEmpty(txtFuncionario.Text));
        }

        private void txtCPF_TextChange(object sender, EventArgs e)
        {
            limpar = false;

            if (limpar == false)
                txtCPF.IconRight = this.GetImage(ValidarIcones.IsBunifuCPFValid(txtCPF.Text));
        }

        private void txtCelular_TextChange(object sender, EventArgs e)
        {
            limpar = false;

            if (limpar == false)
                txtCelular.IconRight = this.GetImage(ValidarIcones.IsBunifuCellPhoneValid(txtCelular.Text));
        }

        private void txtEmail_TextChange(object sender, EventArgs e)
        {
            limpar = false;

            if (limpar == false)
                txtEmail.IconRight = this.GetImage(ValidarIcones.IsBunifuEmailValid(txtEmail.Text));
        }

        private void txtCEP_TextChange(object sender, EventArgs e)
        {
            limpar = false;

            if (limpar == false)
                txtCEP.IconRight = this.GetImage(ValidarIcones.IsBunifuCEPValid(txtCEP.Text));
        }

        private void txtCidade_TextChange(object sender, EventArgs e)
        {
            limpar = false;

            if (limpar == false)
                txtCidade.IconRight = this.GetImage(ValidarIcones.IsBunifuTextBoxEmpty(txtCidade.Text));
        }

        private void txtEndereco_TextChange(object sender, EventArgs e)
        {
            limpar = false;

            if (limpar == false)
                txtEndereco.IconRight = this.GetImage(ValidarIcones.IsBunifuTextBoxEmpty(txtEndereco.Text));
        }

        private void txtNumero_TextChange(object sender, EventArgs e)
        {
            limpar = false;

            if (limpar == false)
                txtNumero.IconRight = this.GetImage(ValidarIcones.IsBunifuNumber(txtNumero.Text));
        }

        private void txtLogin_TextChange(object sender, EventArgs e)
        {
            limpar = false;

            if (limpar == false)
                txtLogin.IconRight = this.GetImage(ValidarIcones.VerifyLogin(txtLogin.Text));
        }


        private void txtCPF_KeyPress(object sender, KeyPressEventArgs e)
        {
            limpar = false;

            if (limpar == false)
                ValidarTextbox.MascaraCPF(txtCPF, e);
        }

        private void txtCelular_KeyPress(object sender, KeyPressEventArgs e)
        {
            limpar = false;

            if (limpar == false)
                ValidarTextbox.MascaraCelular(txtCelular, e);
        }

        private void txtCEP_KeyPress(object sender, KeyPressEventArgs e)
        {
            limpar = false;

            if (limpar == false)
                ValidarTextbox.MascaraCEP(txtCEP, e);
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            limpar = false;

            if (limpar == false)
                ValidarTextbox.SoNumero(e);
        }
        #endregion

        #region LIMPAR CONTROLES
        bool limpar = false;
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            limpar = true;

            frmAddUpdateLibrarian cleanControl = new frmAddUpdateLibrarian();
            frmMain.UserControl.CleanControl(cleanControl);
        }
        #endregion

        private void txtFuncionario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btnSaveUpdate_Click(null, null);
        }
    }
}
