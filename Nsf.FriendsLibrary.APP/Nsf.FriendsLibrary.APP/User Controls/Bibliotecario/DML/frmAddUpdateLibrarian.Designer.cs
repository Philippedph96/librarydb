﻿namespace Nsf.FriendsLibrary.APP.User_Controls.Bibliotecario
{
    partial class frmAddUpdateLibrarian
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAddUpdateLibrarian));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            this.pnSeparator1 = new System.Windows.Forms.Panel();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.lblCPF = new System.Windows.Forms.Label();
            this.lblData = new System.Windows.Forms.Label();
            this.lblCEP = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblNumero = new System.Windows.Forms.Label();
            this.lblEndereco = new System.Windows.Forms.Label();
            this.lblDados = new System.Windows.Forms.Label();
            this.lblAdress = new System.Windows.Forms.Label();
            this.lblEmail = new System.Windows.Forms.Label();
            this.lblFuncionario = new System.Windows.Forms.Label();
            this.pnSeparatorEndereco = new System.Windows.Forms.Panel();
            this.pnSeparatorDados = new System.Windows.Forms.Panel();
            this.lblCondicao = new System.Windows.Forms.Label();
            this.pnSeparatorCondicao = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnCancelar = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.btnSaveUpdate = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.txtCelular = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtSenha = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtCidade = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtNumero = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtLogin = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtCPF = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtCEP = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtEndereco = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtEmail = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtFuncionario = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.imageListIconValidator = new System.Windows.Forms.ImageList(this.components);
            this.btnEditar = new Bunifu.Framework.UI.BunifuImageButton();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditar)).BeginInit();
            this.SuspendLayout();
            // 
            // pnSeparator1
            // 
            this.pnSeparator1.BackColor = System.Drawing.Color.Gainsboro;
            this.pnSeparator1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnSeparator1.Location = new System.Drawing.Point(18, 56);
            this.pnSeparator1.Name = "pnSeparator1";
            this.pnSeparator1.Size = new System.Drawing.Size(590, 1);
            this.pnSeparator1.TabIndex = 1;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTitulo.Location = new System.Drawing.Point(39, 14);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(78, 32);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "{Title}";
            // 
            // lblCPF
            // 
            this.lblCPF.AutoSize = true;
            this.lblCPF.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCPF.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCPF.Location = new System.Drawing.Point(68, 172);
            this.lblCPF.Name = "lblCPF";
            this.lblCPF.Size = new System.Drawing.Size(35, 20);
            this.lblCPF.TabIndex = 6;
            this.lblCPF.Text = "CPF";
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            this.lblData.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblData.Location = new System.Drawing.Point(306, 172);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(118, 20);
            this.lblData.TabIndex = 8;
            this.lblData.Text = "Número Celular";
            // 
            // lblCEP
            // 
            this.lblCEP.AutoSize = true;
            this.lblCEP.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCEP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCEP.Location = new System.Drawing.Point(68, 327);
            this.lblCEP.Name = "lblCEP";
            this.lblCEP.Size = new System.Drawing.Size(35, 20);
            this.lblCEP.TabIndex = 14;
            this.lblCEP.Text = "CEP";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(306, 327);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 20);
            this.label1.TabIndex = 16;
            this.label1.Text = "Cidade";
            // 
            // lblNumero
            // 
            this.lblNumero.AutoSize = true;
            this.lblNumero.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumero.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblNumero.Location = new System.Drawing.Point(419, 393);
            this.lblNumero.Name = "lblNumero";
            this.lblNumero.Size = new System.Drawing.Size(101, 20);
            this.lblNumero.TabIndex = 20;
            this.lblNumero.Text = "Número Casa";
            // 
            // lblEndereco
            // 
            this.lblEndereco.AutoSize = true;
            this.lblEndereco.Font = new System.Drawing.Font("Segoe UI Semibold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEndereco.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblEndereco.Location = new System.Drawing.Point(40, 289);
            this.lblEndereco.Name = "lblEndereco";
            this.lblEndereco.Size = new System.Drawing.Size(89, 25);
            this.lblEndereco.TabIndex = 12;
            this.lblEndereco.Text = "Endereço";
            // 
            // lblDados
            // 
            this.lblDados.AutoSize = true;
            this.lblDados.Font = new System.Drawing.Font("Segoe UI Semibold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDados.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblDados.Location = new System.Drawing.Point(40, 77);
            this.lblDados.Name = "lblDados";
            this.lblDados.Size = new System.Drawing.Size(139, 25);
            this.lblDados.TabIndex = 2;
            this.lblDados.Text = "Dados Pessoais";
            // 
            // lblAdress
            // 
            this.lblAdress.AutoSize = true;
            this.lblAdress.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAdress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblAdress.Location = new System.Drawing.Point(68, 393);
            this.lblAdress.Name = "lblAdress";
            this.lblAdress.Size = new System.Drawing.Size(73, 20);
            this.lblAdress.TabIndex = 18;
            this.lblAdress.Text = "Endereço";
            // 
            // lblEmail
            // 
            this.lblEmail.AutoSize = true;
            this.lblEmail.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblEmail.Location = new System.Drawing.Point(68, 229);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(46, 20);
            this.lblEmail.TabIndex = 10;
            this.lblEmail.Text = "Email";
            // 
            // lblFuncionario
            // 
            this.lblFuncionario.AutoSize = true;
            this.lblFuncionario.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFuncionario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblFuncionario.Location = new System.Drawing.Point(68, 113);
            this.lblFuncionario.Name = "lblFuncionario";
            this.lblFuncionario.Size = new System.Drawing.Size(137, 20);
            this.lblFuncionario.TabIndex = 4;
            this.lblFuncionario.Text = "Nome Funcionário";
            // 
            // pnSeparatorEndereco
            // 
            this.pnSeparatorEndereco.BackColor = System.Drawing.Color.Gainsboro;
            this.pnSeparatorEndereco.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnSeparatorEndereco.Location = new System.Drawing.Point(35, 318);
            this.pnSeparatorEndereco.Name = "pnSeparatorEndereco";
            this.pnSeparatorEndereco.Size = new System.Drawing.Size(530, 1);
            this.pnSeparatorEndereco.TabIndex = 13;
            // 
            // pnSeparatorDados
            // 
            this.pnSeparatorDados.BackColor = System.Drawing.Color.Gainsboro;
            this.pnSeparatorDados.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnSeparatorDados.Location = new System.Drawing.Point(35, 105);
            this.pnSeparatorDados.Name = "pnSeparatorDados";
            this.pnSeparatorDados.Size = new System.Drawing.Size(530, 1);
            this.pnSeparatorDados.TabIndex = 3;
            // 
            // lblCondicao
            // 
            this.lblCondicao.AutoSize = true;
            this.lblCondicao.Font = new System.Drawing.Font("Segoe UI Semibold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCondicao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCondicao.Location = new System.Drawing.Point(40, 458);
            this.lblCondicao.Name = "lblCondicao";
            this.lblCondicao.Size = new System.Drawing.Size(69, 25);
            this.lblCondicao.TabIndex = 22;
            this.lblCondicao.Text = "Acesso";
            // 
            // pnSeparatorCondicao
            // 
            this.pnSeparatorCondicao.BackColor = System.Drawing.Color.Gainsboro;
            this.pnSeparatorCondicao.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnSeparatorCondicao.Location = new System.Drawing.Point(35, 486);
            this.pnSeparatorCondicao.Name = "pnSeparatorCondicao";
            this.pnSeparatorCondicao.Size = new System.Drawing.Size(530, 1);
            this.pnSeparatorCondicao.TabIndex = 23;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(306, 492);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 20);
            this.label2.TabIndex = 26;
            this.label2.Text = "Senha";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(68, 492);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 20);
            this.label3.TabIndex = 24;
            this.label3.Text = "Login";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Location = new System.Drawing.Point(18, 555);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(590, 1);
            this.panel1.TabIndex = 28;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancelar.BackColor = System.Drawing.Color.Transparent;
            this.btnCancelar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancelar.BackgroundImage")));
            this.btnCancelar.ButtonText = "Limpar";
            this.btnCancelar.ButtonTextMarginLeft = 0;
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnCancelar.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnCancelar.DisabledForecolor = System.Drawing.Color.White;
            this.btnCancelar.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnCancelar.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnCancelar.IconPadding = 10;
            this.btnCancelar.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnCancelar.IdleBorderColor = System.Drawing.Color.Silver;
            this.btnCancelar.IdleBorderRadius = 40;
            this.btnCancelar.IdleBorderThickness = 0;
            this.btnCancelar.IdleFillColor = System.Drawing.Color.Silver;
            this.btnCancelar.IdleIconLeftImage = null;
            this.btnCancelar.IdleIconRightImage = null;
            this.btnCancelar.Location = new System.Drawing.Point(330, 568);
            this.btnCancelar.Name = "btnCancelar";
            stateProperties1.BorderColor = System.Drawing.Color.WhiteSmoke;
            stateProperties1.BorderRadius = 40;
            stateProperties1.BorderThickness = 0;
            stateProperties1.FillColor = System.Drawing.Color.WhiteSmoke;
            stateProperties1.IconLeftImage = null;
            stateProperties1.IconRightImage = null;
            this.btnCancelar.onHoverState = stateProperties1;
            this.btnCancelar.Size = new System.Drawing.Size(136, 45);
            this.btnCancelar.TabIndex = 29;
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSaveUpdate
            // 
            this.btnSaveUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveUpdate.BackColor = System.Drawing.Color.Transparent;
            this.btnSaveUpdate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSaveUpdate.BackgroundImage")));
            this.btnSaveUpdate.ButtonText = "Salvar";
            this.btnSaveUpdate.ButtonTextMarginLeft = 0;
            this.btnSaveUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSaveUpdate.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnSaveUpdate.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnSaveUpdate.DisabledForecolor = System.Drawing.Color.White;
            this.btnSaveUpdate.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveUpdate.ForeColor = System.Drawing.Color.White;
            this.btnSaveUpdate.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnSaveUpdate.IconPadding = 10;
            this.btnSaveUpdate.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnSaveUpdate.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnSaveUpdate.IdleBorderRadius = 40;
            this.btnSaveUpdate.IdleBorderThickness = 0;
            this.btnSaveUpdate.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnSaveUpdate.IdleIconLeftImage = null;
            this.btnSaveUpdate.IdleIconRightImage = null;
            this.btnSaveUpdate.Location = new System.Drawing.Point(472, 568);
            this.btnSaveUpdate.Name = "btnSaveUpdate";
            stateProperties2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(133)))), ((int)(((byte)(255)))));
            stateProperties2.BorderRadius = 40;
            stateProperties2.BorderThickness = 0;
            stateProperties2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(133)))), ((int)(((byte)(255)))));
            stateProperties2.IconLeftImage = null;
            stateProperties2.IconRightImage = null;
            this.btnSaveUpdate.onHoverState = stateProperties2;
            this.btnSaveUpdate.Size = new System.Drawing.Size(136, 45);
            this.btnSaveUpdate.TabIndex = 30;
            this.btnSaveUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSaveUpdate.Click += new System.EventHandler(this.btnSaveUpdate_Click);
            // 
            // txtCelular
            // 
            this.txtCelular.AcceptsReturn = false;
            this.txtCelular.AcceptsTab = false;
            this.txtCelular.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtCelular.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtCelular.BackColor = System.Drawing.Color.Gainsboro;
            this.txtCelular.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtCelular.BackgroundImage")));
            this.txtCelular.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtCelular.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtCelular.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(38)))), ((int)(((byte)(157)))));
            this.txtCelular.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtCelular.BorderRadius = 1;
            this.txtCelular.BorderThickness = 1;
            this.txtCelular.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtCelular.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCelular.DefaultText = "";
            this.txtCelular.FillColor = System.Drawing.Color.Gainsboro;
            this.txtCelular.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtCelular.HideSelection = true;
            this.txtCelular.IconLeft = ((System.Drawing.Image)(resources.GetObject("txtCelular.IconLeft")));
            this.txtCelular.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtCelular.IconPadding = 4;
            this.txtCelular.IconRight = null;
            this.txtCelular.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtCelular.Location = new System.Drawing.Point(310, 192);
            this.txtCelular.MaxLength = 15;
            this.txtCelular.MinimumSize = new System.Drawing.Size(100, 30);
            this.txtCelular.Modified = false;
            this.txtCelular.Name = "txtCelular";
            this.txtCelular.PasswordChar = '\0';
            this.txtCelular.ReadOnly = false;
            this.txtCelular.SelectedText = "";
            this.txtCelular.SelectionLength = 0;
            this.txtCelular.SelectionStart = 0;
            this.txtCelular.ShortcutsEnabled = true;
            this.txtCelular.Size = new System.Drawing.Size(229, 30);
            this.txtCelular.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtCelular.TabIndex = 9;
            this.txtCelular.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCelular.TextMarginLeft = 10;
            this.txtCelular.TextPlaceholder = "";
            this.txtCelular.UseSystemPasswordChar = false;
            this.txtCelular.TextChange += new System.EventHandler(this.txtCelular_TextChange);
            this.txtCelular.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFuncionario_KeyDown);
            this.txtCelular.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCelular_KeyPress);
            // 
            // txtSenha
            // 
            this.txtSenha.AcceptsReturn = false;
            this.txtSenha.AcceptsTab = false;
            this.txtSenha.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtSenha.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtSenha.BackColor = System.Drawing.Color.Gainsboro;
            this.txtSenha.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtSenha.BackgroundImage")));
            this.txtSenha.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtSenha.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtSenha.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(38)))), ((int)(((byte)(157)))));
            this.txtSenha.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtSenha.BorderRadius = 1;
            this.txtSenha.BorderThickness = 1;
            this.txtSenha.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtSenha.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSenha.DefaultText = "";
            this.txtSenha.FillColor = System.Drawing.Color.Gainsboro;
            this.txtSenha.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtSenha.HideSelection = true;
            this.txtSenha.IconLeft = ((System.Drawing.Image)(resources.GetObject("txtSenha.IconLeft")));
            this.txtSenha.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtSenha.IconPadding = 4;
            this.txtSenha.IconRight = null;
            this.txtSenha.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtSenha.Location = new System.Drawing.Point(310, 514);
            this.txtSenha.MaxLength = 16;
            this.txtSenha.MinimumSize = new System.Drawing.Size(100, 30);
            this.txtSenha.Modified = false;
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.PasswordChar = '\0';
            this.txtSenha.ReadOnly = false;
            this.txtSenha.SelectedText = "";
            this.txtSenha.SelectionLength = 0;
            this.txtSenha.SelectionStart = 0;
            this.txtSenha.ShortcutsEnabled = true;
            this.txtSenha.Size = new System.Drawing.Size(229, 30);
            this.txtSenha.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtSenha.TabIndex = 27;
            this.txtSenha.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtSenha.TextMarginLeft = 10;
            this.txtSenha.TextPlaceholder = "";
            this.txtSenha.UseSystemPasswordChar = false;
            this.txtSenha.TextChange += new System.EventHandler(this.txtSenha_TextChange);
            this.txtSenha.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFuncionario_KeyDown);
            // 
            // txtCidade
            // 
            this.txtCidade.AcceptsReturn = false;
            this.txtCidade.AcceptsTab = false;
            this.txtCidade.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtCidade.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtCidade.BackColor = System.Drawing.Color.Gainsboro;
            this.txtCidade.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtCidade.BackgroundImage")));
            this.txtCidade.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtCidade.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtCidade.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(38)))), ((int)(((byte)(157)))));
            this.txtCidade.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtCidade.BorderRadius = 1;
            this.txtCidade.BorderThickness = 1;
            this.txtCidade.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtCidade.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCidade.DefaultText = "";
            this.txtCidade.FillColor = System.Drawing.Color.Gainsboro;
            this.txtCidade.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtCidade.HideSelection = true;
            this.txtCidade.IconLeft = global::Nsf.FriendsLibrary.APP.Properties.Resources.WorldWide;
            this.txtCidade.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtCidade.IconPadding = 4;
            this.txtCidade.IconRight = null;
            this.txtCidade.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtCidade.Location = new System.Drawing.Point(310, 349);
            this.txtCidade.MaxLength = 50;
            this.txtCidade.MinimumSize = new System.Drawing.Size(100, 30);
            this.txtCidade.Modified = false;
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.PasswordChar = '\0';
            this.txtCidade.ReadOnly = false;
            this.txtCidade.SelectedText = "";
            this.txtCidade.SelectionLength = 0;
            this.txtCidade.SelectionStart = 0;
            this.txtCidade.ShortcutsEnabled = true;
            this.txtCidade.Size = new System.Drawing.Size(229, 30);
            this.txtCidade.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtCidade.TabIndex = 17;
            this.txtCidade.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCidade.TextMarginLeft = 10;
            this.txtCidade.TextPlaceholder = "";
            this.txtCidade.UseSystemPasswordChar = false;
            this.txtCidade.TextChange += new System.EventHandler(this.txtCidade_TextChange);
            this.txtCidade.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFuncionario_KeyDown);
            // 
            // txtNumero
            // 
            this.txtNumero.AcceptsReturn = false;
            this.txtNumero.AcceptsTab = false;
            this.txtNumero.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtNumero.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtNumero.BackColor = System.Drawing.Color.Gainsboro;
            this.txtNumero.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtNumero.BackgroundImage")));
            this.txtNumero.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtNumero.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtNumero.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(38)))), ((int)(((byte)(157)))));
            this.txtNumero.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtNumero.BorderRadius = 1;
            this.txtNumero.BorderThickness = 1;
            this.txtNumero.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtNumero.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumero.DefaultText = "";
            this.txtNumero.FillColor = System.Drawing.Color.Gainsboro;
            this.txtNumero.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtNumero.HideSelection = true;
            this.txtNumero.IconLeft = global::Nsf.FriendsLibrary.APP.Properties.Resources.Número;
            this.txtNumero.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtNumero.IconPadding = 4;
            this.txtNumero.IconRight = null;
            this.txtNumero.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtNumero.Location = new System.Drawing.Point(423, 414);
            this.txtNumero.MaxLength = 32767;
            this.txtNumero.MinimumSize = new System.Drawing.Size(100, 30);
            this.txtNumero.Modified = false;
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.PasswordChar = '\0';
            this.txtNumero.ReadOnly = false;
            this.txtNumero.SelectedText = "";
            this.txtNumero.SelectionLength = 0;
            this.txtNumero.SelectionStart = 0;
            this.txtNumero.ShortcutsEnabled = true;
            this.txtNumero.Size = new System.Drawing.Size(116, 30);
            this.txtNumero.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtNumero.TabIndex = 21;
            this.txtNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtNumero.TextMarginLeft = 10;
            this.txtNumero.TextPlaceholder = "";
            this.txtNumero.UseSystemPasswordChar = false;
            this.txtNumero.TextChange += new System.EventHandler(this.txtNumero_TextChange);
            this.txtNumero.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFuncionario_KeyDown);
            this.txtNumero.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumero_KeyPress);
            // 
            // txtLogin
            // 
            this.txtLogin.AcceptsReturn = false;
            this.txtLogin.AcceptsTab = false;
            this.txtLogin.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtLogin.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtLogin.BackColor = System.Drawing.Color.Gainsboro;
            this.txtLogin.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtLogin.BackgroundImage")));
            this.txtLogin.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtLogin.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtLogin.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(38)))), ((int)(((byte)(157)))));
            this.txtLogin.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtLogin.BorderRadius = 1;
            this.txtLogin.BorderThickness = 1;
            this.txtLogin.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtLogin.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLogin.DefaultText = "";
            this.txtLogin.FillColor = System.Drawing.Color.Gainsboro;
            this.txtLogin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtLogin.HideSelection = true;
            this.txtLogin.IconLeft = ((System.Drawing.Image)(resources.GetObject("txtLogin.IconLeft")));
            this.txtLogin.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtLogin.IconPadding = 4;
            this.txtLogin.IconRight = null;
            this.txtLogin.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtLogin.Location = new System.Drawing.Point(72, 514);
            this.txtLogin.MaxLength = 30;
            this.txtLogin.MinimumSize = new System.Drawing.Size(100, 30);
            this.txtLogin.Modified = false;
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.PasswordChar = '\0';
            this.txtLogin.ReadOnly = false;
            this.txtLogin.SelectedText = "";
            this.txtLogin.SelectionLength = 0;
            this.txtLogin.SelectionStart = 0;
            this.txtLogin.ShortcutsEnabled = true;
            this.txtLogin.Size = new System.Drawing.Size(220, 30);
            this.txtLogin.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtLogin.TabIndex = 25;
            this.txtLogin.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtLogin.TextMarginLeft = 10;
            this.txtLogin.TextPlaceholder = "";
            this.txtLogin.UseSystemPasswordChar = false;
            this.txtLogin.TextChange += new System.EventHandler(this.txtLogin_TextChange);
            this.txtLogin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFuncionario_KeyDown);
            // 
            // txtCPF
            // 
            this.txtCPF.AcceptsReturn = false;
            this.txtCPF.AcceptsTab = false;
            this.txtCPF.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtCPF.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtCPF.BackColor = System.Drawing.Color.Gainsboro;
            this.txtCPF.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtCPF.BackgroundImage")));
            this.txtCPF.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtCPF.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtCPF.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(38)))), ((int)(((byte)(157)))));
            this.txtCPF.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtCPF.BorderRadius = 1;
            this.txtCPF.BorderThickness = 1;
            this.txtCPF.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtCPF.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCPF.DefaultText = "";
            this.txtCPF.FillColor = System.Drawing.Color.Gainsboro;
            this.txtCPF.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtCPF.HideSelection = true;
            this.txtCPF.IconLeft = global::Nsf.FriendsLibrary.APP.Properties.Resources.Identification;
            this.txtCPF.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtCPF.IconPadding = 4;
            this.txtCPF.IconRight = null;
            this.txtCPF.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtCPF.Location = new System.Drawing.Point(72, 192);
            this.txtCPF.MaxLength = 14;
            this.txtCPF.MinimumSize = new System.Drawing.Size(100, 30);
            this.txtCPF.Modified = false;
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.PasswordChar = '\0';
            this.txtCPF.ReadOnly = false;
            this.txtCPF.SelectedText = "";
            this.txtCPF.SelectionLength = 0;
            this.txtCPF.SelectionStart = 0;
            this.txtCPF.ShortcutsEnabled = true;
            this.txtCPF.Size = new System.Drawing.Size(220, 30);
            this.txtCPF.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtCPF.TabIndex = 7;
            this.txtCPF.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCPF.TextMarginLeft = 10;
            this.txtCPF.TextPlaceholder = "";
            this.txtCPF.UseSystemPasswordChar = false;
            this.txtCPF.TextChange += new System.EventHandler(this.txtCPF_TextChange);
            this.txtCPF.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFuncionario_KeyDown);
            this.txtCPF.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCPF_KeyPress);
            // 
            // txtCEP
            // 
            this.txtCEP.AcceptsReturn = false;
            this.txtCEP.AcceptsTab = false;
            this.txtCEP.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtCEP.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtCEP.BackColor = System.Drawing.Color.Gainsboro;
            this.txtCEP.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtCEP.BackgroundImage")));
            this.txtCEP.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtCEP.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtCEP.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(38)))), ((int)(((byte)(157)))));
            this.txtCEP.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtCEP.BorderRadius = 1;
            this.txtCEP.BorderThickness = 1;
            this.txtCEP.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtCEP.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCEP.DefaultText = "";
            this.txtCEP.FillColor = System.Drawing.Color.Gainsboro;
            this.txtCEP.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtCEP.HideSelection = true;
            this.txtCEP.IconLeft = global::Nsf.FriendsLibrary.APP.Properties.Resources.Map;
            this.txtCEP.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtCEP.IconPadding = 4;
            this.txtCEP.IconRight = null;
            this.txtCEP.IconRightCursor = System.Windows.Forms.Cursors.Hand;
            this.txtCEP.Location = new System.Drawing.Point(72, 349);
            this.txtCEP.MaxLength = 9;
            this.txtCEP.MinimumSize = new System.Drawing.Size(100, 30);
            this.txtCEP.Modified = false;
            this.txtCEP.Name = "txtCEP";
            this.txtCEP.PasswordChar = '\0';
            this.txtCEP.ReadOnly = false;
            this.txtCEP.SelectedText = "";
            this.txtCEP.SelectionLength = 0;
            this.txtCEP.SelectionStart = 0;
            this.txtCEP.ShortcutsEnabled = true;
            this.txtCEP.Size = new System.Drawing.Size(220, 30);
            this.txtCEP.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtCEP.TabIndex = 15;
            this.txtCEP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCEP.TextMarginLeft = 10;
            this.txtCEP.TextPlaceholder = "";
            this.txtCEP.UseSystemPasswordChar = false;
            this.txtCEP.TextChange += new System.EventHandler(this.txtCEP_TextChange);
            this.txtCEP.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFuncionario_KeyDown);
            this.txtCEP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCEP_KeyPress);
            this.txtCEP.Leave += new System.EventHandler(this.txtCEP_Leave);
            // 
            // txtEndereco
            // 
            this.txtEndereco.AcceptsReturn = false;
            this.txtEndereco.AcceptsTab = false;
            this.txtEndereco.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtEndereco.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtEndereco.BackColor = System.Drawing.Color.Gainsboro;
            this.txtEndereco.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtEndereco.BackgroundImage")));
            this.txtEndereco.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtEndereco.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtEndereco.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(38)))), ((int)(((byte)(157)))));
            this.txtEndereco.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtEndereco.BorderRadius = 1;
            this.txtEndereco.BorderThickness = 1;
            this.txtEndereco.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtEndereco.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEndereco.DefaultText = "";
            this.txtEndereco.FillColor = System.Drawing.Color.Gainsboro;
            this.txtEndereco.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtEndereco.HideSelection = true;
            this.txtEndereco.IconLeft = global::Nsf.FriendsLibrary.APP.Properties.Resources.Home;
            this.txtEndereco.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtEndereco.IconPadding = 4;
            this.txtEndereco.IconRight = null;
            this.txtEndereco.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.txtEndereco.Location = new System.Drawing.Point(72, 414);
            this.txtEndereco.MaxLength = 100;
            this.txtEndereco.MinimumSize = new System.Drawing.Size(100, 30);
            this.txtEndereco.Modified = false;
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.PasswordChar = '\0';
            this.txtEndereco.ReadOnly = false;
            this.txtEndereco.SelectedText = "";
            this.txtEndereco.SelectionLength = 0;
            this.txtEndereco.SelectionStart = 0;
            this.txtEndereco.ShortcutsEnabled = true;
            this.txtEndereco.Size = new System.Drawing.Size(333, 30);
            this.txtEndereco.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtEndereco.TabIndex = 19;
            this.txtEndereco.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtEndereco.TextMarginLeft = 10;
            this.txtEndereco.TextPlaceholder = "";
            this.txtEndereco.UseSystemPasswordChar = false;
            this.txtEndereco.TextChange += new System.EventHandler(this.txtEndereco_TextChange);
            this.txtEndereco.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFuncionario_KeyDown);
            // 
            // txtEmail
            // 
            this.txtEmail.AcceptsReturn = false;
            this.txtEmail.AcceptsTab = false;
            this.txtEmail.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtEmail.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtEmail.BackColor = System.Drawing.Color.Gainsboro;
            this.txtEmail.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtEmail.BackgroundImage")));
            this.txtEmail.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtEmail.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtEmail.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(38)))), ((int)(((byte)(157)))));
            this.txtEmail.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtEmail.BorderRadius = 1;
            this.txtEmail.BorderThickness = 1;
            this.txtEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtEmail.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEmail.DefaultText = "";
            this.txtEmail.FillColor = System.Drawing.Color.Gainsboro;
            this.txtEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtEmail.HideSelection = true;
            this.txtEmail.IconLeft = global::Nsf.FriendsLibrary.APP.Properties.Resources.Email1;
            this.txtEmail.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtEmail.IconPadding = 4;
            this.txtEmail.IconRight = null;
            this.txtEmail.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.txtEmail.Location = new System.Drawing.Point(72, 251);
            this.txtEmail.MaxLength = 80;
            this.txtEmail.MinimumSize = new System.Drawing.Size(100, 30);
            this.txtEmail.Modified = false;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.PasswordChar = '\0';
            this.txtEmail.ReadOnly = false;
            this.txtEmail.SelectedText = "";
            this.txtEmail.SelectionLength = 0;
            this.txtEmail.SelectionStart = 0;
            this.txtEmail.ShortcutsEnabled = true;
            this.txtEmail.Size = new System.Drawing.Size(467, 30);
            this.txtEmail.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtEmail.TabIndex = 11;
            this.txtEmail.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtEmail.TextMarginLeft = 10;
            this.txtEmail.TextPlaceholder = "";
            this.txtEmail.UseSystemPasswordChar = false;
            this.txtEmail.TextChange += new System.EventHandler(this.txtEmail_TextChange);
            this.txtEmail.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFuncionario_KeyDown);
            // 
            // txtFuncionario
            // 
            this.txtFuncionario.AcceptsReturn = false;
            this.txtFuncionario.AcceptsTab = false;
            this.txtFuncionario.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtFuncionario.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtFuncionario.BackColor = System.Drawing.Color.Gainsboro;
            this.txtFuncionario.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtFuncionario.BackgroundImage")));
            this.txtFuncionario.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtFuncionario.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtFuncionario.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(38)))), ((int)(((byte)(157)))));
            this.txtFuncionario.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtFuncionario.BorderRadius = 1;
            this.txtFuncionario.BorderThickness = 1;
            this.txtFuncionario.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtFuncionario.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFuncionario.DefaultText = "";
            this.txtFuncionario.FillColor = System.Drawing.Color.Gainsboro;
            this.txtFuncionario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtFuncionario.HideSelection = true;
            this.txtFuncionario.IconLeft = global::Nsf.FriendsLibrary.APP.Properties.Resources.User;
            this.txtFuncionario.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtFuncionario.IconPadding = 4;
            this.txtFuncionario.IconRight = null;
            this.txtFuncionario.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.txtFuncionario.Location = new System.Drawing.Point(72, 135);
            this.txtFuncionario.MaxLength = 100;
            this.txtFuncionario.MinimumSize = new System.Drawing.Size(100, 30);
            this.txtFuncionario.Modified = false;
            this.txtFuncionario.Name = "txtFuncionario";
            this.txtFuncionario.PasswordChar = '\0';
            this.txtFuncionario.ReadOnly = false;
            this.txtFuncionario.SelectedText = "";
            this.txtFuncionario.SelectionLength = 0;
            this.txtFuncionario.SelectionStart = 0;
            this.txtFuncionario.ShortcutsEnabled = true;
            this.txtFuncionario.Size = new System.Drawing.Size(467, 30);
            this.txtFuncionario.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtFuncionario.TabIndex = 5;
            this.txtFuncionario.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtFuncionario.TextMarginLeft = 10;
            this.txtFuncionario.TextPlaceholder = "";
            this.txtFuncionario.UseSystemPasswordChar = false;
            this.txtFuncionario.TextChange += new System.EventHandler(this.txtFuncionario_TextChange);
            this.txtFuncionario.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtFuncionario_KeyDown);
            // 
            // imageListIconValidator
            // 
            this.imageListIconValidator.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListIconValidator.ImageStream")));
            this.imageListIconValidator.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListIconValidator.Images.SetKeyName(0, "Checkmark.png");
            this.imageListIconValidator.Images.SetKeyName(1, "Error.png");
            // 
            // btnEditar
            // 
            this.btnEditar.BackColor = System.Drawing.Color.White;
            this.btnEditar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEditar.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.Image")));
            this.btnEditar.ImageActive = null;
            this.btnEditar.Location = new System.Drawing.Point(545, 514);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(27, 30);
            this.btnEditar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnEditar.TabIndex = 47;
            this.btnEditar.TabStop = false;
            this.btnEditar.Zoom = 10;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // frmAddUpdateLibrarian
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnSaveUpdate);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblCondicao);
            this.Controls.Add(this.pnSeparatorCondicao);
            this.Controls.Add(this.txtCelular);
            this.Controls.Add(this.txtSenha);
            this.Controls.Add(this.txtCidade);
            this.Controls.Add(this.txtNumero);
            this.Controls.Add(this.txtLogin);
            this.Controls.Add(this.txtCPF);
            this.Controls.Add(this.txtCEP);
            this.Controls.Add(this.txtEndereco);
            this.Controls.Add(this.txtEmail);
            this.Controls.Add(this.txtFuncionario);
            this.Controls.Add(this.lblCPF);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblData);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblCEP);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblNumero);
            this.Controls.Add(this.lblEndereco);
            this.Controls.Add(this.lblDados);
            this.Controls.Add(this.lblAdress);
            this.Controls.Add(this.lblEmail);
            this.Controls.Add(this.lblFuncionario);
            this.Controls.Add(this.pnSeparatorEndereco);
            this.Controls.Add(this.pnSeparatorDados);
            this.Controls.Add(this.pnSeparator1);
            this.Controls.Add(this.lblTitulo);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmAddUpdateLibrarian";
            this.Size = new System.Drawing.Size(625, 632);
            ((System.ComponentModel.ISupportInitialize)(this.btnEditar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnSeparator1;
        private System.Windows.Forms.Label lblTitulo;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtCelular;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtCidade;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtNumero;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtCPF;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtCEP;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtEndereco;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtEmail;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtFuncionario;
        private System.Windows.Forms.Label lblCPF;
        private System.Windows.Forms.Label lblData;
        private System.Windows.Forms.Label lblCEP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblNumero;
        private System.Windows.Forms.Label lblEndereco;
        private System.Windows.Forms.Label lblDados;
        private System.Windows.Forms.Label lblAdress;
        private System.Windows.Forms.Label lblEmail;
        private System.Windows.Forms.Label lblFuncionario;
        private System.Windows.Forms.Panel pnSeparatorEndereco;
        private System.Windows.Forms.Panel pnSeparatorDados;
        private System.Windows.Forms.Label lblCondicao;
        private System.Windows.Forms.Panel pnSeparatorCondicao;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtLogin;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtSenha;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnCancelar;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnSaveUpdate;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ImageList imageListIconValidator;
        private Bunifu.Framework.UI.BunifuImageButton btnEditar;
    }
}
