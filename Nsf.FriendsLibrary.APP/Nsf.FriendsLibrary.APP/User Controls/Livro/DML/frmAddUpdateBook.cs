﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf.FriendsLibrary.Tools;
using Nsf.FriendsLibrary.APP.User_Controls.Complemento_dos_Livros.Autor;
using Nsf.FriendsLibrary.Business.Complementos_dos_Livros.Autor;
using Nsf.FriendsLibrary.DB;
using Nsf.FriendsLibrary.Business.Complementos_dos_Livros.Categoria;
using Nsf.FriendsLibrary.Business.Complementos_dos_Livros.Editora;
using System.ComponentModel.DataAnnotations;
using Nsf.FriendsLibrary.Tools.Validações;
using Biblioteca.Utilitarios.ImagemPLugin;
using Nsf.FriendsLibrary.Business.Livro;
using Biblioteca.Utilitarios.Notificacoes;
using System.Threading;
using Biblioteca.Validacoes;
using Biblioteca;

namespace Nsf.FriendsLibrary.APP.User_Controls.Livro
{
    public partial class frmAddUpdateBook : UserControl
    {
        public frmAddUpdateBook()
        {
            InitializeComponent();
            lblTitulo.Text = "Registre novos Livros";

            cboAutor.Items.Add("Selecione");
            cboEditora.Items.Add("Selecione");
            cboCategoria.Items.Add("Selecione");
        }

        #region TimerEffect
        int valor1 = 0;
        private void TimerAddCombo_Tick(object sender, EventArgs e)
        {
            if (valor1 > 110)
            {
                TimerAddCombo.Stop();
                verificadora3 = false;
                valor1 = 0;
            }
            else
            {
                //Fecha o Panel com a Animação
                pnCombo.Visible = true;
                pnCombo.Size = new Size(pnCombo.Width, valor1);
                valor1 += 10;
            }
        }

        int valor2 = 110;
        private void TimerHideCombo_Tick(object sender, EventArgs e)
        {
            if (valor2 < 0)
            {
                pnCombo.Visible = false;
                TimerHideCombo.Stop();
                verificadora3 = true;
                valor2 = 110;
            }
            else
            {
                pnCombo.Size = new Size(pnCombo.Width, valor2);
                valor2 -= 10;
            }
        }

        bool verificadora3 = true;
        private void btnPlus_Click(object sender, EventArgs e)
        {
            if (verificadora3 == true)
                TimerAddCombo.Start();
            else
                TimerHideCombo.Start();

        }
        #endregion

        #region Chamada Formulário
        private void btnAutor_Click(object sender, EventArgs e)
        {
            frmAutor tela = new frmAutor();
            tela.ShowDialog();
            LoadComboboxAutor();
        }

        private void btnCategoria_Click(object sender, EventArgs e)
        {
            frmCategoria tela = new frmCategoria();
            tela.ShowDialog();
            LoadComboboxCategoria();
        }

        private void btnEditora_Click(object sender, EventArgs e)
        {
            frmEditora tela = new frmEditora();
            tela.ShowDialog();
            LoadComboboxEditora();
        }
        #endregion

        #region LoadCombobox
        private void LoadComboboxAutor()
        {
            try
            {
                AutorBusiness db = new AutorBusiness();
                var autor = db.List();

                cboAutor.ValueMember = nameof(tb_autor.id_autor);
                cboAutor.DisplayMember = nameof(tb_autor.nm_autor);

                cboAutor.Items.Remove("Selecione");
                cboAutor.DataSource = autor;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro" + ex.Message + "/n" + "Tente mais tarde.",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }

        }

        private void LoadComboboxCategoria()
        {
            try
            {
                CategoriaBusiness db = new CategoriaBusiness();
                var categoria = db.List();

                cboCategoria.ValueMember = nameof(tb_categoria.id_categoria);
                cboCategoria.DisplayMember = nameof(tb_categoria.nm_categoria);

                cboCategoria.DataSource = categoria;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro" + ex.Message + "/n" + "Tente mais tarde.",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private void LoadComboboxEditora()
        {
            try
            {
                EditoraBusiness db = new EditoraBusiness();
                var editora = db.List();

                cboEditora.ValueMember = nameof(tb_editora.id_editora);
                cboEditora.DisplayMember = nameof(tb_editora.nm_editora);

                cboEditora.DataSource = editora;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro" + ex.Message + "/n" + "Tente mais tarde.",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private void cboAutor_Click(object sender, EventArgs e)
        {
            this.LoadComboboxAutor();
        }

        private void cboEditora_Click(object sender, EventArgs e)
        {
            this.LoadComboboxEditora();
        }

        private void cboCategoria_Click(object sender, EventArgs e)
        {
            this.LoadComboboxCategoria();
        }
        #endregion

        #region Botão Cancelar / Limpar Formulário

        bool validar = false;
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            validar = true;

            frmAddUpdateBook cleanControl = new frmAddUpdateBook();
            frmMain.UserControl.CleanControl(cleanControl);
        }
        #endregion

        #region Importar Imagens para o Formulário
        private void AbrirDialogo()
        {
            Thread td = new Thread(new ThreadStart(this.EscolherArquivo));
            td.SetApartmentState(ApartmentState.STA);
            td.IsBackground = true;
            td.Start();
        }

        [STAThread]
        private void EscolherArquivo()
        {
            try
            {
                using (var SearchImage = new OpenFileDialog())
                {
                    SearchImage.Filter = "JPG Files(*.jpg)|*.jpg|PNG Files (*.png)|*.png";

                    if (SearchImage.ShowDialog() == DialogResult.OK)
                        imgAutor.ImageLocation = SearchImage.FileName;
                    else
                        if (lblTitulo.Text == "Alterar as informações dos Livros")
                        imgAutor.ImageLocation = SearchImage.FileName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro" + ex.Message + "/n" + "Tente mais tarde.",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        [STAThread]
        private void btnProcurar_Click(object sender, EventArgs e)
        {
            AbrirDialogo();
        }

        #endregion

        #region LOADSCREEN - Alteração dos Dados
        tb_livro livro = null;
        //Método que irá carregar todos os campos com os valares passados por parâmetros
        public void LoadScreen(tb_livro dto)
        {
            //Com a variável de escopo, eu pego o valor passado pelo parâmetro
            this.livro = dto;
            btnSaveUpdate.ButtonText = "Editar";
            lblTitulo.Text = "Altere as informações dos Livros";

            this.LoadComboboxAutor();
            this.LoadComboboxEditora();
            this.LoadComboboxCategoria();

            //Chaves (Primárias e/ou Estrangeiras)
            lblID.Text = dto.id_livro.ToString();
            cboAutor.SelectedValue = dto.id_autor;
            cboEditora.SelectedValue = dto.id_editora;
            cboCategoria.SelectedValue = dto.id_categoria;

            //Campos de Texto (Não necessariamente que aceitam somente textos, mas que estão recenbendo essa conversão)
            txtTitulo.Text = dto.nm_livro;
            txtNumero.Text = dto.nr_paginas.ToString();
            txtAno.Text = dto.dt_ano.ToString();

            //Campos Booleanos (Para fazer o teste lógico)
            chkStatus.Checked = dto.bl_status;

            //Imagem
            imgAutor.Image = ImagemPlugin.ConverterParaImagem(dto.img_livro);
        }
        #endregion

        #region CRUD - Carregando dados e executando no banco

        //Carrega os valores encontrados na Combobox Autor
        private tb_autor LoadAuthor()
        {
            tb_autor autor = new tb_autor();
            if (cboAutor.InvokeRequired)
                cboAutor.Invoke((MethodInvoker)delegate
                {
                    autor = cboAutor.SelectedItem as tb_autor;
                });
            return autor;
        }

        //Pegar os valores encontrados na Combobox Editora
        private tb_editora LoadPublish()
        {
            tb_editora editora = new tb_editora();
            if (cboEditora.InvokeRequired)
                cboEditora.Invoke((MethodInvoker)delegate
               {
                   editora = cboEditora.SelectedItem as tb_editora;
               });
            return editora;
        }

        //Pegar os valores encontrados na Combobox Categoria
        private tb_categoria LoadCategory()
        {
            tb_categoria categoria = new tb_categoria();
            if (cboCategoria.InvokeRequired)
                cboCategoria.Invoke((MethodInvoker)delegate
                {
                    categoria = cboCategoria.SelectedItem as tb_categoria;
                });
            return categoria;
        }

        //Verifica se o controle foi ou não checado e dá um determinado valor
        private bool LoadStatus(Bunifu.UI.WinForms.BunifuCheckBox chk)
        {
            //Varíavel de escopo para o método
            bool valorLogico;

            if (chk.Checked == true)
                valorLogico = true;
            else
                valorLogico = false;

            return valorLogico;
        }
        private tb_livro LoadControlsForSaveUpdate(tb_livro controls)
        {
            //Pegando os valores das Chaves estrangeiras na combobox
            var autor = LoadAuthor();
            var editora = LoadPublish();
            var categoria = LoadCategory();

            //Chaves estrangeiras
            controls.id_autor = autor.id_autor;
            controls.id_editora = editora.id_editora;
            controls.id_categoria = categoria.id_categoria;

            //Compos com o retorno string
            controls.nm_livro = txtTitulo.Text;
            controls.dt_ano = Convert.ToDateTime(txtAno.Text);
            controls.nr_paginas = Convert.ToInt32(txtNumero.Text);

            //Campos Booleanos
            controls.bl_status = LoadStatus(chkStatus);

            //Campos com Imagem (Só tem um campo, mas vamos manter campos para dar aquela normailizada)
            controls.img_livro = ImagemPlugin.ConverterParaString(imgAutor.Image);

            return controls;
        }
        private tb_livro SaveControls()
        {
            //Instancio a variável de escopo para conseguir inserir novos valores
            livro = new tb_livro();

            //Recebo o valor retornado do método já com os controles com os seus valores
            var retorno = LoadControlsForSaveUpdate(livro);

            //E retorno para o método principal
            return retorno;
        }

        private tb_livro UpdateControls()
        {
            using (LibraryEntities context = new LibraryEntities())
            {
                livro = context.tb_livro.Where(c => c.id_autor == livro.id_autor).FirstOrDefault();

                var retorno = LoadControlsForSaveUpdate(livro);
                return retorno;
            }
        }
        //Novas várias de esopo que serão utilizadas nos cenários de notificação
        Image imagem;
        string text;

        //Ou Salva ou Atualiza os dados no banco de dados
        private async void btnSaveUpdate_Click(object sender, EventArgs e)
        {
            await Task.Run(async () =>
            {
                try
                {
                    if (lblTitulo.Text == "Registre novos Livros")
                    {
                        //Retorno os valores carregados no método CarregarControlesSaveUpdate
                        var save = this.SaveControls();

                        //Tela chama business e passa o valor da váriavel save
                        LivroBusiness db = new LivroBusiness();
                        db.Save(save);

                        //Notificação
                        await Task.Factory.StartNew(() =>
                       {
                           imagem = Properties.Resources.ThumpUP;
                           text = "Novo Livro registrado na base de dados com êxito!";
                           this.SelectorClass(1, text, imagem);
                       });
                    }
                    else
                    {
                        //Retorno os valores carregados no método CarregarControlesSaveUpdate
                        var update = this.UpdateControls();

                        //Tela chama business e passa o valor da váriavel update
                        LivroBusiness db = new LivroBusiness();
                        db.Update(update);

                        //Notificação
                        await Task.Factory.StartNew(() =>
                        {
                            imagem = Properties.Resources.Warning;
                            text = "As informações do Livro foram alteras na base de dados com êxito!!";
                            this.SelectorClass(2, text, imagem);
                        });
                    }

                }
                catch (ArgumentException business)
                {
                    MessageBox.Show(business.Message,
                                    "Friends Library",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro: " + ex.Message + "\n" + "Tente mais tarde.",
                                    "Friends Library",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                }
            });
        }
        #endregion

        #region VALIDAÇÃO RUNTIME - Validador de BunifuTextBox
        private Image GetImage(bool valid)
        {
            if (valid)
                return imageListIconValidator.Images[0];
            else
                return imageListIconValidator.Images[1];
        }

        private void txtTitulo_TextChange(object sender, EventArgs e)
        {
            validar = false;
            if (validar == false)
                txtTitulo.IconRight = GetImage(ValidarIcones.IsBunifuTextBoxEmpty(txtTitulo.Text));
        }

        private void txtNumero_TextChange(object sender, EventArgs e)
        {
            validar = false;
            if (validar == false)
                txtNumero.IconRight = GetImage(ValidarIcones.IsBunifuNumber(txtNumero.Text));
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            validar = false;
            if (validar == false)
                ValidarTextbox.SoNumero(e);
        }
        #endregion
    }
}
