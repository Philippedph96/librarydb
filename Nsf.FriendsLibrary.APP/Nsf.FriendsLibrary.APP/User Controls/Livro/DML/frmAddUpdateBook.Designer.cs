﻿namespace Nsf.FriendsLibrary.APP.User_Controls.Livro
{
    partial class frmAddUpdateBook
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAddUpdateBook));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties19 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties20 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties21 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties22 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties23 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties24 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.pnSeparator1 = new System.Windows.Forms.Panel();
            this.lblTituloLivro = new System.Windows.Forms.Label();
            this.lblCategoria = new System.Windows.Forms.Label();
            this.lblAutor = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cboAutor = new System.Windows.Forms.ComboBox();
            this.lblNumeroPaginas = new System.Windows.Forms.Label();
            this.cboCategoria = new System.Windows.Forms.ComboBox();
            this.lblEditora = new System.Windows.Forms.Label();
            this.cboEditora = new System.Windows.Forms.ComboBox();
            this.lblAno = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblSimNao = new System.Windows.Forms.Label();
            this.pnSeparatos2 = new System.Windows.Forms.Panel();
            this.pnCombo = new System.Windows.Forms.Panel();
            this.btnEditora = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.pnCima2 = new System.Windows.Forms.Panel();
            this.btnCategoria = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.pnCima = new System.Windows.Forms.Panel();
            this.btnAutor = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.TimerAddCombo = new System.Windows.Forms.Timer(this.components);
            this.TimerHideCombo = new System.Windows.Forms.Timer(this.components);
            this.btnPlus = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnCancelar = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.btnSaveUpdate = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.chkStatus = new Bunifu.UI.WinForms.BunifuCheckBox();
            this.btnProcurar = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.imgLivro = new Bunifu.UI.WinForms.BunifuPictureBox();
            this.lblID = new System.Windows.Forms.Label();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.imgAutor = new Bunifu.UI.WinForms.BunifuPictureBox();
            this.imageListIconValidator = new System.Windows.Forms.ImageList(this.components);
            this.txtTitulo = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtNumero = new Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox();
            this.txtAno = new Bunifu.UI.WinForms.BunifuDatePicker();
            this.pnCombo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnPlus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLivro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAutor)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Segoe UI Semibold", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.Location = new System.Drawing.Point(39, 22);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(71, 30);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "{Title}";
            // 
            // pnSeparator1
            // 
            this.pnSeparator1.BackColor = System.Drawing.Color.Gainsboro;
            this.pnSeparator1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnSeparator1.Location = new System.Drawing.Point(18, 67);
            this.pnSeparator1.Name = "pnSeparator1";
            this.pnSeparator1.Size = new System.Drawing.Size(590, 1);
            this.pnSeparator1.TabIndex = 1;
            // 
            // lblTituloLivro
            // 
            this.lblTituloLivro.AutoSize = true;
            this.lblTituloLivro.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold);
            this.lblTituloLivro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTituloLivro.Location = new System.Drawing.Point(223, 142);
            this.lblTituloLivro.Name = "lblTituloLivro";
            this.lblTituloLivro.Size = new System.Drawing.Size(108, 20);
            this.lblTituloLivro.TabIndex = 5;
            this.lblTituloLivro.Text = "Título do Livro";
            // 
            // lblCategoria
            // 
            this.lblCategoria.AutoSize = true;
            this.lblCategoria.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold);
            this.lblCategoria.Location = new System.Drawing.Point(326, 296);
            this.lblCategoria.Name = "lblCategoria";
            this.lblCategoria.Size = new System.Drawing.Size(75, 20);
            this.lblCategoria.TabIndex = 12;
            this.lblCategoria.Text = "Categoria";
            // 
            // lblAutor
            // 
            this.lblAutor.AutoSize = true;
            this.lblAutor.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold);
            this.lblAutor.Location = new System.Drawing.Point(223, 198);
            this.lblAutor.Name = "lblAutor";
            this.lblAutor.Size = new System.Drawing.Size(48, 20);
            this.lblAutor.TabIndex = 7;
            this.lblAutor.Text = "Autor";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Gainsboro;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Location = new System.Drawing.Point(18, 278);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(590, 1);
            this.panel2.TabIndex = 9;
            // 
            // cboAutor
            // 
            this.cboAutor.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboAutor.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboAutor.BackColor = System.Drawing.Color.Gainsboro;
            this.cboAutor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboAutor.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboAutor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cboAutor.FormattingEnabled = true;
            this.cboAutor.Location = new System.Drawing.Point(227, 219);
            this.cboAutor.Name = "cboAutor";
            this.cboAutor.Size = new System.Drawing.Size(373, 29);
            this.cboAutor.TabIndex = 8;
            this.cboAutor.Text = "Selecione";
            this.cboAutor.Click += new System.EventHandler(this.cboAutor_Click);
            // 
            // lblNumeroPaginas
            // 
            this.lblNumeroPaginas.AutoSize = true;
            this.lblNumeroPaginas.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold);
            this.lblNumeroPaginas.Location = new System.Drawing.Point(326, 362);
            this.lblNumeroPaginas.Name = "lblNumeroPaginas";
            this.lblNumeroPaginas.Size = new System.Drawing.Size(123, 20);
            this.lblNumeroPaginas.TabIndex = 16;
            this.lblNumeroPaginas.Text = "Número Páginas";
            // 
            // cboCategoria
            // 
            this.cboCategoria.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboCategoria.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboCategoria.BackColor = System.Drawing.Color.Gainsboro;
            this.cboCategoria.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboCategoria.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboCategoria.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cboCategoria.FormattingEnabled = true;
            this.cboCategoria.Location = new System.Drawing.Point(330, 318);
            this.cboCategoria.Name = "cboCategoria";
            this.cboCategoria.Size = new System.Drawing.Size(270, 29);
            this.cboCategoria.TabIndex = 13;
            this.cboCategoria.Text = "Selecione";
            this.cboCategoria.Click += new System.EventHandler(this.cboCategoria_Click);
            // 
            // lblEditora
            // 
            this.lblEditora.AutoSize = true;
            this.lblEditora.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold);
            this.lblEditora.Location = new System.Drawing.Point(14, 296);
            this.lblEditora.Name = "lblEditora";
            this.lblEditora.Size = new System.Drawing.Size(58, 20);
            this.lblEditora.TabIndex = 10;
            this.lblEditora.Text = "Editora";
            // 
            // cboEditora
            // 
            this.cboEditora.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cboEditora.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboEditora.BackColor = System.Drawing.Color.Gainsboro;
            this.cboEditora.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cboEditora.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboEditora.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cboEditora.FormattingEnabled = true;
            this.cboEditora.Location = new System.Drawing.Point(18, 320);
            this.cboEditora.Name = "cboEditora";
            this.cboEditora.Size = new System.Drawing.Size(270, 29);
            this.cboEditora.TabIndex = 11;
            this.cboEditora.Text = "Selecione";
            this.cboEditora.Click += new System.EventHandler(this.cboEditora_Click);
            // 
            // lblAno
            // 
            this.lblAno.AutoSize = true;
            this.lblAno.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold);
            this.lblAno.Location = new System.Drawing.Point(14, 362);
            this.lblAno.Name = "lblAno";
            this.lblAno.Size = new System.Drawing.Size(119, 20);
            this.lblAno.TabIndex = 14;
            this.lblAno.Text = "Data Publicação";
            // 
            // lblStatus
            // 
            this.lblStatus.AccessibleRole = System.Windows.Forms.AccessibleRole.CheckButton;
            this.lblStatus.AutoSize = true;
            this.lblStatus.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblStatus.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold);
            this.lblStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblStatus.Location = new System.Drawing.Point(18, 426);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(50, 20);
            this.lblStatus.TabIndex = 18;
            this.lblStatus.Text = "Status";
            // 
            // lblSimNao
            // 
            this.lblSimNao.AccessibleRole = System.Windows.Forms.AccessibleRole.CheckButton;
            this.lblSimNao.AutoSize = true;
            this.lblSimNao.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblSimNao.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold);
            this.lblSimNao.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblSimNao.Location = new System.Drawing.Point(51, 454);
            this.lblSimNao.Name = "lblSimNao";
            this.lblSimNao.Size = new System.Drawing.Size(176, 20);
            this.lblSimNao.TabIndex = 20;
            this.lblSimNao.Text = "Disponível | Indisponível";
            // 
            // pnSeparatos2
            // 
            this.pnSeparatos2.BackColor = System.Drawing.Color.Gainsboro;
            this.pnSeparatos2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnSeparatos2.Location = new System.Drawing.Point(18, 502);
            this.pnSeparatos2.Name = "pnSeparatos2";
            this.pnSeparatos2.Size = new System.Drawing.Size(590, 1);
            this.pnSeparatos2.TabIndex = 21;
            // 
            // pnCombo
            // 
            this.pnCombo.Controls.Add(this.btnEditora);
            this.pnCombo.Controls.Add(this.pnCima2);
            this.pnCombo.Controls.Add(this.btnCategoria);
            this.pnCombo.Controls.Add(this.pnCima);
            this.pnCombo.Controls.Add(this.btnAutor);
            this.pnCombo.Location = new System.Drawing.Point(63, 515);
            this.pnCombo.Name = "pnCombo";
            this.pnCombo.Size = new System.Drawing.Size(170, 0);
            this.pnCombo.TabIndex = 208;
            // 
            // btnEditora
            // 
            this.btnEditora.BackColor = System.Drawing.Color.Transparent;
            this.btnEditora.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnEditora.BackgroundImage")));
            this.btnEditora.ButtonText = "Adicionar Editora";
            this.btnEditora.ButtonTextMarginLeft = -17;
            this.btnEditora.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnEditora.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnEditora.DisabledForecolor = System.Drawing.Color.White;
            this.btnEditora.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEditora.ForeColor = System.Drawing.Color.White;
            this.btnEditora.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnEditora.IconPadding = 10;
            this.btnEditora.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnEditora.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(26)))), ((int)(((byte)(26)))));
            this.btnEditora.IdleBorderRadius = 1;
            this.btnEditora.IdleBorderThickness = 0;
            this.btnEditora.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(26)))), ((int)(((byte)(26)))));
            this.btnEditora.IdleIconLeftImage = null;
            this.btnEditora.IdleIconRightImage = null;
            this.btnEditora.Location = new System.Drawing.Point(0, 72);
            this.btnEditora.Name = "btnEditora";
            stateProperties19.BorderColor = System.Drawing.Color.Maroon;
            stateProperties19.BorderRadius = 1;
            stateProperties19.BorderThickness = 1;
            stateProperties19.FillColor = System.Drawing.Color.Maroon;
            stateProperties19.IconLeftImage = null;
            stateProperties19.IconRightImage = null;
            this.btnEditora.onHoverState = stateProperties19;
            this.btnEditora.Size = new System.Drawing.Size(170, 34);
            this.btnEditora.TabIndex = 7;
            this.btnEditora.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnEditora.Click += new System.EventHandler(this.btnEditora_Click);
            // 
            // pnCima2
            // 
            this.pnCima2.BackColor = System.Drawing.Color.White;
            this.pnCima2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnCima2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnCima2.Location = new System.Drawing.Point(0, 70);
            this.pnCima2.Name = "pnCima2";
            this.pnCima2.Size = new System.Drawing.Size(170, 2);
            this.pnCima2.TabIndex = 6;
            // 
            // btnCategoria
            // 
            this.btnCategoria.BackColor = System.Drawing.Color.Transparent;
            this.btnCategoria.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCategoria.BackgroundImage")));
            this.btnCategoria.ButtonText = "Adicionar Categoria";
            this.btnCategoria.ButtonTextMarginLeft = 0;
            this.btnCategoria.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnCategoria.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnCategoria.DisabledForecolor = System.Drawing.Color.White;
            this.btnCategoria.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCategoria.ForeColor = System.Drawing.Color.White;
            this.btnCategoria.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnCategoria.IconPadding = 10;
            this.btnCategoria.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnCategoria.IdleBorderColor = System.Drawing.Color.Crimson;
            this.btnCategoria.IdleBorderRadius = 1;
            this.btnCategoria.IdleBorderThickness = 0;
            this.btnCategoria.IdleFillColor = System.Drawing.Color.Crimson;
            this.btnCategoria.IdleIconLeftImage = null;
            this.btnCategoria.IdleIconRightImage = null;
            this.btnCategoria.Location = new System.Drawing.Point(0, 36);
            this.btnCategoria.Name = "btnCategoria";
            stateProperties20.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(20)))), ((int)(((byte)(58)))));
            stateProperties20.BorderRadius = 1;
            stateProperties20.BorderThickness = 1;
            stateProperties20.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(20)))), ((int)(((byte)(58)))));
            stateProperties20.IconLeftImage = null;
            stateProperties20.IconRightImage = null;
            this.btnCategoria.onHoverState = stateProperties20;
            this.btnCategoria.Size = new System.Drawing.Size(170, 34);
            this.btnCategoria.TabIndex = 5;
            this.btnCategoria.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnCategoria.Click += new System.EventHandler(this.btnCategoria_Click);
            // 
            // pnCima
            // 
            this.pnCima.BackColor = System.Drawing.Color.White;
            this.pnCima.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnCima.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnCima.Location = new System.Drawing.Point(0, 34);
            this.pnCima.Name = "pnCima";
            this.pnCima.Size = new System.Drawing.Size(170, 2);
            this.pnCima.TabIndex = 4;
            // 
            // btnAutor
            // 
            this.btnAutor.BackColor = System.Drawing.Color.Transparent;
            this.btnAutor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAutor.BackgroundImage")));
            this.btnAutor.ButtonText = "Adicionar Autor";
            this.btnAutor.ButtonTextMarginLeft = -27;
            this.btnAutor.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnAutor.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnAutor.DisabledForecolor = System.Drawing.Color.White;
            this.btnAutor.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAutor.ForeColor = System.Drawing.Color.White;
            this.btnAutor.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnAutor.IconPadding = 10;
            this.btnAutor.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnAutor.IdleBorderColor = System.Drawing.Color.DodgerBlue;
            this.btnAutor.IdleBorderRadius = 1;
            this.btnAutor.IdleBorderThickness = 0;
            this.btnAutor.IdleFillColor = System.Drawing.Color.DodgerBlue;
            this.btnAutor.IdleIconLeftImage = null;
            this.btnAutor.IdleIconRightImage = null;
            this.btnAutor.Location = new System.Drawing.Point(0, 0);
            this.btnAutor.Name = "btnAutor";
            stateProperties21.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            stateProperties21.BorderRadius = 1;
            stateProperties21.BorderThickness = 1;
            stateProperties21.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(102)))), ((int)(((byte)(204)))));
            stateProperties21.IconLeftImage = null;
            stateProperties21.IconRightImage = null;
            this.btnAutor.onHoverState = stateProperties21;
            this.btnAutor.Size = new System.Drawing.Size(170, 34);
            this.btnAutor.TabIndex = 3;
            this.btnAutor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAutor.Click += new System.EventHandler(this.btnAutor_Click);
            // 
            // TimerAddCombo
            // 
            this.TimerAddCombo.Tick += new System.EventHandler(this.TimerAddCombo_Tick);
            // 
            // TimerHideCombo
            // 
            this.TimerHideCombo.Tick += new System.EventHandler(this.TimerHideCombo_Tick);
            // 
            // btnPlus
            // 
            this.btnPlus.BackColor = System.Drawing.Color.White;
            this.btnPlus.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnPlus.Image = ((System.Drawing.Image)(resources.GetObject("btnPlus.Image")));
            this.btnPlus.ImageActive = null;
            this.btnPlus.Location = new System.Drawing.Point(12, 530);
            this.btnPlus.Name = "btnPlus";
            this.btnPlus.Size = new System.Drawing.Size(45, 45);
            this.btnPlus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnPlus.TabIndex = 205;
            this.btnPlus.TabStop = false;
            this.btnPlus.Zoom = 10;
            this.btnPlus.Click += new System.EventHandler(this.btnPlus_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCancelar.BackColor = System.Drawing.Color.Transparent;
            this.btnCancelar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnCancelar.BackgroundImage")));
            this.btnCancelar.ButtonText = "Limpar";
            this.btnCancelar.ButtonTextMarginLeft = 0;
            this.btnCancelar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCancelar.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnCancelar.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnCancelar.DisabledForecolor = System.Drawing.Color.White;
            this.btnCancelar.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCancelar.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnCancelar.IconPadding = 10;
            this.btnCancelar.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnCancelar.IdleBorderColor = System.Drawing.Color.Gainsboro;
            this.btnCancelar.IdleBorderRadius = 40;
            this.btnCancelar.IdleBorderThickness = 0;
            this.btnCancelar.IdleFillColor = System.Drawing.Color.Gainsboro;
            this.btnCancelar.IdleIconLeftImage = null;
            this.btnCancelar.IdleIconRightImage = null;
            this.btnCancelar.Location = new System.Drawing.Point(319, 542);
            this.btnCancelar.Name = "btnCancelar";
            stateProperties22.BorderColor = System.Drawing.SystemColors.Control;
            stateProperties22.BorderRadius = 40;
            stateProperties22.BorderThickness = 0;
            stateProperties22.FillColor = System.Drawing.SystemColors.Control;
            stateProperties22.IconLeftImage = null;
            stateProperties22.IconRightImage = null;
            this.btnCancelar.onHoverState = stateProperties22;
            this.btnCancelar.Size = new System.Drawing.Size(136, 45);
            this.btnCancelar.TabIndex = 22;
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnSaveUpdate
            // 
            this.btnSaveUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveUpdate.BackColor = System.Drawing.Color.Transparent;
            this.btnSaveUpdate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnSaveUpdate.BackgroundImage")));
            this.btnSaveUpdate.ButtonText = "Salvar";
            this.btnSaveUpdate.ButtonTextMarginLeft = 0;
            this.btnSaveUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSaveUpdate.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnSaveUpdate.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnSaveUpdate.DisabledForecolor = System.Drawing.Color.White;
            this.btnSaveUpdate.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveUpdate.ForeColor = System.Drawing.Color.White;
            this.btnSaveUpdate.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnSaveUpdate.IconPadding = 10;
            this.btnSaveUpdate.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnSaveUpdate.IdleBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnSaveUpdate.IdleBorderRadius = 40;
            this.btnSaveUpdate.IdleBorderThickness = 0;
            this.btnSaveUpdate.IdleFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(82)))), ((int)(((byte)(204)))));
            this.btnSaveUpdate.IdleIconLeftImage = null;
            this.btnSaveUpdate.IdleIconRightImage = null;
            this.btnSaveUpdate.Location = new System.Drawing.Point(464, 542);
            this.btnSaveUpdate.Name = "btnSaveUpdate";
            stateProperties23.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(133)))), ((int)(((byte)(255)))));
            stateProperties23.BorderRadius = 40;
            stateProperties23.BorderThickness = 0;
            stateProperties23.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(133)))), ((int)(((byte)(255)))));
            stateProperties23.IconLeftImage = null;
            stateProperties23.IconRightImage = null;
            this.btnSaveUpdate.onHoverState = stateProperties23;
            this.btnSaveUpdate.Size = new System.Drawing.Size(136, 45);
            this.btnSaveUpdate.TabIndex = 207;
            this.btnSaveUpdate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSaveUpdate.Click += new System.EventHandler(this.btnSaveUpdate_Click);
            // 
            // chkStatus
            // 
            this.chkStatus.AllowBindingControlAnimation = true;
            this.chkStatus.AllowBindingControlColorChanges = false;
            this.chkStatus.AllowBindingControlLocation = true;
            this.chkStatus.AllowCheckBoxAnimation = true;
            this.chkStatus.AllowCheckmarkAnimation = true;
            this.chkStatus.AllowOnHoverStates = true;
            this.chkStatus.BackColor = System.Drawing.Color.Transparent;
            this.chkStatus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("chkStatus.BackgroundImage")));
            this.chkStatus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.chkStatus.BindingControl = this.lblSimNao;
            this.chkStatus.BindingControlPosition = Bunifu.UI.WinForms.BunifuCheckBox.BindingControlPositions.Right;
            this.chkStatus.Checked = false;
            this.chkStatus.CheckState = Bunifu.UI.WinForms.BunifuCheckBox.CheckStates.Unchecked;
            this.chkStatus.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkStatus.CustomCheckmarkImage = null;
            this.chkStatus.Location = new System.Drawing.Point(22, 449);
            this.chkStatus.MinimumSize = new System.Drawing.Size(17, 17);
            this.chkStatus.Name = "chkStatus";
            this.chkStatus.OnCheck.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(113)))), ((int)(((byte)(242)))));
            this.chkStatus.OnCheck.BorderRadius = 2;
            this.chkStatus.OnCheck.BorderThickness = 2;
            this.chkStatus.OnCheck.CheckBoxColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(113)))), ((int)(((byte)(242)))));
            this.chkStatus.OnCheck.CheckmarkColor = System.Drawing.Color.White;
            this.chkStatus.OnCheck.CheckmarkThickness = 2;
            this.chkStatus.OnDisable.BorderColor = System.Drawing.Color.LightGray;
            this.chkStatus.OnDisable.BorderRadius = 2;
            this.chkStatus.OnDisable.BorderThickness = 2;
            this.chkStatus.OnDisable.CheckBoxColor = System.Drawing.Color.Transparent;
            this.chkStatus.OnDisable.CheckmarkColor = System.Drawing.Color.LightGray;
            this.chkStatus.OnDisable.CheckmarkThickness = 2;
            this.chkStatus.OnHoverChecked.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(151)))), ((int)(((byte)(246)))));
            this.chkStatus.OnHoverChecked.BorderRadius = 2;
            this.chkStatus.OnHoverChecked.BorderThickness = 2;
            this.chkStatus.OnHoverChecked.CheckBoxColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(151)))), ((int)(((byte)(246)))));
            this.chkStatus.OnHoverChecked.CheckmarkColor = System.Drawing.Color.White;
            this.chkStatus.OnHoverChecked.CheckmarkThickness = 2;
            this.chkStatus.OnHoverUnchecked.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(73)))), ((int)(((byte)(138)))), ((int)(((byte)(245)))));
            this.chkStatus.OnHoverUnchecked.BorderRadius = 2;
            this.chkStatus.OnHoverUnchecked.BorderThickness = 2;
            this.chkStatus.OnHoverUnchecked.CheckBoxColor = System.Drawing.Color.Transparent;
            this.chkStatus.OnUncheck.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkStatus.OnUncheck.BorderRadius = 2;
            this.chkStatus.OnUncheck.BorderThickness = 2;
            this.chkStatus.OnUncheck.CheckBoxColor = System.Drawing.Color.Gainsboro;
            this.chkStatus.Size = new System.Drawing.Size(26, 26);
            this.chkStatus.Style = Bunifu.UI.WinForms.BunifuCheckBox.CheckBoxStyles.Bunifu;
            this.chkStatus.TabIndex = 19;
            this.chkStatus.ThreeState = false;
            this.chkStatus.ToolTipText = null;
            // 
            // btnProcurar
            // 
            this.btnProcurar.BackColor = System.Drawing.Color.Transparent;
            this.btnProcurar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnProcurar.BackgroundImage")));
            this.btnProcurar.ButtonText = "   Escolha foto";
            this.btnProcurar.ButtonTextMarginLeft = -13;
            this.btnProcurar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProcurar.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnProcurar.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnProcurar.DisabledForecolor = System.Drawing.Color.White;
            this.btnProcurar.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Bold);
            this.btnProcurar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnProcurar.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnProcurar.IconPadding = 5;
            this.btnProcurar.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnProcurar.IdleBorderColor = System.Drawing.Color.Gainsboro;
            this.btnProcurar.IdleBorderRadius = 40;
            this.btnProcurar.IdleBorderThickness = 1;
            this.btnProcurar.IdleFillColor = System.Drawing.Color.Gainsboro;
            this.btnProcurar.IdleIconLeftImage = null;
            this.btnProcurar.IdleIconRightImage = null;
            this.btnProcurar.Location = new System.Drawing.Point(14, 209);
            this.btnProcurar.Name = "btnProcurar";
            stateProperties24.BorderColor = System.Drawing.SystemColors.Control;
            stateProperties24.BorderRadius = 40;
            stateProperties24.BorderThickness = 0;
            stateProperties24.FillColor = System.Drawing.SystemColors.Control;
            stateProperties24.IconLeftImage = null;
            stateProperties24.IconRightImage = null;
            this.btnProcurar.onHoverState = stateProperties24;
            this.btnProcurar.Size = new System.Drawing.Size(172, 39);
            this.btnProcurar.TabIndex = 2;
            this.btnProcurar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnProcurar.Click += new System.EventHandler(this.btnProcurar_Click);
            // 
            // imgLivro
            // 
            this.imgLivro.AllowFocused = false;
            this.imgLivro.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.imgLivro.BorderRadius = 50;
            this.imgLivro.Image = global::Nsf.FriendsLibrary.APP.Properties.Resources.FlashPadrao;
            this.imgLivro.IsCircle = true;
            this.imgLivro.Location = new System.Drawing.Point(10, 10);
            this.imgLivro.Name = "imgLivro";
            this.imgLivro.Size = new System.Drawing.Size(500, 200);
            this.imgLivro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgLivro.TabIndex = 203;
            this.imgLivro.TabStop = false;
            this.imgLivro.Type = Bunifu.UI.WinForms.BunifuPictureBox.Types.Square;
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.lblID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblID.Location = new System.Drawing.Point(325, 106);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(13, 17);
            this.lblID.TabIndex = 211;
            this.lblID.Text = "-";
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold);
            this.lblCodigo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCodigo.Location = new System.Drawing.Point(223, 104);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(100, 20);
            this.lblCodigo.TabIndex = 210;
            this.lblCodigo.Text = "Código Livro:";
            // 
            // imgAutor
            // 
            this.imgAutor.AllowFocused = false;
            this.imgAutor.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.imgAutor.BackColor = System.Drawing.Color.Gainsboro;
            this.imgAutor.BorderRadius = 55;
            this.imgAutor.Image = global::Nsf.FriendsLibrary.APP.Properties.Resources.FlashPadrao;
            this.imgAutor.IsCircle = true;
            this.imgAutor.Location = new System.Drawing.Point(45, 96);
            this.imgAutor.Name = "imgAutor";
            this.imgAutor.Size = new System.Drawing.Size(110, 110);
            this.imgAutor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgAutor.TabIndex = 209;
            this.imgAutor.TabStop = false;
            this.imgAutor.Type = Bunifu.UI.WinForms.BunifuPictureBox.Types.Square;
            // 
            // imageListIconValidator
            // 
            this.imageListIconValidator.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListIconValidator.ImageStream")));
            this.imageListIconValidator.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListIconValidator.Images.SetKeyName(0, "Checkmark.png");
            this.imageListIconValidator.Images.SetKeyName(1, "Error.png");
            // 
            // txtTitulo
            // 
            this.txtTitulo.AcceptsReturn = false;
            this.txtTitulo.AcceptsTab = false;
            this.txtTitulo.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtTitulo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtTitulo.BackColor = System.Drawing.Color.Gainsboro;
            this.txtTitulo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtTitulo.BackgroundImage")));
            this.txtTitulo.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtTitulo.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtTitulo.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(38)))), ((int)(((byte)(157)))));
            this.txtTitulo.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtTitulo.BorderRadius = 1;
            this.txtTitulo.BorderThickness = 1;
            this.txtTitulo.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtTitulo.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitulo.DefaultText = "";
            this.txtTitulo.FillColor = System.Drawing.Color.Gainsboro;
            this.txtTitulo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtTitulo.HideSelection = true;
            this.txtTitulo.IconLeft = null;
            this.txtTitulo.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtTitulo.IconPadding = 4;
            this.txtTitulo.IconRight = null;
            this.txtTitulo.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.txtTitulo.Location = new System.Drawing.Point(227, 163);
            this.txtTitulo.MaxLength = 100;
            this.txtTitulo.MinimumSize = new System.Drawing.Size(100, 29);
            this.txtTitulo.Modified = false;
            this.txtTitulo.Name = "txtTitulo";
            this.txtTitulo.PasswordChar = '\0';
            this.txtTitulo.ReadOnly = false;
            this.txtTitulo.SelectedText = "";
            this.txtTitulo.SelectionLength = 0;
            this.txtTitulo.SelectionStart = 0;
            this.txtTitulo.ShortcutsEnabled = true;
            this.txtTitulo.Size = new System.Drawing.Size(373, 30);
            this.txtTitulo.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtTitulo.TabIndex = 212;
            this.txtTitulo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtTitulo.TextMarginLeft = 10;
            this.txtTitulo.TextPlaceholder = "";
            this.txtTitulo.UseSystemPasswordChar = false;
            this.txtTitulo.TextChange += new System.EventHandler(this.txtTitulo_TextChange);
            // 
            // txtNumero
            // 
            this.txtNumero.AcceptsReturn = false;
            this.txtNumero.AcceptsTab = false;
            this.txtNumero.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.txtNumero.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.txtNumero.BackColor = System.Drawing.Color.Gainsboro;
            this.txtNumero.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("txtNumero.BackgroundImage")));
            this.txtNumero.BorderColorActive = System.Drawing.Color.DodgerBlue;
            this.txtNumero.BorderColorDisabled = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.txtNumero.BorderColorHover = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(38)))), ((int)(((byte)(157)))));
            this.txtNumero.BorderColorIdle = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(107)))), ((int)(((byte)(107)))));
            this.txtNumero.BorderRadius = 1;
            this.txtNumero.BorderThickness = 1;
            this.txtNumero.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal;
            this.txtNumero.DefaultFont = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumero.DefaultText = "";
            this.txtNumero.FillColor = System.Drawing.Color.Gainsboro;
            this.txtNumero.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtNumero.HideSelection = true;
            this.txtNumero.IconLeft = null;
            this.txtNumero.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.txtNumero.IconPadding = 4;
            this.txtNumero.IconRight = null;
            this.txtNumero.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.txtNumero.Location = new System.Drawing.Point(330, 384);
            this.txtNumero.MaxLength = 100;
            this.txtNumero.MinimumSize = new System.Drawing.Size(100, 29);
            this.txtNumero.Modified = false;
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.PasswordChar = '\0';
            this.txtNumero.ReadOnly = false;
            this.txtNumero.SelectedText = "";
            this.txtNumero.SelectionLength = 0;
            this.txtNumero.SelectionStart = 0;
            this.txtNumero.ShortcutsEnabled = true;
            this.txtNumero.Size = new System.Drawing.Size(270, 30);
            this.txtNumero.Style = Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox._Style.Material;
            this.txtNumero.TabIndex = 232;
            this.txtNumero.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtNumero.TextMarginLeft = 10;
            this.txtNumero.TextPlaceholder = "";
            this.txtNumero.UseSystemPasswordChar = false;
            this.txtNumero.TextChange += new System.EventHandler(this.txtNumero_TextChange);
            this.txtNumero.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumero_KeyPress);
            // 
            // txtAno
            // 
            this.txtAno.BorderRadius = 1;
            this.txtAno.Color = System.Drawing.Color.Gainsboro;
            this.txtAno.CustomFormat = "dd/MM/yyyy";
            this.txtAno.DateBorderThickness = Bunifu.UI.WinForms.BunifuDatePicker.BorderThickness.Thick;
            this.txtAno.DateTextAlign = Bunifu.UI.WinForms.BunifuDatePicker.TextAlign.Left;
            this.txtAno.DisabledColor = System.Drawing.Color.Gray;
            this.txtAno.DisplayWeekNumbers = false;
            this.txtAno.DPHeight = 0;
            this.txtAno.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.txtAno.FillDatePicker = true;
            this.txtAno.Font = new System.Drawing.Font("Segoe UI", 11.25F);
            this.txtAno.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtAno.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtAno.Icon = ((System.Drawing.Image)(resources.GetObject("txtAno.Icon")));
            this.txtAno.IconColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtAno.IconLocation = Bunifu.UI.WinForms.BunifuDatePicker.Indicator.Right;
            this.txtAno.Location = new System.Drawing.Point(18, 384);
            this.txtAno.MinDate = new System.DateTime(1760, 1, 1, 0, 0, 0, 0);
            this.txtAno.MinimumSize = new System.Drawing.Size(220, 30);
            this.txtAno.Name = "txtAno";
            this.txtAno.Size = new System.Drawing.Size(270, 30);
            this.txtAno.TabIndex = 233;
            // 
            // frmAddUpdateBook
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.txtAno);
            this.Controls.Add(this.txtNumero);
            this.Controls.Add(this.txtTitulo);
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.lblCodigo);
            this.Controls.Add(this.imgAutor);
            this.Controls.Add(this.btnPlus);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnSaveUpdate);
            this.Controls.Add(this.chkStatus);
            this.Controls.Add(this.cboEditora);
            this.Controls.Add(this.cboCategoria);
            this.Controls.Add(this.cboAutor);
            this.Controls.Add(this.pnSeparatos2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pnSeparator1);
            this.Controls.Add(this.btnProcurar);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.lblTituloLivro);
            this.Controls.Add(this.lblAno);
            this.Controls.Add(this.lblNumeroPaginas);
            this.Controls.Add(this.lblEditora);
            this.Controls.Add(this.lblCategoria);
            this.Controls.Add(this.lblAutor);
            this.Controls.Add(this.lblSimNao);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.pnCombo);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 10.75F, System.Drawing.FontStyle.Bold);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Name = "frmAddUpdateBook";
            this.Size = new System.Drawing.Size(625, 632);
            this.pnCombo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnPlus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLivro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgAutor)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnProcurar;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Panel pnSeparator1;
        private System.Windows.Forms.Label lblTituloLivro;
        private System.Windows.Forms.Label lblCategoria;
        private System.Windows.Forms.Label lblAutor;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox cboAutor;
        private System.Windows.Forms.Label lblNumeroPaginas;
        private System.Windows.Forms.ComboBox cboCategoria;
        private System.Windows.Forms.Label lblEditora;
        private System.Windows.Forms.ComboBox cboEditora;
        private System.Windows.Forms.Label lblAno;
        private Bunifu.UI.WinForms.BunifuPictureBox imgLivro;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblSimNao;
        private Bunifu.UI.WinForms.BunifuCheckBox chkStatus;
        private System.Windows.Forms.Panel pnSeparatos2;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnSaveUpdate;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnCancelar;
        private Bunifu.UI.WinForms.BunifuToolTip ToolTipControls;
        private System.Windows.Forms.Panel pnCombo;
        private System.Windows.Forms.Timer TimerAddCombo;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnEditora;
        private System.Windows.Forms.Panel pnCima2;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnCategoria;
        private System.Windows.Forms.Panel pnCima;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnAutor;
        private Bunifu.Framework.UI.BunifuImageButton btnPlus;
        private System.Windows.Forms.Timer TimerHideCombo;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Label lblCodigo;
        private Bunifu.UI.WinForms.BunifuPictureBox imgAutor;
        private System.Windows.Forms.ImageList imageListIconValidator;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtTitulo;
        private Bunifu.UI.WinForms.BunifuTextbox.BunifuTextBox txtNumero;
        private Bunifu.UI.WinForms.BunifuDatePicker txtAno;
    }
}
