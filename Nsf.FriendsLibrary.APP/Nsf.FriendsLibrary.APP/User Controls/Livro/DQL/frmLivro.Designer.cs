﻿namespace Nsf.FriendsLibrary.APP.User_Controls.Livro
{
    partial class frmLivro
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLivro));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            this.lblNumberPages = new System.Windows.Forms.Label();
            this.dgvLivro = new Bunifu.UI.WinForms.BunifuDataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Condição = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.cdsHearder = new Bunifu.Framework.UI.BunifuCards();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.lblDados = new System.Windows.Forms.Label();
            this.pnSeparatorDados = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblTituloLivro = new System.Windows.Forms.Label();
            this.lblCategoria = new System.Windows.Forms.Label();
            this.lblAutor = new System.Windows.Forms.Label();
            this.chkAutor = new Bunifu.UI.WinForms.BunifuRadioButton();
            this.chkCategoria = new Bunifu.UI.WinForms.BunifuRadioButton();
            this.chkTitulo = new Bunifu.UI.WinForms.BunifuRadioButton();
            this.btnEditar = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnRemove = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnProximo = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.btnAnterior = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.frmFiltroLivro2 = new Nsf.FriendsLibrary.APP.User_Controls.Filtro.frmFiltroLivro();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLivro)).BeginInit();
            this.cdsHearder.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemove)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNumberPages
            // 
            this.lblNumberPages.AutoSize = true;
            this.lblNumberPages.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumberPages.Location = new System.Drawing.Point(275, 599);
            this.lblNumberPages.Name = "lblNumberPages";
            this.lblNumberPages.Size = new System.Drawing.Size(47, 19);
            this.lblNumberPages.TabIndex = 11;
            this.lblNumberPages.Text = "34/42";
            // 
            // dgvLivro
            // 
            this.dgvLivro.AllowCustomTheming = true;
            this.dgvLivro.AllowUserToAddRows = false;
            this.dgvLivro.AllowUserToDeleteRows = false;
            this.dgvLivro.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.dgvLivro.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvLivro.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvLivro.BackgroundColor = System.Drawing.Color.White;
            this.dgvLivro.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvLivro.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvLivro.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.DodgerBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLivro.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvLivro.ColumnHeadersHeight = 40;
            this.dgvLivro.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Condição});
            this.dgvLivro.CurrentTheme.AlternatingRowsStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.dgvLivro.CurrentTheme.AlternatingRowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.dgvLivro.CurrentTheme.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Black;
            this.dgvLivro.CurrentTheme.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.dgvLivro.CurrentTheme.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvLivro.CurrentTheme.BackColor = System.Drawing.Color.White;
            this.dgvLivro.CurrentTheme.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            this.dgvLivro.CurrentTheme.HeaderStyle.BackColor = System.Drawing.Color.DodgerBlue;
            this.dgvLivro.CurrentTheme.HeaderStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            this.dgvLivro.CurrentTheme.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgvLivro.CurrentTheme.Name = null;
            this.dgvLivro.CurrentTheme.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgvLivro.CurrentTheme.RowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.dgvLivro.CurrentTheme.RowsStyle.ForeColor = System.Drawing.Color.Black;
            this.dgvLivro.CurrentTheme.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.dgvLivro.CurrentTheme.RowsStyle.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLivro.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvLivro.EnableHeadersVisualStyles = false;
            this.dgvLivro.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            this.dgvLivro.HeaderBackColor = System.Drawing.Color.DodgerBlue;
            this.dgvLivro.HeaderBgColor = System.Drawing.Color.Empty;
            this.dgvLivro.HeaderForeColor = System.Drawing.Color.White;
            this.dgvLivro.Location = new System.Drawing.Point(3, 211);
            this.dgvLivro.MultiSelect = false;
            this.dgvLivro.Name = "dgvLivro";
            this.dgvLivro.ReadOnly = true;
            this.dgvLivro.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLivro.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvLivro.RowHeadersVisible = false;
            this.dgvLivro.RowHeadersWidth = 35;
            this.dgvLivro.RowTemplate.DividerHeight = 1;
            this.dgvLivro.RowTemplate.Height = 40;
            this.dgvLivro.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvLivro.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvLivro.Size = new System.Drawing.Size(619, 371);
            this.dgvLivro.TabIndex = 13;
            this.dgvLivro.Theme = Bunifu.UI.WinForms.BunifuDataGridView.PresetThemes.Light;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "nm_livro";
            this.Column1.FillWeight = 180F;
            this.Column1.HeaderText = "Título";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "nm_autor";
            this.Column2.HeaderText = "Autor";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "nm_editora";
            this.Column3.FillWeight = 90F;
            this.Column3.HeaderText = "Editora";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "nm_categoria";
            this.Column4.FillWeight = 85F;
            this.Column4.HeaderText = "Categoria";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "nr_paginas";
            this.Column5.FillWeight = 65F;
            this.Column5.HeaderText = "Páginas";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Condição
            // 
            this.Condição.DataPropertyName = "bl_status";
            this.Condição.FillWeight = 75F;
            this.Condição.HeaderText = "Condição";
            this.Condição.Name = "Condição";
            this.Condição.ReadOnly = true;
            // 
            // cdsHearder
            // 
            this.cdsHearder.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cdsHearder.BorderRadius = 5;
            this.cdsHearder.BottomSahddow = true;
            this.cdsHearder.color = System.Drawing.Color.White;
            this.cdsHearder.Controls.Add(this.lblTitulo);
            this.cdsHearder.LeftSahddow = false;
            this.cdsHearder.Location = new System.Drawing.Point(-8, -5);
            this.cdsHearder.Name = "cdsHearder";
            this.cdsHearder.RightSahddow = true;
            this.cdsHearder.ShadowDepth = 20;
            this.cdsHearder.Size = new System.Drawing.Size(686, 54);
            this.cdsHearder.TabIndex = 32;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Segoe UI Semibold", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTitulo.Location = new System.Drawing.Point(20, 15);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(249, 28);
            this.lblTitulo.TabIndex = 1;
            this.lblTitulo.Text = "Gerenciamento dos Livros";
            // 
            // lblDados
            // 
            this.lblDados.AutoSize = true;
            this.lblDados.BackColor = System.Drawing.Color.White;
            this.lblDados.Font = new System.Drawing.Font("Segoe UI Semibold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDados.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblDados.Location = new System.Drawing.Point(34, 59);
            this.lblDados.Name = "lblDados";
            this.lblDados.Size = new System.Drawing.Size(191, 25);
            this.lblDados.TabIndex = 34;
            this.lblDados.Text = "Filtros para Pesquisas";
            // 
            // pnSeparatorDados
            // 
            this.pnSeparatorDados.BackColor = System.Drawing.Color.Gainsboro;
            this.pnSeparatorDados.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnSeparatorDados.Location = new System.Drawing.Point(17, 90);
            this.pnSeparatorDados.Name = "pnSeparatorDados";
            this.pnSeparatorDados.Size = new System.Drawing.Size(590, 1);
            this.pnSeparatorDados.TabIndex = 35;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.lblTituloLivro);
            this.panel2.Controls.Add(this.lblCategoria);
            this.panel2.Controls.Add(this.lblAutor);
            this.panel2.Controls.Add(this.chkAutor);
            this.panel2.Controls.Add(this.chkCategoria);
            this.panel2.Controls.Add(this.chkTitulo);
            this.panel2.Location = new System.Drawing.Point(39, 100);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(127, 101);
            this.panel2.TabIndex = 33;
            // 
            // lblTituloLivro
            // 
            this.lblTituloLivro.AutoSize = true;
            this.lblTituloLivro.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloLivro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTituloLivro.Location = new System.Drawing.Point(38, 11);
            this.lblTituloLivro.Name = "lblTituloLivro";
            this.lblTituloLivro.Size = new System.Drawing.Size(81, 19);
            this.lblTituloLivro.TabIndex = 8;
            this.lblTituloLivro.Text = "Título Livro";
            // 
            // lblCategoria
            // 
            this.lblCategoria.AutoSize = true;
            this.lblCategoria.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCategoria.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCategoria.Location = new System.Drawing.Point(38, 41);
            this.lblCategoria.Name = "lblCategoria";
            this.lblCategoria.Size = new System.Drawing.Size(69, 19);
            this.lblCategoria.TabIndex = 8;
            this.lblCategoria.Text = "Categoria";
            // 
            // lblAutor
            // 
            this.lblAutor.AutoSize = true;
            this.lblAutor.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAutor.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblAutor.Location = new System.Drawing.Point(38, 72);
            this.lblAutor.Name = "lblAutor";
            this.lblAutor.Size = new System.Drawing.Size(44, 19);
            this.lblAutor.TabIndex = 8;
            this.lblAutor.Text = "Autor";
            // 
            // chkAutor
            // 
            this.chkAutor.Checked = false;
            this.chkAutor.Location = new System.Drawing.Point(12, 71);
            this.chkAutor.Name = "chkAutor";
            this.chkAutor.OutlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkAutor.RadioColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkAutor.Size = new System.Drawing.Size(20, 20);
            this.chkAutor.TabIndex = 6;
            this.chkAutor.Tag = "2";
            this.chkAutor.Text = null;
            this.chkAutor.Click += new System.EventHandler(this.chkTitulo_Click);
            // 
            // chkCategoria
            // 
            this.chkCategoria.Checked = false;
            this.chkCategoria.Location = new System.Drawing.Point(12, 41);
            this.chkCategoria.Name = "chkCategoria";
            this.chkCategoria.OutlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkCategoria.RadioColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkCategoria.Size = new System.Drawing.Size(20, 20);
            this.chkCategoria.TabIndex = 6;
            this.chkCategoria.Tag = "1";
            this.chkCategoria.Text = null;
            this.chkCategoria.Click += new System.EventHandler(this.chkTitulo_Click);
            // 
            // chkTitulo
            // 
            this.chkTitulo.Checked = true;
            this.chkTitulo.ForeColor = System.Drawing.Color.White;
            this.chkTitulo.Location = new System.Drawing.Point(12, 11);
            this.chkTitulo.Name = "chkTitulo";
            this.chkTitulo.OutlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkTitulo.RadioColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkTitulo.Size = new System.Drawing.Size(20, 20);
            this.chkTitulo.TabIndex = 6;
            this.chkTitulo.Tag = "0";
            this.chkTitulo.Text = null;
            this.chkTitulo.Click += new System.EventHandler(this.chkTitulo_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.BackColor = System.Drawing.Color.White;
            this.btnEditar.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.Image")));
            this.btnEditar.ImageActive = null;
            this.btnEditar.Location = new System.Drawing.Point(540, 58);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(26, 26);
            this.btnEditar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnEditar.TabIndex = 36;
            this.btnEditar.TabStop = false;
            this.btnEditar.Zoom = 10;
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.BackColor = System.Drawing.Color.White;
            this.btnRemove.Image = ((System.Drawing.Image)(resources.GetObject("btnRemove.Image")));
            this.btnRemove.ImageActive = null;
            this.btnRemove.Location = new System.Drawing.Point(572, 58);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(26, 26);
            this.btnRemove.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnRemove.TabIndex = 37;
            this.btnRemove.TabStop = false;
            this.btnRemove.Zoom = 10;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // btnProximo
            // 
            this.btnProximo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnProximo.BackColor = System.Drawing.Color.Transparent;
            this.btnProximo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnProximo.BackgroundImage")));
            this.btnProximo.ButtonText = "Próxima";
            this.btnProximo.ButtonTextMarginLeft = 10;
            this.btnProximo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProximo.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnProximo.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnProximo.DisabledForecolor = System.Drawing.Color.White;
            this.btnProximo.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProximo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnProximo.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnProximo.IconPadding = 10;
            this.btnProximo.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnProximo.IdleBorderColor = System.Drawing.Color.Gainsboro;
            this.btnProximo.IdleBorderRadius = 10;
            this.btnProximo.IdleBorderThickness = 0;
            this.btnProximo.IdleFillColor = System.Drawing.Color.Gainsboro;
            this.btnProximo.IdleIconLeftImage = null;
            this.btnProximo.IdleIconRightImage = ((System.Drawing.Image)(resources.GetObject("btnProximo.IdleIconRightImage")));
            this.btnProximo.Location = new System.Drawing.Point(320, 588);
            this.btnProximo.Name = "btnProximo";
            stateProperties1.BorderColor = System.Drawing.Color.Silver;
            stateProperties1.BorderRadius = 10;
            stateProperties1.BorderThickness = 0;
            stateProperties1.FillColor = System.Drawing.Color.Silver;
            stateProperties1.IconLeftImage = null;
            stateProperties1.IconRightImage = ((System.Drawing.Image)(resources.GetObject("stateProperties1.IconRightImage")));
            this.btnProximo.onHoverState = stateProperties1;
            this.btnProximo.Size = new System.Drawing.Size(111, 41);
            this.btnProximo.TabIndex = 30;
            this.btnProximo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnProximo.Click += new System.EventHandler(this.btnProximo_Click);
            // 
            // btnAnterior
            // 
            this.btnAnterior.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAnterior.BackColor = System.Drawing.Color.Transparent;
            this.btnAnterior.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAnterior.BackgroundImage")));
            this.btnAnterior.ButtonText = "Anterior";
            this.btnAnterior.ButtonTextMarginLeft = -22;
            this.btnAnterior.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnterior.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnAnterior.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnAnterior.DisabledForecolor = System.Drawing.Color.White;
            this.btnAnterior.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnterior.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAnterior.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnAnterior.IconPadding = 10;
            this.btnAnterior.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnAnterior.IdleBorderColor = System.Drawing.Color.Gainsboro;
            this.btnAnterior.IdleBorderRadius = 10;
            this.btnAnterior.IdleBorderThickness = 0;
            this.btnAnterior.IdleFillColor = System.Drawing.Color.Gainsboro;
            this.btnAnterior.IdleIconLeftImage = ((System.Drawing.Image)(resources.GetObject("btnAnterior.IdleIconLeftImage")));
            this.btnAnterior.IdleIconRightImage = null;
            this.btnAnterior.Location = new System.Drawing.Point(158, 588);
            this.btnAnterior.Name = "btnAnterior";
            stateProperties2.BorderColor = System.Drawing.Color.Silver;
            stateProperties2.BorderRadius = 10;
            stateProperties2.BorderThickness = 0;
            stateProperties2.FillColor = System.Drawing.Color.Silver;
            stateProperties2.IconLeftImage = ((System.Drawing.Image)(resources.GetObject("stateProperties2.IconLeftImage")));
            stateProperties2.IconRightImage = null;
            this.btnAnterior.onHoverState = stateProperties2;
            this.btnAnterior.Size = new System.Drawing.Size(111, 41);
            this.btnAnterior.TabIndex = 31;
            this.btnAnterior.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAnterior.Click += new System.EventHandler(this.btnAnterior_Click);
            // 
            // frmFiltroLivro2
            // 
            this.frmFiltroLivro2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.frmFiltroLivro2.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frmFiltroLivro2.Location = new System.Drawing.Point(189, 100);
            this.frmFiltroLivro2.Name = "frmFiltroLivro2";
            this.frmFiltroLivro2.Size = new System.Drawing.Size(402, 70);
            this.frmFiltroLivro2.TabIndex = 38;
            // 
            // frmLivro
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.frmFiltroLivro2);
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.lblDados);
            this.Controls.Add(this.pnSeparatorDados);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.cdsHearder);
            this.Controls.Add(this.btnProximo);
            this.Controls.Add(this.btnAnterior);
            this.Controls.Add(this.dgvLivro);
            this.Controls.Add(this.lblNumberPages);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Name = "frmLivro";
            this.Size = new System.Drawing.Size(625, 632);
            this.Load += new System.EventHandler(this.frmLivro_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvLivro)).EndInit();
            this.cdsHearder.ResumeLayout(false);
            this.cdsHearder.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemove)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblNumberPages;
        private Bunifu.UI.WinForms.BunifuDataGridView dgvLivro;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnProximo;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnAnterior;
        private Bunifu.Framework.UI.BunifuCards cdsHearder;
        private System.Windows.Forms.Label lblTitulo;
        private Bunifu.Framework.UI.BunifuImageButton btnEditar;
        private Bunifu.Framework.UI.BunifuImageButton btnRemove;
        private System.Windows.Forms.Label lblDados;
        private System.Windows.Forms.Panel pnSeparatorDados;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblTituloLivro;
        private System.Windows.Forms.Label lblCategoria;
        private System.Windows.Forms.Label lblAutor;
        private Bunifu.UI.WinForms.BunifuRadioButton chkAutor;
        private Bunifu.UI.WinForms.BunifuRadioButton chkCategoria;
        private Bunifu.UI.WinForms.BunifuRadioButton chkTitulo;
        private Filtro.frmFiltroLivro frmFiltroLivro1;
        private Filtro.frmFiltroLivro frmFiltroLivro2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Condição;
    }
}
