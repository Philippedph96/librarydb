﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KimtToo.VisualReactive;
using Nsf.FriendsLibrary.Business.Livro;
using Nsf.FriendsLibrary.DB;
using Biblioteca;
using PagedList;
using Biblioteca.Utilitarios.Notificacoes;

namespace Nsf.FriendsLibrary.APP.User_Controls.Livro
{
    public partial class frmLivro : UserControl
    {
        public static frmLivro GridView;

        int pageNumber = 1;
        IPagedList<vw_livro> list;

        public frmLivro()
        {
            GridView = this;
            InitializeComponent();
            PopulateGrid();
        }

        private void ConfigGrid(DataGridView dgv, List<vw_livro> valor)
        {
            dgv.AutoGenerateColumns = false;
            dgv.DataSource = valor;
        }

        private async Task<IPagedList<vw_livro>> GetPagedListAsync(int pageNumber = 1, int pageSize = 8)
        {
            return await Task.Factory.StartNew(() =>
            {
                LivroBusiness db = new LivroBusiness();
                var listview = db.ListView();
                return listview.ToPagedList(pageNumber, pageSize);
            });
        }

        private void PopulateGrid()
        {
            try
            {
                LivroBusiness db = new LivroBusiness();
                var listview = db.ListView();

                dgvLivro.AutoGenerateColumns = false;
                dgvLivro.DataSource = listview;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + "\n" + "Tente mais tarde.",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        public List<vw_livro> PopulateGridWithTitleFilter(string valor)
        {
            LivroBusiness db = new LivroBusiness();
            var listtitle = db.FilterTitleView(valor);

            ConfigGrid(dgvLivro, listtitle);
            return listtitle;
        }

        public List<vw_livro> PopulateGridWithAuthorFilter(string valor)
        {
            LivroBusiness db = new LivroBusiness();
            var listauthor = db.FilterAuthorView(valor);

            ConfigGrid(dgvLivro, listauthor);
            return listauthor;
        }


        public List<vw_livro> PopulateGridWithCategoryFilter(string valor)
        {
            LivroBusiness db = new LivroBusiness();
            var listcategory = db.FilterCategoryView(valor);

            ConfigGrid(dgvLivro, listcategory);
            return listcategory;
        }

        private void RemoveItens()
        {
            var remove = CatchItens();

            LivroBusiness db = new LivroBusiness();
            db.Remove(remove.id_livro);

            PopulateGrid();
        }

        private void UpdateItens()
        {
            //Pegando o valor inteiro de um registro da consulta relacionada a View
            var update = CatchItens();

            //Instanciando a tabela referente a view e carregando todos os valores que seram atualizados
            tb_livro dto = new tb_livro();
            dto.id_livro = update.id_livro;
            dto.id_autor = update.id_autor;
            dto.id_editora = update.id_editora;
            dto.id_categoria = update.id_categoria;

            dto.dt_ano = update.dt_ano;
            dto.nm_livro = update.nm_livro;
            dto.bl_status = update.bl_status;
            dto.img_livro = update.img_livro;
            dto.nr_paginas = update.nr_paginas;

            //Instancio a tela de Atualizar e passo o valor carregado do objeto DTO
            frmAddUpdateBook updateitens = new frmAddUpdateBook();
            updateitens.LoadScreen(dto);

            //Abra o tela no panel principal
            frmMain.UserControl.OpenUserControl(updateitens);
        }

        private vw_livro CatchItens()
        {
            vw_livro livro = dgvLivro.CurrentRow.DataBoundItem as vw_livro;
            return livro;
        }

        private async void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                this.UpdateItens();
                await GetPagedListAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + "\n" + "Tente mais tarde.",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private async void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                this.RemoveItens();

                //Saída de dados
                Image imagem = Properties.Resources.Delete;
                string texto = "As informações do livro foram removidas com êxito!";
                await Task.Run(() => this.SelectorClass(3, texto, imagem));

                await GetPagedListAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + "\n" + "Tente mais tarde.",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private void chkTitulo_Click(object sender, EventArgs e)
        {
            VSReactive<int>.SetState("livro", int.Parse(((Control)sender).Tag.ToString()));
        }

        private async void frmLivro_Load(object sender, EventArgs e)
        {
            try
            {

                list = await GetPagedListAsync();

                btnProximo.Enabled = list.HasNextPage;
                btnAnterior.Enabled = list.HasPreviousPage;

                dgvLivro.DataSource = list.ToList();
                lblNumberPages.Text = string.Format("{0}/{1}", pageNumber, list.PageCount);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + "\n" + "Tente mais tarde.",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private async void btnAnterior_Click(object sender, EventArgs e)
        {
            try
            {

                list = await GetPagedListAsync(--pageNumber);

                btnProximo.Enabled = list.HasNextPage;
                btnAnterior.Enabled = list.HasPreviousPage;

                dgvLivro.DataSource = list.ToList();
                lblNumberPages.Text = string.Format("{0}/{1}", pageNumber, list.PageCount);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + "\n" + "Tente mais tarde.",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }
        private async void btnProximo_Click(object sender, EventArgs e)
        {
            try
            {
                list = await GetPagedListAsync(++pageNumber);

                btnProximo.Enabled = list.HasNextPage;
                btnAnterior.Enabled = list.HasPreviousPage;

                dgvLivro.DataSource = list.ToList();
                lblNumberPages.Text = string.Format("{0}/{1}", pageNumber, list.PageCount);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + "\n" + "Tente mais tarde.",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }
    }
}
