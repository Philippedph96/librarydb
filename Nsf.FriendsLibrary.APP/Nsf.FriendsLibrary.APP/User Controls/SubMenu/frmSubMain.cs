﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KimtToo.VisualReactive;
using Nsf.FriendsLibrary.APP;
using Nsf.FriendsLibrary.APP.User_Controls.Usuario;
using Nsf.FriendsLibrary.APP.User_Controls.Livro;
using Nsf.FriendsLibrary.APP.User_Controls.Bibliotecario;
using Nsf.FriendsLibrary.APP.User_Controls.Emprestimos;
using Nsf.FriendsLibrary.APP.User_Controls.Listas.Atraso;
using Nsf.FriendsLibrary.APP.User_Controls.Listas.Banido;
using Nsf.FriendsLibrary.APP.User_Controls.Listas;
using Nsf.FriendsLibrary.APP.User_Controls.Devolução;

namespace Biblioteca.Forms.Alternativas__Marcos_
{
    public partial class frmSubMain : UserControl
    {
        public frmSubMain()
        {
            InitializeComponent();

            if (Program.IsInDesignMode()) return;
            pnHider.Height = 30;

            VSReactive<int>.Subscribe("menu", e => tbcSubMain.SelectedIndex = e);
        }

        private void btnRegistrarLivros_Click(object sender, EventArgs e)
        {
            frmAddUpdateBook addUpdateBook = new frmAddUpdateBook();
            frmMain.UserControl.OpenUserControl(addUpdateBook);
        }

        //Por hora sem método Assíncrono, pois o rendimento com o método Assíncrono está sendo bem inferior (Apesar de não fazer a tela travar)
        private void btnRegistrarMembros_Click(object sender, EventArgs e)
        {
            //await Task.Factory.StartNew(() =>
            //{
            frmAddUpdateUser addUser = new frmAddUpdateUser();
            frmMain.UserControl.OpenUserControl(addUser);
            //});
        }

        //Por hora sem método Assíncrono, pois o rendimento com o método Assíncrono está sendo bem inferior (Apesar de não fazer a tela travar)
        private void btnRegistrarFuncionarios_Click(object sender, EventArgs e)
        {
            frmAddUpdateLibrarian addLibrarian = new frmAddUpdateLibrarian();
            frmMain.UserControl.OpenUserControl(addLibrarian);
        }

        //Por hora sem método Assíncrono, pois o rendimento com o método Assíncrono está sendo bem inferior (Apesar de não fazer a tela travar)
        private void btnRegistrarEmprestimos_Click(object sender, EventArgs e)
        {
            //await Task.Factory.StartNew(() =>
            //{
                frmAddUpdateLoan addLoan = new frmAddUpdateLoan();
                frmMain.UserControl.OpenUserControl(addLoan);
            //});
        }

        //Por hora sem método Assíncrono, pois o rendimento com o método Assíncrono está sendo bem inferior (Apesar de não fazer a tela travar)
        private void btnAtrasados_Click_1(object sender, EventArgs e)
        {
            frmListaAtraso listaAtraso = new frmListaAtraso();
            frmMain.UserControl.OpenUserControl(listaAtraso);
        }

        //Por hora sem método Assíncrono, pois o rendimento com o método Assíncrono está sendo bem inferior (Apesar de não fazer a tela travar)
        private void btnBanidos_Click_1(object sender, EventArgs e)
        {
            frmListaBanido listaBanido = new frmListaBanido();
            frmMain.UserControl.OpenUserControl(listaBanido);
        }

        //Por hora sem método Assíncrono, pois o rendimento com o método Assíncrono está sendo bem inferior (Apesar de não fazer a tela travar)
        private void btnEspera_Click(object sender, EventArgs e)
        {
            frmListaEspera listaEspera = new frmListaEspera();
            frmMain.UserControl.OpenUserControl(listaEspera);
        }

        private void btnGerenciarLivros_Click(object sender, EventArgs e)
        {
            frmLivro livro = new frmLivro();
            frmMain.UserControl.OpenUserControl(livro);

        }

        private void btnGerenciarMembro_Click(object sender, EventArgs e)
        {
            frmUsuario usuario = new frmUsuario();
            frmMain.UserControl.OpenUserControl(usuario);
        }

        private void btnGerenciarFuncionario_Click(object sender, EventArgs e)
        {
            frmBibliotecario bibliotecario = new frmBibliotecario();
            frmMain.UserControl.OpenUserControl(bibliotecario);
        }

        private void btnGerenciarEmprestimos_Click(object sender, EventArgs e)
        {
            frmEmprestimo emprestimo = new frmEmprestimo();
            frmMain.UserControl.OpenUserControl(emprestimo);
        }

        private void btnRetornos_Click(object sender, EventArgs e)
        {
            //await Task.Factory.StartNew(() =>
            //{
                frmDevolucao devolucao = new frmDevolucao();
                frmMain.UserControl.OpenUserControl(devolucao);
            //});
        }
    }

}
