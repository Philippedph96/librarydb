﻿namespace Biblioteca.Forms.Alternativas__Marcos_
{
    partial class frmSubMain
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSubMain));
            this.pnHider = new System.Windows.Forms.Panel();
            this.pgFuncionario = new System.Windows.Forms.TabPage();
            this.btnRegistrarFuncionarios = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnGerenciarFuncionario = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pgEmprestimo = new System.Windows.Forms.TabPage();
            this.btnRegistrarEmprestimos = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnRetornos = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnGerenciarEmprestimos = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pgRelatorio = new System.Windows.Forms.TabPage();
            this.btnRelatorioEmprestimos = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnRelatorioMembros = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnRelatorios = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pgMembro = new System.Windows.Forms.TabPage();
            this.btnEspera = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnBanidos = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnAtrasados = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnRegistrarMembros = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnGerenciarMembro = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pgLivro = new System.Windows.Forms.TabPage();
            this.btnRegistrarLivros = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnGerenciarLivros = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pgInventario = new System.Windows.Forms.TabPage();
            this.btnAnaliseEmprestimos = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnAnaliseMembros = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnAnaliseLivros = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnDashboard = new Bunifu.Framework.UI.BunifuFlatButton();
            this.tbcSubMain = new System.Windows.Forms.TabControl();
            this.pgFuncionario.SuspendLayout();
            this.pgEmprestimo.SuspendLayout();
            this.pgRelatorio.SuspendLayout();
            this.pgMembro.SuspendLayout();
            this.pgLivro.SuspendLayout();
            this.pgInventario.SuspendLayout();
            this.tbcSubMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnHider
            // 
            this.pnHider.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pnHider.Location = new System.Drawing.Point(-1, 1);
            this.pnHider.Name = "pnHider";
            this.pnHider.Size = new System.Drawing.Size(234, 1);
            this.pnHider.TabIndex = 1;
            // 
            // pgFuncionario
            // 
            this.pgFuncionario.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pgFuncionario.Controls.Add(this.btnRegistrarFuncionarios);
            this.pgFuncionario.Controls.Add(this.btnGerenciarFuncionario);
            this.pgFuncionario.Location = new System.Drawing.Point(4, 25);
            this.pgFuncionario.Name = "pgFuncionario";
            this.pgFuncionario.Padding = new System.Windows.Forms.Padding(3);
            this.pgFuncionario.Size = new System.Drawing.Size(212, 704);
            this.pgFuncionario.TabIndex = 5;
            this.pgFuncionario.Text = "5";
            // 
            // btnRegistrarFuncionarios
            // 
            this.btnRegistrarFuncionarios.Activecolor = System.Drawing.Color.Gainsboro;
            this.btnRegistrarFuncionarios.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnRegistrarFuncionarios.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRegistrarFuncionarios.BorderRadius = 0;
            this.btnRegistrarFuncionarios.ButtonText = "   Registrar Funcionários";
            this.btnRegistrarFuncionarios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRegistrarFuncionarios.DisabledColor = System.Drawing.SystemColors.GrayText;
            this.btnRegistrarFuncionarios.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRegistrarFuncionarios.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrarFuncionarios.Iconcolor = System.Drawing.Color.Transparent;
            this.btnRegistrarFuncionarios.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnRegistrarFuncionarios.Iconimage")));
            this.btnRegistrarFuncionarios.Iconimage_right = null;
            this.btnRegistrarFuncionarios.Iconimage_right_Selected = null;
            this.btnRegistrarFuncionarios.Iconimage_Selected = null;
            this.btnRegistrarFuncionarios.IconMarginLeft = 0;
            this.btnRegistrarFuncionarios.IconMarginRight = 0;
            this.btnRegistrarFuncionarios.IconRightVisible = false;
            this.btnRegistrarFuncionarios.IconRightZoom = 0D;
            this.btnRegistrarFuncionarios.IconVisible = false;
            this.btnRegistrarFuncionarios.IconZoom = 70D;
            this.btnRegistrarFuncionarios.IsTab = true;
            this.btnRegistrarFuncionarios.Location = new System.Drawing.Point(3, 58);
            this.btnRegistrarFuncionarios.Margin = new System.Windows.Forms.Padding(5);
            this.btnRegistrarFuncionarios.Name = "btnRegistrarFuncionarios";
            this.btnRegistrarFuncionarios.Normalcolor = System.Drawing.Color.WhiteSmoke;
            this.btnRegistrarFuncionarios.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btnRegistrarFuncionarios.OnHoverTextColor = System.Drawing.Color.White;
            this.btnRegistrarFuncionarios.selected = false;
            this.btnRegistrarFuncionarios.Size = new System.Drawing.Size(206, 55);
            this.btnRegistrarFuncionarios.TabIndex = 13;
            this.btnRegistrarFuncionarios.Tag = "0";
            this.btnRegistrarFuncionarios.Text = "   Registrar Funcionários";
            this.btnRegistrarFuncionarios.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRegistrarFuncionarios.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnRegistrarFuncionarios.TextFont = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrarFuncionarios.Click += new System.EventHandler(this.btnRegistrarFuncionarios_Click);
            // 
            // btnGerenciarFuncionario
            // 
            this.btnGerenciarFuncionario.Activecolor = System.Drawing.Color.Gainsboro;
            this.btnGerenciarFuncionario.BackColor = System.Drawing.Color.Gainsboro;
            this.btnGerenciarFuncionario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGerenciarFuncionario.BorderRadius = 0;
            this.btnGerenciarFuncionario.ButtonText = "   Gerenciar Funcionários";
            this.btnGerenciarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGerenciarFuncionario.DisabledColor = System.Drawing.SystemColors.GrayText;
            this.btnGerenciarFuncionario.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnGerenciarFuncionario.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGerenciarFuncionario.Iconcolor = System.Drawing.Color.Transparent;
            this.btnGerenciarFuncionario.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnGerenciarFuncionario.Iconimage")));
            this.btnGerenciarFuncionario.Iconimage_right = null;
            this.btnGerenciarFuncionario.Iconimage_right_Selected = null;
            this.btnGerenciarFuncionario.Iconimage_Selected = null;
            this.btnGerenciarFuncionario.IconMarginLeft = 0;
            this.btnGerenciarFuncionario.IconMarginRight = 0;
            this.btnGerenciarFuncionario.IconRightVisible = false;
            this.btnGerenciarFuncionario.IconRightZoom = 0D;
            this.btnGerenciarFuncionario.IconVisible = false;
            this.btnGerenciarFuncionario.IconZoom = 70D;
            this.btnGerenciarFuncionario.IsTab = true;
            this.btnGerenciarFuncionario.Location = new System.Drawing.Point(3, 3);
            this.btnGerenciarFuncionario.Margin = new System.Windows.Forms.Padding(5);
            this.btnGerenciarFuncionario.Name = "btnGerenciarFuncionario";
            this.btnGerenciarFuncionario.Normalcolor = System.Drawing.Color.WhiteSmoke;
            this.btnGerenciarFuncionario.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btnGerenciarFuncionario.OnHoverTextColor = System.Drawing.Color.White;
            this.btnGerenciarFuncionario.selected = true;
            this.btnGerenciarFuncionario.Size = new System.Drawing.Size(206, 55);
            this.btnGerenciarFuncionario.TabIndex = 14;
            this.btnGerenciarFuncionario.Tag = "0";
            this.btnGerenciarFuncionario.Text = "   Gerenciar Funcionários";
            this.btnGerenciarFuncionario.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGerenciarFuncionario.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnGerenciarFuncionario.TextFont = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGerenciarFuncionario.Click += new System.EventHandler(this.btnGerenciarFuncionario_Click);
            // 
            // pgEmprestimo
            // 
            this.pgEmprestimo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pgEmprestimo.Controls.Add(this.btnRegistrarEmprestimos);
            this.pgEmprestimo.Controls.Add(this.btnRetornos);
            this.pgEmprestimo.Controls.Add(this.btnGerenciarEmprestimos);
            this.pgEmprestimo.Location = new System.Drawing.Point(4, 25);
            this.pgEmprestimo.Name = "pgEmprestimo";
            this.pgEmprestimo.Padding = new System.Windows.Forms.Padding(3);
            this.pgEmprestimo.Size = new System.Drawing.Size(212, 704);
            this.pgEmprestimo.TabIndex = 4;
            this.pgEmprestimo.Text = "4";
            // 
            // btnRegistrarEmprestimos
            // 
            this.btnRegistrarEmprestimos.Activecolor = System.Drawing.Color.Gainsboro;
            this.btnRegistrarEmprestimos.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnRegistrarEmprestimos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRegistrarEmprestimos.BorderRadius = 0;
            this.btnRegistrarEmprestimos.ButtonText = "   Registrar Empréstimos";
            this.btnRegistrarEmprestimos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRegistrarEmprestimos.DisabledColor = System.Drawing.SystemColors.GrayText;
            this.btnRegistrarEmprestimos.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRegistrarEmprestimos.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrarEmprestimos.Iconcolor = System.Drawing.Color.Transparent;
            this.btnRegistrarEmprestimos.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnRegistrarEmprestimos.Iconimage")));
            this.btnRegistrarEmprestimos.Iconimage_right = null;
            this.btnRegistrarEmprestimos.Iconimage_right_Selected = null;
            this.btnRegistrarEmprestimos.Iconimage_Selected = null;
            this.btnRegistrarEmprestimos.IconMarginLeft = 0;
            this.btnRegistrarEmprestimos.IconMarginRight = 0;
            this.btnRegistrarEmprestimos.IconRightVisible = false;
            this.btnRegistrarEmprestimos.IconRightZoom = 0D;
            this.btnRegistrarEmprestimos.IconVisible = false;
            this.btnRegistrarEmprestimos.IconZoom = 70D;
            this.btnRegistrarEmprestimos.IsTab = true;
            this.btnRegistrarEmprestimos.Location = new System.Drawing.Point(3, 113);
            this.btnRegistrarEmprestimos.Margin = new System.Windows.Forms.Padding(5);
            this.btnRegistrarEmprestimos.Name = "btnRegistrarEmprestimos";
            this.btnRegistrarEmprestimos.Normalcolor = System.Drawing.Color.WhiteSmoke;
            this.btnRegistrarEmprestimos.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btnRegistrarEmprestimos.OnHoverTextColor = System.Drawing.Color.White;
            this.btnRegistrarEmprestimos.selected = false;
            this.btnRegistrarEmprestimos.Size = new System.Drawing.Size(206, 55);
            this.btnRegistrarEmprestimos.TabIndex = 10;
            this.btnRegistrarEmprestimos.Tag = "0";
            this.btnRegistrarEmprestimos.Text = "   Registrar Empréstimos";
            this.btnRegistrarEmprestimos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRegistrarEmprestimos.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnRegistrarEmprestimos.TextFont = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrarEmprestimos.Click += new System.EventHandler(this.btnRegistrarEmprestimos_Click);
            // 
            // btnRetornos
            // 
            this.btnRetornos.Activecolor = System.Drawing.Color.Gainsboro;
            this.btnRetornos.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnRetornos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRetornos.BorderRadius = 0;
            this.btnRetornos.ButtonText = "   Gerenciar Retornos";
            this.btnRetornos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRetornos.DisabledColor = System.Drawing.SystemColors.GrayText;
            this.btnRetornos.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRetornos.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetornos.Iconcolor = System.Drawing.Color.Transparent;
            this.btnRetornos.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnRetornos.Iconimage")));
            this.btnRetornos.Iconimage_right = null;
            this.btnRetornos.Iconimage_right_Selected = null;
            this.btnRetornos.Iconimage_Selected = null;
            this.btnRetornos.IconMarginLeft = 0;
            this.btnRetornos.IconMarginRight = 0;
            this.btnRetornos.IconRightVisible = false;
            this.btnRetornos.IconRightZoom = 0D;
            this.btnRetornos.IconVisible = false;
            this.btnRetornos.IconZoom = 70D;
            this.btnRetornos.IsTab = true;
            this.btnRetornos.Location = new System.Drawing.Point(3, 58);
            this.btnRetornos.Margin = new System.Windows.Forms.Padding(5);
            this.btnRetornos.Name = "btnRetornos";
            this.btnRetornos.Normalcolor = System.Drawing.Color.WhiteSmoke;
            this.btnRetornos.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btnRetornos.OnHoverTextColor = System.Drawing.Color.White;
            this.btnRetornos.selected = false;
            this.btnRetornos.Size = new System.Drawing.Size(206, 55);
            this.btnRetornos.TabIndex = 11;
            this.btnRetornos.Tag = "0";
            this.btnRetornos.Text = "   Gerenciar Retornos";
            this.btnRetornos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRetornos.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnRetornos.TextFont = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetornos.Click += new System.EventHandler(this.btnRetornos_Click);
            // 
            // btnGerenciarEmprestimos
            // 
            this.btnGerenciarEmprestimos.Activecolor = System.Drawing.Color.Gainsboro;
            this.btnGerenciarEmprestimos.BackColor = System.Drawing.Color.Gainsboro;
            this.btnGerenciarEmprestimos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGerenciarEmprestimos.BorderRadius = 0;
            this.btnGerenciarEmprestimos.ButtonText = "   Gerenciar Empréstimos";
            this.btnGerenciarEmprestimos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGerenciarEmprestimos.DisabledColor = System.Drawing.SystemColors.GrayText;
            this.btnGerenciarEmprestimos.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnGerenciarEmprestimos.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGerenciarEmprestimos.Iconcolor = System.Drawing.Color.Transparent;
            this.btnGerenciarEmprestimos.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnGerenciarEmprestimos.Iconimage")));
            this.btnGerenciarEmprestimos.Iconimage_right = null;
            this.btnGerenciarEmprestimos.Iconimage_right_Selected = null;
            this.btnGerenciarEmprestimos.Iconimage_Selected = null;
            this.btnGerenciarEmprestimos.IconMarginLeft = 0;
            this.btnGerenciarEmprestimos.IconMarginRight = 0;
            this.btnGerenciarEmprestimos.IconRightVisible = false;
            this.btnGerenciarEmprestimos.IconRightZoom = 0D;
            this.btnGerenciarEmprestimos.IconVisible = false;
            this.btnGerenciarEmprestimos.IconZoom = 70D;
            this.btnGerenciarEmprestimos.IsTab = true;
            this.btnGerenciarEmprestimos.Location = new System.Drawing.Point(3, 3);
            this.btnGerenciarEmprestimos.Margin = new System.Windows.Forms.Padding(5);
            this.btnGerenciarEmprestimos.Name = "btnGerenciarEmprestimos";
            this.btnGerenciarEmprestimos.Normalcolor = System.Drawing.Color.WhiteSmoke;
            this.btnGerenciarEmprestimos.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btnGerenciarEmprestimos.OnHoverTextColor = System.Drawing.Color.White;
            this.btnGerenciarEmprestimos.selected = true;
            this.btnGerenciarEmprestimos.Size = new System.Drawing.Size(206, 55);
            this.btnGerenciarEmprestimos.TabIndex = 12;
            this.btnGerenciarEmprestimos.Tag = "0";
            this.btnGerenciarEmprestimos.Text = "   Gerenciar Empréstimos";
            this.btnGerenciarEmprestimos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGerenciarEmprestimos.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnGerenciarEmprestimos.TextFont = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGerenciarEmprestimos.Click += new System.EventHandler(this.btnGerenciarEmprestimos_Click);
            // 
            // pgRelatorio
            // 
            this.pgRelatorio.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pgRelatorio.Controls.Add(this.btnRelatorioEmprestimos);
            this.pgRelatorio.Controls.Add(this.btnRelatorioMembros);
            this.pgRelatorio.Controls.Add(this.btnRelatorios);
            this.pgRelatorio.Location = new System.Drawing.Point(4, 25);
            this.pgRelatorio.Name = "pgRelatorio";
            this.pgRelatorio.Padding = new System.Windows.Forms.Padding(3);
            this.pgRelatorio.Size = new System.Drawing.Size(212, 704);
            this.pgRelatorio.TabIndex = 3;
            this.pgRelatorio.Text = "3";
            // 
            // btnRelatorioEmprestimos
            // 
            this.btnRelatorioEmprestimos.Activecolor = System.Drawing.Color.Gainsboro;
            this.btnRelatorioEmprestimos.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnRelatorioEmprestimos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRelatorioEmprestimos.BorderRadius = 0;
            this.btnRelatorioEmprestimos.ButtonText = "   Relatórios Empréstimos";
            this.btnRelatorioEmprestimos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRelatorioEmprestimos.DisabledColor = System.Drawing.SystemColors.GrayText;
            this.btnRelatorioEmprestimos.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRelatorioEmprestimos.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRelatorioEmprestimos.Iconcolor = System.Drawing.Color.Transparent;
            this.btnRelatorioEmprestimos.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnRelatorioEmprestimos.Iconimage")));
            this.btnRelatorioEmprestimos.Iconimage_right = null;
            this.btnRelatorioEmprestimos.Iconimage_right_Selected = null;
            this.btnRelatorioEmprestimos.Iconimage_Selected = null;
            this.btnRelatorioEmprestimos.IconMarginLeft = 0;
            this.btnRelatorioEmprestimos.IconMarginRight = 0;
            this.btnRelatorioEmprestimos.IconRightVisible = false;
            this.btnRelatorioEmprestimos.IconRightZoom = 0D;
            this.btnRelatorioEmprestimos.IconVisible = false;
            this.btnRelatorioEmprestimos.IconZoom = 70D;
            this.btnRelatorioEmprestimos.IsTab = true;
            this.btnRelatorioEmprestimos.Location = new System.Drawing.Point(3, 113);
            this.btnRelatorioEmprestimos.Margin = new System.Windows.Forms.Padding(5);
            this.btnRelatorioEmprestimos.Name = "btnRelatorioEmprestimos";
            this.btnRelatorioEmprestimos.Normalcolor = System.Drawing.Color.WhiteSmoke;
            this.btnRelatorioEmprestimos.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btnRelatorioEmprestimos.OnHoverTextColor = System.Drawing.Color.White;
            this.btnRelatorioEmprestimos.selected = false;
            this.btnRelatorioEmprestimos.Size = new System.Drawing.Size(206, 55);
            this.btnRelatorioEmprestimos.TabIndex = 10;
            this.btnRelatorioEmprestimos.Tag = "0";
            this.btnRelatorioEmprestimos.Text = "   Relatórios Empréstimos";
            this.btnRelatorioEmprestimos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRelatorioEmprestimos.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnRelatorioEmprestimos.TextFont = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // btnRelatorioMembros
            // 
            this.btnRelatorioMembros.Activecolor = System.Drawing.Color.Gainsboro;
            this.btnRelatorioMembros.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnRelatorioMembros.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRelatorioMembros.BorderRadius = 0;
            this.btnRelatorioMembros.ButtonText = "   Relatórios dos Membros";
            this.btnRelatorioMembros.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRelatorioMembros.DisabledColor = System.Drawing.SystemColors.GrayText;
            this.btnRelatorioMembros.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRelatorioMembros.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRelatorioMembros.Iconcolor = System.Drawing.Color.Transparent;
            this.btnRelatorioMembros.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnRelatorioMembros.Iconimage")));
            this.btnRelatorioMembros.Iconimage_right = null;
            this.btnRelatorioMembros.Iconimage_right_Selected = null;
            this.btnRelatorioMembros.Iconimage_Selected = null;
            this.btnRelatorioMembros.IconMarginLeft = 0;
            this.btnRelatorioMembros.IconMarginRight = 0;
            this.btnRelatorioMembros.IconRightVisible = false;
            this.btnRelatorioMembros.IconRightZoom = 0D;
            this.btnRelatorioMembros.IconVisible = false;
            this.btnRelatorioMembros.IconZoom = 70D;
            this.btnRelatorioMembros.IsTab = true;
            this.btnRelatorioMembros.Location = new System.Drawing.Point(3, 58);
            this.btnRelatorioMembros.Margin = new System.Windows.Forms.Padding(5);
            this.btnRelatorioMembros.Name = "btnRelatorioMembros";
            this.btnRelatorioMembros.Normalcolor = System.Drawing.Color.WhiteSmoke;
            this.btnRelatorioMembros.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btnRelatorioMembros.OnHoverTextColor = System.Drawing.Color.White;
            this.btnRelatorioMembros.selected = false;
            this.btnRelatorioMembros.Size = new System.Drawing.Size(206, 55);
            this.btnRelatorioMembros.TabIndex = 11;
            this.btnRelatorioMembros.Tag = "0";
            this.btnRelatorioMembros.Text = "   Relatórios dos Membros";
            this.btnRelatorioMembros.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRelatorioMembros.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnRelatorioMembros.TextFont = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // btnRelatorios
            // 
            this.btnRelatorios.Activecolor = System.Drawing.Color.Gainsboro;
            this.btnRelatorios.BackColor = System.Drawing.Color.Gainsboro;
            this.btnRelatorios.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRelatorios.BorderRadius = 0;
            this.btnRelatorios.ButtonText = "   Relatórios Gerais";
            this.btnRelatorios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRelatorios.DisabledColor = System.Drawing.SystemColors.GrayText;
            this.btnRelatorios.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRelatorios.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRelatorios.Iconcolor = System.Drawing.Color.Transparent;
            this.btnRelatorios.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnRelatorios.Iconimage")));
            this.btnRelatorios.Iconimage_right = null;
            this.btnRelatorios.Iconimage_right_Selected = null;
            this.btnRelatorios.Iconimage_Selected = null;
            this.btnRelatorios.IconMarginLeft = 0;
            this.btnRelatorios.IconMarginRight = 0;
            this.btnRelatorios.IconRightVisible = false;
            this.btnRelatorios.IconRightZoom = 0D;
            this.btnRelatorios.IconVisible = false;
            this.btnRelatorios.IconZoom = 70D;
            this.btnRelatorios.IsTab = true;
            this.btnRelatorios.Location = new System.Drawing.Point(3, 3);
            this.btnRelatorios.Margin = new System.Windows.Forms.Padding(5);
            this.btnRelatorios.Name = "btnRelatorios";
            this.btnRelatorios.Normalcolor = System.Drawing.Color.WhiteSmoke;
            this.btnRelatorios.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btnRelatorios.OnHoverTextColor = System.Drawing.Color.White;
            this.btnRelatorios.selected = true;
            this.btnRelatorios.Size = new System.Drawing.Size(206, 55);
            this.btnRelatorios.TabIndex = 12;
            this.btnRelatorios.Tag = "0";
            this.btnRelatorios.Text = "   Relatórios Gerais";
            this.btnRelatorios.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRelatorios.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnRelatorios.TextFont = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // pgMembro
            // 
            this.pgMembro.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pgMembro.Controls.Add(this.btnEspera);
            this.pgMembro.Controls.Add(this.btnBanidos);
            this.pgMembro.Controls.Add(this.btnAtrasados);
            this.pgMembro.Controls.Add(this.btnRegistrarMembros);
            this.pgMembro.Controls.Add(this.btnGerenciarMembro);
            this.pgMembro.Location = new System.Drawing.Point(4, 25);
            this.pgMembro.Name = "pgMembro";
            this.pgMembro.Padding = new System.Windows.Forms.Padding(3);
            this.pgMembro.Size = new System.Drawing.Size(212, 704);
            this.pgMembro.TabIndex = 2;
            this.pgMembro.Text = "2";
            // 
            // btnEspera
            // 
            this.btnEspera.Activecolor = System.Drawing.Color.Gainsboro;
            this.btnEspera.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnEspera.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEspera.BorderRadius = 0;
            this.btnEspera.ButtonText = "   Lista de Espera";
            this.btnEspera.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEspera.DisabledColor = System.Drawing.SystemColors.GrayText;
            this.btnEspera.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEspera.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEspera.Iconcolor = System.Drawing.Color.Transparent;
            this.btnEspera.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnEspera.Iconimage")));
            this.btnEspera.Iconimage_right = null;
            this.btnEspera.Iconimage_right_Selected = null;
            this.btnEspera.Iconimage_Selected = null;
            this.btnEspera.IconMarginLeft = 0;
            this.btnEspera.IconMarginRight = 0;
            this.btnEspera.IconRightVisible = false;
            this.btnEspera.IconRightZoom = 0D;
            this.btnEspera.IconVisible = false;
            this.btnEspera.IconZoom = 70D;
            this.btnEspera.IsTab = true;
            this.btnEspera.Location = new System.Drawing.Point(3, 223);
            this.btnEspera.Margin = new System.Windows.Forms.Padding(5);
            this.btnEspera.Name = "btnEspera";
            this.btnEspera.Normalcolor = System.Drawing.Color.WhiteSmoke;
            this.btnEspera.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btnEspera.OnHoverTextColor = System.Drawing.Color.White;
            this.btnEspera.selected = false;
            this.btnEspera.Size = new System.Drawing.Size(206, 55);
            this.btnEspera.TabIndex = 8;
            this.btnEspera.Tag = "0";
            this.btnEspera.Text = "   Lista de Espera";
            this.btnEspera.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEspera.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnEspera.TextFont = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEspera.Visible = false;
            this.btnEspera.Click += new System.EventHandler(this.btnEspera_Click);
            // 
            // btnBanidos
            // 
            this.btnBanidos.Activecolor = System.Drawing.Color.Gainsboro;
            this.btnBanidos.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnBanidos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBanidos.BorderRadius = 0;
            this.btnBanidos.ButtonText = "   Lista de Banidos";
            this.btnBanidos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBanidos.DisabledColor = System.Drawing.SystemColors.GrayText;
            this.btnBanidos.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnBanidos.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanidos.Iconcolor = System.Drawing.Color.Transparent;
            this.btnBanidos.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnBanidos.Iconimage")));
            this.btnBanidos.Iconimage_right = null;
            this.btnBanidos.Iconimage_right_Selected = null;
            this.btnBanidos.Iconimage_Selected = null;
            this.btnBanidos.IconMarginLeft = 0;
            this.btnBanidos.IconMarginRight = 0;
            this.btnBanidos.IconRightVisible = false;
            this.btnBanidos.IconRightZoom = 0D;
            this.btnBanidos.IconVisible = false;
            this.btnBanidos.IconZoom = 70D;
            this.btnBanidos.IsTab = true;
            this.btnBanidos.Location = new System.Drawing.Point(3, 168);
            this.btnBanidos.Margin = new System.Windows.Forms.Padding(5);
            this.btnBanidos.Name = "btnBanidos";
            this.btnBanidos.Normalcolor = System.Drawing.Color.WhiteSmoke;
            this.btnBanidos.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btnBanidos.OnHoverTextColor = System.Drawing.Color.White;
            this.btnBanidos.selected = false;
            this.btnBanidos.Size = new System.Drawing.Size(206, 55);
            this.btnBanidos.TabIndex = 4;
            this.btnBanidos.Tag = "0";
            this.btnBanidos.Text = "   Lista de Banidos";
            this.btnBanidos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBanidos.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnBanidos.TextFont = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBanidos.Visible = false;
            this.btnBanidos.Click += new System.EventHandler(this.btnBanidos_Click_1);
            // 
            // btnAtrasados
            // 
            this.btnAtrasados.Activecolor = System.Drawing.Color.Gainsboro;
            this.btnAtrasados.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnAtrasados.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAtrasados.BorderRadius = 0;
            this.btnAtrasados.ButtonText = "   Lista de Atrasados";
            this.btnAtrasados.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAtrasados.DisabledColor = System.Drawing.SystemColors.GrayText;
            this.btnAtrasados.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAtrasados.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtrasados.Iconcolor = System.Drawing.Color.Transparent;
            this.btnAtrasados.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnAtrasados.Iconimage")));
            this.btnAtrasados.Iconimage_right = null;
            this.btnAtrasados.Iconimage_right_Selected = null;
            this.btnAtrasados.Iconimage_Selected = null;
            this.btnAtrasados.IconMarginLeft = 0;
            this.btnAtrasados.IconMarginRight = 0;
            this.btnAtrasados.IconRightVisible = false;
            this.btnAtrasados.IconRightZoom = 0D;
            this.btnAtrasados.IconVisible = false;
            this.btnAtrasados.IconZoom = 70D;
            this.btnAtrasados.IsTab = true;
            this.btnAtrasados.Location = new System.Drawing.Point(3, 113);
            this.btnAtrasados.Margin = new System.Windows.Forms.Padding(5);
            this.btnAtrasados.Name = "btnAtrasados";
            this.btnAtrasados.Normalcolor = System.Drawing.Color.WhiteSmoke;
            this.btnAtrasados.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btnAtrasados.OnHoverTextColor = System.Drawing.Color.White;
            this.btnAtrasados.selected = false;
            this.btnAtrasados.Size = new System.Drawing.Size(206, 55);
            this.btnAtrasados.TabIndex = 5;
            this.btnAtrasados.Tag = "0";
            this.btnAtrasados.Text = "   Lista de Atrasados";
            this.btnAtrasados.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAtrasados.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAtrasados.TextFont = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtrasados.Visible = false;
            this.btnAtrasados.Click += new System.EventHandler(this.btnAtrasados_Click_1);
            // 
            // btnRegistrarMembros
            // 
            this.btnRegistrarMembros.Activecolor = System.Drawing.Color.Gainsboro;
            this.btnRegistrarMembros.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnRegistrarMembros.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRegistrarMembros.BorderRadius = 0;
            this.btnRegistrarMembros.ButtonText = "   Registrar Leitores";
            this.btnRegistrarMembros.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRegistrarMembros.DisabledColor = System.Drawing.SystemColors.GrayText;
            this.btnRegistrarMembros.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRegistrarMembros.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrarMembros.Iconcolor = System.Drawing.Color.Transparent;
            this.btnRegistrarMembros.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnRegistrarMembros.Iconimage")));
            this.btnRegistrarMembros.Iconimage_right = null;
            this.btnRegistrarMembros.Iconimage_right_Selected = null;
            this.btnRegistrarMembros.Iconimage_Selected = null;
            this.btnRegistrarMembros.IconMarginLeft = 0;
            this.btnRegistrarMembros.IconMarginRight = 0;
            this.btnRegistrarMembros.IconRightVisible = false;
            this.btnRegistrarMembros.IconRightZoom = 0D;
            this.btnRegistrarMembros.IconVisible = false;
            this.btnRegistrarMembros.IconZoom = 70D;
            this.btnRegistrarMembros.IsTab = true;
            this.btnRegistrarMembros.Location = new System.Drawing.Point(3, 58);
            this.btnRegistrarMembros.Margin = new System.Windows.Forms.Padding(5);
            this.btnRegistrarMembros.Name = "btnRegistrarMembros";
            this.btnRegistrarMembros.Normalcolor = System.Drawing.Color.WhiteSmoke;
            this.btnRegistrarMembros.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btnRegistrarMembros.OnHoverTextColor = System.Drawing.Color.White;
            this.btnRegistrarMembros.selected = false;
            this.btnRegistrarMembros.Size = new System.Drawing.Size(206, 55);
            this.btnRegistrarMembros.TabIndex = 6;
            this.btnRegistrarMembros.Tag = "0";
            this.btnRegistrarMembros.Text = "   Registrar Leitores";
            this.btnRegistrarMembros.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRegistrarMembros.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnRegistrarMembros.TextFont = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrarMembros.Click += new System.EventHandler(this.btnRegistrarMembros_Click);
            // 
            // btnGerenciarMembro
            // 
            this.btnGerenciarMembro.Activecolor = System.Drawing.Color.Gainsboro;
            this.btnGerenciarMembro.BackColor = System.Drawing.Color.Gainsboro;
            this.btnGerenciarMembro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGerenciarMembro.BorderRadius = 0;
            this.btnGerenciarMembro.ButtonText = "   Gerenciar Leitores";
            this.btnGerenciarMembro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGerenciarMembro.DisabledColor = System.Drawing.SystemColors.GrayText;
            this.btnGerenciarMembro.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnGerenciarMembro.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGerenciarMembro.Iconcolor = System.Drawing.Color.Transparent;
            this.btnGerenciarMembro.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnGerenciarMembro.Iconimage")));
            this.btnGerenciarMembro.Iconimage_right = null;
            this.btnGerenciarMembro.Iconimage_right_Selected = null;
            this.btnGerenciarMembro.Iconimage_Selected = null;
            this.btnGerenciarMembro.IconMarginLeft = 0;
            this.btnGerenciarMembro.IconMarginRight = 0;
            this.btnGerenciarMembro.IconRightVisible = false;
            this.btnGerenciarMembro.IconRightZoom = 0D;
            this.btnGerenciarMembro.IconVisible = false;
            this.btnGerenciarMembro.IconZoom = 70D;
            this.btnGerenciarMembro.IsTab = true;
            this.btnGerenciarMembro.Location = new System.Drawing.Point(3, 3);
            this.btnGerenciarMembro.Margin = new System.Windows.Forms.Padding(5);
            this.btnGerenciarMembro.Name = "btnGerenciarMembro";
            this.btnGerenciarMembro.Normalcolor = System.Drawing.Color.WhiteSmoke;
            this.btnGerenciarMembro.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btnGerenciarMembro.OnHoverTextColor = System.Drawing.Color.White;
            this.btnGerenciarMembro.selected = true;
            this.btnGerenciarMembro.Size = new System.Drawing.Size(206, 55);
            this.btnGerenciarMembro.TabIndex = 7;
            this.btnGerenciarMembro.Tag = "0";
            this.btnGerenciarMembro.Text = "   Gerenciar Leitores";
            this.btnGerenciarMembro.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGerenciarMembro.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnGerenciarMembro.TextFont = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGerenciarMembro.Click += new System.EventHandler(this.btnGerenciarMembro_Click);
            // 
            // pgLivro
            // 
            this.pgLivro.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pgLivro.Controls.Add(this.btnRegistrarLivros);
            this.pgLivro.Controls.Add(this.btnGerenciarLivros);
            this.pgLivro.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pgLivro.Location = new System.Drawing.Point(4, 25);
            this.pgLivro.Name = "pgLivro";
            this.pgLivro.Padding = new System.Windows.Forms.Padding(3);
            this.pgLivro.Size = new System.Drawing.Size(212, 704);
            this.pgLivro.TabIndex = 1;
            this.pgLivro.Text = "1";
            // 
            // btnRegistrarLivros
            // 
            this.btnRegistrarLivros.Activecolor = System.Drawing.Color.Gainsboro;
            this.btnRegistrarLivros.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnRegistrarLivros.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRegistrarLivros.BorderRadius = 0;
            this.btnRegistrarLivros.ButtonText = "   Registrar Livros";
            this.btnRegistrarLivros.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRegistrarLivros.DisabledColor = System.Drawing.SystemColors.GrayText;
            this.btnRegistrarLivros.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRegistrarLivros.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrarLivros.Iconcolor = System.Drawing.Color.Transparent;
            this.btnRegistrarLivros.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnRegistrarLivros.Iconimage")));
            this.btnRegistrarLivros.Iconimage_right = null;
            this.btnRegistrarLivros.Iconimage_right_Selected = null;
            this.btnRegistrarLivros.Iconimage_Selected = null;
            this.btnRegistrarLivros.IconMarginLeft = 0;
            this.btnRegistrarLivros.IconMarginRight = 0;
            this.btnRegistrarLivros.IconRightVisible = false;
            this.btnRegistrarLivros.IconRightZoom = 0D;
            this.btnRegistrarLivros.IconVisible = false;
            this.btnRegistrarLivros.IconZoom = 70D;
            this.btnRegistrarLivros.IsTab = true;
            this.btnRegistrarLivros.Location = new System.Drawing.Point(3, 58);
            this.btnRegistrarLivros.Margin = new System.Windows.Forms.Padding(5);
            this.btnRegistrarLivros.Name = "btnRegistrarLivros";
            this.btnRegistrarLivros.Normalcolor = System.Drawing.Color.WhiteSmoke;
            this.btnRegistrarLivros.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btnRegistrarLivros.OnHoverTextColor = System.Drawing.Color.White;
            this.btnRegistrarLivros.selected = false;
            this.btnRegistrarLivros.Size = new System.Drawing.Size(206, 55);
            this.btnRegistrarLivros.TabIndex = 5;
            this.btnRegistrarLivros.Tag = "0";
            this.btnRegistrarLivros.Text = "   Registrar Livros";
            this.btnRegistrarLivros.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRegistrarLivros.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnRegistrarLivros.TextFont = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrarLivros.Click += new System.EventHandler(this.btnRegistrarLivros_Click);
            // 
            // btnGerenciarLivros
            // 
            this.btnGerenciarLivros.Activecolor = System.Drawing.Color.Gainsboro;
            this.btnGerenciarLivros.BackColor = System.Drawing.Color.Gainsboro;
            this.btnGerenciarLivros.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnGerenciarLivros.BorderRadius = 0;
            this.btnGerenciarLivros.ButtonText = "   Gerenciar Livros";
            this.btnGerenciarLivros.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGerenciarLivros.DisabledColor = System.Drawing.SystemColors.GrayText;
            this.btnGerenciarLivros.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnGerenciarLivros.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGerenciarLivros.Iconcolor = System.Drawing.Color.Transparent;
            this.btnGerenciarLivros.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnGerenciarLivros.Iconimage")));
            this.btnGerenciarLivros.Iconimage_right = null;
            this.btnGerenciarLivros.Iconimage_right_Selected = null;
            this.btnGerenciarLivros.Iconimage_Selected = null;
            this.btnGerenciarLivros.IconMarginLeft = 0;
            this.btnGerenciarLivros.IconMarginRight = 0;
            this.btnGerenciarLivros.IconRightVisible = false;
            this.btnGerenciarLivros.IconRightZoom = 0D;
            this.btnGerenciarLivros.IconVisible = false;
            this.btnGerenciarLivros.IconZoom = 70D;
            this.btnGerenciarLivros.IsTab = true;
            this.btnGerenciarLivros.Location = new System.Drawing.Point(3, 3);
            this.btnGerenciarLivros.Margin = new System.Windows.Forms.Padding(5);
            this.btnGerenciarLivros.Name = "btnGerenciarLivros";
            this.btnGerenciarLivros.Normalcolor = System.Drawing.Color.WhiteSmoke;
            this.btnGerenciarLivros.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btnGerenciarLivros.OnHoverTextColor = System.Drawing.Color.White;
            this.btnGerenciarLivros.selected = true;
            this.btnGerenciarLivros.Size = new System.Drawing.Size(206, 55);
            this.btnGerenciarLivros.TabIndex = 4;
            this.btnGerenciarLivros.Tag = "0";
            this.btnGerenciarLivros.Text = "   Gerenciar Livros";
            this.btnGerenciarLivros.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGerenciarLivros.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnGerenciarLivros.TextFont = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGerenciarLivros.Click += new System.EventHandler(this.btnGerenciarLivros_Click);
            // 
            // pgInventario
            // 
            this.pgInventario.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pgInventario.Controls.Add(this.btnAnaliseEmprestimos);
            this.pgInventario.Controls.Add(this.btnAnaliseMembros);
            this.pgInventario.Controls.Add(this.btnAnaliseLivros);
            this.pgInventario.Controls.Add(this.btnDashboard);
            this.pgInventario.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pgInventario.Location = new System.Drawing.Point(4, 25);
            this.pgInventario.Name = "pgInventario";
            this.pgInventario.Padding = new System.Windows.Forms.Padding(3);
            this.pgInventario.Size = new System.Drawing.Size(212, 704);
            this.pgInventario.TabIndex = 0;
            this.pgInventario.Text = "0";
            // 
            // btnAnaliseEmprestimos
            // 
            this.btnAnaliseEmprestimos.Activecolor = System.Drawing.Color.Gainsboro;
            this.btnAnaliseEmprestimos.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnAnaliseEmprestimos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAnaliseEmprestimos.BorderRadius = 0;
            this.btnAnaliseEmprestimos.ButtonText = "   Análise dos Empréstimos";
            this.btnAnaliseEmprestimos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnaliseEmprestimos.DisabledColor = System.Drawing.Color.Gainsboro;
            this.btnAnaliseEmprestimos.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAnaliseEmprestimos.Enabled = false;
            this.btnAnaliseEmprestimos.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnaliseEmprestimos.Iconcolor = System.Drawing.Color.Transparent;
            this.btnAnaliseEmprestimos.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnAnaliseEmprestimos.Iconimage")));
            this.btnAnaliseEmprestimos.Iconimage_right = null;
            this.btnAnaliseEmprestimos.Iconimage_right_Selected = null;
            this.btnAnaliseEmprestimos.Iconimage_Selected = null;
            this.btnAnaliseEmprestimos.IconMarginLeft = 0;
            this.btnAnaliseEmprestimos.IconMarginRight = 0;
            this.btnAnaliseEmprestimos.IconRightVisible = false;
            this.btnAnaliseEmprestimos.IconRightZoom = 0D;
            this.btnAnaliseEmprestimos.IconVisible = false;
            this.btnAnaliseEmprestimos.IconZoom = 70D;
            this.btnAnaliseEmprestimos.IsTab = true;
            this.btnAnaliseEmprestimos.Location = new System.Drawing.Point(3, 168);
            this.btnAnaliseEmprestimos.Margin = new System.Windows.Forms.Padding(5);
            this.btnAnaliseEmprestimos.Name = "btnAnaliseEmprestimos";
            this.btnAnaliseEmprestimos.Normalcolor = System.Drawing.Color.WhiteSmoke;
            this.btnAnaliseEmprestimos.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btnAnaliseEmprestimos.OnHoverTextColor = System.Drawing.Color.White;
            this.btnAnaliseEmprestimos.selected = false;
            this.btnAnaliseEmprestimos.Size = new System.Drawing.Size(206, 55);
            this.btnAnaliseEmprestimos.TabIndex = 3;
            this.btnAnaliseEmprestimos.Tag = "0";
            this.btnAnaliseEmprestimos.Text = "   Análise dos Empréstimos";
            this.btnAnaliseEmprestimos.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAnaliseEmprestimos.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAnaliseEmprestimos.TextFont = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // btnAnaliseMembros
            // 
            this.btnAnaliseMembros.Activecolor = System.Drawing.Color.Gainsboro;
            this.btnAnaliseMembros.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnAnaliseMembros.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAnaliseMembros.BorderRadius = 0;
            this.btnAnaliseMembros.ButtonText = "   Análise dos Membros";
            this.btnAnaliseMembros.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnaliseMembros.DisabledColor = System.Drawing.Color.Gainsboro;
            this.btnAnaliseMembros.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAnaliseMembros.Enabled = false;
            this.btnAnaliseMembros.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnaliseMembros.Iconcolor = System.Drawing.Color.Transparent;
            this.btnAnaliseMembros.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnAnaliseMembros.Iconimage")));
            this.btnAnaliseMembros.Iconimage_right = null;
            this.btnAnaliseMembros.Iconimage_right_Selected = null;
            this.btnAnaliseMembros.Iconimage_Selected = null;
            this.btnAnaliseMembros.IconMarginLeft = 0;
            this.btnAnaliseMembros.IconMarginRight = 0;
            this.btnAnaliseMembros.IconRightVisible = false;
            this.btnAnaliseMembros.IconRightZoom = 0D;
            this.btnAnaliseMembros.IconVisible = false;
            this.btnAnaliseMembros.IconZoom = 70D;
            this.btnAnaliseMembros.IsTab = true;
            this.btnAnaliseMembros.Location = new System.Drawing.Point(3, 113);
            this.btnAnaliseMembros.Margin = new System.Windows.Forms.Padding(5);
            this.btnAnaliseMembros.Name = "btnAnaliseMembros";
            this.btnAnaliseMembros.Normalcolor = System.Drawing.Color.WhiteSmoke;
            this.btnAnaliseMembros.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btnAnaliseMembros.OnHoverTextColor = System.Drawing.Color.White;
            this.btnAnaliseMembros.selected = false;
            this.btnAnaliseMembros.Size = new System.Drawing.Size(206, 55);
            this.btnAnaliseMembros.TabIndex = 3;
            this.btnAnaliseMembros.Tag = "0";
            this.btnAnaliseMembros.Text = "   Análise dos Membros";
            this.btnAnaliseMembros.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAnaliseMembros.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAnaliseMembros.TextFont = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // btnAnaliseLivros
            // 
            this.btnAnaliseLivros.Activecolor = System.Drawing.Color.Gainsboro;
            this.btnAnaliseLivros.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnAnaliseLivros.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAnaliseLivros.BorderRadius = 0;
            this.btnAnaliseLivros.ButtonText = "   Análise dos Livros";
            this.btnAnaliseLivros.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnaliseLivros.DisabledColor = System.Drawing.Color.Gainsboro;
            this.btnAnaliseLivros.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAnaliseLivros.Enabled = false;
            this.btnAnaliseLivros.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnaliseLivros.Iconcolor = System.Drawing.Color.Transparent;
            this.btnAnaliseLivros.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnAnaliseLivros.Iconimage")));
            this.btnAnaliseLivros.Iconimage_right = null;
            this.btnAnaliseLivros.Iconimage_right_Selected = null;
            this.btnAnaliseLivros.Iconimage_Selected = null;
            this.btnAnaliseLivros.IconMarginLeft = 0;
            this.btnAnaliseLivros.IconMarginRight = 0;
            this.btnAnaliseLivros.IconRightVisible = false;
            this.btnAnaliseLivros.IconRightZoom = 0D;
            this.btnAnaliseLivros.IconVisible = false;
            this.btnAnaliseLivros.IconZoom = 70D;
            this.btnAnaliseLivros.IsTab = true;
            this.btnAnaliseLivros.Location = new System.Drawing.Point(3, 58);
            this.btnAnaliseLivros.Margin = new System.Windows.Forms.Padding(5);
            this.btnAnaliseLivros.Name = "btnAnaliseLivros";
            this.btnAnaliseLivros.Normalcolor = System.Drawing.Color.WhiteSmoke;
            this.btnAnaliseLivros.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btnAnaliseLivros.OnHoverTextColor = System.Drawing.Color.White;
            this.btnAnaliseLivros.selected = false;
            this.btnAnaliseLivros.Size = new System.Drawing.Size(206, 55);
            this.btnAnaliseLivros.TabIndex = 3;
            this.btnAnaliseLivros.Tag = "0";
            this.btnAnaliseLivros.Text = "   Análise dos Livros";
            this.btnAnaliseLivros.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAnaliseLivros.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAnaliseLivros.TextFont = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // btnDashboard
            // 
            this.btnDashboard.Activecolor = System.Drawing.Color.Gainsboro;
            this.btnDashboard.BackColor = System.Drawing.Color.Gainsboro;
            this.btnDashboard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnDashboard.BorderRadius = 0;
            this.btnDashboard.ButtonText = "   Visão Geral";
            this.btnDashboard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDashboard.DisabledColor = System.Drawing.Color.Gainsboro;
            this.btnDashboard.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnDashboard.Enabled = false;
            this.btnDashboard.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDashboard.Iconcolor = System.Drawing.Color.Transparent;
            this.btnDashboard.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnDashboard.Iconimage")));
            this.btnDashboard.Iconimage_right = null;
            this.btnDashboard.Iconimage_right_Selected = null;
            this.btnDashboard.Iconimage_Selected = null;
            this.btnDashboard.IconMarginLeft = 0;
            this.btnDashboard.IconMarginRight = 0;
            this.btnDashboard.IconRightVisible = false;
            this.btnDashboard.IconRightZoom = 0D;
            this.btnDashboard.IconVisible = false;
            this.btnDashboard.IconZoom = 70D;
            this.btnDashboard.IsTab = true;
            this.btnDashboard.Location = new System.Drawing.Point(3, 3);
            this.btnDashboard.Margin = new System.Windows.Forms.Padding(5);
            this.btnDashboard.Name = "btnDashboard";
            this.btnDashboard.Normalcolor = System.Drawing.Color.WhiteSmoke;
            this.btnDashboard.OnHovercolor = System.Drawing.Color.Gainsboro;
            this.btnDashboard.OnHoverTextColor = System.Drawing.Color.White;
            this.btnDashboard.selected = true;
            this.btnDashboard.Size = new System.Drawing.Size(206, 55);
            this.btnDashboard.TabIndex = 3;
            this.btnDashboard.Tag = "0";
            this.btnDashboard.Text = "   Visão Geral";
            this.btnDashboard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDashboard.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnDashboard.TextFont = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // tbcSubMain
            // 
            this.tbcSubMain.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbcSubMain.Controls.Add(this.pgInventario);
            this.tbcSubMain.Controls.Add(this.pgLivro);
            this.tbcSubMain.Controls.Add(this.pgMembro);
            this.tbcSubMain.Controls.Add(this.pgRelatorio);
            this.tbcSubMain.Controls.Add(this.pgEmprestimo);
            this.tbcSubMain.Controls.Add(this.pgFuncionario);
            this.tbcSubMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbcSubMain.Location = new System.Drawing.Point(0, 3);
            this.tbcSubMain.Name = "tbcSubMain";
            this.tbcSubMain.SelectedIndex = 0;
            this.tbcSubMain.Size = new System.Drawing.Size(220, 733);
            this.tbcSubMain.TabIndex = 1;
            // 
            // frmSubMain
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.pnHider);
            this.Controls.Add(this.tbcSubMain);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmSubMain";
            this.Size = new System.Drawing.Size(220, 682);
            this.pgFuncionario.ResumeLayout(false);
            this.pgEmprestimo.ResumeLayout(false);
            this.pgRelatorio.ResumeLayout(false);
            this.pgMembro.ResumeLayout(false);
            this.pgLivro.ResumeLayout(false);
            this.pgInventario.ResumeLayout(false);
            this.tbcSubMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnHider;
        private System.Windows.Forms.TabPage pgFuncionario;
        private Bunifu.Framework.UI.BunifuFlatButton btnRegistrarFuncionarios;
        private Bunifu.Framework.UI.BunifuFlatButton btnGerenciarFuncionario;
        private System.Windows.Forms.TabPage pgEmprestimo;
        private Bunifu.Framework.UI.BunifuFlatButton btnRegistrarEmprestimos;
        private Bunifu.Framework.UI.BunifuFlatButton btnRetornos;
        private Bunifu.Framework.UI.BunifuFlatButton btnGerenciarEmprestimos;
        private System.Windows.Forms.TabPage pgRelatorio;
        private Bunifu.Framework.UI.BunifuFlatButton btnRelatorioEmprestimos;
        private Bunifu.Framework.UI.BunifuFlatButton btnRelatorioMembros;
        private Bunifu.Framework.UI.BunifuFlatButton btnRelatorios;
        private System.Windows.Forms.TabPage pgMembro;
        private Bunifu.Framework.UI.BunifuFlatButton btnEspera;
        private Bunifu.Framework.UI.BunifuFlatButton btnBanidos;
        private Bunifu.Framework.UI.BunifuFlatButton btnAtrasados;
        private Bunifu.Framework.UI.BunifuFlatButton btnRegistrarMembros;
        private Bunifu.Framework.UI.BunifuFlatButton btnGerenciarMembro;
        private System.Windows.Forms.TabPage pgLivro;
        private Bunifu.Framework.UI.BunifuFlatButton btnRegistrarLivros;
        private Bunifu.Framework.UI.BunifuFlatButton btnGerenciarLivros;
        private System.Windows.Forms.TabPage pgInventario;
        private Bunifu.Framework.UI.BunifuFlatButton btnAnaliseEmprestimos;
        private Bunifu.Framework.UI.BunifuFlatButton btnAnaliseMembros;
        private Bunifu.Framework.UI.BunifuFlatButton btnAnaliseLivros;
        private Bunifu.Framework.UI.BunifuFlatButton btnDashboard;
        private System.Windows.Forms.TabControl tbcSubMain;
    }
}
