﻿namespace Nsf.FriendsLibrary.APP.User_Controls.Listas
{
    partial class frmListaEspera
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmListaEspera));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties3 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties4 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnEditar = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnRemove = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnAdd = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnProximo = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.btnAnterior = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.lblRegistro = new System.Windows.Forms.Label();
            this.lblDados = new System.Windows.Forms.Label();
            this.pnSeparatorDados = new System.Windows.Forms.Panel();
            this.dgvEspera = new Bunifu.UI.WinForms.BunifuDataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblTituloLivro = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.lblData = new System.Windows.Forms.Label();
            this.chkTitulo = new Bunifu.UI.WinForms.BunifuRadioButton();
            this.chkUsuario = new Bunifu.UI.WinForms.BunifuRadioButton();
            this.chkData = new Bunifu.UI.WinForms.BunifuRadioButton();
            this.cdsHearder = new Bunifu.Framework.UI.BunifuCards();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.rdnUrgencia = new Bunifu.UI.WinForms.BunifuRadioButton();
            this.lblUrgencia = new System.Windows.Forms.Label();
            this.frmFiltroEspera1 = new Nsf.FriendsLibrary.APP.User_Controls.Filtro.frmFiltroEspera();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEspera)).BeginInit();
            this.panel2.SuspendLayout();
            this.cdsHearder.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnEditar
            // 
            this.btnEditar.BackColor = System.Drawing.Color.White;
            this.btnEditar.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.Image")));
            this.btnEditar.ImageActive = null;
            this.btnEditar.Location = new System.Drawing.Point(540, 58);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(26, 26);
            this.btnEditar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnEditar.TabIndex = 41;
            this.btnEditar.TabStop = false;
            this.btnEditar.Zoom = 10;
            // 
            // btnRemove
            // 
            this.btnRemove.BackColor = System.Drawing.Color.White;
            this.btnRemove.Image = ((System.Drawing.Image)(resources.GetObject("btnRemove.Image")));
            this.btnRemove.ImageActive = null;
            this.btnRemove.Location = new System.Drawing.Point(572, 58);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(26, 26);
            this.btnRemove.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnRemove.TabIndex = 42;
            this.btnRemove.TabStop = false;
            this.btnRemove.Zoom = 10;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.White;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageActive = null;
            this.btnAdd.Location = new System.Drawing.Point(508, 58);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(26, 26);
            this.btnAdd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnAdd.TabIndex = 43;
            this.btnAdd.TabStop = false;
            this.btnAdd.Zoom = 10;
            // 
            // btnProximo
            // 
            this.btnProximo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnProximo.BackColor = System.Drawing.Color.Transparent;
            this.btnProximo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnProximo.BackgroundImage")));
            this.btnProximo.ButtonText = "Próxima";
            this.btnProximo.ButtonTextMarginLeft = 10;
            this.btnProximo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProximo.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnProximo.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnProximo.DisabledForecolor = System.Drawing.Color.White;
            this.btnProximo.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProximo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnProximo.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnProximo.IconPadding = 10;
            this.btnProximo.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnProximo.IdleBorderColor = System.Drawing.Color.Gainsboro;
            this.btnProximo.IdleBorderRadius = 10;
            this.btnProximo.IdleBorderThickness = 0;
            this.btnProximo.IdleFillColor = System.Drawing.Color.Gainsboro;
            this.btnProximo.IdleIconLeftImage = null;
            this.btnProximo.IdleIconRightImage = ((System.Drawing.Image)(resources.GetObject("btnProximo.IdleIconRightImage")));
            this.btnProximo.Location = new System.Drawing.Point(320, 588);
            this.btnProximo.Name = "btnProximo";
            stateProperties3.BorderColor = System.Drawing.Color.Silver;
            stateProperties3.BorderRadius = 10;
            stateProperties3.BorderThickness = 0;
            stateProperties3.FillColor = System.Drawing.Color.Silver;
            stateProperties3.IconLeftImage = null;
            stateProperties3.IconRightImage = ((System.Drawing.Image)(resources.GetObject("stateProperties3.IconRightImage")));
            this.btnProximo.onHoverState = stateProperties3;
            this.btnProximo.Size = new System.Drawing.Size(111, 41);
            this.btnProximo.TabIndex = 39;
            this.btnProximo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAnterior
            // 
            this.btnAnterior.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAnterior.BackColor = System.Drawing.Color.Transparent;
            this.btnAnterior.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAnterior.BackgroundImage")));
            this.btnAnterior.ButtonText = "Anterior";
            this.btnAnterior.ButtonTextMarginLeft = -22;
            this.btnAnterior.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnterior.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnAnterior.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnAnterior.DisabledForecolor = System.Drawing.Color.White;
            this.btnAnterior.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnterior.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAnterior.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnAnterior.IconPadding = 10;
            this.btnAnterior.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnAnterior.IdleBorderColor = System.Drawing.Color.Gainsboro;
            this.btnAnterior.IdleBorderRadius = 10;
            this.btnAnterior.IdleBorderThickness = 0;
            this.btnAnterior.IdleFillColor = System.Drawing.Color.Gainsboro;
            this.btnAnterior.IdleIconLeftImage = ((System.Drawing.Image)(resources.GetObject("btnAnterior.IdleIconLeftImage")));
            this.btnAnterior.IdleIconRightImage = null;
            this.btnAnterior.Location = new System.Drawing.Point(158, 588);
            this.btnAnterior.Name = "btnAnterior";
            stateProperties4.BorderColor = System.Drawing.Color.Silver;
            stateProperties4.BorderRadius = 10;
            stateProperties4.BorderThickness = 0;
            stateProperties4.FillColor = System.Drawing.Color.Silver;
            stateProperties4.IconLeftImage = ((System.Drawing.Image)(resources.GetObject("stateProperties4.IconLeftImage")));
            stateProperties4.IconRightImage = null;
            this.btnAnterior.onHoverState = stateProperties4;
            this.btnAnterior.Size = new System.Drawing.Size(111, 41);
            this.btnAnterior.TabIndex = 40;
            this.btnAnterior.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRegistro
            // 
            this.lblRegistro.AutoSize = true;
            this.lblRegistro.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblRegistro.Location = new System.Drawing.Point(275, 599);
            this.lblRegistro.Name = "lblRegistro";
            this.lblRegistro.Size = new System.Drawing.Size(39, 19);
            this.lblRegistro.TabIndex = 33;
            this.lblRegistro.Text = "1/34";
            // 
            // lblDados
            // 
            this.lblDados.AutoSize = true;
            this.lblDados.BackColor = System.Drawing.Color.White;
            this.lblDados.Font = new System.Drawing.Font("Segoe UI Semibold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDados.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblDados.Location = new System.Drawing.Point(34, 59);
            this.lblDados.Name = "lblDados";
            this.lblDados.Size = new System.Drawing.Size(191, 25);
            this.lblDados.TabIndex = 37;
            this.lblDados.Text = "Filtros para Pesquisas";
            // 
            // pnSeparatorDados
            // 
            this.pnSeparatorDados.BackColor = System.Drawing.Color.Gainsboro;
            this.pnSeparatorDados.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnSeparatorDados.Location = new System.Drawing.Point(-6, 94);
            this.pnSeparatorDados.Name = "pnSeparatorDados";
            this.pnSeparatorDados.Size = new System.Drawing.Size(590, 1);
            this.pnSeparatorDados.TabIndex = 38;
            // 
            // dgvEspera
            // 
            this.dgvEspera.AllowCustomTheming = true;
            this.dgvEspera.AllowUserToAddRows = false;
            this.dgvEspera.AllowUserToDeleteRows = false;
            this.dgvEspera.AllowUserToResizeColumns = false;
            this.dgvEspera.AllowUserToResizeRows = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(253)))), ((int)(((byte)(248)))));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            this.dgvEspera.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvEspera.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvEspera.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvEspera.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvEspera.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvEspera.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(157)))), ((int)(((byte)(88)))));
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEspera.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvEspera.ColumnHeadersHeight = 40;
            this.dgvEspera.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.dgvEspera.CurrentTheme.AlternatingRowsStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(253)))), ((int)(((byte)(248)))));
            this.dgvEspera.CurrentTheme.AlternatingRowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.dgvEspera.CurrentTheme.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Black;
            this.dgvEspera.CurrentTheme.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(245)))), ((int)(((byte)(214)))));
            this.dgvEspera.CurrentTheme.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Black;
            this.dgvEspera.CurrentTheme.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(157)))), ((int)(((byte)(88)))));
            this.dgvEspera.CurrentTheme.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(251)))), ((int)(((byte)(230)))));
            this.dgvEspera.CurrentTheme.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(157)))), ((int)(((byte)(88)))));
            this.dgvEspera.CurrentTheme.HeaderStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            this.dgvEspera.CurrentTheme.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgvEspera.CurrentTheme.Name = null;
            this.dgvEspera.CurrentTheme.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgvEspera.CurrentTheme.RowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.dgvEspera.CurrentTheme.RowsStyle.ForeColor = System.Drawing.Color.Black;
            this.dgvEspera.CurrentTheme.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(245)))), ((int)(((byte)(214)))));
            this.dgvEspera.CurrentTheme.RowsStyle.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(245)))), ((int)(((byte)(214)))));
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEspera.DefaultCellStyle = dataGridViewCellStyle7;
            this.dgvEspera.EnableHeadersVisualStyles = false;
            this.dgvEspera.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(251)))), ((int)(((byte)(230)))));
            this.dgvEspera.HeaderBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(157)))), ((int)(((byte)(88)))));
            this.dgvEspera.HeaderBgColor = System.Drawing.Color.Empty;
            this.dgvEspera.HeaderForeColor = System.Drawing.Color.White;
            this.dgvEspera.Location = new System.Drawing.Point(5, 244);
            this.dgvEspera.MultiSelect = false;
            this.dgvEspera.Name = "dgvEspera";
            this.dgvEspera.ReadOnly = true;
            this.dgvEspera.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(238)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEspera.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvEspera.RowHeadersVisible = false;
            this.dgvEspera.RowHeadersWidth = 35;
            this.dgvEspera.RowTemplate.DividerHeight = 1;
            this.dgvEspera.RowTemplate.Height = 40;
            this.dgvEspera.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEspera.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvEspera.Size = new System.Drawing.Size(616, 338);
            this.dgvEspera.TabIndex = 35;
            this.dgvEspera.Theme = Bunifu.UI.WinForms.BunifuDataGridView.PresetThemes.ForestGreen;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Column4";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.lblUrgencia);
            this.panel2.Controls.Add(this.lblTituloLivro);
            this.panel2.Controls.Add(this.lblUsuario);
            this.panel2.Controls.Add(this.lblData);
            this.panel2.Controls.Add(this.rdnUrgencia);
            this.panel2.Controls.Add(this.chkTitulo);
            this.panel2.Controls.Add(this.chkUsuario);
            this.panel2.Controls.Add(this.chkData);
            this.panel2.Location = new System.Drawing.Point(39, 102);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(127, 124);
            this.panel2.TabIndex = 36;
            // 
            // lblTituloLivro
            // 
            this.lblTituloLivro.AutoSize = true;
            this.lblTituloLivro.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTituloLivro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTituloLivro.Location = new System.Drawing.Point(38, 68);
            this.lblTituloLivro.Name = "lblTituloLivro";
            this.lblTituloLivro.Size = new System.Drawing.Size(81, 19);
            this.lblTituloLivro.TabIndex = 8;
            this.lblTituloLivro.Text = "Título Livro";
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblUsuario.Location = new System.Drawing.Point(38, 37);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(57, 19);
            this.lblUsuario.TabIndex = 8;
            this.lblUsuario.Text = "Usuário";
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            this.lblData.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblData.Location = new System.Drawing.Point(38, 7);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(38, 19);
            this.lblData.TabIndex = 8;
            this.lblData.Text = "Data";
            // 
            // chkTitulo
            // 
            this.chkTitulo.Checked = false;
            this.chkTitulo.Location = new System.Drawing.Point(12, 67);
            this.chkTitulo.Name = "chkTitulo";
            this.chkTitulo.OutlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkTitulo.RadioColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkTitulo.Size = new System.Drawing.Size(20, 20);
            this.chkTitulo.TabIndex = 6;
            this.chkTitulo.Tag = "2";
            this.chkTitulo.Text = null;
            this.chkTitulo.Click += new System.EventHandler(this.chkData_Click);
            // 
            // chkUsuario
            // 
            this.chkUsuario.Checked = false;
            this.chkUsuario.Location = new System.Drawing.Point(12, 37);
            this.chkUsuario.Name = "chkUsuario";
            this.chkUsuario.OutlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkUsuario.RadioColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkUsuario.Size = new System.Drawing.Size(20, 20);
            this.chkUsuario.TabIndex = 6;
            this.chkUsuario.Tag = "1";
            this.chkUsuario.Text = null;
            this.chkUsuario.Click += new System.EventHandler(this.chkData_Click);
            // 
            // chkData
            // 
            this.chkData.Checked = true;
            this.chkData.ForeColor = System.Drawing.Color.White;
            this.chkData.Location = new System.Drawing.Point(12, 7);
            this.chkData.Name = "chkData";
            this.chkData.OutlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkData.RadioColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkData.Size = new System.Drawing.Size(20, 20);
            this.chkData.TabIndex = 6;
            this.chkData.Tag = "0";
            this.chkData.Text = null;
            this.chkData.Click += new System.EventHandler(this.chkData_Click);
            // 
            // cdsHearder
            // 
            this.cdsHearder.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cdsHearder.BorderRadius = 5;
            this.cdsHearder.BottomSahddow = true;
            this.cdsHearder.color = System.Drawing.Color.White;
            this.cdsHearder.Controls.Add(this.lblTitulo);
            this.cdsHearder.LeftSahddow = false;
            this.cdsHearder.Location = new System.Drawing.Point(-8, -5);
            this.cdsHearder.Name = "cdsHearder";
            this.cdsHearder.RightSahddow = true;
            this.cdsHearder.ShadowDepth = 20;
            this.cdsHearder.Size = new System.Drawing.Size(686, 54);
            this.cdsHearder.TabIndex = 34;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Segoe UI Semibold", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTitulo.Location = new System.Drawing.Point(20, 15);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(147, 28);
            this.lblTitulo.TabIndex = 1;
            this.lblTitulo.Text = "Lista de Espera";
            // 
            // rdnUrgencia
            // 
            this.rdnUrgencia.Checked = false;
            this.rdnUrgencia.Location = new System.Drawing.Point(12, 97);
            this.rdnUrgencia.Name = "rdnUrgencia";
            this.rdnUrgencia.OutlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.rdnUrgencia.RadioColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.rdnUrgencia.Size = new System.Drawing.Size(20, 20);
            this.rdnUrgencia.TabIndex = 6;
            this.rdnUrgencia.Tag = "3";
            this.rdnUrgencia.Text = null;
            this.rdnUrgencia.Click += new System.EventHandler(this.chkData_Click);
            // 
            // lblUrgencia
            // 
            this.lblUrgencia.AutoSize = true;
            this.lblUrgencia.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUrgencia.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblUrgencia.Location = new System.Drawing.Point(38, 98);
            this.lblUrgencia.Name = "lblUrgencia";
            this.lblUrgencia.Size = new System.Drawing.Size(65, 19);
            this.lblUrgencia.TabIndex = 8;
            this.lblUrgencia.Text = "Urgência";
            // 
            // frmFiltroEspera1
            // 
            this.frmFiltroEspera1.BackColor = System.Drawing.Color.White;
            this.frmFiltroEspera1.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frmFiltroEspera1.Location = new System.Drawing.Point(177, 71);
            this.frmFiltroEspera1.Name = "frmFiltroEspera1";
            this.frmFiltroEspera1.Size = new System.Drawing.Size(401, 154);
            this.frmFiltroEspera1.TabIndex = 44;
            // 
            // frmListaEspera
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnProximo);
            this.Controls.Add(this.btnAnterior);
            this.Controls.Add(this.lblRegistro);
            this.Controls.Add(this.lblDados);
            this.Controls.Add(this.pnSeparatorDados);
            this.Controls.Add(this.dgvEspera);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.cdsHearder);
            this.Controls.Add(this.frmFiltroEspera1);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Name = "frmListaEspera";
            this.Size = new System.Drawing.Size(625, 632);
            ((System.ComponentModel.ISupportInitialize)(this.btnEditar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEspera)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.cdsHearder.ResumeLayout(false);
            this.cdsHearder.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuImageButton btnEditar;
        private Bunifu.Framework.UI.BunifuImageButton btnRemove;
        private Bunifu.Framework.UI.BunifuImageButton btnAdd;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnProximo;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnAnterior;
        private System.Windows.Forms.Label lblRegistro;
        private System.Windows.Forms.Label lblDados;
        private System.Windows.Forms.Panel pnSeparatorDados;
        private Bunifu.UI.WinForms.BunifuDataGridView dgvEspera;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblTituloLivro;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Label lblData;
        private Bunifu.UI.WinForms.BunifuRadioButton chkTitulo;
        private Bunifu.UI.WinForms.BunifuRadioButton chkUsuario;
        private Bunifu.UI.WinForms.BunifuRadioButton chkData;
        private Bunifu.Framework.UI.BunifuCards cdsHearder;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Label lblUrgencia;
        private Bunifu.UI.WinForms.BunifuRadioButton rdnUrgencia;
        private Filtro.frmFiltroEspera frmFiltroEspera1;
    }
}
