﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KimtToo.VisualReactive;

namespace Nsf.FriendsLibrary.APP.User_Controls.Listas
{
    public partial class frmListaEspera : UserControl
    {
        public frmListaEspera()
        {
            InitializeComponent();

            PopulateGrid();
        }

        private void PopulateGrid ()
        {
            dgvEspera.Rows.Clear();
            for (int i = 0; i < 5; i++)
            {
                dgvEspera.Rows.Add(
                    new object[]
                    {
                        " column1",
                        "column2",
                        "column3",
                        "column4"
                    });
            }
        }

        private void chkData_Click(object sender, EventArgs e)
        {
            VSReactive<int>.SetState("espera", int.Parse(((Control)sender).Tag.ToString()));
        }
    }
}
