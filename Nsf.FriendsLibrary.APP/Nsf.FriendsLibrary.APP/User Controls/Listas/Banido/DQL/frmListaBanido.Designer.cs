﻿namespace Nsf.FriendsLibrary.APP.User_Controls.Listas.Banido
{
    partial class frmListaBanido
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmListaBanido));
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties1 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties stateProperties2 = new Bunifu.UI.WinForms.BunifuButton.BunifuButton.StateProperties();
            this.lblRegistro = new System.Windows.Forms.Label();
            this.lblDados = new System.Windows.Forms.Label();
            this.pnSeparatorDados = new System.Windows.Forms.Panel();
            this.dgvBanidos = new Bunifu.UI.WinForms.BunifuDataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.Email = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.chkEmail = new Bunifu.UI.WinForms.BunifuRadioButton();
            this.chkUsuario = new Bunifu.UI.WinForms.BunifuRadioButton();
            this.cdsHearder = new Bunifu.Framework.UI.BunifuCards();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.btnEditar = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnRemove = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnAdd = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnProximo = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.btnAnterior = new Bunifu.UI.WinForms.BunifuButton.BunifuButton();
            this.lblData = new System.Windows.Forms.Label();
            this.chkCodigo = new Bunifu.UI.WinForms.BunifuRadioButton();
            this.frmFiltroBanidos1 = new Nsf.FriendsLibrary.APP.User_Controls.Filtro.frmFiltroBanidos();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBanidos)).BeginInit();
            this.panel2.SuspendLayout();
            this.cdsHearder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAdd)).BeginInit();
            this.SuspendLayout();
            // 
            // lblRegistro
            // 
            this.lblRegistro.AutoSize = true;
            this.lblRegistro.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRegistro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblRegistro.Location = new System.Drawing.Point(275, 599);
            this.lblRegistro.Name = "lblRegistro";
            this.lblRegistro.Size = new System.Drawing.Size(39, 19);
            this.lblRegistro.TabIndex = 33;
            this.lblRegistro.Text = "1/34";
            // 
            // lblDados
            // 
            this.lblDados.AutoSize = true;
            this.lblDados.BackColor = System.Drawing.Color.White;
            this.lblDados.Font = new System.Drawing.Font("Segoe UI Semibold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDados.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblDados.Location = new System.Drawing.Point(34, 59);
            this.lblDados.Name = "lblDados";
            this.lblDados.Size = new System.Drawing.Size(191, 25);
            this.lblDados.TabIndex = 37;
            this.lblDados.Text = "Filtros para Pesquisas";
            // 
            // pnSeparatorDados
            // 
            this.pnSeparatorDados.BackColor = System.Drawing.Color.Gainsboro;
            this.pnSeparatorDados.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnSeparatorDados.Location = new System.Drawing.Point(17, 90);
            this.pnSeparatorDados.Name = "pnSeparatorDados";
            this.pnSeparatorDados.Size = new System.Drawing.Size(590, 1);
            this.pnSeparatorDados.TabIndex = 38;
            // 
            // dgvBanidos
            // 
            this.dgvBanidos.AllowCustomTheming = true;
            this.dgvBanidos.AllowUserToAddRows = false;
            this.dgvBanidos.AllowUserToDeleteRows = false;
            this.dgvBanidos.AllowUserToResizeColumns = false;
            this.dgvBanidos.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(247)))), ((int)(((byte)(249)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.dgvBanidos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvBanidos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvBanidos.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvBanidos.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvBanidos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvBanidos.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Crimson;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(232)))), ((int)(((byte)(236)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBanidos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvBanidos.ColumnHeadersHeight = 40;
            this.dgvBanidos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.dgvBanidos.CurrentTheme.AlternatingRowsStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(253)))), ((int)(((byte)(247)))), ((int)(((byte)(249)))));
            this.dgvBanidos.CurrentTheme.AlternatingRowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.dgvBanidos.CurrentTheme.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Black;
            this.dgvBanidos.CurrentTheme.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(208)))), ((int)(((byte)(216)))));
            this.dgvBanidos.CurrentTheme.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.White;
            this.dgvBanidos.CurrentTheme.BackColor = System.Drawing.Color.Crimson;
            this.dgvBanidos.CurrentTheme.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(189)))), ((int)(((byte)(200)))));
            this.dgvBanidos.CurrentTheme.HeaderStyle.BackColor = System.Drawing.Color.Crimson;
            this.dgvBanidos.CurrentTheme.HeaderStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 11.75F, System.Drawing.FontStyle.Bold);
            this.dgvBanidos.CurrentTheme.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgvBanidos.CurrentTheme.Name = null;
            this.dgvBanidos.CurrentTheme.RowsStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(211)))), ((int)(((byte)(218)))));
            this.dgvBanidos.CurrentTheme.RowsStyle.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            this.dgvBanidos.CurrentTheme.RowsStyle.ForeColor = System.Drawing.Color.Black;
            this.dgvBanidos.CurrentTheme.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(208)))), ((int)(((byte)(216)))));
            this.dgvBanidos.CurrentTheme.RowsStyle.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(194)))), ((int)(((byte)(209)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBanidos.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvBanidos.EnableHeadersVisualStyles = false;
            this.dgvBanidos.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(245)))), ((int)(((byte)(189)))), ((int)(((byte)(200)))));
            this.dgvBanidos.HeaderBackColor = System.Drawing.Color.Crimson;
            this.dgvBanidos.HeaderBgColor = System.Drawing.Color.Empty;
            this.dgvBanidos.HeaderForeColor = System.Drawing.Color.White;
            this.dgvBanidos.Location = new System.Drawing.Point(5, 232);
            this.dgvBanidos.MultiSelect = false;
            this.dgvBanidos.Name = "dgvBanidos";
            this.dgvBanidos.ReadOnly = true;
            this.dgvBanidos.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(208)))), ((int)(((byte)(216)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBanidos.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvBanidos.RowHeadersVisible = false;
            this.dgvBanidos.RowHeadersWidth = 35;
            this.dgvBanidos.RowTemplate.DividerHeight = 1;
            this.dgvBanidos.RowTemplate.Height = 40;
            this.dgvBanidos.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvBanidos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvBanidos.Size = new System.Drawing.Size(616, 350);
            this.dgvBanidos.TabIndex = 35;
            this.dgvBanidos.Theme = Bunifu.UI.WinForms.BunifuDataGridView.PresetThemes.Crimson;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Column1";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Column2";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Column3";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Column4";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.lblData);
            this.panel2.Controls.Add(this.chkCodigo);
            this.panel2.Controls.Add(this.Email);
            this.panel2.Controls.Add(this.lblUsuario);
            this.panel2.Controls.Add(this.chkEmail);
            this.panel2.Controls.Add(this.chkUsuario);
            this.panel2.Location = new System.Drawing.Point(39, 102);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(127, 101);
            this.panel2.TabIndex = 36;
            // 
            // Email
            // 
            this.Email.AutoSize = true;
            this.Email.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Email.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Email.Location = new System.Drawing.Point(38, 72);
            this.Email.Name = "Email";
            this.Email.Size = new System.Drawing.Size(43, 19);
            this.Email.TabIndex = 8;
            this.Email.Text = "Email";
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsuario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblUsuario.Location = new System.Drawing.Point(38, 41);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(57, 19);
            this.lblUsuario.TabIndex = 8;
            this.lblUsuario.Text = "Usuário";
            // 
            // chkEmail
            // 
            this.chkEmail.Checked = false;
            this.chkEmail.Location = new System.Drawing.Point(12, 71);
            this.chkEmail.Name = "chkEmail";
            this.chkEmail.OutlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEmail.RadioColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkEmail.Size = new System.Drawing.Size(20, 20);
            this.chkEmail.TabIndex = 6;
            this.chkEmail.Tag = "2";
            this.chkEmail.Text = null;
            this.chkEmail.Click += new System.EventHandler(this.chkCodigo_Click);
            // 
            // chkUsuario
            // 
            this.chkUsuario.Checked = false;
            this.chkUsuario.Location = new System.Drawing.Point(12, 41);
            this.chkUsuario.Name = "chkUsuario";
            this.chkUsuario.OutlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkUsuario.RadioColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkUsuario.Size = new System.Drawing.Size(20, 20);
            this.chkUsuario.TabIndex = 6;
            this.chkUsuario.Tag = "1";
            this.chkUsuario.Text = null;
            this.chkUsuario.Click += new System.EventHandler(this.chkCodigo_Click);
            // 
            // cdsHearder
            // 
            this.cdsHearder.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cdsHearder.BorderRadius = 5;
            this.cdsHearder.BottomSahddow = true;
            this.cdsHearder.color = System.Drawing.Color.White;
            this.cdsHearder.Controls.Add(this.lblTitulo);
            this.cdsHearder.LeftSahddow = false;
            this.cdsHearder.Location = new System.Drawing.Point(-8, -5);
            this.cdsHearder.Name = "cdsHearder";
            this.cdsHearder.RightSahddow = true;
            this.cdsHearder.ShadowDepth = 20;
            this.cdsHearder.Size = new System.Drawing.Size(686, 54);
            this.cdsHearder.TabIndex = 34;
            // 
            // lblTitulo
            // 
            this.lblTitulo.AutoSize = true;
            this.lblTitulo.Font = new System.Drawing.Font("Segoe UI Semibold", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitulo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTitulo.Location = new System.Drawing.Point(20, 15);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(160, 28);
            this.lblTitulo.TabIndex = 1;
            this.lblTitulo.Text = "Lista de Banidos";
            // 
            // btnEditar
            // 
            this.btnEditar.BackColor = System.Drawing.Color.White;
            this.btnEditar.Image = ((System.Drawing.Image)(resources.GetObject("btnEditar.Image")));
            this.btnEditar.ImageActive = null;
            this.btnEditar.Location = new System.Drawing.Point(540, 58);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(26, 26);
            this.btnEditar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnEditar.TabIndex = 41;
            this.btnEditar.TabStop = false;
            this.btnEditar.Zoom = 10;
            // 
            // btnRemove
            // 
            this.btnRemove.BackColor = System.Drawing.Color.White;
            this.btnRemove.Image = ((System.Drawing.Image)(resources.GetObject("btnRemove.Image")));
            this.btnRemove.ImageActive = null;
            this.btnRemove.Location = new System.Drawing.Point(572, 58);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(26, 26);
            this.btnRemove.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnRemove.TabIndex = 42;
            this.btnRemove.TabStop = false;
            this.btnRemove.Zoom = 10;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.White;
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageActive = null;
            this.btnAdd.Location = new System.Drawing.Point(508, 58);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(26, 26);
            this.btnAdd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnAdd.TabIndex = 43;
            this.btnAdd.TabStop = false;
            this.btnAdd.Zoom = 10;
            // 
            // btnProximo
            // 
            this.btnProximo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnProximo.BackColor = System.Drawing.Color.Transparent;
            this.btnProximo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnProximo.BackgroundImage")));
            this.btnProximo.ButtonText = "Próxima";
            this.btnProximo.ButtonTextMarginLeft = 10;
            this.btnProximo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProximo.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnProximo.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnProximo.DisabledForecolor = System.Drawing.Color.White;
            this.btnProximo.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProximo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnProximo.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnProximo.IconPadding = 10;
            this.btnProximo.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnProximo.IdleBorderColor = System.Drawing.Color.Gainsboro;
            this.btnProximo.IdleBorderRadius = 10;
            this.btnProximo.IdleBorderThickness = 0;
            this.btnProximo.IdleFillColor = System.Drawing.Color.Gainsboro;
            this.btnProximo.IdleIconLeftImage = null;
            this.btnProximo.IdleIconRightImage = ((System.Drawing.Image)(resources.GetObject("btnProximo.IdleIconRightImage")));
            this.btnProximo.Location = new System.Drawing.Point(320, 588);
            this.btnProximo.Name = "btnProximo";
            stateProperties1.BorderColor = System.Drawing.Color.Silver;
            stateProperties1.BorderRadius = 10;
            stateProperties1.BorderThickness = 0;
            stateProperties1.FillColor = System.Drawing.Color.Silver;
            stateProperties1.IconLeftImage = null;
            stateProperties1.IconRightImage = ((System.Drawing.Image)(resources.GetObject("stateProperties1.IconRightImage")));
            this.btnProximo.onHoverState = stateProperties1;
            this.btnProximo.Size = new System.Drawing.Size(111, 41);
            this.btnProximo.TabIndex = 39;
            this.btnProximo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnAnterior
            // 
            this.btnAnterior.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAnterior.BackColor = System.Drawing.Color.Transparent;
            this.btnAnterior.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAnterior.BackgroundImage")));
            this.btnAnterior.ButtonText = "Anterior";
            this.btnAnterior.ButtonTextMarginLeft = -22;
            this.btnAnterior.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnterior.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(161)))), ((int)(((byte)(161)))), ((int)(((byte)(161)))));
            this.btnAnterior.DisabledFillColor = System.Drawing.Color.Gray;
            this.btnAnterior.DisabledForecolor = System.Drawing.Color.White;
            this.btnAnterior.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnterior.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnAnterior.IconLeftCursor = System.Windows.Forms.Cursors.Default;
            this.btnAnterior.IconPadding = 10;
            this.btnAnterior.IconRightCursor = System.Windows.Forms.Cursors.Default;
            this.btnAnterior.IdleBorderColor = System.Drawing.Color.Gainsboro;
            this.btnAnterior.IdleBorderRadius = 10;
            this.btnAnterior.IdleBorderThickness = 0;
            this.btnAnterior.IdleFillColor = System.Drawing.Color.Gainsboro;
            this.btnAnterior.IdleIconLeftImage = ((System.Drawing.Image)(resources.GetObject("btnAnterior.IdleIconLeftImage")));
            this.btnAnterior.IdleIconRightImage = null;
            this.btnAnterior.Location = new System.Drawing.Point(158, 588);
            this.btnAnterior.Name = "btnAnterior";
            stateProperties2.BorderColor = System.Drawing.Color.Silver;
            stateProperties2.BorderRadius = 10;
            stateProperties2.BorderThickness = 0;
            stateProperties2.FillColor = System.Drawing.Color.Silver;
            stateProperties2.IconLeftImage = ((System.Drawing.Image)(resources.GetObject("stateProperties2.IconLeftImage")));
            stateProperties2.IconRightImage = null;
            this.btnAnterior.onHoverState = stateProperties2;
            this.btnAnterior.Size = new System.Drawing.Size(111, 41);
            this.btnAnterior.TabIndex = 40;
            this.btnAnterior.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblData
            // 
            this.lblData.AutoSize = true;
            this.lblData.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblData.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblData.Location = new System.Drawing.Point(38, 11);
            this.lblData.Name = "lblData";
            this.lblData.Size = new System.Drawing.Size(54, 19);
            this.lblData.TabIndex = 10;
            this.lblData.Text = "Código";
            // 
            // chkCodigo
            // 
            this.chkCodigo.Checked = true;
            this.chkCodigo.ForeColor = System.Drawing.Color.White;
            this.chkCodigo.Location = new System.Drawing.Point(12, 11);
            this.chkCodigo.Name = "chkCodigo";
            this.chkCodigo.OutlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkCodigo.RadioColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.chkCodigo.Size = new System.Drawing.Size(20, 20);
            this.chkCodigo.TabIndex = 9;
            this.chkCodigo.Tag = "0";
            this.chkCodigo.Text = null;
            this.chkCodigo.Click += new System.EventHandler(this.chkCodigo_Click);
            // 
            // frmFiltroBanidos1
            // 
            this.frmFiltroBanidos1.BackColor = System.Drawing.Color.White;
            this.frmFiltroBanidos1.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frmFiltroBanidos1.Location = new System.Drawing.Point(189, 92);
            this.frmFiltroBanidos1.Name = "frmFiltroBanidos1";
            this.frmFiltroBanidos1.Size = new System.Drawing.Size(401, 97);
            this.frmFiltroBanidos1.TabIndex = 44;
            // 
            // frmListaBanido
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.btnEditar);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.btnProximo);
            this.Controls.Add(this.btnAnterior);
            this.Controls.Add(this.lblRegistro);
            this.Controls.Add(this.lblDados);
            this.Controls.Add(this.pnSeparatorDados);
            this.Controls.Add(this.dgvBanidos);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.cdsHearder);
            this.Controls.Add(this.frmFiltroBanidos1);
            this.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmListaBanido";
            this.Size = new System.Drawing.Size(625, 632);
            this.Load += new System.EventHandler(this.frmListaBanido_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBanidos)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.cdsHearder.ResumeLayout(false);
            this.cdsHearder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnEditar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRemove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnAdd)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuImageButton btnEditar;
        private Bunifu.Framework.UI.BunifuImageButton btnRemove;
        private Bunifu.Framework.UI.BunifuImageButton btnAdd;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnProximo;
        private Bunifu.UI.WinForms.BunifuButton.BunifuButton btnAnterior;
        private System.Windows.Forms.Label lblRegistro;
        private System.Windows.Forms.Label lblDados;
        private System.Windows.Forms.Panel pnSeparatorDados;
        private Bunifu.UI.WinForms.BunifuDataGridView dgvBanidos;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label Email;
        private System.Windows.Forms.Label lblUsuario;
        private Bunifu.UI.WinForms.BunifuRadioButton chkEmail;
        private Bunifu.UI.WinForms.BunifuRadioButton chkUsuario;
        private Bunifu.Framework.UI.BunifuCards cdsHearder;
        private System.Windows.Forms.Label lblTitulo;
        private System.Windows.Forms.Label lblData;
        private Bunifu.UI.WinForms.BunifuRadioButton chkCodigo;
        private Filtro.frmFiltroBanidos frmFiltroBanidos1;
    }
}
