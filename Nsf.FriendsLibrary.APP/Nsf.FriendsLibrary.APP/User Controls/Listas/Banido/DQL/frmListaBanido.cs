﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KimtToo.VisualReactive;

namespace Nsf.FriendsLibrary.APP.User_Controls.Listas.Banido
{
    public partial class frmListaBanido : UserControl
    {
        //BackColor: Color [Crimson]; GridColor: Color [A=255, R=252, G=233, B=236];
        //252; 233; 236
        public frmListaBanido()
        {
            InitializeComponent();
            ConfGrid();
            PopulateGrid();
        }
        private void ConfGrid ()
        {
            //Retorno Booleano
            dgvBanidos.AllowCustomTheming = true;
            dgvBanidos.AutoGenerateColumns = false;
        }
        private void PopulateGrid ()
        {
            //Limpa a gried
            dgvBanidos.Rows.Clear();

            //Inserir cinco valores na gried
            for (int i = 0; i < 5; i++)
            {
                //Adicionar linhas na grid
                dgvBanidos.Rows.Add(
                    new object[]
                    {
                        //valores da gried
                        " column1",
                        "column2",
                        "column3",
                        "column4"
                    });
            }
        }
        private void chkCodigo_Click(object sender, EventArgs e)
        {
            VSReactive<int>.SetState("banido", int.Parse(((Control)sender).Tag.ToString()));
        }

        private void frmListaBanido_Load(object sender, EventArgs e)
        {
            
        }
    }
}
