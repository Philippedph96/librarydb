﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KimtToo.VisualReactive;
using System.Data.SqlClient;

namespace Nsf.FriendsLibrary.APP.User_Controls.Listas.Atraso
{
    public partial class frmListaAtraso : UserControl
    {
        public frmListaAtraso()
        {
            InitializeComponent();

            PopulateGrid();
        }

        private void chkData_Click_1(object sender, EventArgs e)
        {
            VSReactive<int>.SetState("atraso", int.Parse(((Control)sender).Tag.ToString()));
        }

        //Resolver Depois (Está na aba favoritos)
        //SqlDataAdapter paginaAdapter;
        //DataSet paginaDS;
            int deslocamento, tamanhoPagina = 10, totalRegistros;
        //
        //private void CarregarGridView ()
        //{
        //
        //}

        private void PopulateGrid ()
        {
            dgvAtrasados.Rows.Clear();

            for (int i = 0; i < 5; i++)
            {
                dgvAtrasados.Rows.Add(
                    new object []
                    {
                        " column1",
                        "column2",
                        "column3",
                        "column4"
                    });
            }
        }
          
        private void btnAnterior_Click(object sender, EventArgs e)
        {
            deslocamento = deslocamento - tamanhoPagina;
            if (deslocamento > totalRegistros)
            {
                deslocamento = deslocamento - totalRegistros;
            }
        }

        private void btnProximo_Click(object sender, EventArgs e)
        {
            deslocamento = deslocamento + tamanhoPagina;
            if (deslocamento > totalRegistros)
            {
                deslocamento = 0;
            }
        }
    }
}
