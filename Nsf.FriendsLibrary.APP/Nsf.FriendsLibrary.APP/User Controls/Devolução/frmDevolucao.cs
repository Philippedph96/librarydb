﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Nsf.FriendsLibrary.Business.Devolucao;
using Nsf.FriendsLibrary.DB;
using Nsf.FriendsLibrary.Business.Emprestimo;
using KimtToo.VisualReactive;
using Biblioteca;
using Nsf.FriendsLibrary.APP.User_Controls.Emprestimos;
using PagedList;
using Biblioteca.Utilitarios.Email;
using Biblioteca.Utilitarios.Notificacoes;

namespace Nsf.FriendsLibrary.APP.User_Controls.Devolução
{
    public partial class frmDevolucao : UserControl
    {
        public static frmDevolucao GridView;

        int pageNumber = 1;
        IPagedList<vw_devolucao_emprestimo> list;

        public frmDevolucao()
        {
            GridView = this;

            InitializeComponent();
            this.PopulateGrid();
        }

        private void ConfigGrid(DataGridView dgv, List<vw_devolucao_emprestimo> valores)
        {
            dgv.AutoGenerateColumns = false;
            dgv.DataSource = valores;
        }

        private async Task<IPagedList<vw_devolucao_emprestimo>> GetPagedListAsync(int pageNumber = 1, int pageSize = 8)
        {
            return await Task.Factory.StartNew(() =>
            {
                DevolucaoBusiness db = new DevolucaoBusiness();
                var valor = db.ListReturns();
                return valor.ToPagedList(pageNumber, pageSize);
            });
        }

        private vw_devolucao_emprestimo RemoveItens()
        {
            var removeItens = this.CatchItens();

            EmprestimoBusiness db = new EmprestimoBusiness();
            db.Remove(removeItens.id_emprestimo);

            return removeItens;
        }

        private void UpdateItens()
        {
            var returnItens = this.CatchItens();

            tb_livro livro = new tb_livro();
            livro.id_livro = returnItens.id_livro;
            livro.nm_livro = returnItens.nm_livro;

            tb_leitor leitor = new tb_leitor();
            leitor.id_leitor = returnItens.id_leitor;
            leitor.nm_leitor = returnItens.nm_leitor;
            leitor.ds_cpf = returnItens.ds_cpf;

            tb_emprestimo emprestimo = new tb_emprestimo();
            emprestimo.id_emprestimo = returnItens.id_emprestimo;
            emprestimo.dt_devolucao = returnItens.dt_devolucao;
            emprestimo.dt_emprestimo = returnItens.dt_emprestimo;

            tb_emprestimo_item item = new tb_emprestimo_item();
            item.id_emprestimo_item = returnItens.id_emprestimo_item;

            frmAddUpdateLoan loan = new frmAddUpdateLoan();
            loan.LoadScreen(livro, leitor, emprestimo, item);

            frmMain.UserControl.OpenUserControl(loan);
        }

        private vw_devolucao_emprestimo CatchItens()
        {
            var retorno = dgvDevolucao.CurrentRow.DataBoundItem as vw_devolucao_emprestimo;
            return retorno;
        }

        private void PopulateGrid()
        {
            DevolucaoBusiness db = new DevolucaoBusiness();
            var lists = db.ListReturns();

            dgvDevolucao.AutoGenerateColumns = false;
            dgvDevolucao.DataSource = lists;
        }

        public void PopulateGridWithTitleFilter(string book)
        {
            DevolucaoBusiness db = new DevolucaoBusiness();
            var titles = db.FilterBookReturns(book);

            this.ConfigGrid(dgvDevolucao, titles);
        }

        public void PopulateGridWithReaderFilter(string reader)
        {
            DevolucaoBusiness db = new DevolucaoBusiness();
            var readers = db.FilterReaderReturns(reader);

            this.ConfigGrid(dgvDevolucao, readers);
        }

        public void PopulateGridWithDateFilter(DateTime date)
        {
            DevolucaoBusiness db = new DevolucaoBusiness();
            var dates = db.FilterDateReturns(date);

            this.ConfigGrid(dgvDevolucao, dates);
        }

        private async void btnRemove_Click(object sender, EventArgs e)
        {
            try
            {
                var valor = this.RemoveItens();

                //Saída de dados
                Image imagem = Properties.Resources.Delete;
                string texto = "O Empréstimo foi removidas com êxito!";
                await Task.Run(() => this.SelectorClass(3, texto, imagem));

                await GetPagedListAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + "\n" + "Tente mais tarde.",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private void chkTitulo_Click(object sender, EventArgs e)
        {
            VSReactive<int>.SetState("devolucao", int.Parse(((Control)sender).Tag.ToString()));
        }

        private async void btnReajustar_Click(object sender, EventArgs e)
        {
            try
            {
                this.UpdateItens();
                await GetPagedListAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message + "\n" + "Tente mais tarde.",
                                "Friends Library",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private async void frmDevolucao_Load(object sender, EventArgs e)
        {
            list = await GetPagedListAsync();
            btnProximo.Enabled = list.HasNextPage;
            btnAnterior.Enabled = list.HasPreviousPage;

            dgvDevolucao.DataSource = list.ToList();
            lblNumberPages.Text = string.Format("{0}/{1}", pageNumber, list.PageCount);
        }

        private async void btnAnterior_Click(object sender, EventArgs e)
        {
            if (list.HasPreviousPage)
            {
                list = await GetPagedListAsync(--pageNumber);
                btnProximo.Enabled = list.HasNextPage;
                btnAnterior.Enabled = list.HasPreviousPage;

                dgvDevolucao.DataSource = list.ToList();
                lblNumberPages.Text = string.Format("{0}/{1}", pageNumber, list.PageCount);
            }
        }

        private async void btnProximo_Click(object sender, EventArgs e)
        {
            if (list.HasNextPage)
            {
                list = await GetPagedListAsync(++pageNumber);
                btnProximo.Enabled = list.HasNextPage;
                btnAnterior.Enabled = list.HasPreviousPage;

                dgvDevolucao.DataSource = list.ToList();
                lblNumberPages.Text = string.Format("{0}/{1}", pageNumber, list.PageCount);
            }
        }
    }
}
