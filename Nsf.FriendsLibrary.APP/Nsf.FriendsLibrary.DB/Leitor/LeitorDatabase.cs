﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Objects;
using Nsf.FriendsLibrary.DB;

namespace Biblioteca.Banco.Database.Usuario
{
    public class LeitorDatabase
    {

        public void Save(tb_leitor dto)
        {
            LibraryEntities context = new LibraryEntities();
            context.tb_leitor.Add(dto);
            context.SaveChanges();
        }

        public void Update(tb_leitor dto)
        {
            LibraryEntities context = new LibraryEntities();
            context.Entry(dto).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void Remove(int id)
        {
            LibraryEntities context = new LibraryEntities();
            tb_leitor leitor = context.tb_leitor.First(x => x.id_leitor == id);
            context.tb_leitor.Remove(leitor);
            context.SaveChanges();
        }

        public List<tb_leitor> List()
        {
            LibraryEntities context = new LibraryEntities();
            return context.tb_leitor.AsNoTracking().ToList();
        }

        public List<tb_leitor> FilterReader(string dto)
        {
            LibraryEntities context = new LibraryEntities();
            return context.tb_leitor.Where(x => x.nm_leitor.Contains(dto)).ToList();
        }

        public List<tb_leitor> FilterCPF(string dto)
        {
            LibraryEntities context = new LibraryEntities();
            return context.tb_leitor.Where(x => x.ds_cpf.Contains(dto)).ToList();
        }

        public tb_leitor GetNamebyCPF(string cpf)
        {
            tb_leitor leitor = null;

            LibraryEntities context = new LibraryEntities();
            var retorno = context.tb_leitor.First(x => x.ds_cpf == cpf);

            if(retorno != null)
            {
                leitor = new tb_leitor();
                leitor = retorno;
            }
            return leitor;
        }

        public string GetCPFbyName(string name)
        {
            string leitor = string.Empty;

            LibraryEntities context = new LibraryEntities();
            var retorno = context.tb_leitor.FirstOrDefault(x => x.nm_leitor == name);

            if (retorno != null)
            {
                leitor = retorno.ds_cpf;
            }

            return leitor;
        }

        public bool VerificarCPF(string cpf)
        {
            LibraryEntities context = new LibraryEntities();
            var retorno = context.tb_leitor.FirstOrDefault(c => c.ds_cpf == cpf);

            if (retorno != null)
            {
                return true; //Tem usuário no banco. Nesse caso, mostrarei a partir de um ícone para o user
            }
            else
            {
                return false; // Não tem nenhum usuário com o mesmo login
            }
        }

        public bool VerificarEmail(string email)
        {
            LibraryEntities context = new LibraryEntities();
            var retorno = context.tb_leitor.FirstOrDefault(c => c.ds_email == email);

            if (retorno != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool VerificarID(int id)
        {
            LibraryEntities context = new LibraryEntities();
            var retorno = context.tb_leitor.FirstOrDefault(c => c.id_leitor == id);

            if (retorno != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<string> ListReaderCPF()
        {
            using (var context = new LibraryEntities())
            {
                var result = context.tb_leitor.Select(n => n.ds_cpf).ToList();
                return result;
            }
        }

        public List<tb_leitor> Filter (int id)
        {
            LibraryEntities contex = new LibraryEntities();
            return  contex.tb_leitor.Where(c => c.id_leitor == id).ToList();
        }
    }
}
