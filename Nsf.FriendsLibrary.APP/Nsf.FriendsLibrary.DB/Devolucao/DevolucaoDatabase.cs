﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.FriendsLibrary.DB.Devolucao
{
    public class DevolucaoDatabase
    {
        public List<vw_devolucao_emprestimo> ListReturns ()
        {
            LibraryEntities context = new LibraryEntities();
            return context.vw_devolucao_emprestimo.ToList();
        }

        public List<vw_devolucao_emprestimo> FilterReaderReturns(string reader)
        {
            LibraryEntities context = new LibraryEntities();
            return context.vw_devolucao_emprestimo.Where(c => c.nm_leitor.Contains(reader)).ToList();
        }

        public List<vw_devolucao_emprestimo> FilterBookReturns(string book)
        {
            LibraryEntities context = new LibraryEntities();
            return context.vw_devolucao_emprestimo.Where(c => c.nm_livro.Contains(book)).ToList();
        }

        public List<vw_devolucao_emprestimo> FilterDateReturns(DateTime date)
        {
            LibraryEntities context = new LibraryEntities();
            return context.vw_devolucao_emprestimo.Where(c => c.dt_devolucao == date).ToList();
        }
    }
}
