﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.FriendsLibrary.DB.Livro
{
    public class LivroDatabase
    {
        public void Save(tb_livro dto)
        {
            using (var context = new LibraryEntities())
            {
                context.tb_livro.Add(dto);
                context.SaveChanges();
            }
        }

        public void Update(tb_livro dto)
        {
            LibraryEntities context = new LibraryEntities();
            context.Entry(dto).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void UpdateView(vw_livro dto)
        {
            LibraryEntities context = new LibraryEntities();
            context.Entry(dto).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void Remove(int id)
        {
            using (var context = new LibraryEntities())
            {
                tb_livro livro = context.tb_livro.First(x => x.id_livro == id);
                context.tb_livro.Remove(livro);
                context.SaveChanges();
            }
        }

        public List<tb_livro> List()
        {
            LibraryEntities context = new LibraryEntities();
            return context.tb_livro.ToList();
        }

        public List<tb_livro> Filter(string dto)
        {
            LibraryEntities context = new LibraryEntities();
            return context.tb_livro.Where(x => x.nm_livro.Contains(dto)).ToList();
        }

        public List<vw_livro> ListView()
        {
            LibraryEntities context = new LibraryEntities();
            return context.vw_livro.ToList();
        }

        public List<vw_livro> FilterTitleView(string title)
        {
            LibraryEntities context = new LibraryEntities();
            return context.vw_livro.Where(x => x.nm_livro.Contains(title)).ToList();
        }

        public List<vw_livro> FilterAuthorView(string author)
        {
            LibraryEntities context = new LibraryEntities();
            return context.vw_livro.Where(x => x.nm_autor.Contains(author)).ToList();
        }


        public List<vw_livro> FilterCategoryView(string category)
        {
            LibraryEntities context = new LibraryEntities();
            return context.vw_livro.Where(x => x.nm_categoria.Contains(category)).ToList();
        }

        public string FilterAuthorID(int id)
        {
            LibraryEntities context = new LibraryEntities();
            var retorno = context.vw_livro.FirstOrDefault(x => x.id_autor == id);
            string nomeAutor = string.Empty;

            if (retorno != null)
            {
                nomeAutor = retorno.nm_autor;
            }
            return nomeAutor;
        }

        public string FilterCategoryID(int id)
        {
            LibraryEntities context = new LibraryEntities();
            var retorno = context.vw_livro.FirstOrDefault(x => x.id_categoria == id);
            string nomeCategoria = string.Empty;

            if (retorno != null)
            {
                nomeCategoria = retorno.nm_categoria;
            }
            return nomeCategoria;
        }

        public bool VerifyID(int codigoLivro)
        {
            LibraryEntities context = new LibraryEntities();
            var retorno = context.tb_livro.First(x => x.id_livro == codigoLivro);

            if (retorno != null)
                return true;
            else
                return false;
        }
    }
}
