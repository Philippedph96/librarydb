﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.FriendsLibrary.DB.Emprestimo
{
    public class EmprestimoDatabase
    {
        public int Save(tb_emprestimo dto)
        {
            LibraryEntities context = new LibraryEntities();
            context.tb_emprestimo.Add(dto);
            return context.SaveChanges();
        }

        public int Update(tb_emprestimo dto)
        {
            LibraryEntities context = new LibraryEntities();
            context.Entry(dto).State = System.Data.Entity.EntityState.Modified;
            return context.SaveChanges();
        }
        public void Remove(int id)
        {
            using (LibraryEntities context = new LibraryEntities())
            {
                tb_emprestimo loan = context.tb_emprestimo.First(x => x.id_emprestimo == id);
                context.tb_emprestimo.Remove(loan);
                context.SaveChanges();
            }
        }

        public List<tb_emprestimo> List()
        {
            LibraryEntities context = new LibraryEntities();
            return context.tb_emprestimo.ToList();
        }
        public List<tb_emprestimo> Filter(string dto)
        {
            LibraryEntities context = new LibraryEntities();
            return context.tb_emprestimo.Where(x => x.dt_emprestimo == x.dt_devolucao).ToList();
        }

        public List<vw_consultar_emprestimo> ListLoan()
        {
            LibraryEntities context = new LibraryEntities();
            return context.vw_consultar_emprestimo.ToList();
        }

        public List<vw_consultar_emprestimo> FilterBook(string book)
        {
            LibraryEntities context = new LibraryEntities();
            return context.vw_consultar_emprestimo.Where(c => c.nm_livro.Contains(book)).ToList();
        }

        public List<vw_consultar_emprestimo> FilterReader(string reader)
        {
            LibraryEntities context = new LibraryEntities();
            return context.vw_consultar_emprestimo.Where(c => c.nm_leitor.Contains(reader)).ToList();
        }

        public List<vw_consultar_emprestimo> FilterLoan(DateTime start, DateTime end)
        {
            LibraryEntities context = new LibraryEntities();
            return context.vw_consultar_emprestimo.Where(c => c.dt_emprestimo >= start && c.dt_devolucao <= end).ToList();
        }

        public bool BuscarNumeroID(int id)
        {
            LibraryEntities context = new LibraryEntities();
            var retorno = context.tb_emprestimo.Where(c => c.id_leitor == id).ToList();

            if (retorno.Count > 2)
                return true;
            else
                return false;
        }

        public string BringCPF(int id)
        {
            string retorno = string.Empty;

            LibraryEntities context = new LibraryEntities();
            var valor = context.tb_leitor.First(c => c.id_leitor == id);

            if (valor != null)
                retorno = valor.ds_cpf;

            return retorno;
        }

        public int GetCombobox(string cpf)
        {
            int leitor = 0;

            LibraryEntities context = new LibraryEntities();
            var valor = context.tb_leitor.First(c => c.ds_cpf == cpf);

            if (valor != null)
                leitor = valor.id_leitor;

            return leitor;
        }
    }
}
