﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.FriendsLibrary.DB.Complemento_dos_Livros.Editora
{
    public class EditoraDatabase
    {
        public void Save (tb_editora dto)
        {
            LibraryEntities context = new LibraryEntities();
            context.tb_editora.Add(dto);
            context.SaveChanges();
        }

        public void Update (tb_editora dto)
        {
            LibraryEntities context = new LibraryEntities();
            context.Entry(dto).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void Remove (int id)
        {
            LibraryEntities context = new LibraryEntities();
            tb_editora editora = context.tb_editora.First(x => x.id_editora == id);
            context.tb_editora.Remove(editora);
            context.SaveChanges();
        }

        public List<tb_editora> List()
        {
            LibraryEntities context = new LibraryEntities();
            return context.tb_editora.AsNoTracking().ToList();
        }

        public List<tb_editora> Filter (string _nomeEditora)
        {
            LibraryEntities context = new LibraryEntities();
            return context.tb_editora.Where(x => x.nm_editora.Contains(_nomeEditora)).ToList();
        }

        public bool VerificarID(int id)
        {
            var context = new LibraryEntities();
            var retorno = context.tb_editora.First(c => c.id_editora == id);

            if (retorno != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
