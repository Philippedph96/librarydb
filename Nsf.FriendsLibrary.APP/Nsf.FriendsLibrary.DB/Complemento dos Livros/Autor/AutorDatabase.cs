﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.FriendsLibrary.DB.Autor
{
    public class AutorDatabase
    {
        public void Save(tb_autor dto)
        {
            using (var context = new LibraryEntities())
            {
                context.tb_autor.Add(dto);
                context.SaveChanges();
            }
        }

        public void Update(tb_autor dto)
        {
            LibraryEntities context = new LibraryEntities();
            context.Entry(dto).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void Remove(int id)
        {
            using (var context = new LibraryEntities())
            {
                tb_autor autor = context.tb_autor.First(x => x.id_autor == id);
                context.tb_autor.Remove(autor);
                context.SaveChanges();
            }
        }

        public List<tb_autor> List()
        {
            using (var context = new LibraryEntities())
                return context.tb_autor.AsNoTracking().ToList();
        }
        public List<tb_autor> Filter(string dto)
        {
            using (var context = new LibraryEntities())
                return context.tb_autor.Where(x => x.nm_autor.Contains(dto)).ToList();
        }

        public List<string> ListStyle()
        {
            using (var context = new LibraryEntities())
            {
                var result = context.tb_autor.Select(n => n.ds_escrita).ToList();
                return result;
            }
        }

        public bool VerificarID(int id)
        {
            var context = new LibraryEntities();
            var retorno = context.tb_autor.First(c => c.id_autor == id);

            if (retorno != null)
            {
                return true;
            }
            else
            {
                return false;   
            }
        }
    }
}
