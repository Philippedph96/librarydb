﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.FriendsLibrary.DB.Categoria
{
    public class CategoriaDatabase
    {
        public void Save(tb_categoria dto)
        {
            LibraryEntities context = new LibraryEntities();
            context.tb_categoria.Add(dto);
            context.SaveChanges();
        }

        public void Update(tb_categoria dto)
        {
            LibraryEntities context = new LibraryEntities();
            context.Entry(dto).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void Remove(int id)
        {
            LibraryEntities context = new LibraryEntities();
            tb_categoria categoria = context.tb_categoria.First(x => x.id_categoria == id);
            context.tb_categoria.Remove(categoria);
            context.SaveChanges();
        }

        public List<tb_categoria> List()
        {
            LibraryEntities context = new LibraryEntities();
            return context.tb_categoria.AsNoTracking().ToList();
        }

        public List<tb_categoria> Filter(string dto)
        {
            LibraryEntities context = new LibraryEntities();
            return context.tb_categoria.Where(x => x.nm_categoria.Contains(dto)).ToList();
        }

        public bool VerificarID(int dto)
        {
            LibraryEntities context = new LibraryEntities();
            var idVerify = context.tb_categoria.First(x => x.id_categoria == dto);

            if (idVerify != null)
                return true;
            else
                return false;
        }
        public bool VerificarValorCategoria(string categoria)
        {
            var context = new LibraryEntities();
            var retorno = context.tb_categoria.First(c => c.nm_categoria == categoria);

            if (retorno != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
