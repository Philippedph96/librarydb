﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.FriendsLibrary.DB.Bibliotecario
{
   public class BibliotecarioDatabase
    {
        public int Save(tb_bibliotecario dto)
        {
            LibraryEntities context = new LibraryEntities();
            context.tb_bibliotecario.Add(dto);
            return context.SaveChanges();
        }

        public List<tb_bibliotecario> List()
        {
            LibraryEntities context = new LibraryEntities();
            return context.tb_bibliotecario.ToList();
        }

        public void Update(tb_bibliotecario dto)
        {
            LibraryEntities context = new LibraryEntities();
            context.Entry(dto).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        public void Remove(int id)
        {
            LibraryEntities context = new LibraryEntities();
            tb_bibliotecario bibliotecario = context.tb_bibliotecario.First(x => x.id_bibliotecario == id);
            context.tb_bibliotecario.Remove(bibliotecario);
            context.SaveChanges();
        }

        public List<tb_bibliotecario> FilterLibrarian(string dto)
        {
            LibraryEntities context = new LibraryEntities();
            return context.tb_bibliotecario.Where(x => x.nm_bibliotecario.Contains(dto)).ToList();
        }

        public List<tb_bibliotecario> FilterCPF(string dto)
        {
            LibraryEntities context = new LibraryEntities();
            return context.tb_bibliotecario.Where(x => x.ds_cpf.Contains(dto)).ToList();
        }

        public tb_bibliotecario Login (string password)
        {
            LibraryEntities context = new LibraryEntities();
            var log = context.tb_bibliotecario.First(c => c.ds_senha == password);

            return log;
        }

        public bool VerificarLogin (string login)
        {
            LibraryEntities context = new LibraryEntities();
            var retorno = context.tb_bibliotecario.FirstOrDefault(c => c.ds_login == login);

            if (retorno != null)
            {
                return false; //Tem usuário no banco. Nesse caso, mostrarei a partir de um ícone para o user
            }
            else
            {
                return true; // Não tem nenhum usuário com o mesmo login
            }
        }

        public bool VerificarCPF(string cpf)
        {
            LibraryEntities context = new LibraryEntities();
            var retorno = context.tb_bibliotecario.FirstOrDefault(c => c.ds_cpf == cpf);

            if (retorno != null)
            {
                return true; //Tem usuário no banco. Nesse caso, mostrarei a partir de um ícone para o user
            }
            else
            {
                return false; // Não tem nenhum usuário com o mesmo login
            }
        }

        public bool VerificarEmail(string email)
        {
            LibraryEntities context = new LibraryEntities();
            var retorno = context.tb_bibliotecario.FirstOrDefault(c => c.ds_email == email);

            if (retorno != null)
            {
                return true; 
            }
            else
            {
                return false; 
            }
        }

        public bool VerificarID (int id)
        {
            LibraryEntities context = new LibraryEntities();
            var retorno = context.tb_bibliotecario.FirstOrDefault(c => c.id_bibliotecario == id);

            if (retorno != null)
            {
                return true; 
            }
            else
            {
                return false;
            }
        }
    }
}
