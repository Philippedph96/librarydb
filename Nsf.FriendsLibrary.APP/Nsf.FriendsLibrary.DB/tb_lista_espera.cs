//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Nsf.FriendsLibrary.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class tb_lista_espera
    {
        public int id_lista_espera { get; set; }
        public int id_emprestimo_item { get; set; }
        public int id_leitor { get; set; }
        public System.DateTime dt_data { get; set; }
        public bool ds_urgencia { get; set; }
    
        public virtual tb_emprestimo_item tb_emprestimo_item { get; set; }
        public virtual tb_leitor tb_leitor { get; set; }
    }
}
