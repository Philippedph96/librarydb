﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf.FriendsLibrary.DB.Item_do_Emprestimo
{
    public class EmprestimoItemDatabase
    {
        public void Save(tb_emprestimo_item dto)
        {
            LibraryEntities context = new LibraryEntities();
            context.tb_emprestimo_item.Add(dto);
            context.SaveChanges();
        }

        public void Update(tb_emprestimo_item dto)
        {
            LibraryEntities context = new LibraryEntities();
            context.Entry(dto).State = System.Data.Entity.EntityState.Modified;
            context.SaveChanges();
        }

        bool retorno;
        public bool BookStatus(int id)
        {
            LibraryEntities context = new LibraryEntities();
            var valor = context.tb_livro.First(c => c.id_livro == id);

            if (valor != null)
                retorno = valor.bl_status;

            return retorno;
        }
    }
}
