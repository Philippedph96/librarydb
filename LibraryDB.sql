--Ter� que ser refeito o banco de dados e verificar as informa��es com dados mais precisos.
CREATE DATABASE LibraryDB;
USE LibraryDB;

-- COMPLEMENTOS DOS EXEMPLARES/LIVROS
CREATE TABLE tb_autor (
	id_autor		   INT PRIMARY KEY NOT NULL IDENTITY (1,1),
	nm_autor		   VARCHAR(70)	   NOT NULL,
	ds_nascimento	   DATE			   NOT NULL,
	ds_escrita		   VARCHAR(100)	   NOT NULL,
	img_autor		   IMAGE    	       NULL
);


CREATE TABLE tb_categoria (
	id_categoria       INT PRIMARY KEY NOT NULL IDENTITY (1,1),
	nm_categoria       VARCHAR(70)	   NOT NULL,
	ds_exemplo	       VARCHAR(200)	   NOT NULL
);

CREATE TABLE tb_editora (
	id_editora		   INT PRIMARY KEY NOT NULL IDENTITY (1,1),
	nm_editora		   VARCHAR(100)	   NOT NULL,
	ds_sede			   VARCHAR(100)	   NOT NULL,
);


-- EXEMPLAR/LIVROS
CREATE TABLE tb_livro (
	id_livro		   INT PRIMARY KEY NOT NULL IDENTITY (1,1),
	id_autor		   INT			   NOT NULL,
	id_categoria	   INT			   NOT NULL,
	id_editora		   INT			   NOT NULL,
	nm_livro		   VARCHAR(100)	   NOT NULL,
	nr_paginas		   INT			   NOT NULL,
	dt_ano			   DATE			   NOT NULL,
    bl_status		   BIT			   NOT NULL,
	img_livro          IMAGE			   NULL,
	
	FOREIGN KEY (id_autor)		   REFERENCES tb_autor		       (id_autor),
	FOREIGN KEY (id_categoria)	   REFERENCES tb_categoria	       (id_categoria),
	FOREIGN KEY (id_editora)	   REFERENCES tb_editora	       (id_editora)

	ON DELETE CASCADE
	ON UPDATE CASCADE
);


-- USU�RIOS (FUNCION�RIO & LEITOR)
CREATE TABLE tb_bibliotecario (
	id_bibliotecario   INT PRIMARY KEY NOT NULL IDENTITY (1,1),
	nm_bibliotecario   VARCHAR(100)    NOT NULL,
	ds_login		   VARCHAR(30)	   NOT NULL,
	ds_senha		   VARCHAR(16)	   NOT NULL,
	ds_cpf			   VARCHAR(14)	   NOT NULL,
	ds_celular		   VARCHAR(15)	   NOT NULL,
	ds_email		   VARCHAR(80)	   NOT NULL,
	ds_cep			   VARCHAR(9)	   NOT NULL, 
	ds_cidade		   VARCHAR(50)	   NOT NULL,
	ds_endereco		   VARCHAR(100)	   NOT NULL,
    nr_numero		   INT			   NOT NULL
); 

CREATE TABLE tb_leitor (
	id_leitor		   INT PRIMARY KEY NOT NULL IDENTITY (1,1),
	bl_professor	   BIT			   NOT NULL,
	bl_estudante	   BIT			   NOT NULL,
	nm_leitor		   VARCHAR(100)	   NOT NULL,
	dt_nascimento	   DATE			   NOT NULL,
	ds_cpf			   VARCHAR(14)     NOT NULL,
	ds_email		   VARCHAR(80)	   NOT NULL,
	ds_cep			   VARCHAR(9)	   NOT NULL, 
	ds_cidade		   VARCHAR(50)	   NOT NULL,
	ds_endereco		   VARCHAR(100)	   NOT NULL,
    nr_numero		   INT			   NOT NULL
);

--EMPRESTIMO
CREATE TABLE tb_emprestimo (
	id_emprestimo      INT PRIMARY KEY NOT NULL IDENTITY (1,1),
	id_bibliotecario   INT			   NOT NULL,
	id_leitor		   INT			   NOT NULL,
	dt_emprestimo	   DATE            NOT NULL,
	dt_devolucao	   DATE            NOT NULL,

	FOREIGN KEY (id_bibliotecario)   REFERENCES tb_bibliotecario   (id_bibliotecario),
	FOREIGN KEY (id_leitor)          REFERENCES tb_leitor  	       (id_leitor)
		
	ON DELETE CASCADE
	ON UPDATE CASCADE
);

CREATE TABLE tb_emprestimo_item (
	id_emprestimo_item INT PRIMARY KEY NOT NULL IDENTITY (1,1),
	id_livro		   INT			   NOT NULL,
	id_emprestimo      INT			   NOT NULL,
	
	FOREIGN KEY (id_livro)		     REFERENCES tb_livro	       (id_livro),
	FOREIGN KEY (id_emprestimo)      REFERENCES tb_emprestimo      (id_emprestimo)
		
	ON DELETE CASCADE
	ON UPDATE CASCADE
);
--LISTAS ATRASO/ESPERA/BANIDOS
CREATE TABLE tb_lista_atraso (
	id_lista_atraso	   INT PRIMARY KEY NOT NULL IDENTITY(1,1),
	id_emprestimo_item INT			   NOT NULL,
	id_leitor          INT			   NOT NULL,
	dt_atraso		   INT			   NOT NULL,

	FOREIGN KEY (id_emprestimo_item) REFERENCES tb_emprestimo_item (id_emprestimo_item),
	FOREIGN KEY (id_leitor)			 REFERENCES tb_leitor		   (id_leitor)
	
	ON DELETE CASCADE
	ON UPDATE CASCADE
);

CREATE TABLE tb_lista_espera (
	id_lista_espera	   INT PRIMARY KEY NOT NULL IDENTITY (1,1),
	id_emprestimo_item INT			   NOT NULL,
	id_leitor          INT			   NOT NULL,
	dt_data			   DATE			   NOT NULL,
	ds_urgencia		   BIT			   NOT NULL,

	FOREIGN KEY (id_emprestimo_item) REFERENCES tb_emprestimo_item (id_emprestimo_item),
	FOREIGN KEY (id_leitor)			 REFERENCES tb_leitor		   (id_leitor)
	
	ON DELETE CASCADE
	ON UPDATE CASCADE
);

CREATE TABLE tb_lista_banido (
	id_lista_banido    INT PRIMARY KEY NOT NULL IDENTITY (1,1),
	id_leitor          INT             NOT NULL,

	FOREIGN KEY (id_leitor)          REFERENCES tb_leitor          (id_leitor)
	
	ON DELETE CASCADE
	ON UPDATE CASCADE
);

-- CONSULTA DE LIVROS
CREATE  VIEW vw_livro
AS
SELECT  tb_livro.id_livro,
		tb_livro.nm_livro,
		tb_autor.id_autor,
		tb_autor.nm_autor,
		tb_editora.id_editora,
		tb_editora.nm_editora,
		tb_categoria.id_categoria,
		tb_categoria.nm_categoria,
		tb_livro.nr_paginas,
		tb_livro.bl_status,
		tb_livro.img_livro,
		tb_livro.dt_ano
		
FROM tb_livro 
INNER JOIN tb_autor
		ON tb_livro.id_autor = tb_autor.id_autor
INNER JOIN tb_editora
	    ON tb_livro.id_editora = tb_editora.id_editora
INNER JOIN tb_categoria
		ON tb_livro.id_categoria = tb_categoria.id_categoria;

CREATE VIEW vw_consultar_emprestimo
AS
SELECT	tb_emprestimo.id_emprestimo,
		tb_emprestimo.dt_emprestimo, 
		tb_emprestimo.dt_devolucao,		

		tb_bibliotecario.nm_bibliotecario,

		tb_leitor.id_leitor,
		tb_leitor.nm_leitor,
		
		tb_livro.id_livro,
		tb_livro.nm_livro 
	
FROM tb_emprestimo
INNER JOIN tb_emprestimo_item
	 	ON tb_emprestimo.id_emprestimo = tb_emprestimo_item.id_emprestimo_item
INNER JOIN tb_bibliotecario
	 	ON tb_emprestimo.id_bibliotecario = tb_bibliotecario.id_bibliotecario
INNER JOIN tb_leitor
		ON tb_emprestimo.id_leitor = tb_leitor.id_leitor 
INNER JOIN tb_livro
	 	ON tb_emprestimo_item.id_livro = tb_livro.id_livro

CREATE VIEW vw_devolucao_emprestimo
AS
SELECT	tb_emprestimo_item.id_emprestimo_item,
		tb_emprestimo.id_emprestimo,
		tb_emprestimo.dt_devolucao,		
		tb_emprestimo.dt_emprestimo, 
		DATEDIFF (DAY, tb_emprestimo.dt_emprestimo, GETDATE()) AS 'dt_dias_devolucao',

		tb_leitor.id_leitor,
		tb_leitor.nm_leitor,
		tb_leitor.ds_cpf,
		tb_leitor.ds_email,
		
		tb_livro.id_livro,
		tb_livro.nm_livro
				
FROM tb_emprestimo
INNER JOIN tb_emprestimo_item
		ON tb_emprestimo.id_emprestimo = tb_emprestimo_item.id_emprestimo
INNER JOIN tb_leitor
		ON tb_emprestimo.id_leitor = tb_leitor.id_leitor
INNER JOIN tb_livro
		ON tb_emprestimo_item.id_livro = tb_livro.id_livro;
	